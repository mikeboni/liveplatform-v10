<cfparam name="subgroupID" default="0">
<cfparam name="groupID" default="0">
<cfparam name="editGroupName" default="false">


<cfif NOT IsDefined("session.subgroupID")><cfset session.subgroupID = '0'></cfif>
<cfif subgroupID NEQ ''><cfset session.subgroupID = subgroupID></cfif>
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<cfinvoke component="CFC.Modules" method="getCMSAppGroups" returnvariable="groups">
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<!--- Group Listing --->
<link href="styles.css" rel="stylesheet" type="text/css">

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Modules" jsclassname="setMyActiveState">

<!--- Apps --->
<cfajaxproxy cfc="CFC.Apps" jsclassname="refreshJSONData">

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Assets" jsclassname="modifyAssets">


<script type="text/javascript">

var jsAssets = new modifyAssets();
var jsContentAccess = new contentAccess();
var jsState = new setMyActiveState();
var jsRefresh = new refreshJSONData();

toggleActiveState = function(theObject,groupID,assetID)
{

    var theState = document.getElementById('activeState_'+theObject).innerHTML;
	theState = theState.trim();
	var theObjState;

	if(theState == 'YES')
	{
		theObjState = 'NO';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
		CSStyleContent = 'contentLinkDisabled';
		
		jsState.setSyncMode();
		jsState.setAssetShareState(assetID,groupID,theNewState);
	
		document.getElementById('shareState_'+theObject).innerHTML = theObjState;
		document.getElementById('shareState_'+theObject).className = CSSStyle;
		
	}else{
		theObjState = 'YES';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
		CSStyleContent = 'contentLink';
	}
	
	jsState.setSyncMode();
	jsState.setCallbackHandler(showStateResult);
	jsState.setAssetActiveState(assetID,groupID,theNewState);
	
	document.getElementById('activeState_'+theObject).innerHTML = theObjState;
	
	document.getElementById('activeState_'+theObject).className = CSSStyle;
	
	document.getElementById('contentAsset_'+theObject).className = CSStyleContent;
	
}

//Share

toggleShareState = function(theObject,groupID,assetID,theState)
{
	if(theState == undefined)
	{
		var theState = document.getElementById('shareState_'+theObject).innerHTML;
		theState = theState.trim();
	}
	var theObjState;
	
	if(theState == 'YES')
	{
		theObjState = 'NO';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
		CSStyleContent = 'contentLinkDisabled';
	}else{
		theObjState = 'YES';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
		CSStyleContent = 'contentLink';
	}
	
	//jsState.setCallbackHandler(showStateResult);
	jsState.setSyncMode();
	jsState.setAssetShareState(assetID,groupID,theNewState);
	
	document.getElementById('shareState_'+theObject).innerHTML = theObjState;
	
	document.getElementById('shareState_'+theObject).className = CSSStyle;
	
}

setSortOrder = function(groupID, assetID, theOrder)
{	
	theOrderNum = parseInt(theOrder);
	
	jsState.setSyncMode();
	jsState.setCallbackHandler(successSortUpdated);
	jsState.setSortOrder(assetID,groupID,theOrderNum);
}

function successSortUpdated()	{
	//location.reload();
}

showStateResult = function(result){
	
}



toggleCachedState = function(theObject,groupID,assetID)
{
	for(i = j = 0; i < theObject.childNodes.length; i++)
    if(theObject.childNodes[i].nodeName == 'IMG'){
        j++;
        var theObj = theObject.childNodes[i];
        break;
    }
	
	theFile = theObj.src.replace(/^.*[\\\/]/, '');
	
	if(theFile == 'cache.png')
	{
		theObj.src = 'images/cached.png';
		theNewState = 1;
	}else{
		theObj.src = 'images/cache.png';
		theNewState = 0;
	}
	
	jsState.setSyncMode();
	jsState.setAssetCachedState(assetID,groupID,theNewState);
	
}

 function showDelete(theObj, state)	{

	theObjD = theObj.getElementsByTagName("input");
	theObjM = theObj.getElementsByTagName("select");
 	
	for(i=0; i < theObjD.length; i++) {
				
		if(theObjD[i].id == "deleteAsset")	{
			delBut = theObjD[i];
			break;
		}
	}
	
	for(i=0; i < theObjM.length; i++) {
				
		if(theObjM[i].id == "moveAsset")	{
			movBut = theObjM[i];
			break;
		}
	}
	
	if(state)	{
		movBut.className = "itemShow";
		delBut.className = "itemShow";
 	}else{

		movBut.className = "itemHide";
		delBut.className = "itemHide";
 	}
 }

function removeAsset(assetID) {
		if(confirm('Are you sure you wish to Remove Asset?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(removeAssetSuccess);
			jsState.deleteAssetInGroup(assetID,<cfoutput>#subgroupID#</cfoutput>);
			
		}else{ 
			//nothing
		}
}

function removeAssetSuccess(result)	{
	location.reload();
	
}

function removeGroup(groupID) {

		if(confirm('Are you sure you wish to Remove the Group?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(removeGroupSuccess);
			jsState.deleteGroupAssets(groupID);
			
		}else{ 
			//nothing
		}
}

function removeGroupSuccess(result)	{
	location.reload();
}

function moveAsset(assetID,moveGroupID,groupName) {
		
		if(confirm('Are you sure you wish to Move Group Asset to "'+ groupName +'"?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(updateMoveAssetSuccess);
			jsState.moveGroupAsset(assetID,moveGroupID,groupName);
			
			
		}else{ 
			//nothing
		}
}

function moveGroup(groupID,moveGroupID,groupName) {
		
		if(confirm('Are you sure you wish to Move Group and All Group Assets to "'+ groupName +'"?'))
		{	
			jsState.setSyncMode();
			jsState.setCallbackHandler(updateMoveGroupSuccess);
			jsState.moveGroup(groupID,moveGroupID,groupName);
			
			
		}else{ 
			//nothing
		}
}

function updateMoveGroupSuccess(result)	{
	location.reload();
}

function updateMoveAssetSuccess(result)	{
	location.reload();
}

var jsContentAccess = new contentAccess();


updateAssetAccess = function(theForm,groupID) 
{
	theLevel = parseInt(theForm.options[theForm.selectedIndex].value);
	
	if(theLevel == 0)
	{
		lock = 'access_unlocked';  
	}else{
		lock = 'access_locked-'+theLevel; 
	}
	
	theForm.style.backgroundImage = 'url(images/'+ lock +'.png)';
	
	jsContentAccess.setSyncMode();
	jsContentAccess.setCallbackHandler(showAccessUpdated);
	jsContentAccess.setContentAccess(groupID,theLevel);
	
}

showAccessUpdated = function() 
{
	location.reload();
}

var jsRefresh = new refreshJSONData();
	
refreshJSON = function()
{	
	theObjRef = document.getElementById('refresh');
	theObjRef.style.opacity = ".2";
	
	//jsRefresh.(showUpdated);
	
	jsRefresh.setSyncMode();
	
	jsRefresh.setCallbackHandler(showUpdated);
	
	<cfoutput>
	jsRefresh.generateContentJSON(#session.subgroupid#, #session.appid#);
	</cfoutput>
	
}

refreshAllJSON = function(dev,vr)	{	
	
	
	var svr = document.getElementById('serverVr');
	//var selectedServerVr = svr.options[svr.selectedIndex].value;
	
	theLinkPaths = document.location.href.split('/');
	
	var foundObj = -1;
	
	for(i=0;i<theLinkPaths.length;i++)	{
			
		if(theLinkPaths[i] == 'API' || theLinkPaths[i] == 'api')	{
			foundObj = i+1;
			break;	
		}
			
	}
	
	curVr = theLinkPaths[foundObj];
	
	if(curVr != ('v'+vr))	{
		//not the same server
		message = 'Current Application does NOT match Server Version!\nYou MUST BE in'+vr+' to Generate this Data!';
	
		if(foundObj > -1)	{
			alert("Wrong CMS to Generate this version - Switching to 'V-"+vr+"' CMS");	
			theLinkPaths[foundObj] = 'v'+vr;
			newURLPath = theLinkPaths.join('/');
			document.location.href = newURLPath;
		}else{
			alert("server version not supported");	
		}
	
		return;
	
	}else{
		//everthing is ok
	}
	
	if(dev === undefined)	{ dev = 0; }
	
	if(dev === 1)	{
		message = 'Refresh and Rebuild JSON on DEV SERVER?';
	}else{
		message = 'Refresh and Rebuild JSON on PROD SERVER?';
	}
	
	if(confirm(message))
	{
		jsRefresh.setSyncMode();
	
		<cfoutput>
		jsRefresh.generateAllContentJSON(#session.appid#,dev);
		</cfoutput>
	}else{ 
		//nothing
	}
}


showUpdated = function(result){
	
	theObjRef = document.getElementById('refresh');
	theObjRef.style.opacity = "1.0";
}

</script>


<!--- Group Content --->
<cfinvoke component="CFC.Modules" method="getGroups" returnvariable="contents">
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
</cfinvoke>

<!--- Group --->
<cfinvoke component="CFC.Modules" method="getGroupName" returnvariable="group">
    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
</cfinvoke>

<!--- Access Levels --->
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<style type="text/css">
.sample {
	font-size: 9px;
}
</style>


<div class="rowhighlighter" style="margin-top:5px">

<form action="updateGroupAsset.cfm" method="post" enctype="multipart/form-data" id="groupDetails">
		<table width="810" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="#AAA">
          <tr>
            <td height="32" colspan="5" bgcolor="#333" class="sectionLinkHighlighted">

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="44" align="left" valign="middle">
                
                <table border="0" cellspacing="5">
  <tr>
    <td height="44"><cfif group.id GT '0'>
                  <a href="AppsView.cfm"><img src="images/home.png" width="44" height="44" border="0" /></a>
                </cfif></td>
    <td><cfoutput>
					<cfif editGroupName>
                    
                   		<cfif subgroupID GT '0'>
                        <input name="groupName" type="text" class="formText" id="groupName" value="#group.name#" size="80" maxlength="128">
                        <input name="groupID" type="hidden" id="groupID" value="#group.id#" />
                         <cfelse>
                         Application Root
                         </cfif>
                     
                    <cfelse>
                        <cfif group.id GT '0'>
                           
                            <cfinvoke component="CFC.Modules" method="getBreadcrumb">
                                <cfinvokeargument name="groupID" value="#subgroupID#"/>
                            </cfinvoke>
                           
                            <cfif group.active IS '0'>
                             <span class="contentwarning"> (NOT ACTIVE)</span>
                           </cfif>
                           
                        <cfelse>
                            <span class="contentLinkWhite">Root Group</span>
                        </cfif>
                  </cfif>
                </cfoutput></td>
                
  </tr>
</table>
                
                <cfif editGroupName>
                <td width="44" align="right">
                <input type="submit" value="" style="background:url(images/ok.png) no-repeat; border:0; width:44px; height:44px; cursor:pointer" />
                </td>
                </cfif>
                
                </td>
                
                <td align="right">
                <cfoutput>
					<cfif editGroupName>
                    <cfelse>
                        <a onclick="refreshJSON()"><img src="images/refresh.png" name="refresh" width="44" height="44" id="refresh" /></a>
                            <a href="AppsView.cfm?subgroupID=#subgroupID#&amp;editGroupName=true"><img src="images/edit.png" width="44" height="44" /></a>
                            <a href="createGroupAsset.cfm?subgroupID=#session.subgroupID#"><img src="images/groups.png" width="44" height="44" /></a>
                        
                    
				
                        <a href="AppsView.cfm?assetTypeTab=3&amp;import=true">
                        <img src="images/import.png" width="44" height="44" /></a>
                        </cfif>
				</cfoutput>
                </td>
                
              </tr>
              
              	<cfif editGroupName>
                
					<cfif subgroupID IS '0'><cfset subgroupID = '-1'></cfif>
                    
                    <!--- Get Group Info --->
                    <cfinvoke component="CFC.Modules" method="getGroupDetails" returnvariable="assets">
                        <cfinvokeargument name="groupID" value="#subgroupID#"/>
                    </cfinvoke>
         
                    <!--- Get All Module Data - Thumbs and Details like Assets --->
                    <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                        <cfinvokeargument name="appID" value="#session.appID#"/>
                        <cfinvokeargument name="images" value="yes"/>
                        <cfinvokeargument name="relative" value="yes"/>
                    </cfinvoke>

                    <cfset theThumbnailImage = '#assetPath#thumbs/#assets.thumbnail#'>
                    <cfset theThumbnailImage_nonretina = '#assetPath#thumbs/nonretina/#assets.thumbnail#'>

               <tr>
                <td height="44" colspan="2">
                
                	<cfinclude template="thumbsDetails.cfm">
                    
                </td>
               </tr>

              </cfif>
              
              
              
            </table>
            
            </td>
          </tr>
        </table>
</form>
      
</div>

<!--- Show if no details for group is displayed --->
<cfif editGroupName>
<cfelse>  
    
<table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">
  <tr>
    <td height="32" class="subheading"></td>
    <td width="80" align="right" class="subheading">Access</td>
    <td width="60" align="right" class="subheading">Cache</td>
    <td width="60" align="center" class="subheading">Active</td>
    <td width="60" align="center" class="subheading">Sharable</td>
    <td width="80" align="center" class="subheading">Order</td>
    <td width="44" align="right" class="subheading"></td>
    <td width="44" align="right" class="subheading"></td>
  </tr>
</table>

    <cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />
    
	<cfloop index="z" from="1" to="#arrayLen(contents)#">
    	
      <cfset content = contents[z]>
        
	  <cfif (z MOD 2)>
          <cfset colColor = "FFF">
        <cfelse>
          <cfset colColor = "EEE">
      </cfif>
        
      <cfset dateCre = DateAdd("s", content.created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
      <cfset dateMod = DateAdd("s", content.modified ,DateConvert("utc2Local", "January 1 1970 00:00"))>

      <!--- Get Asset Name --->
      <cfif content.asset_id GT '0'>
          <cfinvoke component="CFC.Assets" method="getAssetName" returnvariable="assetInfo">
            <cfinvokeargument name="assetID" value="#content.asset_id#"/>
          </cfinvoke>
      </cfif>
	  
      <cfif content.active IS ''>
      	<cfset content.active = -1>
      </cfif>
      
        <cfoutput>
        <cfif content.active>
        	<cfset state = "contentLink">
        <cfelse>
        	<cfset state = "contentLinkDisabled">
        </cfif>

        <!--- All Assets --->  
        <div class="rowhighlighter" onmouseover="showDelete(this,1)" onmouseout="showDelete(this,0)">
        
              <table width="810" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td width="44">
                  <cfif content.asset_id GT '0'>
                    <img src="images/#assetInfo.icon#" width="44" height="44" />
                  <cfelseif content.subgroup_id GT '0'>
                    <img src="images/groups.png" width="44" height="44" />
                  <cfelse>
                    <img src="images/modules.png" width="44" height="44" />
                  </cfif>
                  </td>
                  <td colspan="2">
                  <cfif content.asset_id GT '0'>
                  <a href="AssetsView.cfm?assetID=#content.asset_id#&amp;assetTypeID=#assetInfo.assetType_id#" class="#state#">
                  <div id="contentAsset_#z#" style="height:32px; padding-top:15px; padding-left:4px">#assetInfo.name#</div>
                  </a>
                  <cfelse>
                  <a href="AppsView.cfm?subgroupID=#content.group_id#" class="#state#">
                  <div id="contentAsset_#z#" style="height:32px; padding-top:15px; padding-left:4px">#content.name#</div>
                  </a>
                  </cfif>
                  </td>
                  <td width="80" align="right" class="content">
                  
                  <cfif content.access IS 0>
                        <cfset lock = 'access_unlocked'>
                    <cfelse>
                        <cfset lock = 'access_locked-'& content.access>
                    </cfif>
                    <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateAssetAccess(this,#content.group_id#)" />
                        <cfloop query="accessLevels">
                        <option value="#accessLevel#" style="color:##333" <cfif accessLevel IS content.access>selected="selected"</cfif>>#levelName#</option>
                        </cfloop>
                    </select>
                  </td>
                  <td width="60" align="right" class="content">
                  
                  <cfif content.asset_id IS ''><cfset content.asset_id = 0></cfif>
                  <cfif content.group_id IS ''><cfset content.group_id = 0></cfif>
                  
                  <div id="contentCached" onclick="toggleCachedState(this,#content.group_id#,#content.asset_id#)" style="cursor:pointer">
                  <cfif content.cached IS '0'>
                  <cfset theCacheState = 'cache'>
                  <cfelse>
                  <cfset theCacheState = 'cached'>
                  </cfif>
                  <img src="images/#theCacheState#.png" name="cachedState" id="cachedState" />
                  </div>                    </td>
                  <td width="60" align="center" class="content">
                  
                  <cfif content.asset_id IS ''><cfset content.asset_id = 0></cfif>
                  <cfif content.group_id IS ''><cfset content.group_id = 0></cfif>
                  <cfif content.active IS '0'>
                    <cfset stateCSS = "contentLinkGreen">
                  <cfelse>
                    <cfset stateCSS = "contentLinkRed">
                  </cfif>
                  <div id="contentActive" style="cursor:pointer" onclick="toggleActiveState(#z#,#content.group_id#,#content.asset_id#)">
                  <a id="activeState_#z#" class="#stateCSS#">
                  <cfif content.active IS '0'>
                  NO
                  <cfelse>
                  YES
                  </cfif>
                  </a>
                  </div>
                  
                  </td>
                  <td width="60" align="center" class="content">
                  
                  <cfif content.asset_id IS ''><cfset content.asset_id = 0></cfif>
                  <cfif content.group_id IS ''><cfset content.group_id = 0></cfif>
                  <cfif content.sharable IS '0'>
                    <cfset stateCSS = "contentLinkGreen">
                  <cfelse>
                    <cfset stateCSS = "contentLinkRed">
                  </cfif>
                  <div id="contentShare" style="cursor:pointer" onclick="toggleShareState('#z#',#content.group_id#,#content.asset_id#)">
                  <span id="shareState_#z#" class="#stateCSS#">
                  <cfif content.sharable IS '0'>
                  NO
                  <cfelse>
                  YES
                  </cfif>
                  </a>
                  </div>
                  
                  </td>
                  <td width="80" align="center" class="contentLinkDisabled">

                  <select name="sortOrder" id="sortOrder" style="width:36px; text-align:center; text-indent:0; height:32px;" type="button" oncselect="setSortOrder(#content.group_id#,#content.asset_id#,this.options[this.selectedIndex].value);" />
                  <option value="0" <cfif content.order IS 0> selected</cfif>>0</option>
                      <cfloop index="z" from="1" to="#arrayLen(contents)#">
                        <option value="#z#" <cfif content.order IS z> selected</cfif>>#z#</option>
                      </cfloop>
                  </select>
                  
                  <!--- <input name="sortOrder_#z#" type="text" id="sortOrder_#z#" value="#content.order#" size="4" maxlength="10" onchange="setSortOrder(#content.group_id#,#content.asset_id#,this.value)" /> --->
                  
                  </td>
                  <td width="60" align="right" class="content">

                  
                  <table border="0">
                  <tr>
                    <td>
                    <cfif content.asset_id GT '0'>
                    <select class="itemHide" id="moveAsset" style="background:url(images/moveItem.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="moveAsset(#content.asset_id#,this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);this.selectedIndex=-1;" />
                    <option value="0" style="color:##333">Root</option>
                    <cfloop query="groups">
                      <option value="#group_id#" style="color:##333">#name#</option>
                    </cfloop>
                    </select>
                  <cfelse>
                  <select class="itemHide" id="moveAsset" style="background:url(images/moveItem.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="moveGroup(#content.group_id#,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);this.selectedIndex=-1;"/>
                  <option value="0" style="color:##333">Root</option>
                    <cfloop query="groups">
                      <option value="#group_id#" style="color:##333">#name#</option>
                    </cfloop>
                    </select>                   
                  </cfif>
                    </td>
                    <td>
                    <cfif content.asset_id GT '0'>
                    <input type="button" class="itemHide" id="deleteAsset" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="removeAsset('#content.asset_id#');" value="" /> 
                  <cfelse>
                  <input type="button" class="itemHide" id="deleteAsset" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="removeGroup('#content.group_id#');" value="" />
                  </cfif>
                    </td>
                  </tr>
                </table>
                  
                  
                  </td>
                </tr>
              </table>
              
        </div>
    
		</cfoutput>
      	
  </cfloop>
  
    
</cfif>




     

   


