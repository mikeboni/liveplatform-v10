<cfparam name="targetID" default="0">
<cfparam name="editGroupName" default="false">

<cfif NOT IsDefined("session.targetID")><cfset session.targetID = '0'></cfif>
<cfif targetID NEQ ''><cfset session.targetID = targetID></cfif>


<!--- Group Listing --->
<link href="styles.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

function showDelete(theObj, state)	{
	
	theObjRef = document.getElementById(theObj);
	
	if(state)
	{
		removeClass(theObjRef,"itemHide");
		addClass(theObjRef, "itemShow");
	}else{
		removeClass(theObjRef,"itemShow");
		addClass(theObjRef, "itemHide");
	}
	
}

function deleteAsset(assetID) {
		if(confirm('Are you sure you wish to Group Asset?'))
		{
			document.location.href = "deleteAssetGroup.cfm?assetID="+assetID;
		}else{ 
			//nothing
		}
}

function deleteGroup(groupID) {

		if(confirm('Are you sure you wish to Group with All Group Assets?'))
		{
			document.location.href = "deleteAssetGroup.cfm?groupID="+groupID;
		}else{ 
			//nothing
		}
}

</script>
<!--- Group Content --->
<cfinvoke component="CFC.Targets" method="getTargets" returnvariable="contents">
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="targetID" value="#targetID#"/>
</cfinvoke>

<!--- Group --->
<cfinvoke component="CFC.Targets" method="getTargetName" returnvariable="target">
    <cfinvokeargument name="targetID" value="#targetID#"/>
</cfinvoke>

<!--- Access Levels --->
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />


<div class="rowhighlighter" onmouseover="showDelete('deleteAsset_#z#',1)" onmouseout="showDelete('deleteAsset_#z#',0)" style="margin-top:5px">

<form action="updateGroupAsset.cfm" method="post" enctype="multipart/form-data" id="groupDetails">
		<table width="810" border="0" cellpadding="5" cellspacing="0" class="content" bgcolor="#AAA">
          <tr>
            <td height="32" colspan="5" bgcolor="#333" class="sectionLinkHighlighted">

            <table width="100%" border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td width="44" height="44">
                <img src="images/target.png" width="44" height="44" border="0" />

                <cfif targetID GT '0'>
                  <a href="AppsView.cfm"><img src="images/home.png" width="44" height="44" border="0" /></a>
                </cfif>
                </td>
                
                <td height="44" align="left" valign="middle">
                <cfoutput>
					<cfif editGroupName AND subgroupID GT '0'>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="300"><input name="groupName" type="text" class="formText" id="groupName" value="#group.name#" size="80" maxlength="128"></td>
                        <td align="left"><a href="AppsView.cfm?subgroupID=#subgroupID#" style="margin-left:10px" class="function">Cancel</a> | <a href="##" class="function" onClick="document.forms['groupDetails'].submit();">Update</a></td>
                      </tr>
                    </table> 
                    <cfelse>
                        <cfif targetID GT '0'>
                           #group.name# <cfif group.active IS '0'>
                             <span class="contentwarning"> (NOT ACTIVE)</span>
                           </cfif>
                        <cfelse>
                            Targets
                        </cfif>
                  </cfif>
                </cfoutput>
                </td>
                
                <td align="right">
                <cfoutput>
                
					<cfif targetID GT '0'>
                        <cfif editGroupName>
                        <cfelse>
                            <a href="AppsView.cfm?subgroupID=#subgroupID#&amp;editGroupName=true"><img src="images/edit.png" width="44" height="44" /></a>
                        </cfif>
                    </cfif>
                    
                    <a href="createGroupAsset.cfm?subgroupID=#session.subgroupID#"><img src="images/add.png" width="44" height="44" /></a>
				</cfoutput>
                <cfif targetID GT '0'>
                <a href="AppsView.cfm?assetTypeTab=3&amp;import=true"><img src="images/import.png" width="44" height="44" /></a>
                </cfif>
                </td>
                
              </tr>
              
              <cfif targetID GT '0'>
              	<cfif editGroupName>
                
                <!--- Get Group Info --->
                <cfinvoke component="CFC.Modules" method="getGroupDetails" returnvariable="assets">
                    <cfinvokeargument name="groupID" value="#subgroupID#"/>
                </cfinvoke>
                
                <!--- Get All Module Data - Thumbs and Details like Assets --->
                <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="appID" value="#session.appID#"/>
                </cfinvoke>
                
                <cfset theThumbnailImage = '#assetPath#images/thumbs/#assets.thumbnail#'>

               <tr>
                <td height="44" colspan="3">
                
                	<!--- <cfinclude template="../thumbsDetails.cfm"> --->
                    
                </td>
               </tr>
               
              	</cfif>
              </cfif>
              
            </table>
            
            </td>
          </tr>
        </table>
 </form>
      
</div>

<!--- Show if no details for group is displayed --->
<cfif editGroupName AND subgroupID GT '0'>
<cfelse>      

	<cfloop index="z" from="1" to="#arrayLen(contents)#">
    	
      <cfset content = contents[z]>
        
	  <cfif (z MOD 2)>
          <cfset colColor = "FFF">
        <cfelse>
          <cfset colColor = "EEE">
      </cfif>
        
      <cfset dateCre = DateAdd("s", content.created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
      <cfset dateMod = DateAdd("s", content.modified ,DateConvert("utc2Local", "January 1 1970 00:00"))>

      <!--- Get Asset Name --->
      <cfif content.asset_id GT '0'>
          <cfinvoke component="CFC.Assets" method="getAssetName" returnvariable="assetInfo">
            <cfinvokeargument name="assetID" value="#content.asset_id#"/>
          </cfinvoke>
      </cfif>
		
        <cfoutput>

             <!--- All Assets --->  
                <div class="rowhighlighter" onmouseover="showDelete('deleteAsset_#z#',1)" onmouseout="showDelete('deleteAsset_#z#',0)">
                  <table width="810" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                      <td width="44">
                      <cfif content.asset_id GT '0'>
                        <img src="images/#assetInfo.icon#" width="44" height="44" />
                      <cfelse>
                        <img src="images/groups.png" width="44" height="44" />
                      </cfif>
                      </td>
                      <td colspan="2">
                      <cfif content.asset_id GT '0'>
                      <a href="AssetsView.cfm?assetID=#content.asset_id#" class="contentLink">
                      <div style="height:32px; padding-top:15px; padding-left:4px">#assetInfo.name#</div>
                      </a>
                      <cfelse>
                      <a href="AppsView.cfm?subgroupID=#content.group_id#" class="contentLink">
                      <div style="height:32px; padding-top:15px; padding-left:4px">#content.name#</div>
                      </a>
                      </cfif>
                      </td>
                      <td width="80" align="right" class="content">#dateFormat(dateCre,'MMM DD, YY')#</td>
                      <td width="80" align="right" class="content">#dateFormat(dateMod,'MMM DD, YY')#</td>
                      <td width="80" align="center" class="content">
                      
                      Active
					  
                      </td>
                      <td width="60" align="right" class="content">
                      <form method="post" name="formAsset_#z#">
                      <cfif content.asset_id GT '0'>
                        <input name="deleteAsset_#z#" type="button" class="itemHide" id="deleteAsset_#z#" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteAsset('#content.asset_id#');" value="" /> 
                      <cfelse>
                      <input name="deleteAsset_#z#" type="button" class="itemHide" id="deleteAsset_#z#" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteGroup('#content.group_id#');" value="" />
                      </cfif>
                      </form>
                      </td>
                    </tr>
                  </table>
                </div>
		
		</cfoutput>
      	
    </cfloop>
    
</cfif>