<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Registration Information</title>
<style type="text/css">
.content1 {	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #333;
	text-decoration:none;
}
.content1 {	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #333;
	text-decoration:none;
}
</style>
</head>

<style type="text/css">
.bannerHeading {
	padding-top:20px;
	padding-bottom:5px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 36px;
	color: #333;
}
.bundleid {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 18px;
	color: #333;
	text-decoration:none;
}
.category {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 16px;
	color: #CCC;
	text-decoration:none;
	background-color: #666;
	padding-bottom: 6px;
	padding-left: 12px;
	height: 26px;
}

.plainLinkGrey {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #999;
	text-decoration:none;
}
.content {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #333;
	text-decoration:none;
}
.note {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #999;
	text-decoration:none;
}
div.centre
{
  width: 760px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}

.contentLink {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #06C;
	text-decoration:none;
	cursor:pointer;
}

.formfieldcontent {
	height: 32px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	border: thin solid #999;
	padding-left: 6px;
}
.functionButton {
	height: 44px;
	width: 44px;
}
</style>

<body>


<table width="800" height="900" border="0" cellpadding="0" cellspacing="10" bgcolor="#EEE">
  <tr>
    <td height="18" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td width="130" rowspan="2" align="left" valign="bottom">
    <cfoutput><img src="#assetPaths.client.icon#" width="100" height="100" border="0" style="padding-right:20px; padding-left:10px" /></cfoutput></td>
    <td height="41" align="right" valign="top" class="mainHeading"><cfoutput><table border="0" cellpadding="0" cellspacing="5">
      <tr>
        <td><a href="#info.support#?subject=Registration Support" class="contentLink" target="_new"><img src="http://www.liveplatform.net/register/images/help.png" alt="Help" width="31" height="31" border="0" /></a></td>
        <td width="150"><a href="mailto:#info.app_support#?subject=Registration Support" class="contentLink" target="_new">Registration Support</a></td>
        </tr>
      </table></cfoutput>
      
    </td>
  </tr>
  <tr>
    <td width="605" height="73" valign="bottom" class="bannerHeading">Congratulations!</td>
  </tr>
  <tr>
    <td height="238" align="left" valign="top"><p>&nbsp;</p></td>
    <td align="left" valign="top">
      <p class="content">Your account has been approved for access to  &quot;<cfoutput>#appName#</cfoutput>&quot; App</p>
      <p class="content">Please keep the following information:</p>
      
      <cfoutput><table width="500" border="0" cellspacing="10">
        <tr>
          <td width="50" align="right" class="content">Email</td>
          <td><span class="content">#email#</span></td>
        </tr>
        <cfif isDefined('password')>
        <tr>
          <td align="right" class="content">Pass</td>
          <td><span class="content1">#password#</span></td>
        </tr>
        </cfif>
      </table>
      </cfoutput>
      
      <p class="content">To download the App, please visit the following URL on your tablet and select the appropriate version for your device.</p>
      
      <!--- Get App Links --->
      <cfinvoke component="LiveAPI" method="getAppSupportLinks" returnvariable="supportLinks">
          <cfinvokeargument name="appID" value="#appID#"/>
      </cfinvoke>
      
      <cfif supportLinks.recordCount GT '0'>
      
      <table width="100%" border="0" cellspacing="5">
      <cfoutput query="supportLinks">
      <cfif supportLinks.url NEQ '' OR supportLinks.url NEQ 'Registration'>
        <tr>
          <td class="content"><a href="#supportLinks.url#" class="contentLink">#supportLinks.urlName#</a></td>
        </tr>
        </cfif>
      </cfoutput>
      </table>
		</cfif>
      </td>
  </tr>
  <tr>
    <td height="18" align="right" valign="top" class="contentLink">&nbsp;</td>
    <td valign="middle" class="content">&nbsp;</td>
  </tr>
  <tr>
    <td height="18" align="right" valign="top" class="contentLink">Step 1</td>
    <td valign="middle" class="content">The Sign In Dialogue appears. Select the password field and type in your email address and password you registered.</td>
  </tr>
  <tr>
    <td height="18" align="right" valign="top" class="contentLink">Step 2</td>
    <td valign="middle" class="content">Once the account has been confirmed with your code, your full name will appear in the top navigation bar. The application will keep you signed in unless you sign out.</td>
  </tr>
  <tr class="content">
    <td height="18" align="right" valign="top" class="contentLink">Step 3</td>
    <td valign="middle" class="message"><p class="content1">Enjoy!</p></td>
  </tr>
  <tr>
    <td height="18" valign="bottom" class="message"></td>
    <td valign="middle" class="message">
    <cfoutput>
    <p class="content">For any questions, please contact support at <a href="mailto:#info.app_support#?subject=Registration Support" class="contentLink" target="_new">#info.app_support#</a></p>
    </cfoutput>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table> 


</body>
</html>