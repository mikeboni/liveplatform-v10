<!--- get all records if not sent --->
<cfquery name="allUnsentEmail">
    SELECT 	SessionCart.cart_id, Applications.appName, SessionCart.email, Users.name
	FROM            SessionCart LEFT OUTER JOIN
                        Users ON SessionCart.user_id = Users.user_id LEFT OUTER JOIN
                        Applications ON SessionCart.app_id = Applications.app_id
    WHERE sent IS NULL
</cfquery>

<cfset success = 0>
<cfset failed = 0>

<cfoutput query="allUnsentEmail">

    <cfinvoke component="CFC.Email" method="sendEmail" returnvariable="result">
        <cfinvokeargument name="cartID" value="#cart_id#"/>
    </cfinvoke>
    
    From:#appName#-#name#, Cart #cart_id# Sent To:#email# - #result#<br>
    
    <cfif result>
		<cfset success++>
    <cfelse>
		<cfset failed++>
    </cfif>
    
</cfoutput>

<cfoutput>
<p>Total Unsent Resent Email is #allUnsentEmail.recordCount#<br>
Success: #success# - Failed:#failed#
</cfoutput>