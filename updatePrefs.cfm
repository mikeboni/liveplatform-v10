<cfparam name="prefsID" default="0">
<cfparam name="serverID" default="0">
<cfparam name="sessionT" default="300">
<cfparam name="connectionT" default="45">
<cfparam name="deviceT" default="240">
<cfparam name="heartbeatT" default="20">
<cfparam name="tokenExpiryT" default="0">
<cfparam name="maxTokens" default="0">
<cfparam name="tokenExpires" default="0">
<cfparam name="serverAPI" default="0">
<cfparam name="refresh" default="900000">
<cfparam name="signinExpires" default="15">
<cfparam name="guestSignin" default="0">
<cfparam name="guestRegister" default="0">
<cfparam name="codeSignin" default="0">

<cfif signinExpires IS ''><cfset signinExpires = 15></cfif>


<cfinvoke component="CFC.Apps" method="updateAppPrefs" returnvariable="appID">
		<cfinvokeargument name="prefsID" value="#prefsID#"/>
		<cfinvokeargument name="serverID" value="#serverID#"/>
		<cfinvokeargument name="sessionT" value="#sessionT#"/>
		<cfinvokeargument name="connectionT" value="#connectionT#"/>
        <cfinvokeargument name="deviceT" value="#deviceT#"/>
        <cfinvokeargument name="heartbeatT" value="#heartbeatT#"/>
        <cfinvokeargument name="tokenExpiryT" value="#tokenExpiryT#"/>
        <cfinvokeargument name="maxTokens" value="#maxTokens#"/>
        <cfinvokeargument name="tokenExpires" value="#tokenExpires#">
        <cfinvokeargument name="serverAPI" value="#serverAPI#">
        <cfinvokeargument name="refresh" value="#refresh#">
        <cfinvokeargument name="signinExpires" value="#signinExpires#">
        <cfinvokeargument name="guestSignin" value="#guestSignin#">
        <cfinvokeargument name="guestRegister" value="#guestRegister#">
        <cfinvokeargument name="codeSignin" value="#codeSignin#">
</cfinvoke>

	<!--- Prefs Path --->
    <cfinvoke component="CFC.Apps" method="getJSONPath" returnvariable="jsonPath">
        <cfinvokeargument name="appID" value="#session.appID#"/>
    </cfinvoke>
    
    <cfset prefsPath = expandPath(jsonPath & 'appPrefs.json')> 
    
    <cfif fileExists(prefsPath)>
        <cffile action = "delete" file = "#prefsPath#">
    </cfif>

	<!--- Update Prefs JSON --->
  <cfinvoke component="LiveAPI" method="getAppPrefs" returnvariable="success">
      <cfinvokeargument name="appID" value="#session.appID#">
      <cfinvokeargument name="update" value="yes">
  </cfinvoke>

<cflocation url="AppsView.cfm?error=Preferences Updated" addtoken="no">