<cfset serverVr = 11>
<cfparam name="editP" default="NO">
<cfparam name="edit" default="NO">
<cfparam name="assetTypeTab" default="0">

<cfif NOT isDefined('session.serverVersion')>
	<cfset session.serverVersion = serverVr>
<cfelse>
	<cfset session.serverVersion = serverVr>
</cfif>

<style type="text/css">
body {
	margin-top: 0px;
}
</style>

<cfif NOT IsDefined("session.clientID")><cfset session.clientID = '0'></cfif>

<cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
    <cfinvokeargument name="clientID" value="#session.clientID#"/>
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<cfinvoke  component="CFC.Clients" method="getClientInfo" returnvariable="clients">
  <cfinvokeargument name="clientID" value="#session.clientID#"/>
  <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<div style="background-color:#666; width:810px;">
<cfoutput>
<div class="mainheading" style="width:810px; height:100px; padding:0px; background-image:url('images/live-platform.png'); background-repeat:no-repeat;">
	
  <cfif error NEQ ''>
    <div class="contentwarning" style="margin-top:12px; margin-left:155px; float:left">#error#</div>
  </cfif>
  
  <cfif assetTypeTab IS 0>
   <div class="contentLinkWhite" style="margin-top:12px; margin-right:10px; float:right; text-align:right">
   	<cfif isDefined('assetPaths.application.api')>
    <div>
    <cfset vr = assetPaths.application.api>
    <cfset wrongAPI = "##C00"><cfset rightAPI = "##0C0">
     App Server: V#vr#
    </div>
    </cfif>
    <cfif session.appID GT 0 AND assetTypeTab IS 0 AND NOT editP AND NOT edit>
    	<div style="margin-top:10px"><a href="exportJSON.cfm" style="cursor:pointer; color:##FFFFFF; text-decoration:none">Export JSON</a> | Refresh
        <a onClick="refreshAllJSON(0,#vr#);" style="cursor:pointer">PROD</a> | 
        <a onClick="refreshAllJSON(1,#vr#);" style="cursor:pointer">DEV</a></div>
    </cfif>
   </div>
  </cfif>
  
</div>

<div style="width:790px; background-color:##333; height:32px; padding-left:10px; padding-right:10px; padding-top:8px">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <a href="CientsView.cfm?clientID=0" class="sectionActiveLink">Clients</a>
    <cfif session.clientID GT '0'>
     <span class="sectionLink">|</span>
     
     <cfif session.appID GT '0'> 
     <a href="CientsView.cfm?clientID=#session.clientID#" class="sectionActiveLink">#assetPaths.client.name#</a>
     <cfelse>
     <span class="sectionLink">#assetPaths.client.name#</span>
     </cfif>
     
    </cfif>
    <cfif session.appID GT '0'>
        <span class="sectionLink">| #assetPaths.application.name#</span>
    </cfif>
    </td>
  </tr>
</table>
	
</div>

</cfoutput>
</div>