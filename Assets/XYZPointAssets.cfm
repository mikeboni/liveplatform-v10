<link href="../styles.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
	function clearObjectRef()
	{
			theObjRef = document.getElementById('objectRef');
			theObjRef.value = '';
	}
</script>

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Location Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <tr>
      <td align="right" valign="middle">XYZ Position </td>
      <td>
        <table width="100%" border="0" cellspacing="5">
          <tr>
            <td width="40" align="right" class="content">X Pos</td>
            <td width="40" align="right" class="content"><img src="images/gps_latt.png" width="44" height="44" /><br /></td>
            <td width="40"><label for="x_pos"></label>
              <input name="x_pos" type="text" class="formfieldcontent" id="x_pos" value="#asset.x_pos#" size="10" maxlength="10" /></td>
            <td width="40" align="right" class="content">Y Pos</td>
            <td width="40" align="right"><p class="content"><img src="images/gps_long.png" alt="" width="44" height="44" /><br />
            </p></td>
            <td><input name="y_pos" type="text" class="formfieldcontent" id="y_pos" value="#asset.y_pos#" size="10" maxlength="10" /></td>
          </tr>
          <tr>
            <td align="right" class="content">Z Pos</td>
            <td align="right" class="content"><img src="images/z_pos.png" alt="" width="44" height="44" /></td>
            <td><input name="z_pos" type="text" class="formfieldcontent" id="z_pos" value="#asset.z_pos#" size="10" maxlength="10" /></td>
            <td colspan="3" align="left" class="content">For X and Y Point Only - Leave Z empty</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Object Ref</td>
      <td valign="middle">
      
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="300" valign="middle"><input name="objectRef" type="text" class="formfieldcontent" id="objectRef" value="#asset.objectRef#" size="60" maxlength="128" /></td>
        <td valign="middle"><a href="##" onclick="clearObjectRef();"><img src="images/clear.png" alt="" width="32" height="32" border="0" style="padding-left:5px" /></a>
        </td>
      </tr>
    </table></td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td valign="middle">Get 3D Object XYZ position
      using the object name. This will then use XYZ os an offset to this XYZ Position</td>
    </tr>
 </table>
<p class="content">&nbsp;</p>
</cfoutput>