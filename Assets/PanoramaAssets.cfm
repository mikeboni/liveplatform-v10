<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfset theImageB = '#assetPath##asset.back#'>
<cfset theImageD = '#assetPath##asset.down#'>
<cfset theImageF = '#assetPath##asset.front#'>
<cfset theImageL = '#assetPath##asset.left#'>
<cfset theImageR = '#assetPath##asset.right#'>
<cfset theImageU = '#assetPath##asset.up#'>

<cfset theImagePano = '#assetPath##asset.pano#'>

<cfset theImageB_nonRetina = '#assetPath#nonretina/#asset.back#'>
<cfset theImageD_nonRetina = '#assetPath#nonretina/#asset.down#'>
<cfset theImageF_nonRetina = '#assetPath#nonretina/#asset.front#'>
<cfset theImageL_nonRetina = '#assetPath#nonretina/#asset.left#'>
<cfset theImageR_nonRetina = '#assetPath#nonretina/#asset.right#'>
<cfset theImageU_nonRetina = '#assetPath#nonretina/#asset.up#'>

<cfset theImagePano_nonRetina = '#assetPath#nonretina/#asset.pano#'>

<cfset fixNonRetina = arrayNew(1)>

<cfif fileExists(expandPath(theImageB_nonRetina))><cfset borderB = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageB),'des':expandPath(theImageB_nonRetina)})>
<cfset borderB = 2></cfif>

<cfif fileExists(expandPath(theImageD_nonRetina))><cfset borderD = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageD),'des':expandPath(theImageD_nonRetina)})>
<cfset borderD = 2></cfif>

<cfif fileExists(expandPath(theImageF_nonRetina))><cfset borderF = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageF),'des':expandPath(theImageF_nonRetina)})>
<cfset borderF = 2></cfif>

<cfif fileExists(expandPath(theImageL_nonRetina))><cfset borderL = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageL),'des':expandPath(theImageL_nonRetina)})>
<cfset borderL = 2></cfif>

<cfif fileExists(expandPath(theImageR_nonRetina))><cfset borderR = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageR),'des':expandPath(theImageR_nonRetina)})>
<cfset borderR = 2></cfif>

<cfif fileExists(expandPath(theImageU_nonRetina))><cfset borderU = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageU),'des':expandPath(theImageU_nonRetina)})>
<cfset borderU = 2></cfif>

<cfif fileExists(expandPath(theImagePano_nonRetina))><cfset borderP = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImagePano),'des':expandPath(theImagePano_nonRetina)})>
<cfset borderP = 2></cfif>

<cfoutput>
<table border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td><label for="assetName"></label>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <cfif fileExists(expandPath(theImagePano))>
    <tr>
      <td align="right" valign="middle">Image</td>
      <td valign="middle" class="content">
            <a href="#theImagePano#" target="_new" class="contentLink">
          	<img src="#theImagePano#" alt="#theImagePano#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderP#px;" />
            <cfif borderP GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </a> 
      </td>
    </tr>
    </cfif>
    <tr>
      <td align="right" valign="middle">360 Pano</td>
      <td valign="middle"><input name="pano360Asset" type="file" class="formfieldcontent" id="pano360Asset" style="padding-left:0; width:500px" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
    </tr>
    <tr>
      <td width="80" align="right" valign="top"><p>&nbsp;</p>
        <p>Front = 0<br />
          Left = 1<br />
          Back = 2<br />
          Right = 3<br />
          Up = 4<br />
      Down= 5</p></td>
      <td>

        <table border="0">
          <tr class="contentHilighted">
            <td align="center">Left Image</td>
            <td align="center">Right Image</td>
            <td align="center">Up Image</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(expandPath(theImageL))>
            <a href="#theImageF#" target="_new" class="contentLink">
          	<img src="#theImageL#" alt="#theImageL#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderL#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(expandPath(theImageR))>
            <a href="#theImageF#" target="_new" class="contentLink">
          	<img src="#theImageR#" alt="#theImageR#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderR#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(expandPath(theImageU))>
            <a href="#theImageF#" target="_new" class="contentLink">
          	<img src="#theImageU#" alt="#theImageU#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderU#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td class="content">
            <span class="contentHilighted">#asset.left#</span>
            <cfif NOT fileExists(expandPath(theImageL))>
			<span class="contentwarning">(File Missing)</span>
            <cfif borderL GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
            <td class="content">
            <span class="contentHilighted">#asset.right#</span>
            <cfif NOT fileExists(expandPath(theImageR))>
			<span class="contentwarning">(File Missing)</span>
            <cfif borderR GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
            <td class="content">
            <span class="contentHilighted">#asset.up#</span>
            <cfif NOT fileExists(expandPath(theImageU))>
			<span class="contentwarning">(File Missing)</span>
            <cfif borderU GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
          </tr>
          <tr>
            <td><input name="panoLAsset" type="file" class="formfieldcontent" id="panoLAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoRAsset" type="file" class="formfieldcontent" id="panoRAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoUAsset" type="file" class="formfieldcontent" id="panoUAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
          <tr>
            <td height="10" colspan="3">&nbsp;</td>
          </tr>
          <tr class="contentHilighted">
            <td align="center">Front Image</td>
            <td align="center">Back Image</td>
            <td align="center">Down Image</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(expandPath(theImageF))>
            <a href="#theImageF#" target="_new" class="contentLink">
          	<img src="#theImageF#" alt="#theImageF#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderF#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(expandPath(theImageB))>
            <a href="#theImageB#" target="_new" class="contentLink">
          	<img src="#theImageB#" alt="#theImageB#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderB#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(expandPath(theImageD))>
            <a href="#theImageD#" target="_new" class="contentLink">
          	<img src="#theImageD#" alt="#theImageD#" width="228" height="167" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderD#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td class="content">
            <span class="contentHilighted">#asset.front#</span>
            <cfif NOT fileExists(expandPath(theImageF))>
			<span class="contentwarning">(File Missing)</span>
            <cfif borderF GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
            <td class="content">
            <span class="contentHilighted">#asset.back#</span>
            <cfif NOT fileExists(expandPath(theImageB))>
			<span class="contentwarning">(File Missing)</span>
            <cfif borderB GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
            <td class="content">
            <span class="contentHilighted">#asset.down#</span>
            <cfif NOT fileExists(expandPath(theImageD))>
            <span class="contentwarning">(File Missing)</span>
            <cfif borderD GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
          </tr>
          <tr>
            <td><input name="panoFAsset" type="file" class="formfieldcontent" id="panoFAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoBAsset" type="file" class="formfieldcontent" id="panoBAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoDAsset" type="file" class="formfieldcontent" id="panoDAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
        </table>

      </td>
    </tr>
 </table>
</cfoutput>

<!--- Fix Non Retina Images --->
<cfif arrayLen(fixNonRetina) GT 0>

<cfset failed = arrayNew(1)>

    <cfloop array="#fixNonRetina#" index="path">
	
		<cfif NOT fileExists(path.des) AND fileExists(path.src)>
        
			<cfoutput>Fixed Missing Nonretina Image:#GetFileFromPath(path.src)#</cfoutput><br>
            
            <cfinvoke component="CFC.Misc" method="createNonRetinaImage" returnvariable="scaled">
                <cfinvokeargument name="imageSrc" value="#path.src#"/>
                <cfinvokeargument name="imageDes" value="#path.des#"/>
            </cfinvoke>
        
        </cfif>
    	
        <cfif NOT fileExists(path.des) AND fileExists(path.src)>
			<cfset arrayAppend(failed,path)>
        </cfif>
        
    </cfloop>
	
   <!--- <cfif NOT isDefined('theFailedPath.src')>
   
       <cfloop array="#failed#" index="theFailedPath">
       
           <cfset FN = GetFileFromPath(theFailedPath.src)>
            
            <cfif FN NEQ ''>
                Missing Images Failed to be Created - Created Missing Image
                <cfdump var="#theFailedPath#">
                <cfset reloadPage = true>
            </cfif>
        
        </cfloop>
    
    </cfif> --->
    
</cfif>