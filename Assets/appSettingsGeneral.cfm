<cfinvoke  component="CFC.Apps" method="getServers" returnvariable="servers" />
     
<cfoutput query="prefs">
	<cfset refreshTime = session_refresh/60>
  <form action="updatePrefs.cfm" method="post" enctype="multipart/form-data" id="updatePrefs">      
  <table width="800" border="0" cellspacing="10">
	<tr>
	  <td width="80" align="right" class="content">Server</td>
	  <td width="44"><span class="content"><img src="images/server.png" width="44" height="44" alt="Server" /></span></td>
	  <td>
	  
	  <table width="100%" border="0">
	    <tr>
	      <td width="400">
          
      <select name="serverID" id="serverID" style="width:400px;">
          <cfloop query="servers">
          
          <cfif serverName IS 'None'>
              <cfset urlName = ''>
          <cfelse>
              <cfset urlName = '(#server_url#)'>
          </cfif>
            <option value="#server_id#" <cfif prefs.server_id IS servers.server_id> selected="selected" </cfif>>#serverName# #urlName#</option>
          </cfloop> 
	  </select>
	  <input name="prefsID" type="hidden" id="prefsID" value="#prefs_id#" />
          </td>
	      <td><table width="100%" border="0">
	        <tr>
	          <td width="80" align="right" class="content">API Version</td>
	          <td><input name="serverAPI" type="text" class="formfieldcontent" id="serverAPI" value="#server_api#" size="5" maxlength="4" /></td>
	          </tr>
	        </table></td>
	      <td width="44">
          <a href="##" class="function" onclick="updatePrefs(1);">
          <img src="images/ok.png" width="44" height="44" />
          </a>
          </td>
	      </tr>
	    </table></td>
	</tr>
	<tr>
	  <td colspan="3" align="right" valign="middle" class="content"><hr size="1" /></td>
	  </tr>
	<tr>
	  <td align="right" valign="middle" class="content">Timeouts</td>
	  <td valign="middle"><span class="content"><img src="images/timeout.png" width="44" height="44" alt="timeout" /></span></td>
	  <td valign="middle"><table width="100%" border="0">
	    <tr>
	      <td width="60" align="right" class="content">Session</td>
	      <td width="100" class="content"><input name="sessionT" type="text" class="formfieldcontent" id="sessionT" value="#session_timeout#" size="5" maxlength="4" /> 
	        secs</td>
	      <td width="80" align="right" class="content"> Connection</td>
	      <td><input name="connectionT" type="text" class="formfieldcontent" id="connectionT" value="#connection_timeout#" size="5" maxlength="4" /></td>
	      <td width="120" align="right" class="content">Device</td>
	      <td class="content"><input name="deviceT" type="text" class="formfieldcontent" id="deviceT" value="#device_timeout#" size="5" maxlength="4" />
	        secs</td>
	      <td class="content">&nbsp;</td>
	      </tr>
	    </table></td>
	  </tr>
	<tr>
	  <td align="right" valign="middle" class="content">Heartbeat</td>
	  <td valign="middle"><span class="content"><img src="images/heartbeat.png" width="44" height="44" alt="Heartbeat" /></span></td>
	  <td valign="middle">
	    <table width="100%" border="0">
	      <tr>
	        <td width="60" align="right" class="content">Timeout</td>
	        <td width="170" class="content"><input name="heartbeatT" type="text" class="formfieldcontent" id="heartbeatT" value="#heartbeat_timeout#" size="5" maxlength="4" />
	          secs</td>
	        <td width="233" align="right" class="content">Refresh App Data</td>
	        <td class="content"><input name="refresh" type="text" class="formfieldcontent" id="refresh" value="#refreshTime#" size="5" maxlength="4" />
	        min</td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	<tr>
	  <td align="right" valign="middle" class="content">Tokens</td>
	  <td valign="middle"><span class="content"><img src="images/token.png" width="44" height="44" alt="token" /></span></td>
	  <td valign="middle"><table width="100%" border="0">
		<tr>
		  <td width="60" align="right" class="content">Expiry</td>
		  <td width="100" class="content"><input name="tokenExpiryT" type="text" class="formfieldcontent" id="tokenExpiryT" value="#token_expiry#" size="5" maxlength="4" /> 
		  secs</td>
		  <td width="80" align="right" class="content">Max Tokens</td>
		  <td><input name="maxTokens" type="text" class="formfieldcontent" id="maxTokens" value="#max_tokens#" size="5" maxlength="2" /></td>
		  <td width="130" align="right" class="content">Server Tokens Expires</td>
		  <td class="content"><input name="tokenExpires" type="text" class="formfieldcontent" id="tokenExpires" value="#token_expires#" size="5" maxlength="2" />
		    (days)</td>
		  <td class="content">&nbsp;</td>
		</tr>
	  </table></td>
	</tr>
	<tr>
	  <td colspan="3" align="right" class="content"><hr size="1" /></td>
	  </tr>
	<tr>
	  <td align="right" class="content">Signin Expires</td>
	  <td><span class="content"><img src="images/timeout.png" width="44" height="44" alt="timeout" /></span></td>
	  <td class="content">
      <table width="100%" border="0">
	    <tr>
	        <td width="60" align="right" valign="middle" class="content"><label for="guestSignin"></label>
            Timeout</td>
	        <td valign="middle" class="content"><input name="signinExpires" type="text" class="formfieldcontent" id="signinExpires" value="#prefs.signin_expires#" size="5" maxlength="4" />
min</td>
	        <td align="right" valign="middle" class="content"><input name="guestSignin" type="checkbox" id="guestSignin" value="1" onchange="" <cfif prefs.guest_signin>checked</cfif> /><label for="guestSignin"></label></td>
	        <td align="left" valign="middle" class="content">Guest Sign-in</td>
	        <td align="right" valign="middle" class="content">
            <input name="guestRegister" type="checkbox" id="guestRegister" value="1" onchange="" <cfif prefs.guest_register>checked</cfif> /><label for="guestRegister"></label>
            </td>
	        <td align="left" valign="middle" class="content"> Registration</td>
	        <td align="right" valign="middle" class="content"><input name="codeSignin" type="checkbox" id="codeSignin" value="1" onChange="" <cfif prefs.code_signin>checked</cfif> /><label for="codeSignin"></label></td>
	        <td align="left" valign="middle" class="content">Use Code</td>
	        </tr>
      </table>
      </td>
	</tr>
	<tr>
	  <td align="right" class="content">&nbsp;</td>
	  <td>&nbsp;</td>
	  <td class="content">&nbsp;</td>
	  </tr>
  </table>
</form>

</cfoutput>