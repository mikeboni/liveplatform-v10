<style type="text/css">
.playButton {
	width: 92px;
 	height:92px;
 	display:block;
	background:transparent url('images/video/play.png') center top no-repeat;
}

.playButton:hover {
	background-image: url('images/video/pause.png');
}

.scrubbar {
	width: 100%;
 	height:44px;
 	display:block;
	background:transparent url('images/video/scrub-max.png') center top no-repeat;
}

</style>

<script type="text/javascript">

function setController(theType)
{
	scrubBar = document.getElementById('scrubbar');
	
	if(theType)
	{
		scrubBar.style.backgroundImage = "url('images/video/scrub-max.png')";
	}else{
		scrubBar.style.backgroundImage = "url('images/video/scrub-min.png')";
	}
	
}

function setAutoplay(theState)
{
	if(theState)
	{
		document.getElementById("autoplay").innerHTML = 'autoplay';
	}else{
		document.getElementById("autoplay").innerHTML = '';
	}
	
}


function setOptions(theObjName, theState)
{
	theControl = document.getElementById(theObjName);
	
	if(theObjName == 'controls' && !theState)
	{
		document.getElementById("rewind").removeAttribute("style");
		document.getElementById("zoom").removeAttribute("style");
		
		document.getElementsByName("rewind")[0].checked = false;
		document.getElementsByName("zoom")[0].checked = false;
		
	}else if(theObjName == 'controls' && theState){
		
		document.getElementsByName("rewind")[0].checked = true;
		document.getElementsByName("zoom")[0].checked = true;
		
	}else if( (theObjName == 'rewind' || theObjName == 'zoom') && document.getElementsByName("controls")[0].checked == false)
	{
		document.getElementsByName("controls")[0].checked = true;
		document.getElementById("controls").style.visibility="visible";
		
		if(theObjName == 'zoom')
		{	
			document.getElementById('rewind').style.visibility="hidden";
			
		}else if(theObjName == 'rewind')
		{	
			document.getElementById('zoom').style.visibility="hidden";
		}
	}
	
	if(theState)
	{
		theControl.style.visibility="visible";
		document.getElementsByName(theObjName)[0].checked = true;
	}else{
		theControl.style.visibility="hidden";
		document.getElementsByName(theObjName)[0].checked = false;
	}
	
}

</script>


<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfif asset.recordCount IS 0>

	<cfset rewind = 0>
    <cfset playpause = 1>
    <cfset zoom = 1>
    <cfset loop = 0>
    <cfset speedControls = 0>
    <cfset scrubbar = 1>
    <cfset autoplay = 1>
    <cfset releaseControls = 0>
    
<cfelse>

	<cfset rewind = asset.rewind>
    <cfset playpause = asset.playpause>
    <cfset zoom = asset.zoom>
    <cfset loop = asset.loop>
    <cfset speedControls = asset.speedControls>
    <cfset scrubbar = asset.scrubbar>
    <cfset autoplay = asset.autoplay>
    <cfset releaseControls = asset.releaseControls>

</cfif>


<script type="text/javascript">

	var jsVideo = new model3DManager();
	
	function updateControllerOptions()
	{
		rewindState = document.getElementsByName("rewind")[0].checked;
		playpauseState = document.getElementsByName("controls")[0].checked;
		zoomState = document.getElementsByName("zoom")[0].checked;
		loopState = document.getElementsByName("loop")[0].checked;
		speedState = document.getElementsByName("speed")[0].checked;
		seekState = document.getElementsByName("scrubbar")[0].checked;
		autoplayState = document.getElementsByName("autoplay")[0].checked;
		releaseState = document.getElementsByName("releaseControls")[0].checked;
		
		theOptions = {'rewind':rewindState, 'playpause':playpauseState, 'zoom':zoomState, 'loop':loopState, 'speed':speedState, 'seek':seekState, 'autoplay':autoplayState, 'release':releaseState};
		
		jsVideo.setSyncMode();
		jsVideo.setCallbackHandler(setVideoControllerOptionsSuccess);
		jsVideo.setVideoControllerOptions(<cfoutput>#asset.asset_id#</cfoutput>, theOptions);	
	}
	
	function setVideoControllerOptionsSuccess(result)
	{
		console.log('updated options');
	}
	
</script>

<cfset theVideo = '#assetPath##asset.url#'>
<cfset theImage = '#assetPath##asset.placeholder#'>

<script type="text/javascript" src="Assets/instanceName.js"></script>

<cfoutput>

<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td><label for="assetName"></label>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <tr>
      <td align="right" valign="bottom">&nbsp;</td>
      <td>
      
      <table cellpadding="0" cellspacing="0">
        <tr>
          <td valign="top">
			  <cfif fileExists(expandPath(theImage))>
              <a href="#theImage#" target="_new" class="contentLink">
                <img src="#theImage#" width="200" height="200" border="1" class="imgLockedAspect" />
              </a>
              <cfelse>
                <cfinclude template="noFile.cfm">
              </cfif>
          </td>
          <td width="10" valign="top"><spacer wdith=10></td>
          <td valign="top">
          <cfif asset.webURL NEQ "">
          
          <!--- <cfmediaplayer name="player_html" source="#asset.webURL#" type="flash" width=320 height=240 align="left" title="#assets.assetName#" > --->
          
          <video height="240" controls id="theVideo">
          	<source src="#asset.webURL#" type="video/mp4">
          </video> 
          
          <cfelse>
          
          <cfif fileExists(expandPath(theVideo))>
          	<!--- <cfmediaplayer name="player_html" source="#theVideo#" type="flash" width=320 height=240 align="left" title="#assets.assetName#" > --->
            
            <video height="240" controls id="theVideo">
          	<source src="#theVideo#" type="video/mp4">
          </video> 
            
          <cfelse>
          	<cfinclude template="noFile.cfm">
          </cfif>
          
          </cfif>
          </td>
        </tr>
        <tr>
          <td class="content">
          Image 
            Placeholder
            <cfif fileExists(expandPath(theImage))><cfelse>
            <span class="contentwarning">(File Missing)</span>
          </cfif>
          </td>
          <td>&nbsp;</td>
          <td class="content">
          Video
          <cfif fileExists(expandPath(theVideo))><cfelse>
            <span class="contentwarning">(File Missing)</span>
          </cfif>
          </td>
        </tr>
        <tr>
          <td class="content">&nbsp;</td>
          <td>&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><span class="contentHilighted">#asset.placeholder#</span></td>
          <td>&nbsp;</td>
          <td class="content"><span class="contentHilighted">#asset.url#</span></td>
        </tr>
        <tr>
          <td class="content">&nbsp;</td>
          <td>&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><input name="imageAsset" type="file" class="formfieldcontent" id="imageAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.jpg,.png')" /></td>
          <td>&nbsp;</td>
          <td class="content"><input name="VideoAsset" type="file" class="formfieldcontent" id="VideoAsset" style="padding-left:0" maxlength="255" onChange="this.form.webURL.value = '';checkInstanceName(this.form,this);checkFileExtention(this.name,'.mp4')" /></td>
        </tr>
        <tr>
          <td class="content">&nbsp;</td>
          <td>&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" class="content"><label for="webURL"></label>
            <table border="0">
              <tr>
                <td>Video URL</td>
                <td><input name="webURL" type="text" class="formfieldcontent" id="webURL" onclick="this.form.VideoAsset.value = ''" value="#asset.webURL#" size="80" maxlength="255" /></td>
                <td style="padding-left:10px">URL Overrides Local Asset<br />Asset Not Stored on Server</td>
              </tr>
          </table></td>
        </tr>
      </table>
      
      </td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="top"><hr size="1"></td>
    </tr>
    <tr>
      <td height="44" colspan="2" align="left" valign="middle" bgcolor="##666666" style="padding-left:10px" class="contentLinkWhite">Video Controller Options</td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="top">
      <div>

    <div style="background:##000; width:495px; border:2px solid ##021a40; float:left">
    
    <div id="loop" style="width:44px; height:44px; text-align: right; float:left"><img src="images/video/loop.png" width="44" height="44"></div>
    
    <div id="speed" style="width:70; height:32px; text-align: right; padding-top:5px; padding-right:5px; float:right"><img src="images/video/speed.png" width="62" height="32"></div>
    
    <div id="controls" style="width:100%; height:150px; text-align: center; margin-top:90px">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <td align="center" valign="middle">&nbsp;</td>
        <td width="60" align="center" valign="middle">
        <div id="rewind">
        <img src="images/video/rewind.png" width="44" height="44">
        </div>
        </td>
        <td width="100" align="center" valign="middle"><a href="##" title="playButton" class="playButton"></a></td>
        <td width="60" align="center">
        <div id="zoom">
        <img src="images/video/zoom.png" width="44" height="44">
        </div>
        </td>
        <td align="center">&nbsp;</td>
      </tr>
    </table>
    </div>
    <div id="autoplay" style="text-align: center; height:32px;vertical-align: top;" class="contentLinkWhite"></div>
    <div class="scrubbar" id="scrubbar"></div>
    </div>
    
    <div style="width:270px; float:left; margin-left:10px">
 
        <div style="height:32px; padding-top:6px; padding-left:10px; margin-top:10px" class="contentLinkGrey">
        <input name="rewind" id="rewindID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif rewind>checked</cfif>>
        <label for="rewindID">Rewind</label>
        </div>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="controls" id="controlsID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif playpause>checked</cfif>>
        <label for="controlsID">Play/Pause</label>
        </div>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="zoom" id="zoomID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif zoom>checked</cfif>>
        <label for="zoomID">Full Screen</label>
        </div>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="loop" id="loopID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif loop>checked</cfif>>
        <label for="loopID">Loop</label>
        </div>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="speed" id="speedID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif speedControls>checked</cfif>>
        <label for="speedID">Speed</label>
        </div>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
    <input name="scrubbar" id="scrubbarID" type="checkbox" onChange="setController(this.checked);updateControllerOptions();" value="1" <cfif scrubbar>checked</cfif>>
    <label for="scrubbarID">Seek</label>
    </div>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
    <input name="autoplay" id="autoplayID" type="checkbox" onChange="setAutoplay(this.checked);updateControllerOptions();" value="1" <cfif autoplay>checked</cfif>>
    <label for="autoplayID">Autoplay</label>
    </div>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
    <input name="releaseControls" id="releaseControlsID" type="checkbox" onChange="updateControllerOptions();" value="1" <cfif releaseControls>checked</cfif>>
    <label for="releaseControlsID">Release controls after played</label>
    </div>
    </div>

</div>
      </td>
  </tr>
</table>

<script type="text/javascript">
	setOptions('controls',#asset.playpause#);
	setOptions('rewind',#asset.rewind#);
	setOptions('zoom',#asset.zoom#);
	
	setOptions('loop',#asset.loop#);
	setOptions('speed',#asset.speedControls#);
	setAutoplay(#asset.autoplay#);
	setController(#asset.scrubbar#);
</script>

</cfoutput>