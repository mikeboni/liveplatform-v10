<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfset theImageN = '#assetPath##asset.north#'>
<cfset theImageS = '#assetPath##asset.south#'>
<cfset theImageW = '#assetPath##asset.west#'>
<cfset theImageE = '#assetPath##asset.east#'>

<cfset theImageN_nonretina = '#assetPath#nonretina/#asset.north#'>
<cfset theImageS_nonretina = '#assetPath#nonretina/#asset.south#'>
<cfset theImageW_nonretina = '#assetPath#nonretina/#asset.west#'>
<cfset theImageE_nonretina = '#assetPath#nonretina/#asset.east#'>

<cfset fixNonRetina = arrayNew(1)>

<cfif fileExists(expandPath(theImageN_nonretina))><cfset borderN = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageN),'des':expandPath(theImageN_nonRetina)})>
<cfset borderN = 2></cfif>

<cfif fileExists(expandPath(theImageS_nonretina))><cfset borderS = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageS),'des':expandPath(theImageS_nonRetina)})>
<cfset borderS = 2></cfif>

<cfif fileExists(expandPath(theImageW_nonretina))><cfset borderW = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageW),'des':expandPath(theImageW_nonRetina)})>
<cfset borderW = 2></cfif>

<cfif fileExists(expandPath(theImageE_nonretina))><cfset borderE = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImageE),'des':expandPath(theImageE_nonRetina)})>
<cfset borderE = 2></cfif>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td align="right" valign="middle">Asset Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <!--- <tr>
      <td align="right" valign="middle">Floor Name</td>
      <td><input name="FloorName" type="text" class="formfieldcontent" id="FloorName" value="#asset.floor#" size="60" maxlength="128" /></td>
    </tr> --->
    <tr>
      <td width="80" align="right" valign="top">&nbsp;</td>
      <td>

        <table border="0">
          <tr class="contentHilighted">
            <td align="center">North View Image</td>
            <td align="center">South View Image</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(expandPath(theImageN))>
            <a href="#theImageN#" target="_new" class="contentLink">
          	<img src="#theImageN#" alt="#theImageN#" width="313" height="133" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderN#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(expandPath(theImageS))>
            <a href="#theImageS#" target="_new" class="contentLink">
            <img src="#theImageS#" alt="#theImageS#" width="313" height="133" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderS#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td class="content">
            <span class="contentHilighted">#asset.north#</span>
            <cfif NOT fileExists(expandPath(theImageN))>
              <span class="contentwarning">(File Missing)</span>
            </cfif>
            <cfif borderN GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </td>
            <td class="content">
            <span class="contentHilighted">#asset.south#</span>
            <cfif NOT fileExists(expandPath(theImageS))>
              <span class="contentwarning">(File Missing)</span>
            </cfif>
            <cfif borderS GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </td>
          </tr>
          <tr>
            <td><input name="BalconyNAsset" type="file" class="formfieldcontent" id="BalconyNAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="BalconySAsset" type="file" class="formfieldcontent" id="BalconySAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
          <tr>
            <td height="10" colspan="3">&nbsp;</td>
          </tr>
          <tr class="contentHilighted">
            <td align="center">West View Image</td>
            <td align="center">East View Image</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(expandPath(theImageW))>
            <a href="#theImageW#" target="_new" class="contentLink">
            <img src="#theImageW#" alt="#theImageW#" width="313" height="133" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderW#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(expandPath(theImageE))>
            <a href="#theImageE#" target="_new" class="contentLink">
            <img src="#theImageE#" alt="#theImageE#" width="313" height="133" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #borderE#px;" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td class="content">
            <span class="contentHilighted">#asset.west#</span>
            <cfif NOT fileExists(expandPath(theImageW))>
              <span class="contentwarning">(File Missing)</span>
              <cfif borderW GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
            <td class="content">
            <span class="contentHilighted">#asset.east#</span>
            <cfif NOT fileExists(expandPath(theImageE))>
              <span class="contentwarning">(File Missing)</span>
              <cfif borderE GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif>
            </cfif>
            </td>
          </tr>
          <tr>
            <td><input name="BalconyWAsset" type="file" class="formfieldcontent" id="BalconyWAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="BalconyEAsset" type="file" class="formfieldcontent" id="BalconyEAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
        </table>

      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Floor Name</td>
      <td><cfoutput>
        <input name="floorName" type="text" class="formfieldcontent" style="width:150px" id="floorName" value="#asset.floorName#" size="60" maxlength="20" />
      </cfoutput></td>
    </tr>
 </table>
</cfoutput>

<cfset exists = true>
<cfloop array="#fixNonRetina#" index="path">
	<cfif GetFileFromPath(path.src) IS ''><cfset exists = false></cfif>
</cfloop>

<!--- Fix Non Retina Images --->
<cfif arrayLen(fixNonRetina) GT 0>

<cfset failed = arrayNew(1)>

    <cfloop array="#fixNonRetina#" index="path">
	
		<cfif NOT fileExists(path.des) AND fileExists(path.src)>
        
			<cfoutput>Fixed Missing Nonretina Image:#GetFileFromPath(path.src)#</cfoutput><br>
            
            <cfinvoke component="CFC.Misc" method="createNonRetinaImage" returnvariable="scaled">
                <cfinvokeargument name="imageSrc" value="#path.src#"/>
                <cfinvokeargument name="imageDes" value="#path.des#"/>
            </cfinvoke>
        
        </cfif>
    	
        <cfif NOT fileExists(path.des) AND fileExists(path.src)>
			<cfset arrayAppend(failed,path)>
        </cfif>
        
    </cfloop>
	
   <cfloop array="#failed#" index="theFailedPath">
   
       <cfset FN = GetFileFromPath(theFailedPath.src)>
		
		<cfif FN NEQ ''>
        	Missing Images Failed to be Created - Created Missing Image
        	<cfdump var="#theFailedPath#">
            <cfset reloadPage = true>
        </cfif>
    
    </cfloop>
    
</cfif>