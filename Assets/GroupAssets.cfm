<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<!--- <link href="../styles.css" rel="stylesheet" type="text/css"> --->

<cfif asset.recordCount GT 0>
    <cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="groupAssets">
            <cfinvokeargument name="assetID" value="#asset.asset_id#"/>
    </cfinvoke>
 </cfif>   

<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="all3DAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="6"/><!--- 3D Type --->
</cfinvoke>

<cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<cfinvoke component="CFC.Assets" method="getAssetsTypes" returnvariable="assetTypes" />


<cfquery name="contentAsset">
    SELECT * 
    FROM 	ContentAssets
    WHERE asset_id = #assetID#
</cfquery>

<script type="text/javascript">

function showDelete(theObj, state)	{
	
	theObjRef = document.getElementById(theObj);
	
	if(state)
	{
		theObjRef.className = 'itemShow';
	}else{
		theObjRef.className = 'itemHide';
	}
	
}

function deleteContent(assetID) {
		if(confirm('Are you sure you wish to Delete this Remove Asset?'))
		{
			//nothing
		}else{ 
			//nothing
		}
}

function updateContentAssetOptions(theObj,theAssetID) 
{
	theLevel = null;
	theCached=null;
	theActive=null;
	theShared=null;
	theOrder=null;
	
	if(theObj.id == 'contentActive')
	{
		theActive = toggleState(theObj);
	}
	
	if(theObj.id == 'contentShared')
	{
		theShared = toggleState(theObj);
	}
	
	if(theObj.id == 'contentCached')
	{
		theState = theObj.style.backgroundImage;
		src = theState.substring(4, theState.length-1);
		
		theSrc = src.replace( /^.+\// , '' );
		srcName = theSrc.replace(/"/g, "");
		
		if( srcName == "cache.png")
		{
			newSrc = "cached"
			theCached = 1
		}else{
			newSrc = "cache"
			theCached = 0;
		}
		
		theObj.style.backgroundImage = 'url(images/'+ newSrc +'.png)';
		
	}
	
	if(theObj.id == 'accessLevel')
	{
		theLevel = parseInt(theObj.options[theObj.selectedIndex].value);
		console.log(theLevel);
		if(theLevel == 0)
		{
			lock = 'access_unlocked';  
		}else{
			lock = 'access_locked-'+theLevel; 
		}
		
		theObj.style.backgroundImage = 'url(images/'+ lock +'.png)';
	}
	
	if(theObj.id == 'sortOrder')
	{
		theOrder = parseInt(theObj.value);
	}
	
	theStruct = {'access':theLevel, 'cached':theCached, 'active':theActive, 'shared':theShared, 'order':theOrder};
	updateContentAsset(theAssetID,theStruct);
}


function toggleState(theObj)
{
	if(theObj.className == 'contentLinkRed')
	{
		state = 0;
		theObj.className = 'contentLinkGreen';
		theObj.innerHTML = 'NO';
	}else{
		state = 1;
		theObj.className = 'contentLinkRed';
		theObj.innerHTML = 'YES';
	}
		
	return state;	
}

function clearUseAssetID()
{
	theObjRef = document.getElementById('useAssetID');
	theObjRef.selectedIndex = 0;
}

function clearInstanceName()
{
	theObjRef = document.getElementById('instanceNameAsset');
	theObjRef.value = '';
}


function displayExtra(theForm)
{
	theSel = parseInt(theForm.options[theForm.selectedIndex].value);
	
	switch(theSel) {
    case 1:
        console.log('on');
		document.getElementById('instanceName').style.display = 'block';
		document.getElementById('instanceNameEntry').style.display = 'block';
        break;
		
    default:
		console.log('off');
        document.getElementById('instanceName').style.display = 'none';
		document.getElementById('instanceNameEntry').style.display = 'none';
	} 

}

</script>

<!--- 3D --->
<cfif contentAsset.displayType IS 1>
  <cfset theState = 'block'>
<cfelse>
  <cfset theState = 'none'>
</cfif>



<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td valign="middle">
      <cfoutput>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" style="width:400px" maxlength="128" />
      </cfoutput>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Behaviour </td>
      <td align="left" valign="middle">
      <cfoutput>
      
      <cfset allTypes = {"3D Model":1, "Modal":2, "Panel":3, "PopOver":4}>
      <label for="useAssetID"></label>
        <select name="behaviourType" id="behaviourType" class="formfieldcontent" style="padding-left:0;" onchange="displayExtra(this);">
        <option value="0">None</option>
        <cfloop collection="#allTypes#" item="type">
          <option value="#allTypes[type]#" <cfif contentAsset.displayType IS allTypes[type]> selected</cfif>>#type#</option>
        </cfloop>
      </select>
      </cfoutput>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">
      <div style="display:#theState#; margin-top:10px" id="instanceName">
      Instance Name
      </div>
      </td>
      <td align="left" valign="middle">
      
      
      <cfoutput>
      <div style="display:#theState#; margin-top:10px" id="instanceNameEntry">
      <input name="instanceNameAsset" type="text" class="formfieldcontent" id="instanceNameAsset" style="width:300px" value="#contentAsset.instanceName#" onchange="clearUseAssetID();"/>
      Use AssetID:
      <label for="useAssetID"></label>
        <select name="useAssetID" id="useAssetID" class="formfieldcontent" style="padding-left:0;" onchange="clearInstanceName();">
        <option value="0"><-- Use Instance Name</option>
        <cfloop query="#all3DAssets#">
          <option value="#asset_id#" <cfif contentAsset.instanceAsset_id IS asset_id> selected</cfif>>#name#</option>
        </cfloop>
      </select>
      
      </div>
      </cfoutput>
      </td>
    </tr>
    
</table>

<!--- <cfif active>
		<cfset state = "contentLink">
    <cfelse>
        <cfset state = "contentLinkDisabled">
</cfif> 
<div class="sectionLink" style="background-color:#333; padding-bottom:5px; height:44px; padding-left:10px; width:800px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="44">Assets</td>
    <td width="44" align="right">
    <a onclick="displayImporter()">
    <img src="images/import.png" width="44" height="44" />
    </a>
    </td>
  </tr>
  <tr>
</table>

</div> --->

<!--- <div style="width:800px; display:none;" id="assetImporter"> 
		<cfoutput>
        <div style="margin-top: 10px;height: 50px;">
        <form method="post" style="padding-top:40px">
          <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssets')" style="height:44px; margin:0px;">
            <option value="0">All Asset Types</option>
            <cfloop query="assetTypes">
                <option value="#type#">#name#</option>
            </cfloop>
          </select>
          <input type="button" class="formText" value="Import Content" onclick="allSelectedContent()" />
          <input name="contentIDs" type="hidden" id="contentIDs" />
          <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
        </form>
        </div>
        </cfoutput>
        <div id="displayAssets" style="overflow:scroll; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
    </div> --->

<!--- <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">                  
                    <tr class="content">
                      <td height="44" style="padding-left:10px">
                      Asset Name
                      </td>
                      <td width="80" align="center" class="content">
						Access
                      </td>
                      <td width="60" align="center" class="content">  
                      Cached
                      </td>
                      <td width="60" align="center" class="content">
                       Active
                      </td>
                      <td width="60" align="center" class="content">
                      Shared</td>
                      <td width="80" align="center" class="content">Sort Order </td>
                      <td width="60" align="right" class="content"></td>
  </tr>  
</table> --->
<!--- 
<cfif asset.recordCount GT 0>

<cfoutput query="groupAssets">
<div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">
					
                    <form id="content_id">
				    <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                    <!--- Assets --->
                    <tr>
                      <td width="44">
                        <img src="images/#icon#" width="44" height="44" />
                      </td>
                      <td colspan="2">
                      <a href="AssetsView.cfm?assetID=#content_id#&assetTypeID=#assetType_id#" class="#state#">
                      <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                      </a>
                      </td>
                      <td width="80" align="center" class="content">

                        <cfif accessLevel IS 0>
							<cfset lock = 'access_unlocked'>
                        <cfelse>
                        	<cfset lock = 'access_locked-'& accessLevel>
                        </cfif>
                        
                        <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                            <cfloop query="accessLevels">
                            <option value="#accessLevel#" style="color:##333" <cfif groupAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                            </cfloop>
                        </select>
                      
                      </td>
                      <td width="60" align="center" class="content">  
                      <cfif cached IS '0'>
                      <cfset theCacheState = 'cache'>
                      <cfelse>
                      <cfset theCacheState = 'cached'>
                      </cfif>
                      <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                      </div>
                      </td>
                      <td width="60" align="center" class="content">
                      
                     
                      <cfif active IS '0'>
                      	<cfset stateCSS = "contentLinkGreen">
                      <cfelse>
                      	<cfset stateCSS = "contentLinkRed">
                      </cfif>
					  <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
						  <cfif active IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                      </div>
                      </td>
                      <td width="60" align="center" class="content">
                      
                      <cfif sharable IS '0'>
                      	<cfset stateCSS = "contentLinkGreen">
                      <cfelse>
                      	<cfset stateCSS = "contentLinkRed">
                      </cfif>
					  <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                      <cfif sharable IS '0'>
                      NO
                      <cfelse>
                      YES
                      </cfif>
                      </div>
                      
                      </td>
                      <td width="80" align="center" class="contentLinkDisabled">
                      <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                      <td width="60" align="right" class="content">
                      
                      <table border="0">
                      <tr>
                        <td>
                        <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#);" value="" /> 
                        </td>
                      </tr>
                      
                      
                      
                    </table>
                      
                      
                      </td>
                    </tr>
                    
                      </table>
                    </form>
</div>
</cfoutput>   

<cfelse>
<span class="content">
No Group Content Assets
</span>
</cfif> --->