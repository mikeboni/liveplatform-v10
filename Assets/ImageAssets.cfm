<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<script type="text/javascript" src="Assets/instanceName.js"></script>

<cfset fixNonRetina = arrayNew(1)>

<cfset theImage = '#assetPath##asset.url#'>
<cfset theImageNonRetina = '#assetPath#nonretina/#asset.url#'>

<cfif fileExists(expandPath(theImageNonRetina))><cfset border = 0><cfelse>
<cfset arrayAppend(fixNonRetina,{'src':expandPath(theImage),'des':expandPath(theImageNonRetina)})>
<cfset border = 2></cfif>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <cfif asset.url NEQ '' OR asset.webURL NEQ ''>
    <tr>
      <td align="right" valign="top">Resolution</td>
      <td> Width: #asset.width#, Height: #asset.height# <cfif border GT 0> <abbr style="color:##FF0004">(Non Retina Image Missing)</abbr></cfif></td>
    </tr>
    <tr>
      <td width="80" align="right" valign="top">&nbsp;</td>
      <td>
      	  <cfif asset.webURL NEQ ''>
          
          <a href="#asset.webURL#" target="_new" class="contentLink">
                <img src="#asset.webURL#" alt="#name#" width="298" height="221" border="1" class="imgLockedAspect" />
                </a>
          
          <cfelse>

              <cfif fileExists(expandPath(theImage))>
                <a href="#theImage#" target="_new" class="contentLink">
                <img src="#theImage#" alt="#theImage#" width="298" height="221" border="1" class="imgLockedAspect" style="border: solid ##FF0004;border-width: #border#px;" />
                </a>
              <cfelse>
                <cfinclude template="noFile.cfm">
              </cfif>
          
          </cfif>
      	  

      </td>
    </tr>
    </cfif>
    <cfif asset.url NEQ ''>
    <tr>
      <td align="right" valign="middle">Image</td>
      <td valign="middle" class="contentHilighted">
      #asset.url#
      <cfif NOT fileExists(expandPath(theImage))>
      <span class="contentwarning">(File Missing)</span>
      </cfif>
      </td>
    </tr>
    </cfif>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td><input name="imageAsset" type="file" class="formfieldcontent" id="imageAsset" style="padding-left:0" maxlength="255" onchange="this.form.webURL.value='';checkInstanceName(this.form,this);checkFileExtention(this.name,'.jpg,.png')" /></td>
    </tr>

    <tr>
      <td align="right" valign="middle">WebLink</td>
      <td><label for="webURL"></label>
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="middle"><input name="webURL" type="text" class="formfieldcontent" id="webURL" value="#asset.webURL#" size="95" maxlength="255" onclick="this.form.imageAsset.value=''" /></td>
            <td valign="middle" style="padding-left:10px">URL Overrides Local Asset<br />
Asset Not Stored on Server</td>
          </tr>
      </table></td>
    </tr>
    <cfif asset.webURL IS ''>
   </cfif> 
 </table>
</cfoutput>

<!--- Fix Non Retina Images --->
<!--- <cfif arrayLen(fixNonRetina) GT 0>

<!--- <cfset failed = arrayNew(1)>

    <cfloop array="#fixNonRetina#" index="path">
	
		<cfif NOT fileExists(path.des) AND fileExists(path.src)>
        
			<cfoutput>Fixed Missing Nonretina Image:#GetFileFromPath(path.src)#</cfoutput><br>
            
            <cfinvoke component="CFC.Misc" method="createNonRetinaImage" returnvariable="scaled">
                <cfinvokeargument name="imageSrc" value="#path.src#"/>
                <cfinvokeargument name="imageDes" value="#path.des#"/>
            </cfinvoke>
        
        </cfif>
    	
        <cfif NOT fileExists(path.des) AND fileExists(path.src)>
			<cfset arrayAppend(failed,path)>
        </cfif>
        
    </cfloop> --->
	
  <!---  <cfloop array="#failed#" index="theFailedPath">
   
       <cfset FN = GetFileFromPath(theFailedPath.src)>
		
		<cfif FN NEQ ''>
        	Missing Images Failed to be Created - Created Missing Image
        	<cfdump var="#theFailedPath#">
            <cfset reloadPage = true>
        </cfif>
    
    </cfloop> --->
    
</cfif> --->