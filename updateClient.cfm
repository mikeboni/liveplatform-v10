<cfparam name="clientID" default="0">

<cfparam name="name" default="">
<cfparam name="folder" default="">
<cfparam name="active" default="true">
<cfparam name="icon" default="">
<cfparam name="clientID" default="0">
<cfparam name="supportEMail" default="">
<cfparam name="clientEMail" default="">

<cfset tempPath = GetTempDirectory()>

<cfif imageFile NEQ ''>
	<cffile action="upload" filefield="imageFile" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset imageFile = uploadFile.serverFile>
<cfelse>
	<cfset imageFile = ''>
</cfif>

<cfif session.clientID GT '0'>
	<!---Update Client--->
	
    <cfif name NEQ ''>
    
	<cfinvoke component="CFC.clients" method="updateClient" returnvariable="cientID">
		<cfinvokeargument name="clientID" value="#session.clientID#"/>
		<cfinvokeargument name="company" value="#name#"/>
		<cfinvokeargument name="path" value="#folder#"/>
		<cfinvokeargument name="image" value="#imageFile#"/>
		<cfinvokeargument name="active" value="#active#"/>
        <cfinvokeargument name="support" value="#supportEMail#"/>
        <cfinvokeargument name="clientEMail" value="#clientEMail#"/>
	</cfinvoke>
	
    <cfif session.clientID GT '0'>
		<cfset error = 'Client Updated'>
		<cflocation url="CientsView.cfm?clientID=#session.clientID#" addtoken="no">
	<cfelse>
		<cfset error = 'Client Already Not Updated'>
		<cflocation url="CientsView.cfm?clientID=0&amp;error=#error#" addtoken="no">
	</cfif>
    
    <cfelse>
    	<cfset error = 'Company Name Cannot be Empty. Client NOT Updated.'>
        <cflocation url="CientsView.cfm?clientID=0&amp;error=#error#" addtoken="no">
    </cfif>
	
<cfelse>
	
    <cfif name NEQ '' AND folder NEQ ''>
		<!---New Client--->
        <cfinvoke component="CFC.clients" method="createClient" returnvariable="cientID">
            <cfinvokeargument name="company" value="#name#"/>
            <cfinvokeargument name="path" value="#folder#"/>
            <cfinvokeargument name="image" value="#imageFile#"/>
            <cfinvokeargument name="active" value="#active#"/>
            <cfinvokeargument name="support" value="#supportEMail#"/>
            <cfinvokeargument name="clientEMail" value="#clientEMail#"/>
        </cfinvoke>

        <cfif cientID GT '0'>
            <cfset error = 'Client Created'>
            <cflocation url="CientsView.cfm?clientID=#cientID#" addtoken="no">
        <cfelse>
            <cfset error = 'Client Already Exists'>
            <cflocation url="CientsView.cfm?clientID=0&amp;error=#error#" addtoken="no">
        </cfif>
    <cfelse>
    		<cfset error = 'Company Name OR Folder Path Cannot be Empty. Client NOT created.'>
            <cflocation url="CientsView.cfm" addtoken="no">
	</cfif>
	
</cfif>
            

