﻿<cfparam name="appID" default="">
<cfparam name="error" default="">
<cfparam name="edit" default="false">
<cfparam name="editP" default="false">
<cfparam name="tab" default="0">
<cfparam name="assetTypeTab" default="0">

<cfparam name="assetID" default="0">
<cfparam name="assetTypeID" default="0">

<cfparam name="search" default="">
<cfparam name="import" default="false">

<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>

<cfif NOT IsDefined("session.clientID")><cflocation url="CientsView.cfm" addtoken="no"></cfif>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Applications</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="JS/mcColorPicker/mcColorPicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="JS/mcColorPicker/mcColorPicker.js"></script>
<script type="text/javascript" src="JS/classSelector.js"></script>

<!--- LiveAPI --->
<cfajaxproxy cfc="CFC.Apps" jsclassname="updateSupportLink">

<!--- LiveAPI AppOptions --->
<cfajaxproxy cfc="CFC.Options" jsclassname="appOptionsSettings">

<!--- LiveAPI EMail --->
<cfajaxproxy cfc="CFC.EMail" jsclassname="appEMailSettings">

<!--- LiveAPI Content --->
<cfajaxproxy cfc="CFC.Modules" jsclassname="contentAccess"> 

<!--- LiveAPI Markers --->
<!--- <cfajaxproxy cfc="CFC.Markers" jsclassname="contentMarkers"> --->

<script type="text/javascript">

//App
function updateApp(form)	{
	if(form==0)
	{
		//new app
		document.forms['newApp'].submit();
			
	}else{
		//update app
		document.forms['updateApp'].submit();
		
	}
}

//Prefs
function updatePrefs()	{
		document.forms['updatePrefs'].submit();
}		

function createForm() { 
	document.getElementById("formContainer").innerHTML = '<input type="text" id="cinput2" class="color" value="#00aa00" />'; 
	MC.ColorPicker.reload();
}

</script>


</head>

<body>

<!--- Main Header --->
<cfinclude template="Assets/header.cfm">

<!--- <cfif NOT IsDefined("session.assetPaths")><cflocation url="CientsView.cfm" addtoken="no"></cfif> --->

<cfset active = assetPaths.application.active>
<cfset iconPath = assetPaths.application.icon>
<cfset bundleID = assetPaths.application.bundleID>
<cfset appName = assetPaths.application.name>
<cfset appPath = assetPaths.application.path>
<cfset version = assetPaths.application.version>
<cfset forced = assetPaths.application.forced>
<cfset support = assetPaths.application.support>
<cfset clientEmail = assetPaths.application.clientEmail>
<cfset download = assetPaths.application.download>
<cfset productID = assetPaths.application.productID>


    <div style="width:800px; background-color:##333; height:32px; padding-left:0px; padding-top:4px; margin-bottom:10px; height:80px; float:none">
		
	<cfoutput>
      
      <div>
      
        <cfif active>
        
          <img src="#iconPath#" width="68" height="68" style="float:left; padding-right:10px;padding-top: 10px;" />
          
        <cfelse>
        
        	<div style="width:68px; height:68px; float: left; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; background-position: 50% 50%; margin-right:10px">
            <img src="images/inactive-app.png" width="68" height="68" style="float:left; padding-right:10px;padding-top: 10px;" />
            </div>
            
        </cfif> 
         
        	<div class="bannerHeading" style="padding-top:0px; width:auto">#appName#<br /><span class="bundleid"> (#bundleID#)</span></div>
        	<div class="content"><span class="contentHilighted">#appPath#</span></div>
            
      </div>
	
      <cfset editState = NOT edit>
      
      <div class="optionButtons">
          <a href="AppsView.cfm?edit=#editState#"><img src="images/info.png" width="44" height="44" alt="users" /></a>
      </div>
          
          
      <cfset editPrefs = NOT editP>
      
      <div class="optionButtons">
          <a href="AppsView.cfm?editP=#editPrefs#"><img src="images/settings.png" width="44" height="44" alt="users" /></a>
      </div>
          
        <div class="optionButtons">
          <a href="usersView.cfm"><img src="images/users.png" width="44" height="44" alt="users" /></a>
      </div>   
          
    </cfoutput>
    
    </div>
 
<cfif edit>
	
	<!---Edit Application Info--->

	<div style="width:810px; background-color:#EEE;">

	<div style="background-color:#069; margin-top:5px; height:36px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
  
	<div style="width:600px;float:left" class="sectionLink">
	<a href="AppsView.cfm?edit=true&amp;tab=0" class="<cfif tab IS '0'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Application</a> | <a href="AppsView.cfm?edit=true&amp;tab=1" class="<cfif tab IS '1'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Update Links</a> | <a href="AppsView.cfm?edit=true&amp;tab=2" class="<cfif tab IS '2'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Options</a> | <a href="AppsView.cfm?edit=true&amp;tab=5" class="<cfif tab IS '5'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Administration</a>
</div>

	</div>   

	<!--- <cfoutput query="theApp"> --->
          
	 <cfif tab IS '0'>
     	<!--- Settings - App Prefs --->
        <cfinvoke  component="CFC.Apps" method="getAppProducts" returnvariable="products" />
        <cfinclude template="Assets/appPrefs.cfm">
     </cfif>
            
     <cfif tab IS '1'>
     	<!--- Settings - App Links --->
     	<cfinclude template="Assets/appSettingsLinks.cfm">
     </cfif>
     
     <cfif tab IS '2'>
     	<!--- Settings - App Links --->
     	<cfinclude template="Assets/appSettingsOptions.cfm">
     </cfif>
     
     <cfif tab IS '5'>
     	<!--- Settings - App Links --->
     	<cfinclude template="Assets/appSettingsAdmin.cfm">
     </cfif>
     
  <!--- </cfoutput> --->
  
 	</div>
 
</cfif>

<cfif editP>

    <cfinvoke  component="CFC.Apps" method="getAppPrefs" returnvariable="prefs">
        <cfinvokeargument name="appID" value="#session.appID#"/>
    </cfinvoke>

    <!---Edit Preferences--->

	<div style="background-color:#EEE; width:810px">
        
	<div style="background-color:#069; margin-top:5px; height:36px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
	<div style="width:400px;float:left" class="sectionLink">
    	<a href="AppsView.cfm?editP=true&amp;tab=0" class="<cfif tab IS '0'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Preferences</a> | <a href="AppsView.cfm?editP=true&amp;tab=1" class="<cfif tab IS '1'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Support Links</a>
    </div>
	</div>
            
    <cfif tab IS '0'>
    	  <!--- Settings - General --->
          <cfinclude template="Assets/appSettingsGeneral.cfm">
          
    <cfelseif tab IS '1'>
        	<!--- Settings - Support Links --->
          <cfinclude template="Assets/appSettingsSupportLinks.cfm">
	</cfif>

   
 <cfelse>   

	<cfif NOT edit>  
      
		<!---Groups List--->
        <div style="background-color:#666; padding-left:5px; margin-top:5px; height:46px; width:805px" class="sectionLink">
    
    <table width="800" border="0" style="padding-top:5px;position: absolute;">
      <tr>
        <td>
        <a href="AppsView.cfm?assetTypeTab=0" class="<cfif assetTypeTab IS 0>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Application Content</a> | 
    <a href="AppsView.cfm?assetTypeTab=3" class="<cfif assetTypeTab IS 3>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Library of Assets</a>
        </td>
        <td align="right" class="function" style="color:#999">
        
        <table border="0">
          <tr>
            <td>Search</td>
            <td>
            <cfoutput>
            <form action="AppsView.cfm?assetTypeTab=3&assetTypeID=#assetTypeID#&import=#import#" method="post" name="searchAssets">
            <input name="search" type="text" id="search" value="#search#" />
			</form>
            </cfoutput>
            </td>
          </tr>
        </table>
        
        </td>
      </tr>
    </table>

      </div>
        
        <cfif assetTypeTab LT 3>
        
       	   <!--- nothing --->
    
          <cfif assetTypeTab IS 0>
          
            	<!--- Contents --->
            	<cfinclude template="groupListing.cfm"> 
                
         </cfif>

        <cfelse>
          
			  <!--- Asset Listing --->
              <cfinclude template="assetListing.cfm">
            
        </cfif>
		
      
  </cfif>

</cfif>

</body>
</html>