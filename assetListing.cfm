﻿<!---Asset Listing--->
<cfparam name="import" default="false">
<cfparam name="assetsToAdd" default="arrayNew(1)">
<script type="text/javascript">

<!--- Asset Types Icon Listing on Line 178--->

var selectedItemsToImport;

function showDelete(theObj, state)	{
	
	theObjRef = document.getElementById(theObj);
	
	if(state)
	{
		removeClass(theObjRef,"itemHide");
		addClass(theObjRef, "itemShow");
	}else{
		removeClass(theObjRef,"itemShow");
		addClass(theObjRef, "itemHide");
	}
	
}

function deleteAsset(assetID) { 
		if(confirm('Are you sure you wish to Delete this Asset?'))
		{
			document.location.href = "deleteAsset.cfm?assetID="+assetID+"<cfoutput>&assetTypeTab=#assetTypeTab#&assetTypeID=#assetTypeID#</cfoutput>";
		}else{ 
			//nothing
		}
}

function addAsset(theObj) {
		
		theObjRef = document.getElementById('addAsset_'+theObj);
		
		theAsset = document.getElementById('assetID_'+theObj);
		assetID = theAsset.value;

		if(theObjRef.className == "checkmarkSelected")
		{
			state = false;
		}else{
			state = true;
		}
		
		setCheckmarkState(theObj,state);
		
		if(assetID == 0)
		{
		
			for( i=1; i<= totalItems; i++)
			{
				setCheckmarkState(i,state);
			}
			
		}else{
			setCheckmarkState(0,false);
		}
		
		getAllSelectedObjects();

}

function setCheckmarkState(theObj,state)	{
	
	theObjRef = document.getElementById('addAsset_'+theObj);
		
		if(state)
		{
			removeClass(theObjRef,"checkmarkNormal");
			addClass(theObjRef, "checkmarkSelected");
			
		}else{
			removeClass(theObjRef,"checkmarkSelected");
			addClass(theObjRef, "checkmarkNormal");
		}
	
}

function getAllSelectedObjects()	{
	
	selectedItemsToImport = [];
	
  for( i=1; i<= totalItems; i++)
	  {
		  theObjRef = document.getElementById('addAsset_'+i);

		  if(theObjRef.className == "checkmarkSelected")	{
			  
			  theAsset = document.getElementById('assetID_'+i);					
			  selectedItemsToImport.push(theAsset.value);
		  }
	  }
	
	theAssetIDs = document.getElementById('assetsIDs');
	theAssetIDs.value = selectedItemsToImport;
}	

</script>

    <cfquery dbtype="query" name="typeSelected">
        SELECT icon,name,path
        FROM   session.assetstypes
        WHERE  type = #assetTypeID#
    </cfquery>

   <cfif import>	
		<cfset color = '333'>
    <cfelse>
    	<cfset color = 'AAA'>
    </cfif> 
    	<cfif import>
        <table width="810" style="margin-top:5px" border="0" cellpadding="0" cellspacing="5" class="content" bgcolor="#FFF">
          <tr>
            <td colspan="4" align="left" class="sectionLink">
            <form id="assets" name="assets" method="post" action="importAssetsInGroup.cfm">
            <input name="assetsIDs" type="hidden" id="assetsIDs" />
            <input type="button" value="Import Selected Assets" onclick="this.form.submit();" />
            </form>
            </td>
          </tr>
        </table>
        </cfif>
       
	<cfif assetTypeID GT '0'> 
    <table width="810" style="margin-top:5px" border="0" cellpadding="0" cellspacing="5" class="content" bgcolor="#<cfoutput>#color#</cfoutput>">
          <tr>
            <td class="sectionLink">
            <form id="form1" name="form1" method="post" action="AppsView.cfm?assetTypeTab=3&amp;import=<cfoutput>#import#</cfoutput>">
            <!--- types filter --->
                <select name="assetTypeID" id="assetTypeID" onchange="this.form.submit()">
                <cfoutput query="session.AssetsTypes">
                  <option value="#type#"<cfif assetTypeID IS type>selected</cfif>>#name#</option>
                </cfoutput>
                </select>     
            </form>
            </td>
            <td width="80" align="center" class="subheading">Thumbnail</td>
            <td width="80" colspan="-1" align="right" class="subheading">Created</td>
            <td width="80" align="right" class="subheading">Modified</td>
            <td width="60" align="right" class="subheading">
            <cfif import>
				<input name="addAsset_0" type="button" id="addAsset_0" class="checkmarkNormal" onClick="addAsset(0);" value="" />
                <input name="assetID_0" type="hidden" id="assetID_0" value="0" />
            <cfelse>
            <cfoutput>
            <a href="updateAsset.cfm?assetTypeID=#assetTypeID#">
            <img src="images/add.png" width="44" height="44" border="0" />
            </a>
            </cfoutput>
            </cfif>
            </td>
          </tr>
        </table>
    </cfif>
    
    <cfif assetTypeID IS '0'>
      
      <cfoutput>

      <cfset cnt = 1>
      <cfset cols = 5>
      <table width="800" border="0" style="margin-top:20px;">
      <tr>
      <cfloop query="session.AssetsTypes">

        <cfinvoke component="CFC.Assets" method="getTotalAssets" returnvariable="total">
        	<cfinvokeargument name="appID" value="#session.appID#"/>
        	<cfinvokeargument name="AssetTypeID" value="#type#"/>
        </cfinvoke>
        
        <td>
        <!--- Button Colors --->
        <cfswitch expression="#categoryType#">
        
        <cfcase value="1">
        	<cfset catColor = "assetButtonCat1">
        </cfcase>

        <cfdefaultcase>
        	<cfset catColor = "assetButton">
        </cfdefaultcase>
        	
        </cfswitch>
        
        
        <!--- Asset Types Icon Listing --->
        <table width="100%" border="0">
        <tr>
          <td align="center" class="contentLink" height="20">
          	  <a href="AppsView.cfm?assetTypeTab=3&amp;assetTypeID=#type#&amp;import=#import#">
              	<img src="images/create/#icon#" width="100" height="100" class="#catColor#" />
              </a>
           </td>
        </tr>
            <tr>
              <td align="center" class="contentLink" height="20">#name#<br />
              <cfif total GT '1'>
              #total# Assets
              <cfelseif total IS '0'>
              No Assets
              <cfelse>
              #total# Asset
              </cfif>
              </td>
            </tr>
        </table>

        </td>
        
         <cfif cnt GTE cols>
        </tr><tr><td colspan="#cols#"></td></tr><tr>
         <cfset cnt = '1'>
        <cfelse>
        	<cfset cnt = cnt + 1>
        </cfif>
	
      </cfloop>
      </tr>
      </table>
      
      </cfoutput>
        
    <cfelse>
    
        <cfinvoke component="CFC.Assets" method="getAssetListing" returnvariable="assets">
          <cfinvokeargument name="appID" value="#session.appID#"/>
          <cfinvokeargument name="AssetTypeID" value="#assetTypeID#"/>
          <cfinvokeargument name="filter" value="#search#"/>
        </cfinvoke>
         
         <cfset cnt = '0'>
         
         <cfif assetTypeID GT '0'>
             <cfoutput>
             <cfif assets.recordCount IS '0'>
               <table width="810" height="85" border="0" cellpadding="6" cellspacing="2" style="margin-top:5px" class="contentLink">
                  <tr>
                    <td>No Assets in this Category Type</td>
                  </tr>
               </table>
               </cfif>
             </cfoutput>
         </cfif>
         
         <cfset cnt = '1'> 
    
          <cfoutput query="assets">
            <cfset assetPath = assetPaths.types[assetTypeID]>
              <cfset dateCre = DateAdd("s", created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
              <cfset dateMod = DateAdd("s", modified ,DateConvert("utc2Local", "January 1 1970 00:00"))>
              <div class="rowhighlighter" onmouseover="showDelete('deleteAsset_#cnt#',1)" onmouseout="showDelete('deleteAsset_#cnt#',0)">
              <form method="post" name="formAsset_#cnt#">
                <table width="810" border="0" cellpadding="0" cellspacing="5">
                  <tr>
                    <td width="44"><img src="images/#icon#" width="44" height="44" alt="#name#" /></td>
                    <td colspan="2">
                    <a href="AssetsView.cfm?assetID=#asset_ID#&amp;assetTypeID=#assetTypeID#" class="contentLink">
                    <div style="height:32px; padding-top:15px; padding-left:4px">#assetName#
                      <input name="assetID_#cnt#" type="hidden" id="assetID_#cnt#" value="#asset_id#" />
                    </div>
                    </a>
                    </td>
                    <td width="80" align="center" class="content">
                    <cfif image NEQ ''>
                    
                    <cfif fileExists(expandPath('#assetPath#thumbs/nonretina/#image#'))><cfset border = 0><cfelse><cfset border = 2></cfif>
                    
                    <cfif fileExists(expandPath('#assetPath#thumbs/#image#'))>
                      <a href="#assetPath#thumbs/#image#" target="_new">
                      <img name="#assetName#" src="#assetPath#thumbs/#image#" width="60" height="44" style="border: solid ##FF0004;border-width: #border#px;" class="imgLockedAspect" />
                      </a>
                    <cfelse>
                    	<span class="contentGreyed">Missing</span>
                    </cfif>
                     
                    <cfelse>
                      <span class="contentGreyed">No Thumb</span>
                    </cfif>
                    </td>
                    <td width="80" align="right" class="content">#dateFormat(dateCre,'MMM DD, YY')#</td>
                    <td width="80" align="right" class="content">#dateFormat(dateMod,'MMM DD, YY')#</td>
                    <td width="60" align="right" class="content">
                    <cfif import>
                    <input name="addAsset_#cnt#" type="button" id="addAsset_#cnt#" class="checkmarkNormal" onClick="addAsset(#cnt#);" />
                    <cfelse>
                    <input name="deleteAsset_#cnt#" type="button" class="itemHide" id="deleteAsset_#cnt#" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteAsset(#assets.asset_id#);" value="" />
                    </cfif>
                    </td>
                  </tr>
                </table>
              </form>
              </div>
              <cfset cnt = cnt + 1>
          </cfoutput>
          
          <cfoutput>
			<script type="text/javascript">
                  var totalItems = "#assets.recordCount#";
            </script>
          </cfoutput>
      
    </cfif>