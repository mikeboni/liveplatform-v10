<cfif NOT IsDefined("session.clientID") OR NOT IsDefined("session.appId")>
No Session variables Defined
<cfelse>

<cfparam name="delete" default="false">

<cfparam name="assetID" default="0">

<cfparam name="assetType" default="0">
<cfparam name="assetName" default="">

<cfparam name="imageFile" default="">
<cfparam name="thumbnailFile" default="">
<cfparam name="useImageAsThumb" default="false">

<cfparam name="Title" default="">
<cfparam name="Subtitle" default="">
<cfparam name="Description" default="">
<cfparam name="Other" default="">

<cfparam name="color" default="">
<cfparam name="bkcolor" default="">

	<!---Current Date--->
	<cfinvoke component="CFC.misc" method="convertDateToEpoch" returnvariable="curDate" />

	<!---Get Path root/client/app/type--->
    <cfinvoke component="CFC.file" method="buildCurrentFileAppPath" returnvariable="destination">
        <cfinvokeargument name="AssetTypeID" value="#assetType#"/>
        <cfinvokeargument name="clientID" value="#session.clientID#"/>
        <cfinvokeargument name="AppID" value="#session.appID#"/>
    </cfinvoke>
    
    <cfset destinationThumb = destination & 'thumbs/'>

	<!---Check if Directory Exists else Create--->
    <cfif not directoryExists(destination)>
    	<cfdirectory action="create" directory="#destination#">
    </cfif>
    
    <!---Check if Thumbs Directory Exists else Create--->
    <cfif not directoryExists(destinationThumb)>
    	<cfdirectory action="create" directory="#destinationThumb#">
    </cfif>
    
     <cfset fileName = ''>
    
	<!---Save Images--->
    <cfif imageFile NEQ ''>
        <cffile action="upload" filefield="imageFile" destination="#destination#" nameconflict="makeunique" result="uploadFile">
        <cfset fileName = uploadFile.serverFile>
    </cfif>
    
    <cfset fileThumbName = "">
    
    <!---Save Thumb--->
    <cfif imageFile NEQ '' OR thumbnailFile NEQ ''>
	
    	<cfset uploadThumbFile = "">
        
		<cfif useImageAsThumb>
            <cffile action="upload" filefield="imageFile" destination="#destinationThumb#" nameconflict="makeunique" result="uploadThumbFile">
            <cfset fileThumbName = uploadThumbFile.serverFile>
        <cfelse>
        	<cfif thumbnailFile NEQ ''>
        		<cffile action="upload" filefield="thumbnailFile" destination="#destinationThumb#" nameconflict="makeunique" result="uploadThumbFile">
            </cfif>
        </cfif>

		<cfif IsStruct(uploadThumbFile)>
            <cfset fileThumbName = uploadThumbFile.serverFile>
        </cfif>

    </cfif>

<cfset created = curDate>
<cfset modified = curDate>

<br />

<cfif assetID GT '0' AND NOT delete>

	<!---Update Existing Asset--->
    <cfquery name="anAsset"> 
        SELECT asset_id, Details.url, Details.thumbnail
        FROM   Assets INNER JOIN 
        Details ON Assets.detail_id = Details.detail_id
        WHERE  Assets.asset_id = #assetID#
    </cfquery>
    
    <cfif fileName NEQ '' AND fileExists(destination & anAsset.url)>
    	<cffile action = "delete" file = "#destination##anAsset.url#">
    </cfif>
    
    <cfif fileThumbName NEQ '' AND fileExists(destinationThumb & anAsset.thumbnail)>
    	<cffile action = "delete" file = "#destinationThumb##anAsset.thumbnail#">
    </cfif>
    
    <cfquery name="updateAsset"> 
         UPDATE Assets
         SET modified = '#modified#',  name = '#assetName#'
         WHERE asset_id = #assetID#
    </cfquery>

	<cfif anAsset.asset_id GT '0'>
        <cfquery name="updateDetail"> 
             UPDATE Details
             SET title = '#title#', subtitle = '#subtitle#', description = '#description#'
             
             <cfif fileName NEQ ''>
             	, url = '#fileName#'
             </cfif>
             <cfif fileThumbName NEQ ''>
             	, thumbnail = '#fileThumbName#' 
             </cfif>
             
             ,color = '#color#', backgroundColor = '#bkColor#', other = '#other#'
             
             WHERE detail_id = #anAsset.asset_id#
        </cfquery>
    </cfif>
    
<cfelseif assetID GT '0' AND delete>

	<!---Update Existing Asset--->
    Delete Asset #assetID#
    
<cfelse>

	<!---New Asset--->
    <cfquery name="newDetail"> 
              INSERT INTO Details (title, subtitle, description, url, thumbnail, color, backgroundColor, other)
              VALUES('#title#', '#subtitle#','#description#','#fileName#','#fileThumbName#','#color#','#bkColor#','#other#')
              SELECT @@IDENTITY AS 'id'
    </cfquery>
    
    <cfquery name="newAsset">           
            INSERT INTO Assets (detail_id, assetType_id, created, modified, app_id, client_id, name) 
             VALUES(#newDetail.id#, #assetType#, '#created#', '#modified#', #session.appID#, #session.clientID#, '#assetName#')
    </cfquery>

</cfif>

<cflocation url="AssetsView.cfm" addtoken="no">

<!---<cfset destination = expandPath("ChannelLogos/")>
    
<cfif not directoryExists(destination)>
	<cfdirectory action="create" directory="#destination#">
</cfif>

<cfif channelIcon NEQ ''>
	<cffile action="upload" filefield="channelIcon" destination="#destination#" nameconflict="overwrite" result="upload">
	<cfset fileName = upload.serverFileName>
<cfelse>
	<cfset fileName = ''>
</cfif>--->

</cfif>