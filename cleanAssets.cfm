<cfset bundleID = "com.liveadz.sample">

<cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appID">
    <cfinvokeargument name="bundleID" value="#bundleID#">
</cfinvoke>

<cfif appID.error.error_code IS 1000>

<cfset appID = appID.app_id>

<!--- Root Path --->
<cfinvoke component="CFC.Apps" method="getClientApps" returnvariable="appPathInfo">
    <cfinvokeargument name="appID" value="#appID#">
</cfinvoke>

<cfset appPath = "#appPathInfo.path#/#appPathInfo.appPath#/assets/">

<cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="assetObject">
    <cfinvokeargument name="assetID" value="127">
</cfinvoke>

<cfloop collection="#assetObject#" item="theKey"></cfloop>

<cfset anObject = assetObject[theKey]>

<cfset allURLs = arrayNew(1)>

<cfloop collection="#anObject#" item="anItem">
	
    <cfset anObjectStruct = anObject[anItem]>
    
    <cfif isStruct(anObjectStruct)>

        <cfif StructKeyExists(anObjectStruct,"url") OR StructKeyExists(anObjectStruct,"mdpi") OR StructKeyExists(anObjectStruct,"xdpi")>
        	<cfif anItem IS 'thumb'>
            
                <cfif StructKeyExists(anObjectStruct,"mdpi")>
                    <cfset arrayAppend(allURLs,anObjectStruct.mdpi.url)>
                </cfif>
                <cfif StructKeyExists(anObjectStruct,"xdpi")>
                    <cfset arrayAppend(allURLs,anObjectStruct.xdpi.url)>
                </cfif>
                
            <cfelse>
            
                <cfif StructKeyExists(anObjectStruct,"xdpi")>
                    <cfset arrayAppend(allURLs,anObjectStruct.mdpi)>
                </cfif>
                <cfif StructKeyExists(anObjectStruct,"xdpi")>
                    <cfset arrayAppend(allURLs,anObjectStruct.xdpi)>
                </cfif>
                
            </cfif>
            
        </cfif>
    
    </cfif>
    
</cfloop>	

In Current DB
<cfdump var="#allURLs#">

<cfset DBfileURLs = arrayNew(1)>

<cfloop array="#allURLs#" index="anItem">
    
    <cfset thePathURL = anItem.split("#appPath#")>
    <cfif trim(ListLast(thePathURL[2],"/")) NEQ ".DS_Store">
        <cfset arrayAppend(DBfileURLs,thePathURL[2])>
    </cfif> 

</cfloop>

<cfdump var="#DBfileURLs#">


<cfinvoke component="CFC.File" method="getAssetPath" returnvariable="assetPath">
    <cfinvokeargument name="assetType" value="8">
</cfinvoke>

<!--- Get All Assets in All Directories --->
<cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="serverPath">
    <cfinvokeargument name="appID" value="#appID#">
</cfinvoke>

<cfset assetFolderPath = "#serverPath#assets/#assetPath#/">

<cfset allFolders = arrayNew(1)>
<cfdirectory action="list" directory="#assetFolderPath#" name="assetDirectory" recurse="true" type="dir" listinfo="all" /> 

<cfoutput query="assetDirectory">
	<cfif name NEQ '_notes'>
		<cfset arrayAppend(allFolders,Directory &"/"& Name)>
    </cfif>
</cfoutput>

<!--- Get All Files in All Directories --->
<cfset allFiles = []>

<cfset appPath = Replace(appPath,"\","/","all")>

<cfloop index="thePath" array="#allFolders#">

	<cfdirectory action="list" directory="#thePath#" name="fileList">
    
    <cfoutput query="fileList">
		<cfif name NEQ '_notes'>
        
        <cfset filePath = Directory &"/"& Name>
		<cfset filePath = Replace(filePath,"\","/","all")>

            <cfset thePathURL = filePath.split("#appPath#")>
            <cfset theFileName = GetFileFromPath(thePathURL[2])>
            <cfdump var="#theFileName#">
            
            <cfif Find(theFileName, ".", 0)>Y<cfelse>N</cfif><br>

            <cfif trim(ListLast(thePathURL[2],"/")) NEQ ".DS_Store">
            	<cfset arrayAppend(allFiles,thePathURL[2])>
            </cfif> 
 
        </cfif>
    </cfoutput>
    
</cfloop>

In Current Folder
<cfdump var="#allFiles#">
<!--- 
<cfloop index="aObjUrl" array="#allURLs#">

	<cfif fileExists(aObjUrl)>

		<cfoutput>#aObjUrl#:#urlRootPath#</cfoutput>-Exists<br>
        
    </cfif>

</cfloop> --->

<!--- Compare --->
<cfset notFound = arrayNew(1)>

<cfloop index="validFile" array="#allFiles#">
	
    <cfif ArrayFind(DBfileURLs,validFile)>
    <cfelse>
    	<cfset arrayAppend(notFound,validFile)>
    </cfif>
    
</cfloop>

To Delete
<cfdump var="#notFound#">

<cfabort>
<!--- <cfinvoke component="CFC.Assets" method="getAsset" returnvariable="assetDB">
    <cfinvokeargument name="assetID" value="127">
</cfinvoke>

<cfdump var="#assetDB#">


<cfinvoke component="CFC.ObjectModels" method="getObjectModel" returnvariable="objectModel">
    <cfinvokeargument name="typeID" value="8">
    <cfinvokeargument name="objectData" value="#assetDB#">
</cfinvoke>

<cfdump var="#objectModel#"> --->

<cfquery name="allAssets">
    SELECT        Assets.asset_id, Assets.assetType_id, Assets.detail_id, Assets.created, Assets.modified, Assets.app_id, Assets.name, Assets.thumb_id, Assets.color_id, 
                  AssetTypes.path
    FROM          Assets INNER JOIN AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id
	WHERE app_id = #appID#
</cfquery>

<cfdump var="#allAssets#">

<cfelse>
	No Such App
</cfif>