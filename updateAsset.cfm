<!--- Define Structs for Data --->
<cfset assetData = structNew()>
<cfset detailData = structNew()>
<cfset thumbData = structNew()>
<cfset colorData = structNew()>

<!---Temp Path for Upload--->
<cfset tempPath = GetTempDirectory()>

<cfparam name="assetPath" default="">
<cfparam name="assetID" default="0">

<cfparam name="assetTypeID" default="0">
<cfparam name="sharable" default="0">

<!--- Asset Details Common --->
<cfparam name="assetName" default="">

<!--- URLAsset --->
<cfparam name="URLLink" default="">

<!--- ImageAsset --->
<cfparam name="imageAsset" default="">
<cfparam name="webURL" default="">

<!--- VideoAsset --->
<cfparam name="VideoAsset" default="">

<!--- MarkerAsset --->
<cfparam name="markerAsset" default="">
<cfparam name="APIKey" default="">
<cfparam name="APISecret" default="">

<cfparam name="ckvisualizer" default="false">
<cfparam name="moodstocks" default="false">
<cfparam name="vuforia" default="false">

<!--- 3DModelAsset --->
<cfparam name="FBXAsset" default="">
<cfparam name="OS" default="0">

<cfparam name="locX" default="0">
<cfparam name="locY" default="0">
<cfparam name="locZ" default="0">

<cfparam name="rotX" default="0">
<cfparam name="rotY" default="0">
<cfparam name="rotZ" default="0">

<cfparam name="scale" default="1">

<cfparam name="CamX" default="0">
<cfparam name="CamY" default="0">
<cfparam name="CamZ" default="0">

<cfparam name="CPan" default="0">
<cfparam name="CZoom" default="0">
<cfparam name="CRotation" default="0">

<cfparam name="TargetX" default="0">
<cfparam name="TargetY" default="0">
<cfparam name="TargetZ" default="0">

<cfparam name="FOV" default="1">
<cfparam name="ar" default="0">

<cfparam name="ar_xml" default="">
<cfparam name="ar_dat" default="">
<cfparam name="ar_img" default="">

<cfif isDefined("FORM.CamX") OR isDefined("FORM.TargetX") OR isDefined("FORM.locX") >
	
	<cfset position = structNew()>
    
    <cfif locX IS ''><cfset locX = '0'></cfif>
    <cfif locY IS ''><cfset locY = '0'></cfif>
    <cfif locZ IS ''><cfset locZ = '0'></cfif>
    
	<cfset structAppend(position,{"x" : locX})>
	<cfset structAppend(position,{"y" : locY})>
    <cfset structAppend(position,{"z" : locZ})>
    
    <cfset rotation = structNew()>
    
    <cfif rotX IS ''><cfset rotX = '0'></cfif>
    <cfif rotY IS ''><cfset rotY = '0'></cfif>
    <cfif rotZ IS ''><cfset rotZ = '0'></cfif>
    
	<cfset structAppend(rotation,{"x" : rotX})>
	<cfset structAppend(rotation,{"y" : rotY})>
    <cfset structAppend(rotation,{"z" : rotZ})>
    
    <cfset camera = structNew()>
    
    <cfif CamX IS ''><cfset CamX = '0'></cfif>
    <cfif CamY IS ''><cfset CamY = '0'></cfif>
    <cfif CamZ IS ''><cfset CamZ = '0'></cfif>
    
	<cfset structAppend(camera,{"x" : CamX})>
	<cfset structAppend(camera,{"y" : CamY})>
    <cfset structAppend(camera,{"z" : CamZ})>
    
    <cfset control = structNew()>
    
	<cfset structAppend(control,{"pan" : CPan})>
	<cfset structAppend(control,{"zoom" : CZoom})>
    <cfset structAppend(control,{"rotation" : CRotation})>
    
    <cfset cameraConst = structNew()>
    
    <cfif CamMin IS ''><cfset CamMin = '0'></cfif>
    <cfif CamMax IS ''><cfset CamMax = '0'></cfif>
    
    <cfif CamTiltMin IS ''><cfset CamTiltMin = '0'></cfif>
    <cfif CamTiltMax IS ''><cfset CamTiltMax = '0'></cfif>
    
    <cfif CamPanMin IS ''><cfset CamPanMin = '0'></cfif>
    <cfif CamPanMax IS ''><cfset CamPanMax = '0'></cfif>
    
	<cfset structAppend(cameraConst,{"min" : CamMin})>
	<cfset structAppend(cameraConst,{"max" : CamMax})>
    
    <cfset structAppend(cameraConst,{"tiltMin" : CamTiltMin})>
	<cfset structAppend(cameraConst,{"tiltMax" : CamTiltMax})>
    
    <cfset structAppend(cameraConst,{"panMin" : CamPanMin})>
	<cfset structAppend(cameraConst,{"panMax" : CamPanMax})>

    
    <cfset target = structNew()>
    
    <cfif TargetX IS ''><cfset TargetX = '0'></cfif>
    <cfif TargetY IS ''><cfset TargetY = '0'></cfif>
    <cfif TargetZ IS ''><cfset TargetZ = '0'></cfif>
    
	<cfset structAppend(target,{"x" : TargetX})>
	<cfset structAppend(target,{"y" : TargetY})>
    <cfset structAppend(target,{"z" : TargetZ})>
    
    <cfif FOV IS ''><cfset FOV = '0'></cfif>
    <cfif scale IS ''><cfset scale = '1'></cfif>
    
    <cfset structAppend(assetData,{"camera" : {"position": camera, "target": target, "fov": FOV, 'limits':cameraConst, 'control':control}, "model" : {"rotation":rotation, "position": position, "scale": scale, "ar": ar} })> 
    
</cfif>

<!--- MarkerAsset --->
<cfif isDefined("FORM.moodstocks")>
	<cfif moodstocks>
		<cfset structAppend(assetData,{"moodstocks" : {"key":#APIKey#, "secret":#APISecret#} })> 
    </cfif>
</cfif>

<cfif isDefined("FORM.ckvisualizer")>
	<cfif ckvisualizer>
    	
        <cfif markerAsset NEQ ''>
            
            <cffile action="upload" filefield="markerAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
            <cfset imageFile = uploadFile.serverFile>
        
            <cfset structAppend(assetData,{"visualizer" : {"url":imageFile} })>
        
        <cfelse>
        
        	<cfset structAppend(assetData,{"visualizer" : {"webURL":webURL} })>
            <cfset webURL = ''>
            
        </cfif>
        
        <cfset structAppend(assetData.visualizer,{"keySet":keySet, "keyData":keyData})>
      
    </cfif>
</cfif>

<cfif isDefined("FORM.vuforia")>
	<cfif vuforia>
	
        <cfif ar_xml NEQ ''>
            <cffile action="upload" filefield="ar_xml" destination="#tempPath#" nameconflict="overwrite" result="uploadFile">
            <cfset xmlFile = uploadFile.serverFile>
            <cfset ar_xml = xmlFile>
        </cfif>
        
        <cfif ar_dat NEQ ''>
            <cffile action="upload" filefield="ar_dat" destination="#tempPath#" nameconflict="overwrite" result="uploadFile">
            <cfset datFile = uploadFile.serverFile>
            <cfset ar_dat = datFile>
        </cfif>
        
        <cfset structAppend(assetData,{"vuforia" : {"xml":#ar_xml#, "dat":#ar_dat#} })> 
        
    </cfif>
</cfif>

<!--- 3D Point --->
<cfparam name="x_pos" default="">
<cfparam name="y_pos" default="">
<cfparam name="z_pos" default="">

<cfif isDefined("FORM.objectRef")>
		<cfset structAppend(assetData,{"objectRef" : #FORM.objectRef #})> 
</cfif>

<cfif isDefined("FORM.objectRef")>
		<cfset structAppend(assetData,{"objectRef" : #FORM.objectRef #})> 
</cfif>

<!--- GPSAsset --->
<cfparam name="gps_long" default="">
<cfparam name="gps_latt" default="">
<cfparam name="gps_radius" default="">
<cfparam name="xpos" default="">
<cfparam name="ypos" default="">
<cfparam name="gps_alt" default="">

<cfif isDefined("FORM.gps_long") OR isDefined("FORM.gps_latt")>
	
    <cfset gps = structNew()>
	<cfset loc = structNew()>
    
    <cfset structAppend(gps,{"long" : 0})>
    <cfset structAppend(gps,{"latt" : 0})>
    <cfset structAppend(gps,{"alt" : 0})>
    
	<cfset structAppend(loc,{"x" : 0})>
    <cfset structAppend(loc,{"y" : 0})>
    
    <cfset structAppend(assetData,{"gps" : {"coords": gps, "position": loc, "radius": 0} })> 
    
</cfif>

<cfif gps_long NEQ "" OR gps_latt NEQ "">
	
    <cfset assetData.gps.coords.long = gps_long>
    <cfset assetData.gps.coords.latt = gps_latt>
    <cfset assetData.gps.coords.alt = gps_alt>
    
    <cfset assetData.gps.position.x = xpos>
    <cfset assetData.gps.position.y = ypos>
    
    <cfset assetData.gps.radius = gps_radius>
     
</cfif>


<!--- Animation Asset --->
<cfif isDefined("FORM.animationSeq")>
	
	<cfset animation = structNew()>
    
    <cfset structAppend(animation,{"modelName" : model3D})>
    <cfset structAppend(animation,{"sequenceName" : animationSeq})>
    
    <cfset structAppend(assetData,{"animation":animation})>
	
</cfif>

<!--- 3D Material --->
<cfif isDefined("FORM.material")>

    <cfset model3DMaterial = structNew()>
    
    <cfset structAppend(model3DMaterial,{"modelName" : model3D})>
    <cfset structAppend(model3DMaterial,{"material" : material})>
    
    <cfset structAppend(assetData,{"shaderMaterial":model3DMaterial})>
    
</cfif>

<!--- DocumentAsset --->
<cfparam name="PDFAsset" default="">

<!--- PanoramaAsset --->
<cfparam name="pano360Asset" default="">

<cfparam name="panoLAsset" default="">
<cfparam name="panoRAsset" default="">
<cfparam name="panoUAsset" default="">
<cfparam name="panoFAsset" default="">
<cfparam name="panoBAsset" default="">
<cfparam name="panoDAsset" default="">

<cfif panoLAsset NEQ "" OR panoRAsset NEQ "" OR panoUAsset NEQ "" OR panoFAsset NEQ "" OR panoBAsset NEQ "" OR panoDAsset NEQ "" OR pano360Asset NEQ "">

	<cfset pano = structNew()>
	
    <!--- Pano Image 360 --->
    <cfif pano360Asset NEQ "">
	
		<cfset structAppend(pano,{"pano360Asset" : pano360Asset})> 

        <cffile action="upload" filefield="pano360Asset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset imagePanoFile = uploadFile.serverFile>

        <!--- Image Size --->
        <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imagePanoSize">
            <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
        </cfinvoke>
         
    </cfif>
    
	<cfset structAppend(pano,{"panoLAsset" : panoLAsset})>
	<cfset structAppend(pano,{"panoRAsset" : panoRAsset})>
	<cfset structAppend(pano,{"panoUAsset" : panoUAsset})>
	<cfset structAppend(pano,{"panoFAsset" : panoFAsset})>
	<cfset structAppend(pano,{"panoBAsset" : panoBAsset})>
	<cfset structAppend(pano,{"panoDAsset" : panoDAsset})>    

    <cfset panoImageFiles = arrayNew(1)>
	
    <cfloop collection="#pano#" item="images"> 
   
		<cfif pano[images] NEQ ''>

            <cffile action="upload" filefield="#images#" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
            <cfset imageFile = uploadFile.serverFile>

            <!--- Image Size --->
            <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
                <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
            </cfinvoke>
         
            <cfswitch expression="#images#">
            	<!--- Back --->
                <cfcase value="panoBAsset">
                	<cfset side = "back">
                </cfcase>
                <!--- Front --->
                <cfcase value="panoFAsset">
                	<cfset side = "front">
                </cfcase>
                <!--- Left --->
                <cfcase value="panoLAsset">
                	<cfset side = "left">
                </cfcase>
                <!--- Right --->
                <cfcase value="panoRAsset">
                	<cfset side = "right">
                </cfcase>
                <!--- Up --->
                <cfcase value="panoUAsset">
                	<cfset side = "up">
                </cfcase>
                <!--- Down --->
                <cfcase value="panoDAsset">
                	<cfset side = "down">
                </cfcase>
                <!--- 360 pano --->
                <cfdefaultcase>
                	<cfset side = "pano">
                </cfdefaultcase>
                
            </cfswitch>
         
            <cfif side IS 'pano'> 
            	<cfset imageFile = {"image" : {"file":imagePanoFile, "size":imagePanoSize , "side":side}}>
            	<cfset arrayAppend(panoImageFiles, structCopy(imageFile))>
            <cfelse>
            	<cfset imageFile = {"image" : {"file":uploadFile.serverFile, "size":imageSize , "side":side}}>
            	<cfset arrayAppend(panoImageFiles,structCopy(imageFile))>
         	</cfif>
            
        </cfif>
       
    </cfloop>

    <cfset structAppend(assetData,{"panorama" : panoImageFiles})>
    
</cfif>
												
<!--- BalconyAsset --->
<cfparam name="BalconyNAsset" default="">
<cfparam name="BalconySAsset" default="">
<cfparam name="BalconyWAsset" default="">
<cfparam name="BalconyEAsset" default="">
<!--- <cfparam name="FloorName" default=""> --->

<cfif BalconyNAsset NEQ "" OR BalconySAsset NEQ "" OR BalconyWAsset NEQ "" OR BalconyEAsset NEQ "">

	<cfset views = structNew()>
    
    <cfset structAppend(views,{"BalconyNAsset" : BalconyNAsset})>
    <cfset structAppend(views,{"BalconySAsset" : BalconySAsset})>
    <cfset structAppend(views,{"BalconyWAsset" :  BalconyWAsset})>
    <cfset structAppend(views,{"BalconyEAsset" : BalconyEAsset})>
    
    <cfset balconyImageFiles = arrayNew(1)>
	
    <cfloop collection="#views#" item="images">
    
		<cfif views[images] NEQ ''>

            <cffile action="upload" filefield="#images#" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
            <cfset imageFile = uploadFile.serverFile>

            <!--- Image Size --->
            <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
                <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
            </cfinvoke>
            
            <cfswitch expression="#images#">
            	<!--- North --->
                <cfcase value="BalconyNAsset">
                	<cfset side = "north">
                </cfcase>
                <!--- South --->
                <cfcase value="BalconySAsset">
                	<cfset side = "south">
                </cfcase>
                <!--- West --->
                <cfcase value="BalconyWAsset">
                	<cfset side = "west">
                </cfcase>
                <!--- East --->
                <cfcase value="BalconyEAsset">
                	<cfset side = "east">
                </cfcase>
            
            </cfswitch>
            
            <cfset imageFile = {"image" : {"file":uploadFile.serverFile, "size":imageSize, "side":side}}>
            <cfset arrayAppend(balconyImageFiles,imageFile)>
         
        </cfif>
       
    </cfloop>

    <cfset structAppend(assetData,{"balcony" : balconyImageFiles})>
    
</cfif>

<cfif isDefined('FloorName')>

	<cfif NOT isDefined('assetData.balcony')>
    	<cfset structAppend(assetData,{"balcony" :[]})>
    </cfif>
    
    <cfset structAppend(assetData,{"floor": FloorName})>
    
</cfif> 

<!--- Thumbnail --->
<cfparam name="useThumb" default="false">
<cfparam name="imageThumb" default="">

<!--- Colors --->
<cfparam name="useColors" default="false">
<cfparam name="contentBackgroundImage" default="">

<!--- Details --->
<cfparam name="useDetails" default="false">
<cfparam name="contentColor" default="">
<cfparam name="contentBGColor" default="">
<cfparam name="title" default="">
<cfparam name="subtitle" default="">
<cfparam name="description" default="">
<cfparam name="titleColor" default="">
<cfparam name="subtitleColor" default="">
<cfparam name="descriptionColor" default="">
<cfparam name="other" default="">

<!--- WebURL --->
<cfif webURL NEQ "">
	<cfset structAppend(assetData,{"webURL" : webURL})>
</cfif>

<!--- Image Master --->
<cfset imageFile = structNew()>

<cfif imageAsset NEQ "">

	<cffile action="upload" filefield="imageAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset imageFile = uploadFile.serverFile>
    
	<!--- Image Size --->
    <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
        <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
    </cfinvoke>
    
	<cfset imageFile = {"file":uploadFile.serverFile, "size":imageSize}>
    
    <cfset structAppend(assetData,{"image" : imageFile})>
    
</cfif>

<!--- Image Thumbnail --->
<cfset imageThumbFile = structNew()>

<cfif imageThumb NEQ "">

	<cffile action="upload" filefield="imageThumb" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">

	<!--- Image Size --->
    <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageThumbSize">
        <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
    </cfinvoke>
    
    <cfset imageThumbFile = {"file":uploadFile.serverFile, "size":imageThumbSize}>
    
<cfelse>
	<cfset imageThumbFile = {"file":""}>
</cfif>

<!--- Colors Background Image --->
<cfset imageBackgroundFile = structNew()>

<cfif contentBackgroundImage NEQ "">

	<cffile action="upload" filefield="contentBackgroundImage" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">

	<!--- Image Size --->
    <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageThumbSize">
        <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
    </cfinvoke>
    
    <cfset imageBackgroundFile = {"file":uploadFile.serverFile, "size":imageThumbSize}>
    
<cfelse>
	<cfset imageBackgroundFile = {"file":""}>
</cfif>


<!---FBX Data--->
<cfif FBXAsset NEQ "">

	<cffile action="upload" filefield="FBXAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset FBXFile = uploadFile.serverFile>
    
    <cfset structAppend(assetData,{"model3D" : FBXFile})>
    <cfset structAppend(assetData,{"os" : OS})>

</cfif>

<!---PDF Data--->
<cfif PDFAsset NEQ "">

	<cffile action="upload" filefield="PDFAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset PDFFile = uploadFile.serverFile>
    
    <cfset structAppend(assetData,{"pdf" : PDFFile})>

</cfif>

<!---URL Data--->
<cfif URLLink NEQ "">
    <cfset structAppend(assetData,{"url" : URLLink})>
</cfif>

<!---Video Data--->
<cfif VideoAsset NEQ "">

	<cffile action="upload" filefield="VideoAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset MPGFile = uploadFile.serverFile>
    
    <cfset structAppend(assetData,{"video" : MPGFile})>

</cfif>

<!--- 3D Model --->
<cfif isDefined("assetData.model.ar")>
	
    <cfset arData = structNew()>
    
    <cfif ar_xml NEQ ''>
    	<cffile action="upload" filefield="ar_xml" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset xmlFile = uploadFile.serverFile>
        <cfset structAppend(arData,{"xml" : xmlFile})>
    </cfif>
    
    <cfif ar_dat NEQ ''>
    	<cffile action="upload" filefield="ar_dat" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset datFile = uploadFile.serverFile>
        <cfset structAppend(arData,{"dat" : datFile})>
    </cfif>
    
    <cfif ar_img NEQ ''>
    	<cffile action="upload" filefield="ar_img" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset imgFile = uploadFile.serverFile>
        <cfset structAppend(arData,{"img" : imgFile})>
    </cfif>
    
	<cfset structAppend(assetData,{"target":arData})>

</cfif>

<!--- AR --->
<cfif isDefined("MarkerAssetID")>
    
    <cfset structAppend(assetData, {"ar":{"markerAssetID":markerAssetID, "markerID":arType}})> 
    
</cfif>


<!--- XYZ Point --->
<cfif isDefined("z_pos")>

	<cfif x_pos IS ''><cfset x_pos = '0'></cfif>
    <cfif y_pos IS ''><cfset y_pos = '0'></cfif>
    <cfif z_pos IS ''><cfset z_pos = '0'></cfif>
    
    <cfset structAppend(assetData, {"x":x_pos, "y":y_pos, "z":z_pos})> 
    
</cfif>


<!--- Quiz Asset --->
<cfif isDefined("questionMessage")>

<cfparam name="needsToBeCompleted" default="0">
<cfparam name="movable" default="0">
<cfparam name="displayCorrect" default="0">
<cfparam name="tryAgain" default="0">
<cfparam name="selectionID" default="0">
<cfparam name="needResponse" default="0">

<cfset quiz = {"needsToBeCompleted": #needsToBeCompleted#, "movable": #movable# , "displayCorrect": #displayCorrect# }>
<cfset question = {"questionMessage": #questionMessage#}>
<cfset gridOptions = {"padding": #gridPadding#, "spacing": #gridSpacing#, "colums": #colums#, "selectionID":#selectionID#}>

<cfif needResponse>
	<cfset structAppend(question,{"needResponse": #needResponse#, "tryAgain":#numberOfTries#, "correctMessage":'#correctMessage#', "incorrectMessage":'#incorrectMessage#'})>
<cfelse>
	<cfset structAppend(question,{"needResponse": 0, "tryAgain":0, "correctMessage":'', "incorrectMessage":''})>
</cfif>

<cfset assetData = {"quiz":#quiz#, "question":#question#, "selection":gridOptions}>

</cfif>

<!--- Content Group --->
<cfif isDefined("behaviourType")>

	<cfif behaviourType NEQ 1>
		<cfset instanceNameAsset = ''>
        <cfset useAssetID = '0'>
    </cfif><!--- Not 3D --->
    <cfset structAppend(assetData, {"behaviourType":behaviourType, "instanceName":instanceNameAsset, 'useAssetID':useAssetID})> 
    
</cfif>

<!--- Colors --->
<cfif useColors>
    <cfset colorData = {"forecolor": '#contentColor#', "backcolor": '#contentBGColor#', "othercolor": '#contentOtherColor#' , "background": '#imageBackgroundFile#' }>
</cfif>

<!--- Colors --->
<cfif isDefined('selectionColor')>

	<cfif selectionColor NEQ '' OR selectionBGColor NEQ '' OR selectionBKImg NEQ ''>
        <cfset colorSelectionData = {"forecolor": '#selectionColor#', "backcolor": '#selectionBGColor#', "othercolor": '#contentOtherColor#' , "background": '#imageBackgroundFile#' }>
    <cfelse>
    	<cfset colorSelectionData = {"forecolor": '', "backcolor": '', "othercolor": '' , "background": '' }>
    </cfif>
    
    <cfset structAppend(assetData.selection,{'colors':colorSelectionData})>
    
</cfif>

<!--- Game --->
<cfif isDefined("gStartDate") OR isDefined("gEndDate")>

	<cfparam name="sendPlayerEmail" default="false">
    <cfparam name="sendClientEmail" default="false">
    
    <cfparam name="includeCode" default="false">
    <cfparam name="includeContent" default="false">
    
    <cfparam name="clientEmailAddress" default="">
    
    <cfparam name="silentMode" default="false">
	
	<cfset gameData = {"startDate": '#gStartDate#', "endDate": '#gEndDate#', "silentMode":#silentMode# }>
    
    <cfset messages = {"start":'#gameInitalMessage#', "end":'#gameCompletionMessage#'}>
    <cfset structAppend(gameData,{"message":messages})>
    
    <cfset structAppend(gameData,{"useCode":#includeCode#, "sendTargets":#includeContent#})>
    
    <cfset email = {"sendPlayer":#sendPlayerEmail#, "sendClient":#sendClientEmail#}>

    <cfif clientEmailAddress NEQ ''>
        <cfset structAppend(email,{"email":'#clientEmailAddress#'})>
    <cfelse>
    	<cfset email.sendClient = false>
    </cfif>
    
    <cfif sendPlayerEmail>
    	<cfset structAppend(email,{"emailMessage":'#EmailMessage#'})>
    <cfelse>
    	<cfset structAppend(email,{"emailMessage":''})>
    </cfif>
    
    <cfset structAppend(gameData,{"email":email})>
    
    <cfset structAppend(assetData,gameData)>
</cfif>

<!--- Construct Data --->
<cfset structAppend(assetData,{"assetID":assetID})><!--- , "path":#assetPath# --->

<cfif useDetails>
	<cfset detailData = {"title": #title#, "subtitle": #subtitle#, "description": #description#, "other": #other#, "titleColor": #titleColor#, "subtitleColor": #subtitleColor#, "descriptionColor": #descriptionColor#}>
</cfif>

<cfif useThumb>
	<cfset thumbData = {"thumbnail": #imageThumb#, "thumb":#imageThumbFile#}>
</cfif>

<cfset structAppend(assetData,{"name" : #assetName# , "sharable" : #sharable#})>

<!--- Mapper Asset Map Object --->
<cfif isDefined("mapAssetID")>
	<cfset structAppend(assetData,{"mapAssetID" : #mapAssetID#})>
</cfif>

<!--- Update Asset Details and Thumbs --->
<cfif assetID IS '0' AND assetTypeID GT '0'>
	
	<!--- Build Asset Path --->
	<cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
		<cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
		<cfinvokeargument name="appID" value="#session.appID#"/>
	</cfinvoke>

	<cfset assetData.path = assetPath>
	<cfset structAppend(assetData,{"assetTypeID" : #assetTypeID#})>
	<cfset structAppend(assetData,{"appID" : #session.appID#})>

</cfif>


<!--- Update Asset --->
<cfinvoke component="CFC.Assets" method="updateAssetDetails" returnvariable="assetTypeID">
	<cfinvokeargument name="assetData" value="#assetData#"/>
	<cfinvokeargument name="detailData" value="#detailData#"/>
	<cfinvokeargument name="thumbData" value="#thumbData#"/>
    <cfinvokeargument name="colorData" value="#colorData#"/>
</cfinvoke>


<!--- <cflocation url="AppsView.cfm?assetTypeTab=3&assetTypeID=#assetTypeID#&error=Updated Asset Details" addtoken="no">  --->
<cflocation url="AssetsView.cfm?assetID=#assetID#&assetTypeID=#assetTypeID#&error=Updated Asset Details" addtoken="no"> 

    

