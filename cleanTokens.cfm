<cfparam name="clientID" default="0">
<cfparam name="appID" default="0">

<cfif clientID GT '0' AND appID GT '0'>

	<!--- Clean User Tokens --->
    <cfinvoke component="CFC.Tokens" method="cleanUpUserTokens" returnvariable="result">
        <cfinvokeargument name="clientID" value="2"/>
        <cfinvokeargument name="appID" value="4"/>
    </cfinvoke>
    
    
    <!--- Reset All Guest Tokens --->
     <cfinvoke component="CFC.Tokens" method="resetGuestTokens" returnvariable="result">
        <cfinvokeargument name="appID" value="4"/>
    </cfinvoke>
    
</cfif>

Database Cleaned