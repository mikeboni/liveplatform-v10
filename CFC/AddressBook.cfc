<cfcomponent>

	<cffunction name="importAddressBook" access="public" returntype="struct">
    	<cfargument	name="auth_token" type="string" required="true" />
    	<cfargument	name="csvFile" type="string" required="true" />
        <cfargument	name="appendType" type="numeric" required="no" default="0" /><!--- Append 0, Replace 1, Update 0 --->
    
    	<!--- Get Token User Info - AppID and UserID --->

        <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        <!--- Read and Parse CSV File --->
		<cfset tempDir = GetTempDirectory()>
        
        <cffile action="upload" fileField="csvFile" destination="#tempDir#" nameconflict="makeunique" result="newFile">
        
        <cfset csvFileLoc = newFile.serverDirectory &'/'& newFile.serverFile>
        
        <cffile action = "read" file = "#csvFileLoc#" variable = "csvData">
        
        <cfif newFile.clientFileExt IS 'vcf'>
        
			<!--- Parse VCARD Parsing Create Query --->
            <cfinvoke component="AddressBook" method="vCardParser" returnvariable="addressBook">
                <cfinvokeargument name="vCardData" value="#csvData#"/>
            </cfinvoke>
        	<!---  --->
            
		<cfelse>
        
			<!--- CSV Parsing Create Query --->
            <cfset csvData = Replace( csvData, Chr(9), ',', 'all' )>
            <cfset csvData = Replace( csvData, '"', ',', 'all' )>
    
            <cfinvoke component="AddressBook" method="getLineOfString" returnvariable="theData">
                <cfinvokeargument name="data" value="#csvData#"/>
                <cfinvokeargument name="line" value="1"/>
            </cfinvoke>
          
            <cfset theData = LCase(theData)>
    
            <cfset pos = ListContainsNoCase(theData,'email',',')>
            <cfif pos GT 0>
                <cfset theData = ListSetAt(theData,pos,'email',',')>
            </cfif>
            
            <!--- <cfset pos1 = ListContainsNoCase(theData,'e-mail 1 - value',',')> --->
            <cfset pos = ListContainsNoCase(theData,'e-mail',',')>
            
            <cfif pos GT 0>
                <cfset theData = ListSetAt(theData,pos,'email',',')>
            </cfif>
            
            <!--- <cfelseif pos2 GT 0>
                <cfset theData = ListSetAt(theData,pos2,'email',',')> 
     --->        
            
            <cfset pos = ListContainsNoCase(theData,'EmailAddress',',')>
            <cfif pos GT 0>
                <cfset theData = ListSetAt(theData,pos,'email',',')>
            </cfif>
            
            <cfset fn = ListContainsNoCase(theData,'first name',',')>
            <cfset ln = ListContainsNoCase(theData,'last name',',')>
            
            <cfif fn GT 0 AND ln GT 0>
                <cfset needToCombineName = true>
            <cfelse>
                <cfset needToCombineName = false>
            </cfif>
            
            <cfset fn = ListContainsNoCase(theData,'givenname',',')>
            <cfset ln = ListContainsNoCase(theData,'familyname',',')>
            
            <cfif fn GT 0 AND ln GT 0>
                <cfset needToCombineNameGiven = true>
            <cfelse>
                <cfset needToCombineNameGiven = false>
            </cfif>
       
            <!--- Replace DB Colums --->
            <cfinvoke component="AddressBook" method="setLineOfString" returnvariable="newData">
              <cfinvokeargument name="data" value="#csvData#"/>
              <cfinvokeargument name="line" value="1"/>
              <cfinvokeargument name="replaceString" value="#theData#"/>
            </cfinvoke>
      
            <!--- Convert to Query --->
            <cfinvoke component="CSV" method="CSVToQuery" returnvariable="addressBookImport">
                <cfinvokeargument name="CSV" value="#newData#"/>
                <cfinvokeargument name="Delimiter" value=","/>
            </cfinvoke>
      
            <cfquery dbtype="query" name="addressBook"> 
                SELECT	Email,
                <cfif needToCombineName>
                    firstName + ' ' + lastName AS Name
                <cfelseif needToCombineNameGiven>
                    givenname + ' ' + familyname AS Name
                <cfelse>
                    Name
                </cfif>
                FROM	addressBookImport
            </cfquery>
		
        </cfif>
        
        <cffile action="delete" file="#csvFileLoc#">
		
        <cfset successImport = {"success":[],"failed":[]}>
        
        <!--- Add Addresses to User Address Book --->
        <cfoutput query="addressBook">
    
            <cfinvoke component="AddressBook" method="addEntryToAddressBook" returnvariable="success">
                <cfinvokeargument name="name" value="#Name#"/>
                <cfinvokeargument name="email" value="#email#"/>
                <cfinvokeargument name="appID" value="#userInfo.appID#"/>
                <cfinvokeargument name="userID" value="#userInfo.userID#"/>
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfif success>
                <cfset arrayAppend(successImport.success,{"name":#name#,"email":#email#})>
            <cfelse>
                <cfset arrayAppend(successImport.failed,{"name":#name#,"email":#email#})>
            </cfif>

        </cfoutput>
        
        <cfreturn successImport>
        
	</cffunction>
    
    
    <cffunction name="addEntryToAddressBook" access="public" returntype="boolean">
    	<cfargument	name="name" type="string" required="no" default="" />
        <cfargument	name="email" type="string" required="no" default="" />
        
        <cfargument	name="appID" type="numeric" required="no" default="0" />
        <cfargument	name="userID" type="numeric" required="no" default="0" />
        
        <cfargument	name="auth_token" type="string" required="no" default="" />
 
        
        <cfif auth_token NEQ ''>
        
			<!--- Get Token User Info - AppID and UserID --->
    
            <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfset appID = userInfo.appID>
            <cfset userID = userInfo.userID>
        
        </cfif>
        
        <cfif appID GT 0 AND userID GT 0>
        
			<cfset entries = structNew()>
            <cfset failedEntries = arrayNew(1)>
            <cfset successEntries = arrayNew(1)>
            
            <cfif email NEQ ''>
            
            	<!--- If No Name use front of email address --->
    			<cfif name IS ''>
                	<cfset name = listGetAt(email,1,"@")>
                </cfif>
                
                <!--- Check if EMail Exists --->
                <cfquery name="foundAddress"> 
                    SELECT	book_id
                    FROM	UserAddressBook
                    WHERE	email = '#trim(email)#' AND app_id = #appID# AND user_id = #userID#
                </cfquery>
                
                <cfset found = false>
                
                <cfif foundAddress.recordCount GT 0>
                    <cfset found = true>
                </cfif>
                
                <cfif found>
                    <!--- Update if email found --->
                    <cfquery name="updateAsset">
                        UPDATE UserAddressBook
                        SET name = '#trim(name)#', email = '#trim(email)#', app_id = #appID#, user_id = #userID#
                        WHERE  book_id = #foundAddress.book_id#
                    </cfquery>
    
                <cfelse>
                    <!--- else Add new entry --->
                    <cfquery name="newAsset"> 
                        INSERT INTO UserAddressBook (name , email, app_id, user_id)
                        VALUES ('#trim(name)#', '#trim(email)#', #appID#, #userID#) 
                    </cfquery>
                
                </cfif>
                
                <cfreturn true>
                
            <cfelse>
            
                <cfreturn false>
                
            </cfif>
        
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>  



 	<!--- Remove Address Book Entry --->
     <cffunction name="removeAddressBookEntry" access="public" returntype="boolean">
        <cfargument	name="addressBookID" type="numeric" required="no" default="0" />
        <cfargument	name="auth_token" type="string" required="no" default="" />
		
        <cfif auth_token NEQ ''>
        
			<!--- Get Token User Info - AppID and UserID --->
    
            <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfif userInfo.appID GT 0 AND userInfo.userID GT 0>
				<cfset authVerified = true>
            <cfelse>
            	<cfset authVerified = false>
            </cfif>
            
        <cfelse>
        	<cfreturn false>
        </cfif>
        
        <cfif authVerified>
        
			<!--- Check if Entry Exists --->
            <cfquery name="foundAddress"> 
                SELECT	book_id
                FROM	UserAddressBook
                WHERE  book_id = #addressBookID#
            </cfquery>
            
            <cfif foundAddress.recordCount GT 0>
            
                <!--- Update if email found --->
                <cfquery name="updateAsset">
                    DELETE FROM UserAddressBook
                    WHERE  book_id = #addressBookID#
                </cfquery>
                
                <cfreturn true>
            
            <cfelse>
                <cfreturn false>
            </cfif>
       
       <cfelse>
       		<cfreturn false>
       </cfif>
        
    </cffunction> 
       
       
       
    <!--- Update Address Book Entry --->
    <cffunction name="UpdateAddressBookEntry" access="public" returntype="boolean">
    	<cfargument	name="name" type="string" required="no" default="" />
        <cfargument	name="email" type="string" required="no" default="" />
        
        <cfargument	name="addressBookID" type="numeric" required="no" default="0" />
		
        <!--- Check if Entry Exists --->
        <cfquery name="foundAddress"> 
            SELECT	book_id
            FROM	UserAddressBook
            WHERE  book_id = #addressBookID#
        </cfquery>
        
        <cfif foundAddress.recordCount GT 0>
        
			<!--- Update if email found --->
            <cfquery name="updateAsset">
                UPDATE UserAddressBook
                SET name = '#trim(name)#', email = '#trim(email)#'
                WHERE  book_id = #addressBookID#
            </cfquery>
            
            <cfreturn true>
 		
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>  
    
    
    <!--- Get Address Book --->
    <cffunction name="getAddressBook" access="public" returntype="array">
    	<cfargument	name="auth_token" type="string" required="true" />
    	
        <!--- Get Token User Info - AppID and UserID --->

        <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        <cfset data = arrayNew(1)>
        
        <cfquery name="addressBook"> 
            SELECT	book_id,Name, Email      
            FROM	UserAddressBook
            WHERE	app_id = #userInfo.appID# AND user_id = #userInfo.userID#
            ORDER BY Name ASC
        </cfquery>
        
        <!--- Check if No addresses and add addresses from carts --->
		<cfif addressBook.recordCount IS 0>
        
            <!--- Add addresses from carts --->
            <cfinvoke component="AddressBook" method="getAddressesFromCarts" returnvariable="addressBookCart">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
          
            <cfif arrayLen(addressBookCart.success) IS 0>
            	<cfreturn data>
            </cfif>
            
            <!--- Get Address Book Again--->
            <cfinvoke component="AddressBook" method="getAddressBook" returnvariable="addressBook">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>

            <cfreturn addressBook>
        
        </cfif>
        
        <!--- Convert to struct --->
        <cfinvoke component="Misc" method="QueryToStruct" returnvariable="data">
            <cfinvokeargument name="query" value="#addressBook#"/>
            <cfinvokeargument name="forceArray" value="true"/>
        </cfinvoke>
 
        <cfreturn data>
        
    </cffunction>
    
    
    
    <!--- Get Address from Session Carts --->
    <cffunction name="getAddressesFromCarts" access="public" returntype="struct">
    	<cfargument	name="auth_token" type="string" required="true" />
    	
        <!--- Get Token User Info - AppID and UserID --->

        <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        <cfquery name="cartAddresses"> 
            SELECT distinct(email)
            FROM SessionCart
            WHERE user_id = #userInfo.userID# AND app_id = #userInfo.appID# AND email <> ''
            ORDER BY email
        </cfquery>
   
        <!--- Convert to struct --->
        <cfinvoke component="Misc" method="QueryToStruct" returnvariable="data">
            <cfinvokeargument name="query" value="#cartAddresses#"/>
            <cfinvokeargument name="forceArray" value="true"/>
        </cfinvoke>

        <!--- Define EMail Success --->
        <cfset successImport = {"success":[],"failed":[]}>
        
 		<cfloop index="z" from="1" to="#arrayLen(data)#">
        
        	<cfset anAddress = data[z].email>
            
        	<!--- Add New Address --->
        	<cfinvoke component="AddressBook" method="addEntryToAddressBook" returnvariable="success">
                <cfinvokeargument name="email" value="#anAddress#"/>
                <cfinvokeargument name="appID" value="#userInfo.appID#"/>
                <cfinvokeargument name="userID" value="#userInfo.userID#"/>
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
        
			<cfif success>
                <cfset arrayAppend(successImport.success,anAddress)>
            <cfelse>
                <cfset arrayAppend(successImport.failed,anAddress)>
            </cfif>
        
        </cfloop>
        
        <cfreturn successImport>
        
    </cffunction>

    
    
    
    <!--- Line Get --->
    <cffunction name="getLineOfString" access="public" returntype="string">
    	<cfargument	name="data" type="string" required="true" />
        <cfargument	name="line" type="numeric" required="no" default="0" />
        
        <!--- Split into Lines --->
        <cfinvoke component="Misc" method="reSplit" returnvariable="result">
            <cfinvokeargument name="regex" value="\r\n?|\n"/>
            <cfinvokeargument name="value" value="#data#"/>
        </cfinvoke>
        
        <cfif line LTE 0>
			<cfset line = 1>
        </cfif>
        
        <cfif line GT arrayLen(result)>
        	<cfset line = arrayLen(result)>
        </cfif>
        
        <cfset theData = result[line]>
        
        <cfreturn theData>
        
    </cffunction>
    
    <!--- Line Replace --->
    <cffunction name="setLineOfString" access="public" returntype="string">
    	<cfargument	name="data" type="string" required="true" />
        <cfargument	name="line" type="numeric" required="no" default="0" />
        <cfargument	name="replaceString" type="string" required="no" default="" />
        
        <!--- Split into Lines --->
        <cfinvoke component="Misc" method="reSplit" returnvariable="result">
            <cfinvokeargument name="regex" value="\r\n?|\n"/>
            <cfinvokeargument name="value" value="#data#"/>
        </cfinvoke>
        
        <cfif line LTE 0>
			<cfset line = 1>
        </cfif>
        
        <cfif line GT arrayLen(result)>
        	<cfset line = arrayLen(result)>
        </cfif>
        
        <cfset result[line] = replaceString>
        
        <cfset theString = "">
        <cfset lineBreak = Chr(13) & Chr(10)>
        
        <cfloop index="z" from="1" to="#arrayLen(result)#">
        
        	<cfset theString = theString & result[z] & lineBreak>
        
        </cfloop>
       
        <cfreturn theString>
        
    </cffunction>
    
    
    <!--- Line Get --->
    <cffunction name="vCardParser" access="public" returntype="query">
    	<cfargument	name="vCardData" type="string" required="true" />
        <cfargument	name="line" type="numeric" required="no" default="0" />
        
        <!--- Split into Lines --->
        <cfinvoke component="Misc" method="reSplit" returnvariable="result">
            <cfinvokeargument name="regex" value="\r\n?|\n"/>
            <cfinvokeargument name="value" value="#vCardData#"/>
        </cfinvoke>
        
        <cfset allCards = arrayNew(1)>
        
        <cfloop index="z" from="1" to="#arrayLen(result)#">
        	
            <cfset theData = result[z]>
			<cfset theObj = listGetAt(theData,1,':')>
            
            <cfif listLen(theData,":") GT 1>
            	<cfset theVal = Replace(theData, ';', '', 'ALL')> 
            	<cfset theVal = listGetAt(theData,2,':')>
            <cfelse>
            	<cfset theVal = ''>
            </cfif>
            
            <cfif theObj IS "BEGIN">
                <cfset aCard = structNew()>
            </cfif>
    
            <cfif theObj IS "FN">
                <cfset structAppend(aCard,{"name":theVal})>
            </cfif>
            
            <cfif theObj IS "EMAIL;TYPE=INTERNET">
                <cfset structAppend(aCard,{"email":theVal})>
            </cfif>
            
            <cfif theObj IS "END">
                <cfset arrayAppend(allCards,aCard)>
            </cfif>
        
        </cfloop>
        
        <cfset vCardQuery = QueryNew("Name, Email", "VarChar, VarChar")>
        <cfset QueryAddRow(vCardQuery, arrayLen(allCards))>
        
        <cfloop index="z" from="1" to="#arrayLen(allCards)#">
        	<cfset aRec = allCards[z]>
        	<cfset QuerySetCell(vCardQuery, 'Name',#aRec.name#,z)>
        	<cfset QuerySetCell(vCardQuery, 'Email',#aRec.email#,z)>
        </cfloop>
        
        <cfreturn vCardQuery>
        
    </cffunction>
    
    
    
    
</cfcomponent>