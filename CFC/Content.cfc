<cfcomponent>

	<!--- Assets --->
	<cffunction name="getAssetContent" access="remote" returntype="struct" output="yes">
        <cfargument name="assetID" type="numeric" required="no" default="0">
		<cfargument name="active" type="boolean" required="no" default="yes">
		
        <!--- Get Asset Table --->
        <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <!--- Get Asset Data --->
        <cfinvoke  component="Content" method="getAsset" returnvariable="assetData">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
  																																		 
        <!--- QueryToStruct --->
         <cfinvoke component="Misc" method="QueryToStruct" returnvariable="objectData">
            <cfinvokeargument name="query" value="#assetData#"/>
         </cfinvoke>
      
		<!--- Get Asset Model --->
        <cfinvoke component="ObjectModels" method="getObjectModel" returnvariable="assetModel">
           <cfinvokeargument name="typeName" value="#assetTable#"/>
           <cfinvokeargument name="objectData" value="#objectData#"/>
        </cfinvoke>
        
        <cfreturn assetModel>
        
    </cffunction>
    
    
    
    <!--- Get Assets Sharable, Modified, Access and cached --->
	<cffunction name="getAssetOptions" access="remote" returntype="struct" output="yes">
        <cfargument name="assetID" type="numeric" required="no" default="0">
		<cfargument name="groupID" type="numeric" required="no" default="0">
		
		<cfquery name="assetOptions"> 
            SELECT	accessLevel AS access, sharable, cached, modified, active
            FROM	Groups
            WHERE	0 = 0
			<cfif assetID GT 0>
            
                AND asset_id = #assetID#
                
                <cfif groupID GT 0>
                        AND subgroup_id = #groupID#
                </cfif>
            <cfelse>
            	   AND group_id = #groupID#
            </cfif>
        </cfquery>
 
        <!--- QueryToStruct --->
         <cfinvoke component="Misc" method="QueryToStruct" returnvariable="assetOptionsModel">
            <cfinvokeargument name="query" value="#assetOptions#"/>
         </cfinvoke>

         <cfif isStruct(assetOptionsModel)>
         
         <cfif assetOptionsModel.access LTE 0>
         	<cfset structDelete(assetOptionsModel,'access')>
         </cfif>
         
         <cfif assetOptionsModel.sharable LTE 0>
         	<cfset structDelete(assetOptionsModel,'sharable')>
         </cfif>
         
         <cfif assetOptionsModel.cached LTE 0>
         	<cfset structDelete(assetOptionsModel,'cached')>
         </cfif>
         
		 <!--- if asset not active then NO options or anything --->
         <cfif assetOptionsModel.active LTE 0>
         	<cfset assetOptionsModel = {}>
         </cfif>
         
         <cfelse>
         	<cfset assetOptionsModel = {}>
         </cfif>
  
        <cfreturn assetOptionsModel>
        
    </cffunction>
    


	<!---Get A Asset By Type--->
    <cffunction name="getAsset" access="public" returntype="query" output="yes">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfset anAsset = queryNew("name, modified, assetModified","VarChar, BigInt, BigInt")>
        
        <cfif assetID GT '0'>
        
        	<!--- Get Asset Table --->
            <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
           
			<!--- Get All Data from Asset Table--->
            <cfquery name="anAsset"> 
                SELECT	#assetTable#.*, Assets.name, Assets.modified AS assetModified
                FROM	#assetTable# LEFT OUTER JOIN Assets ON #assetTable#.asset_id = Assets.asset_id
                <cfif AssetID GT'0'>
                WHERE 	#assetTable#.asset_id = #AssetID#
                </cfif>
            </cfquery>
            
        </cfif>
        
        <cfreturn anAsset>
        
    </cffunction>



	<!--- Group Assets --->
	<cffunction name="getGroupContent" access="remote" returntype="array">
		<cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="active" type="boolean" required="no" default="yes">
        
		<cfquery name="assets">
            SELECT   asset_id, accessLevel, cached, sharable, group_id, modified, name
            FROM     Groups
            WHERE    (subgroup_id = #groupID#) <cfif appID GT 0>AND app_id = #appID#</cfif>
            
            <cfif active>AND active = 1</cfif>            
            ORDER BY sortOrder
        </cfquery>
        
        <!--- Get Root Group Active --->
        <cfquery name="assetsRoot">
            SELECT   active
            FROM     Groups
            WHERE    (group_id = #groupID#) <cfif appID GT 0>AND app_id = #appID#</cfif>
            ORDER BY sortOrder
        </cfquery>
        
        <cfif assetsRoot.recordCount GT 0>
        	<cfset activeRoot = assetsRoot.active>
		<cfelse>
        	<cfset activeRoot = true>
        </cfif>
        
        <cfset theData = arrayNew(1)>
        
        <cfif activeRoot>
        
        <cfoutput query="assets">
         
			<!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="groupID" value="#group_id#">
            </cfinvoke>
        
            <cfset theObj = {"assets":[], "modified":modified, "groupID":group_id, "assetID":asset_id}>
            <cfset structAppend(theObj,assetData)>
            
            <cfif sharable IS 1>
            	<cfset structAppend(theObj,{"sharable":sharable})>
            </cfif>
            
            <cfif accessLevel GT 0>
            	<cfset structAppend(theObj,{"access":accessLevel})>
            </cfif>
            
            <cfif cached IS 1>
            	<cfset structAppend(theObj,{"cached":cached})>
            </cfif>
            
            <cfset theObjModel = {"#name#":theObj}>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
            <!--- Check if a Code Exists --->
        	<cfinvoke  component="Access" method="getGroupAccessCodes" returnvariable="accessCodes">
                <cfinvokeargument name="groupID" value="#group_id#"/>
            </cfinvoke>
            
            <cfif accessCodes.recordCount GT 0>
            	<cfset structAppend(theObj,{"code":1})>
            </cfif>
            
            <cfset arrayAppend(theData,theObjModel)>
            
         </cfoutput>
		
        </cfif>

        <cfreturn theData>
        
    </cffunction>



	<!--- Group Info --->
	<cffunction name="getGroupInfo" access="remote" returntype="struct">
		<cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="yes">
		<cfargument name="active" type="boolean" required="no" default="yes">
        
        <!--- Get Group Assets --->
        <cfquery name="group">
            SELECT   asset_id, accessLevel, cached, sharable, modified, name
            FROM     Groups
            WHERE    (group_id = #groupID#) 
            
			<cfif active>
            	AND active = 1
            <cfelse>
           		AND active = 0
            </cfif>
            
            <cfif appID GT 0>AND app_id = #appID#</cfif>
        </cfquery>
		
        <cfif group.recordCount IS 0>
        	<cfreturn {}>
        </cfif>

		<!--- Thumbs, Details and Colors --->
        <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
            <cfinvokeargument name="groupID" value="#groupID#">
        </cfinvoke>
      
        <cfset theObj = {"assets":[], "modified":group.modified, "groupID":groupID, "access":group.accessLevel, "sharable":group.sharable, "cached":group.cached}>
        
        <cfset structAppend(theObj,assetData)>
        
        <!--- Sharable, Access and Cached --->
        <cfif theObj.sharable IS 0>
        	<cfset structDelete(theObj,'sharable')>
        </cfif>
        <cfif theObj.cached IS 0>
        	<cfset structDelete(theObj,'cached')>
        </cfif>
        <cfif theObj.access IS 0>
        	<cfset structDelete(theObj,'access')>
        </cfif>
        
        <!--- <cfset theObjModel = {"#group.name#":theObj}> --->
        <cfset theObjModel = theObj>
        
        <!--- Clean --->
        <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
            <cfinvokeargument name="theStruct" value="#theObjModel#"/>
        </cfinvoke>

        <cfreturn theObjModel>
        
    </cffunction>




	<!--- Assets and Actions --->
	<cffunction name="getAssetsActions" access="remote" returntype="struct">
		<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="active" type="boolean" required="no" default="yes">
		
        <cfset assets_actions = {"actions":[],"assets":[]}>
        
        <cfif assetID GT 0>
        
			<!--- Get Group Assets --->
            <cfquery name="assets">
                SELECT   content_id AS Asset_id, accessLevel, cached, sharable, sortOrder, action, quiz
                FROM     GroupAssets
                WHERE    (asset_id = #assetID#) 
                <cfif active> AND active = 1<cfelse>AND active = 0</cfif>
                ORDER BY sortOrder
            </cfquery>
    
            <!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theData">
                <cfinvokeargument name="query" value="#assets#"/>
                <cfinvokeargument name="forceArray" value="true"/>
             </cfinvoke>
    
            <cfloop index="aRec" array="#theData#">
            
                <cfif aRec.action>
                    <cfset arrayAppend(assets_actions.actions,aRec)>
                <cfelseif aRec.action IS 0 AND aRec.quiz IS 0>
                    <cfset arrayAppend(assets_actions.assets,aRec)>
                </cfif>
            
            </cfloop>
        
        </cfif>

        <cfreturn assets_actions>
        
    </cffunction>
    
    
    
<!--- Get All Content Groups --->
	<cffunction name="getGroups" access="remote" returntype="array">
		<cfargument name="appID" type="numeric" required="yes">
	
		<!--- Get Group Assets --->
        <cfquery name="groups">
            SELECT DISTINCT group_id, subgroup_id, active
  			FROM Groups
  			WHERE app_id = #appID# AND asset_id IS NULL
        </cfquery>
		
        <cfset theData = arrayNew(1)>
        <cfset removeData = arrayNew(1)>
		
        <cfoutput query="groups">
        
            <cfinvoke  component="Content" method="getOriginGroupState" returnvariable="originData">
                <cfinvokeargument name="groupID" value="#group_id#"/>
            </cfinvoke>
      
        	<cfif originData.active IS 0 OR active IS 0>
				<!--- nothing --->
            <cfelse>
            
				<cfif arrayFind(removeData,subgroup_id) IS 0>
                    <cfset arrayAppend(theData,group_id)>
                </cfif>
            
            </cfif>
            
		</cfoutput>

        <cfreturn theData>
        
    </cffunction>
    
    
     <!--- Get Origin Group info --->
	<cffunction name="getOriginGroupState" access="remote" returntype="struct">
		<cfargument name="groupID" type="numeric" required="yes">
        	
            <cfset data = {}>
            
			<!--- Get Group Assets --->
            <cfquery name="group">
                SELECT subgroup_id, active, sharable, cached
                FROM Groups
                WHERE group_id = #groupID#
            </cfquery>
            
            <cfset subgroupID = group.subgroup_id>
  			
            <cfif subgroupID GT 0>
                
                <cfinvoke  component="Content" method="getOriginGroupState" returnvariable="data">
                    <cfinvokeargument name="groupID" value="#subgroupID#"/>
                </cfinvoke>
                
            <cfelse>
            
				<cfset data = {"active":group.active,"sharable":group.sharable,"cached":group.cached}>
			
            </cfif>
            
            <cfreturn data>
            
     </cffunction>
    
    
    <!--- Get All Content Groups --->
	<cffunction name="getJSONPath" access="remote" returntype="string">
		<cfargument name="appID" type="numeric" required="yes">
            
		<!--- Build JSON Client/App Path --->
        <cfquery name="appPath">
            SELECT       Clients.path + '/' + Applications.path + '/' AS filePath
            FROM         Clients INNER JOIN Applications ON Clients.client_id = Applications.client_id 
            WHERE        Applications.app_id = #appID#
        </cfquery> 
        
        <cfset path = appPath.filePath>

        <!--- Server API Version --->
        <cfinvoke  component="Apps" method="getAppPrefs" returnvariable="prefs">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfset apiVr = prefs.server_API>
        
        <cfif apiVr GTE 10>
            <cfset jsonPath = path & "JSON_" & apiVr &"/">
        <cfelse>
            <cfset jsonPath = path & "JSON/">
        </cfif>
        
        <cfreturn jsonPath>
        
	</cffunction>
        
</cfcomponent>