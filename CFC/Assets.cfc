<cfcomponent>

<!--- Set Video Controller Options --->
 	<cffunction name="setVideoControllerOptions" access="remote" returntype="boolean" output="no">
        <cfargument name="assetID" type="numeric" required="yes">
		<cfargument name="options" type="struct"  required="yes">
        
        <cfif options.playpause IS 'NO'><cfset options.playpause = 0><cfset options.controls = 0><cfelse><cfset options.playpause = 1><cfset options.controls = 1></cfif>
        <cfif options.autoplay IS 'NO'><cfset options.autoplay = 0><cfelse><cfset options.autoplay = 1></cfif>
        <cfif options.zoom IS 'NO'><cfset options.zoom = 0><cfelse><cfset options.zoom = 1></cfif>
        <cfif options.speed IS 'NO'><cfset options.speed = 0><cfelse><cfset options.speed = 1></cfif>
        <cfif options.rewind IS 'NO'><cfset options.rewind = 0><cfelse><cfset options.rewind = 1></cfif>
        <cfif options.loop IS 'NO'><cfset options.loop = 0><cfelse><cfset options.loop = 1></cfif>
        <cfif options.seek IS 'NO'><cfset options.seek = 0><cfelse><cfset options.seek = 1></cfif>
        <cfif options.release IS 'NO'><cfset options.release = 0><cfelse><cfset options.release = 1></cfif>
        
          <cfquery name="info">
              UPDATE VideoAssets
              SET playpause = #options.playpause#, autoPlay = #options.autoplay#, zoom = #options.zoom#, speedControls = #options.speed#, rewind = #options.rewind#, loop = #options.loop#, scrubbar = #options.seek# , releaseControls = #options.release#
              
              WHERE	 asset_id = #assetID#
          </cfquery>
        
        <cfreturn true>
        
	</cffunction>
    
    
    
<!--- Update Assets to Group Content --->
	<cffunction name="getAllAssets" access="remote" returntype="query">
		
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="assetType" type="numeric" required="no" default="0">

  		<cfquery name="allAsseets">
        
            SELECT DISTINCT Assets.name, Assets.asset_id AS asset_id
            
            <cfif assetType IS 0>
			    , Assets.assetType_id, AssetTypes.name AS assetTypeName
                
                FROM     Assets INNER JOIN
                         AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id RIGHT OUTER JOIN
                         Groups ON Assets.asset_id = Groups.asset_id
            <cfelse>         
				FROM     GroupAssets RIGHT OUTER JOIN
                         Assets ON GroupAssets.asset_id = Assets.asset_id
         
            </cfif>
            
               WHERE    0=0
			   			<cfif appID GT 0>
            			AND (Assets.app_id = #appID#) 
                        </cfif>
                        <cfif assetType GT 0>
                        AND (Assets.assetType_id = #assetType#) 
                        </cfif>
                        
        </cfquery>
  
        <cfreturn allAsseets>    
        
	</cffunction>
    
    

<!--- Update Assets to Group Content --->
	<cffunction name="updatedContentOptions" access="remote" returntype="boolean">
		
        <cfargument name="groupAssetID" type="numeric" required="yes">
        <cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="assetOptions" type="struct" required="yes">
  
        <!--- {'access':theLevel, 'cached':theCached, 'active':theActive, 'shared':theShared, 'order':theOrder}; --->
        
        <!--- Update Asset Options --->
        <cfquery name="updateColors">
            UPDATE GroupAssets
            SET content_id = #assetID#
            <cfif NOT isNull(assetOptions.access)>
            	,accessLevel = #assetOptions.access#
            </cfif>
            <cfif NOT isNull(assetOptions.cached)>
            	,cached = #assetOptions.cached#
            </cfif>
            <cfif NOT isNull(assetOptions.active)>
            	,active = #assetOptions.active#
            </cfif>
            <cfif NOT isNull(assetOptions.shared)>
            	,sharable = #assetOptions.shared#
            </cfif>
            <cfif NOT isNull(assetOptions.order)>
            	,sortOrder = #assetOptions.order#
            </cfif>
            
            WHERE asset_id = #groupAssetID# AND content_id = #assetID#
        </cfquery>

		<cfif NOT isNull(assetOptions.order)>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>    
        </cfif>
            
	</cffunction>




<!--- Remove Assets to Group Content --->
	<cffunction name="deleteGroupAssets" access="remote" returntype="boolean">
		
        <cfargument name="groupAssetID" type="numeric" required="yes" default="0">
        <cfargument name="assetIDs" type="array" required="yes">
<!---         <cfargument name="action" type="numeric" required="no" default="0">
        
        <cfargument name="quiz" type="boolean" required="no" default="0">
        <cfargument name="quizCorrect" type="boolean" required="no" default="0">
        <cfargument name="quizIncorrect" type="boolean" required="no" default="0"> --->
        
        <cfloop index="z" from="1" to="#arrayLen(assetIDs)#">

			<cfset assetID = assetIDs[z]>
            
            <cfinvoke component="Modules" method="getGroupAssets" returnvariable="theAsset">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            
            <cfif arrayLen(theAsset.assets) GT 0>
                
                <cfset anAsset = theAsset.assets[1]>
                
                <!---Delete Detail--->
                <cfquery name="deleteDetails">
                    DELETE FROM GroupAssets
                    WHERE asset_id = #groupAssetID# AND content_id = #assetID# <!--- AND (action = #action# OR quiz = #quiz# OR quizCorrect = #quizCorrect# OR quizIncorrect = #quizIncorrect#) --->
                </cfquery>
        	
            </cfif>
            
        </cfloop>
 		
           
          <cfreturn true>
            
	</cffunction>
    
    
    

<!--- Add Assets to Group Assets --->
	<cffunction name="setGroupAssets" access="remote" returntype="struct">
		
        <cfargument name="groupAssetID" type="numeric" required="yes" default="0">
        <cfargument name="assetIDs" type="array" required="yes">
        <cfargument name="action" type="numeric" required="no" default="0">
		
        <cfargument name="quiz" type="boolean" required="no" default="0">
        <cfargument name="quizCorrect" type="boolean" required="no" default="0">
        <cfargument name="quizIncorrect" type="boolean" required="no" default="0">
        
        <cfset assetsAdded = arrayNew(1)>
        <cfset assetsNotAdded = arrayNew(1)>
																										
        <cfloop index="z" from="1" to="#arrayLen(assetIDs)#">

			<cfset assetID = assetIDs[z]>
            
            <!--- Check if Already Exists --->
            <cfinvoke component="Assets" method="groupAssetExists" returnvariable="assetExists">
                <cfinvokeargument name="groupAssetID" value="#groupAssetID#"/>
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="action" value="#action#"/>
                
                <cfinvokeargument name="quiz" value="#quiz#"/>
                <cfinvokeargument name="quizCorrect" value="#quizCorrect#"/>
                <cfinvokeargument name="quizIncorrect" value="#quizIncorrect#"/>
            </cfinvoke>

            <cfif NOT assetExists>
           
                <cfinvoke component="Modules" method="getGroupAssets" returnvariable="theAsset">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
                </cfinvoke>
    <!--- <cfdump var="#assetID#"><cfdump var="#theAsset#"><cfabort>   --->      
                <cfif arrayLen(theAsset.assets) GT 0>
                    
                    <cfset anAsset = theAsset.assets[1]>
                    
                    <!--- Set Assets from Group --->
                    <cfquery name="groupContentAsset">
                        INSERT INTO GroupAssets (content_id , asset_id, accessLevel, cached, active, sharable, sortOrder, action, quiz, quizCorrect, quizIncorrect)
                        VALUES (#assetID#, #groupAssetID#, 0, 0, 0, 0, 0, #action#, #quiz#, #quizCorrect#, #quizIncorrect#) 
                    </cfquery>
					
                    <cfset arrayAppend(assetsAdded,assetID)>
                    
                </cfif>
            <cfelse>
            	<cfset arrayAppend(assetsNotAdded,assetID)>
            </cfif>
            
        </cfloop>
        
        <cfset allAssets = {"failed": assetsNotAdded,"success": assetsAdded}>

          <cfreturn allAssets>
            
	</cffunction>
  
  
  
<!--- Group Asset Exists? --->
	<cffunction name="groupAssetExists" access="remote" returntype="boolean">
		
        <cfargument name="groupAssetID" type="numeric" required="yes">
        <cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="action" type="numeric" required="no" default="0">
        
        <cfargument name="quiz" type="boolean" required="no" default="0">
        <cfargument name="quizCorrect" type="boolean" required="no" default="0">
        <cfargument name="quizIncorrect" type="boolean" required="no" default="0">
               
		<!--- Set Assets from Group --->
        <cfquery name="anAsset">
            SELECT	asset_id
            FROM	GroupAssets
            WHERE	content_id = #assetID# AND asset_id = #groupAssetID#  
            
            <cfif action>AND action = #action#</cfif>
            
            <cfif quiz>
            	AND quiz = 1
				<cfif quizCorrect>AND quizCorrect = 1</cfif>
                <cfif quizIncorrect>AND quizIncorrect = 1</cfif>
			</cfif>
            
        </cfquery>
           
        <cfif anAsset.recordCount GT 0>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
            
	</cffunction>
    
    
      
    

<!--- Get Group Assets --->
	<cffunction name="getGroupAssets" access="remote" returntype="query">
		
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfargument name="action" type="boolean" required="no" default="false">
        
        <cfargument name="quiz" type="boolean" required="no" default="0">
        <cfargument name="quizCorrect" type="boolean" required="no" default="0">
        <cfargument name="quizIncorrect" type="boolean" required="no" default="0">
        
        <cfif action><cfset action = 1><cfelse><cfset action = 0></cfif>
        
        <cfif action><cfset action = 1><cfelse><cfset action = 0></cfif>
        <cfif quiz><cfset quiz = 1><cfelse><cfset quiz = 0></cfif>
        <cfif quizCorrect><cfset quizCorrect = 1><cfelse><cfset quizCorrect = 0></cfif>
        <cfif quizIncorrect><cfset quizIncorrect = 1><cfelse><cfset quizIncorrect = 0></cfif>

        
 		<!--- Get Assets from Group --->
        <cfquery name="allAssets">
            SELECT        GroupAssets.cached, GroupAssets.active, GroupAssets.sharable, GroupAssets.sortOrder, Assets.name, Assets.assetType_id, 
                         AssetTypes.icon, AccessLevels.accessLevel, GroupAssets.content_id, GroupAssets.action, GroupAssets.quiz, GroupAssets.quizCorrect, GroupAssets.quizIncorrect
			FROM            AssetTypes INNER JOIN
                         Assets ON AssetTypes.assetType_id = Assets.assetType_id RIGHT OUTER JOIN
                         GroupAssets ON Assets.asset_id = GroupAssets.content_id LEFT OUTER JOIN
                         AccessLevels ON GroupAssets.accessLevel = AccessLevels.accessLevel
                         
            <cfif assetID GT 0> 
            WHERE	GroupAssets.asset_id = #assetID# 
            <cfelse>
            WHERE	GroupAssets.group_id = #groupID#
            </cfif>
			
            <cfif quiz IS 0 AND quizCorrect IS 0 AND quizIncorrect IS 0>
            AND action = #action# AND quiz = 0 AND quizCorrect = 0 AND quizIncorrect = 0
            <cfelse>
            
			<cfif quiz>
                AND quiz = 1
            <cfelseif quizCorrect>
                AND quizCorrect = 1
            <cfelseif quizIncorrect>
                AND quizIncorrect = 1
            </cfif>
            
            </cfif>
            
            ORDER BY GroupAssets.sortOrder
        </cfquery>
   
          <cfreturn allAssets>
            
	</cffunction>
    
    
    
<!---Set Color Properties --->
	<cffunction name="setColorProperties" access="remote" returntype="numeric">
		
        <cfargument name="colorID" type="numeric" required="yes" default="0">
        
        <cfargument name="backcolor" type="string" required="yes" default="0">
 		<cfargument name="forecolor" type="string" required="yes" default="0">
        <cfargument name="background" type="string" required="yes" default="0">
        
        <cfif colorID GT 0>
        
			<!--- Update Marker Type --->
            <cfquery name="updateColors">
                UPDATE Colors
                SET 
                forecolor = <cfif forecolor IS ''>NULL<cfelse>'#forecolor#'</cfif>,
                backcolor = <cfif backcolor IS ''>NULL<cfelse>'#backcolor#'</cfif>,
                background = <cfif background IS ''>NULL<cfelse>'#background#'</cfif>
                WHERE color_id = #colorID#
            </cfquery>
            
        <cfelse>
        
        	<!--- Insert New Record --->
            <cfquery name="newAsset"> 
                INSERT INTO Colors (forecolor , backcolor, background)
                VALUES ('#forecolor#', '#backcolor#', '#background#') 
             	SELECT @@IDENTITY AS colorID
            </cfquery>
            
            <cfset colorID = newAsset.colorID>
        
        </cfif>
        
        <cfreturn colorID>
            
	</cffunction>
    
    

    <!---Set AR Model Type --->
	<cffunction name="get3DModelARMarker" access="remote" returntype="struct">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
 		
        <cfset arSupported = {"vuforia":0,"visualizer":0,"moodstocks":0}>
        
        <!--- Update Marker Type --->
        <cfquery name="arSupport">
            SELECT	useVisualizer, useVuforia, useMoodstocks
            FROM	MarkerAssets
            WHERE	asset_id = #assetID#
        </cfquery>
        
        <cfif arSupport.useVisualizer IS 1><cfset arSupported.visualizer = 1></cfif>
        <cfif arSupport.useVuforia IS 1><cfset arSupported.vuforia = 1></cfif>
        <cfif arSupport.useMoodstocks IS 1><cfset arSupported.moodstocks = 1></cfif>
        
		<cfreturn arSupported>
        
	</cffunction>
    
    
    <!---Set AR Asset  --->
	<cffunction name="setARAsset" access="remote" returntype="boolean">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="arAssetID" type="numeric" required="no" default="0">
 		
        <!--- Check if Exists --->
        <cfquery name="ARAsset">
            SELECT	asset_id 
            FROM	ARAssets
            WHERE	asset_id = #assetID#
        </cfquery>
        
        <cfif ARAsset.recordCount IS 0>
        	<cfreturn false>
        </cfif>
        
        <!--- Update Marker Type --->
        <cfquery name="updateARAsset">
            UPDATE ARAssets
            SET markerAsset_id = <cfif arAssetID IS '0'>NULL<cfelse>#arAssetID#</cfif>
            WHERE asset_id = #assetID#
        </cfquery>
        
		<cfreturn true>
        
	</cffunction>
    

    <!---Set AR Model Type --->
	<cffunction name="set3DModelARMarker" access="remote" returntype="boolean">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="arAssetID" type="numeric" required="no" default="0">
 		
        <!--- Check if Exists --->
        <cfquery name="ARAsset">
            SELECT	asset_id 
            FROM	ARAssets
            WHERE	asset_id = #assetID#
        </cfquery>
        
        <cfif ARAsset.recordCount IS 0>
        	<cfreturn false>
        </cfif>
        
        <!--- Update Marker Type --->
        <cfquery name="updateARAsset">
            UPDATE ARAssets
            SET marker_id = <cfif arAssetID IS '0'>NULL<cfelse>#arAssetID#</cfif>
            WHERE asset_id = #assetID#
        </cfquery>
        
		<cfreturn true>
        
	</cffunction>
    
    <!---Set AR Markers --->
	<cffunction name="get3DModelARMarkers" access="remote" returntype="query">

        <cfargument name="appID" type="numeric" required="yes" default="0">
 
        <!--- Update Marker Type --->
        <cfquery name="arMarkers">
            SELECT	asset_id, name
            FROM	Assets
            WHERE	app_id = #appID# AND assetType_id = 15
        </cfquery>
        
		<cfreturn arMarkers>
        
	</cffunction>
    
   
   
    <!---Set Model Date to Current Date --->
	<cffunction name="set3DModelDate" access="remote" returntype="boolean">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
 		
        <!--- Check if Exists --->
        <cfquery name="asset3D">
            SELECT	asset_id, modifiedModel
            FROM	Model3DAssets
            WHERE	asset_id = #assetID#
        </cfquery>
        
        <cfif asset3D.recordCount IS 0>
        	<cfreturn false>
        </cfif>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
       
        <cfif asset3D.modifiedModel IS curDate>
        	<cfreturn false>
        <cfelse>
        
			<!--- Update Marker Type --->
            <cfquery name="updateARAsset">
                UPDATE Model3DAssets
                SET modifiedModel = #curDate#
                WHERE asset_id = #assetID#
            </cfquery>
            
        </cfif>
        
		<cfreturn true>
        
	</cffunction>
   
   
    

    <!---Set Marker Type --->
	<cffunction name="setMarkerAssetType" access="remote" returntype="boolean">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="markerType" type="string" required="yes" default="">
        <cfargument name="markerState" type="boolean" required="no" default="false">
        
        <cfif markerType IS ''><cfreturn false></cfif>
        
        <!--- Update Marker Type --->
        <cfquery name="updateAsset">
            UPDATE MarkerAssets
            
            <cfif markerType IS 'ckvisualizer'>
            	SET useVisualizer = #markerState#
            <cfelseif markerType IS 'moodstocks'>
            	SET useMoodstocks = #markerState#
            <cfelseif markerType IS 'vuforia'>
            	SET useVuforia = #markerState#
            </cfif>
            
            WHERE asset_id = #assetID#
            
        </cfquery>
        
		<cfreturn true>
        
	</cffunction>



    <!---Get Total Assets--->
	<cffunction name="getMapperAssets" access="public" returntype="query">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfquery name="allAssets">

                SELECT   MapperPoints.active, MapperPoints.location_id, Assets.name, Assets.assetType_id, AssetTypes.dbTable, 
                         AssetTypes.icon
				FROM     Assets RIGHT OUTER JOIN
                         MapperPoints ON Assets.asset_id = MapperPoints.location_id LEFT OUTER JOIN
                         AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id
				WHERE	 MapperPoints.asset_id = #assetID#
        </cfquery>
        
		<cfreturn allAssets>
        
	</cffunction>





    <!---Get Total Assets--->
	<cffunction name="getTotalAssets" access="public" returntype="numeric">

        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        <cfquery name="allAssets">

            SELECT        asset_id
            FROM          Assets
            WHERE		  app_id = #appID# AND assetType_id = #assetTypeID#     

        </cfquery>
        
		<cfreturn allAssets.recordCount>
        
	</cffunction>






    <!---Get Asset Name--->
	<cffunction name="getAssetName" access="public" returntype="query">

        <cfargument name="assetID" type="numeric" required="no" default="0">
        
        <cfquery name="anAsset">

            SELECT        Assets.name, AssetTypes.name AS assetType, AssetTypes.icon, Assets.assetType_id
            FROM          Assets INNER JOIN
            			  AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id
            WHERE		  Assets.asset_id = #assetID#             

        </cfquery>
        
		<cfreturn anAsset>
        
	</cffunction>
    
    
    
    
    
    
    
   <!---Get ClientID and AppID From AssetID --->
	<cffunction name="getAssetAppClientID" access="public" returntype="struct">

        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfquery name="anAsset">

            SELECT        Applications.app_id, Applications.client_id
            FROM          Applications RIGHT OUTER JOIN Assets ON Applications.app_id = Assets.app_id
            WHERE        (Assets.asset_id = #assetID#)       

        </cfquery>
        
        <cfset assetInfo = {"clientID":anAsset.client_id ,"AppID":anAsset.app_id}>
        
		<cfreturn assetInfo>
        
	</cffunction>
    
    
    
    
    

    <!---Get Basic Asset Listing--->
	<cffunction name="getAssetListing" access="public" returntype="query">
    
        <cfargument name="AppID" type="numeric" required="no" default="0">
        <cfargument name="AssetTypeID" type="numeric" required="no" default="0">
        <cfargument name="filter" type="string" required="no" default="">
        
        <cfquery name="allAssets">

            SELECT        AssetTypes.name, AssetTypes.icon, Assets.asset_id, 
                          Assets.name AS assetName, Assets.modified, Assets.created, AssetTypes.assetType_id, Thumbnails.image
            FROM            Assets INNER JOIN
                                     AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id LEFT OUTER JOIN
                                     Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id
            WHERE			0 = 0 
                            <cfif AppID GT '0'>
                            AND (Assets.app_id = #AppID#) 
                            </cfif>
                            <cfif AssetTypeID GT '0'>
                            AND (Assets.assetType_id = #AssetTypeID#)
                            </cfif>
          					<cfif trim(filter) NEQ ''>
                            AND Assets.name LIKE '%#trim(filter)#%'
                            </cfif>
           ORDER BY 		AssetTypes.assetType_id ASC

        </cfquery>
        
		<cfreturn allAssets>
        
	</cffunction>
    
    
    
    
    
    
    <!---Get Asset Types--->
	<cffunction name="getAssetTypes" access="public" returntype="struct">
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        <cfquery name="assetTypes">
        	SELECT assetType_id, name, icon
            FROM AssetTypes
            <cfif assetTypeID GT 0>
            WHERE assetType_id = #assetTypeID#
            </cfif>
        </cfquery>
        
        <cfset theAssetTypes = structNew()>
        
        <cfif assetTypes.recordCount GT '1'>
        
            <cfoutput query="assetTypes">
				<cfset structAppend(theAssetTypes,{"#assetType_id#":{"name":name, "icon":icon}})>
			</cfoutput>
            
		<cfelseif assetTypes.recordCount IS '1'>
        	
            <cfoutput query="assetTypes">
            	<cfset structAppend(theAssetTypes,{"name":name, "icon":icon})>
            </cfoutput>
            
        </cfif>

        <cfreturn theAssetTypes>
        
 	</cffunction>  
    
    
    
    <!---Get Asset Type--->
	<cffunction name="getAssetType" access="public" returntype="numeric">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        
        <cfset assetType = 0>
		
		<cfif assetID GT 0>
        
            <cfquery name="assetTypes">
                SELECT        asset_id, assetType_id
                FROM            Assets
                WHERE        (asset_id = #assetID#)
            </cfquery>
            
            <cfif assetTypes.recordCount IS 0>
            	<cfset assetType = 0>
            <cfelse>
            	<cfset assetType = assetTypes.assetType_id>
			</cfif>
		
        </cfif>
        
        <cfreturn assetType>
        
 	</cffunction> 
    
    
    
    <!---Get Group of Assets--->
	<cffunction name="getAssets" access="public" returntype="query">
    
        <cfargument name="AppID" type="numeric" required="no" default="0">
        <cfargument name="AssetTypeID" type="numeric" required="no" default="0">
		<cfargument name="AssetID" type="numeric" required="no" default="0">
        
        <cfquery name="allAssets"> 
        	<!---Details--->
            SELECT        AssetTypes.name, AssetTypes.path, AssetTypes.dbTable, AssetTypes.icon, Assets.asset_id, 
                         Details.detail_id, Details.title, Details.subtitle, Details.description, Details.color, Details.backgroundColor, 
                         Details.titleColor, Details.subtitleColor, Details.descriptionColor, Details.other, Assets.name AS assetName, 
                         Assets.modified, Assets.app_id, Assets.created, AssetTypes.assetType_id, Assets.thumb_id, 
                         Thumbnails.image AS thumbnail, Thumbnails.width, Thumbnails.height,
                         Assets.color_id, Colors.foreColor, Colors.backColor, Colors.otherColor, Colors.background, Colors.displayAsset_id
			FROM            Assets INNER JOIN
                         AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id LEFT OUTER JOIN
                         Colors ON Assets.color_id = Colors.color_id LEFT OUTER JOIN
                         Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id LEFT OUTER JOIN
                         Details ON Assets.detail_id = Details.detail_id
            WHERE			0 = 0
            <cfif AssetID GT '0'>  
            AND (Assets.asset_id = #AssetID#) 
            <cfelse>   
                            <cfif AppID GT '0'>
                            AND (Assets.app_id = #AppID#) 
                            </cfif>
                            <cfif AssetTypeID GT '0'>
                            AND (Assets.assetType_id = #AssetTypeID#)
                            </cfif>
            ORDER BY 		AssetTypes.assetType_id ASC
            </cfif>
        
        
        
        </cfquery>
        
		<cfreturn allAssets>
        
	</cffunction>


    <!---Get Assets Title--->
	<cffunction name="getAssetTitle" access="public" returntype="string">
    
		<cfargument name="AssetID" type="numeric" required="yes" default="0">
        
        <cfquery name="theAssetInfo"> 
        	
            SELECT CASE
            WHEN Details.title = '' OR Details.title IS NULL THEN Assets.name
            ELSE Details.title
            END AS title
            FROM          Assets LEFT OUTER JOIN Details ON Assets.detail_id = Details.detail_id
            WHERE        (Assets.asset_id = #AssetID#)
        
        </cfquery>
        
		<cfreturn theAssetInfo.title>
        
	</cffunction>


    
    
    
    <!---Get A Asset By Type--->
    <cffunction name="getAsset" access="public" returntype="query" output="yes">
    	
        <cfargument name="assetID" type="numeric" required="yes" default="0">
          
        <cfif assetID GT '0'>
        
        	<!--- Get Asset Table --->
            <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            
            <cfif assetTable IS 'GroupAssets'>
            	<cfset assetTable = 'Assets'>
            </cfif>
                    
           <!--- Get All Data from Asset Table--->
            <cfquery name="anAsset"> 
                SELECT	*
                FROM	#assetTable#
                <cfif AssetID GT'0'>
                WHERE asset_id = #AssetID#
                </cfif>
            </cfquery>

        </cfif>
        
        <cfreturn anAsset>
        
    </cffunction>


    <!---Get Table Asseet Columns--->
    <cffunction name="getAssetTableColums" access="public" returntype="array" output="yes">
    	
        <cfargument name="assetTable" type="string" required="yes" default="">
        
         	<!--- Get Colums from Table --->
            <cfquery name="theTable">
                SELECT COLUMN_NAME
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = N'#assetTable#'
        	</cfquery>
        
           <!--- Table Colums --->
          <cfset tableColums = arrayNew(1)>
          
          <cfloop query="theTable">
		  	<cfset arrayAppend(tableColums,column_name)>
		  </cfloop>
        
        <cfreturn tableColums>
        
    </cffunction>

    
      
   <!---Get All Asset Details--->
    <cffunction name="getAllAssetDetails" access="public" returntype="query" output="yes">
    	
        <cfargument name="assetID" type="numeric" required="yes" default="structNew()">
           
          <cfquery name="details"> 
                SELECT   Details.detail_id, Thumbnails.thumb_id, Thumbnails.image AS thumbnail, Colors.color_id
				FROM     Assets LEFT OUTER JOIN
                         Colors ON Assets.color_id = Colors.color_id LEFT OUTER JOIN
                         Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id LEFT OUTER JOIN
                         Details ON Assets.detail_id = Details.detail_id
    
                WHERE        (Assets.asset_id = #assetID#)
           </cfquery>
          
          <cfreturn details>
      
    </cffunction>
 
 
 
 
 
     <!--- Update Asset Details																											--->
    <cffunction name="updateAsset" access="public" returntype="boolean" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
		
        <!---Get Asset Type--->
        <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetType">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

 		<!--- Asset Types --->
        <cfswitch expression="#assetType#">
      		
            <!--- ContentAssets --->    
            <cfcase value="GroupAssets">

		   <cfinvoke component="Assets" method="updateContentAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>   
           
            <cfset assetData.assetID = assetID> 					
            
            </cfcase>
            
            
            <!--- AnimationSeqAssets --->    
            <cfcase value="AnimationAssets">
            
            <cfinvoke component="Assets" method="updateAnimationAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 

            </cfcase>
            
            
            
            <!--- 3D Model Material Shaders --->    
            <cfcase value="MaterialAssets">
            
            <cfinvoke component="Assets" method="updateMaterialAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 

            </cfcase>

            
			<!--- Image --->    
            <cfcase value="ImageAssets">
            
            <cfinvoke component="Assets" method="updateImageAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 

            </cfcase>
            
            
            <!--- Game --->    
            <cfcase value="GameAssets">
            
            <cfinvoke component="Assets" method="updateGameAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 

            </cfcase>
            
            
            <!--- Video --->
            <cfcase value="VideoAssets">
    		
            <cfinvoke component="Assets" method="updateVideoAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 
            
            </cfcase>
            
            <!--- Document --->
            <cfcase value="PDFAssets"> 
            
    		<cfinvoke component="Assets" method="updateDocumentAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID>
            
            </cfcase>
            
            <!--- URL --->
            <cfcase value="URLAssets">
               
              <cfinvoke component="Assets" method="updateURLAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
            
            <cfset assetData.assetID = assetID>
             
            </cfcase>
            
            <!--- GPS Location --->    
            <cfcase value="GPSAssets">         
    		
              <cfinvoke component="Assets" method="updateGPSLocationAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
            
            <cfset assetData.assetID = assetID> 
            
            </cfcase>
            
            <!--- 3D Point --->    
            <cfcase value="XYZPointAssets">         
            
              <cfinvoke component="Assets" method="updatePointAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
            
            <cfset assetData.assetID = assetID>
            
            </cfcase>
    
            <!--- 3D Model --->
            <cfcase value="Model3DAssets">
         	
             <cfinvoke component="Assets" method="update3DModelAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
        
            <cfset assetData.assetID = assetID>
            
            </cfcase>
            
            <!--- Panorama --->    
            <cfcase value="PanoramaAssets" delimiters=","> 
            
             <cfinvoke component="Assets" method="updateBalconyPanoramaAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
            
            <cfset assetData.assetID = assetID>
            
            </cfcase>
            
            <!--- Balcony --->    
            <cfcase value="BalconyAssets" delimiters=","> 
            
              <cfinvoke component="Assets" method="updateBalconyPanoramaAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
            
            <cfset assetData.assetID = assetID>
            
            </cfcase>
            
            <!--- Markers --->    
            <cfcase value="MarkerAssets" delimiters=","> 
            
              <cfinvoke component="Assets" method="updateMarkerAsset" returnvariable="assetID">
                    <cfinvokeargument name="assetData" value="#assetData#"/>
              </cfinvoke>
            
            <cfset assetData.assetID = assetID>
            
            </cfcase>
            
            <!--- Quiz --->    
            <cfcase value="QuizAssets">
            
            <cfinvoke component="Assets" method="updateQuizAsset" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 

            </cfcase>
            
            <!--- Program Asset --->    
            <cfcase value="ProgramAssets">
            																											
            <cfinvoke component="Assets" method="ProgramAssets" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>  
            
            <cfset assetData.assetID = assetID> 

            </cfcase>
            
                                    
            <!--- Nothing Error --->
            
            <cfdefaultcase>
    			<cfreturn false>
            </cfdefaultcase>
    
		</cfswitch>
     
        <!--- Get Epoch --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="modDate" />
        
		<!--- Update Asset - Name and Modified --->
        <cfquery name="updateAsset">
            UPDATE Assets
            SET modified = #modDate#, name = '#assetData.name#'
            WHERE asset_id = #assetData.assetID#
        </cfquery>
        
        <!--- Get AppID + Group from assetID to Update --->
        <cfinvoke component="CFC.Modules" method="getAssetGroup" returnvariable="groupsToUpdate">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Update Cashed JSON --->
        <cfoutput query="groupsToUpdate">
            <cfinvoke component="Apps" method="generateContentJSON" returnvariable="result">
                <cfinvokeargument name="groupID" value="#groupID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
        </cfoutput>
        
        <cfreturn true>
 
 
 	</cffunction>
    
    
    
    
    	<!--- Building TYPE 																																--->
     <cffunction name="updateBuildingAssets" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
           
           <!--- <cfquery name="updateDetails">
                 UPDATE ImageAssets  
                 SET url = '#assetData.image.file#' , width = #assetData.image.size.width# , height = #assetData.image.size.height#, webURL = NULL
                 WHERE (asset_id = #assetID#) 
            </cfquery>--->

        <cfelse>
        
        	<!--- New Asset ---> 
             
             <!--- Insert DB Image--->
             <!--- <cfquery name="newAsset"> 
                INSERT INTO ImageAssets (url , width, height, asset_id)
                VALUES ('#assetData.image.file#', #assetData.image.size.width#, #assetData.image.size.height#, #assetData.assetID#) 
             </cfquery>--->

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 
 
 
 
 
   	<!--- Floor TYPE 																																--->
     <cffunction name="updateFloorAssets" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
           
           <cfquery name="updateDetails">
                 <!--- UPDATE ImageAssets  
                 SET url = '#assetData.image.file#' , width = #assetData.image.size.width# , height = #assetData.image.size.height#, webURL = NULL
                 WHERE (asset_id = #assetID#) --->
            </cfquery>

        <cfelse>
        
        	<!--- New Asset ---> 
             
             <!--- Insert DB Image--->
             <!--- <cfquery name="newAsset"> 
                INSERT INTO ImageAssets (url , width, height, asset_id)
                VALUES ('#assetData.image.file#', #assetData.image.size.width#, #assetData.image.size.height#, #assetData.assetID#) 
             </cfquery>--->

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 
 
 
 
  	<!--- Suite TYPE 																																--->
     <cffunction name="updateSuiteAssets" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
           
            <!--- <cfquery name="updateDetails">
                UPDATE ImageAssets  
                 SET url = '#assetData.image.file#' , width = #assetData.image.size.width# , height = #assetData.image.size.height#, webURL = NULL
                 WHERE (asset_id = #assetID#)
            </cfquery> --->

        <cfelse>
        
        	<!--- New Asset ---> 
             
             <!--- Insert DB Image--->
             <!--- <cfquery name="newAsset"> 
                INSERT INTO ImageAssets (url , width, height, asset_id)
                VALUES ('#assetData.image.file#', #assetData.image.size.width#, #assetData.image.size.height#, #assetData.assetID#)
             </cfquery> --->

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 																																																				
  	<!--- Content Asset TYPE --->
     <cffunction name="updateContentAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
      
        <cfquery name="contentAsset"> 
          SELECT	*
          FROM		ContentAssets
          WHERE 	asset_id = #assetData.assetID#
        </cfquery>

        <cfset assetID = '0'>
     
        <!--- Assets Exists? --->
        <cfif contentAsset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = assetData.assetID>
           
           <cfquery name="updateAsset">
                 UPDATE ContentAssets  
                 SET displayType = #assetData.behaviourType#, instanceName = '#assetData.instanceName#', 
                 <cfif assetData.useAssetID IS 0>
                 	instanceAsset_id = NULL
                 <cfelse>
                 	instanceAsset_id = #assetData.useAssetID#
                 </cfif>
                 WHERE (asset_id = #assetID#)
            </cfquery>

        <cfelse>
        
        	<!--- New Asset ---> 
             
             <!--- Insert DB Image--->
             <cfquery name="newAsset"> 
                INSERT INTO ContentAssets (asset_id, displayType, instanceName, instanceAsset_id)
                VALUES (#assetData.assetID#, #assetData.behaviourType#, '#assetData.instanceName#', 
                <cfif assetData.useAssetID IS 0>
                	NULL
                <cfelse>
                	#assetData.useAssetID#
                </cfif>
                )
              </cfquery>

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
     
     
 
 
 

 
 
 
 
  	<!--- ANIMATION SEQ TYPE --->
     <cffunction name="updateAnimationAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
    
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
           
           <cfquery name="updateAsset">
                 UPDATE AnimationAssets  
                 SET model3d = '#assetData.animation.modelName#', animationSequence = '#assetData.animation.sequenceName#'
                 WHERE (asset_id = #assetID#)
            </cfquery>

        <cfelse>
        
        	<!--- New Asset ---> 
             
             <!--- Insert DB Image--->
             <cfquery name="newAsset"> 
                INSERT INTO AnimationAssets (asset_id , model3d, animationSequence)
                VALUES ('#assetData.assetID#', '#assetData.animation.modelName#', '#assetData.animation.sequenceName#')
             </cfquery>

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 
 
 
 
 <!--- 3D MODEL MATERIAL SHADER TYPE --->
     <cffunction name="updateMaterialAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
    
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
           
           <cfquery name="updateAsset">
                 UPDATE MaterialAssets  
                 SET model3d = '#assetData.shaderMaterial.modelName#', material = '#assetData.shaderMaterial.material#'
                 WHERE (asset_id = #assetID#)
            </cfquery>

        <cfelse>
        
        	<!--- New Asset ---> 
             
             <!--- Insert DB Image--->
             <cfquery name="newAsset"> 
                INSERT INTO MaterialAssets (asset_id , model3d, material)
                VALUES ('#assetData.assetID#', '#assetData.shaderMaterial.modelName#', '#assetData.shaderMaterial.material#')
             </cfquery>

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 

   	
 
 
  	<!--- IMAGE TYPE 																																--->
     <cffunction name="updateImageAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <cfif StructKeyExists(assetData,"image")>
            
            	<!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.url>
            
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.image.file>
                
				<!--- Move Asset --->
                <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.image.file = ImageFileName>
                
                <!--- Update DB Image--->
                <cfquery name="updateDetails">
                     UPDATE ImageAssets  
                     SET url = '#assetData.image.file#' , width = #assetData.image.size.width# , height = #assetData.image.size.height#, webURL = NULL
                     WHERE (asset_id = #assetID#)
            	</cfquery>
            
            <cfelseif StructKeyExists(assetData,"webURL")>
            	
                <!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.url>
            
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
                <!--- Update DB URL Image--->
                <cfquery name="updateDetails">
                     UPDATE ImageAssets
                     SET webURL = '#assetData.webURL#' , width = NULL , height = NULL, URL = NULL
                     WHERE (asset_id = #assetID#)   
                </cfquery>
            
            </cfif>

        <cfelse>
        
        	<!--- New Asset --->
   
            <!--- Move Asset --->
			<cfif StructKeyExists(assetData,"image")>
            
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.image.file>
            
                <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.image.file = ImageFileName>
				 
				 <!--- Insert DB Image--->
                 <cfquery name="newAsset"> 
                    INSERT INTO ImageAssets (url , width, height, asset_id)
                    VALUES ('#assetData.image.file#', #assetData.image.size.width#, #assetData.image.size.height#, #assetData.assetID#)
              	 </cfquery>
             
             
             <cfelseif StructKeyExists(assetData,"webURL")>
                
                <!--- Insert DB URL Image--->
                <cfquery name="newAsset"> 
                    INSERT INTO ImageAssets (url,webURL , width, height, asset_id)
                    VALUES (NULL,'#assetData.webURL#', NULL, NULL, #assetData.assetID#)
          		</cfquery>
          
            </cfif>

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 
 
 
 
 
 
  	<!--- PANORAMA TYPE 																																--->
     <cffunction name="updateBalconyPanoramaAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
      
        <cfset assetID = '0'>
        <cfset pano = false>
        <cfset allAssets = arrayNew(1)>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <cfif StructKeyExists(assetData,"balcony")>
            	
				<cfset allAssets = assetData.balcony>
	
                <cfinvoke component="Misc" method="QueryToStruct" returnvariable="assets">
                    <cfinvokeargument name="query" value="#asset#"/>
                </cfinvoke>           
   
				<cfloop index="z" from="1" to="#arrayLen(allAssets)#">
                	
                    <cfset theObj = allAssets[z]> 
                    
                    <!--- Get a Side --->
                    <cfset sideToDelete = theObj.image.side>
                     
						<!--- Old Asset Path --->
                        <cfset fileToDelete = assetPath & assets['#sideToDelete#']>
                  
                        <!--- Delete Old Asset --->
                        <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                          <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                        </cfinvoke>
                        
                        <!--- New File + Path --->
                        <cfset fileDestPath = assetPath & theObj.image.file>
                        
                        <!--- Move Asset --->
                        <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                          <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                        </cfinvoke>
                        
                        <cfset theObj.image.file = ImageFileName>

                </cfloop>
  				
             </cfif>
                    
			  <!--- Pano --->
              <cfif StructKeyExists(assetData,"panorama")>
              
                  <cfinvoke component="Misc" method="QueryToStruct" returnvariable="assets">
                      <cfinvokeargument name="query" value="#asset#"/>
                  </cfinvoke> 
              
                  <!--- Get Pano FIle --->
                  <cfset largePano = trim(assetData.panorama[1].image.file)>
              
                  <!--- Old Asset Path --->
                  <cfset fileToDelete = assetPath & assets['pano']>
                    
                  <!--- Delete Old Asset --->
                  <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                    <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                  </cfinvoke>
              
                  <!--- New File + Path --->
                  <cfset fileDestPath = assetPath & largePano>
                  
                  <!--- Move Asset --->
                  <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                    <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                  </cfinvoke>
                  
                  <cfset largePano = ImageFileName>  
                  <cfset pano = true>
                  
              </cfif>
              
              <cfif StructKeyExists(assetData,"panorama") OR StructKeyExists(assetData,"balcony")>
              
                <!--- Update DB Image--->
                <cfquery name="updateDetails">
                	 <cfif StructKeyExists(assetData,"balcony")>
                     UPDATE BalconyAssets
                     <cfelse>
                     UPDATE PanoramaAssets
                     </cfif>
                     SET
                     <cfif pano>
					 	pano = '#largePano#',
					 </cfif>
                     
                     <cfloop index="z" from="1" to="#arrayLen(allAssets)#">
                     	[#allAssets[z].image.side#] = '#allAssets[z].image.file#',
                     </cfloop>

                     asset_id = #assetID#
                     
                     <cfif StructKeyExists(assetData,"balcony")>
                     	, floorName = '#assetData.floor#'
                     </cfif>
                     
                     WHERE (asset_id = #assetID#)
            	</cfquery>

            
            </cfif>

        <cfelse>
         
            <!--- New Asset --->
			<cfif StructKeyExists(assetData,"panorama") OR StructKeyExists(assetData,"balcony")>
				
                <cfif StructKeyExists(assetData,"balcony")>
					<cfset allAssets = assetData.balcony>
				<cfelse>
            		<cfset allAssets = assetData.panorama>
            	</cfif>
                
                <!--- Move Asset --->
                <cfloop index="z" from="1" to="#arrayLen(allAssets)#">
               
					<!--- New File + Path --->
                    <cfset fileDestPath = assetPath & allAssets[z].image.file>
                
                    <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                      <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                    </cfinvoke>
                    
                    <cfset allAssets[z].image.file = ImageFileName>
				
                </cfloop>
             
				 <!--- Insert DB Image--->
                 <cfquery name="newAsset"> 
                    INSERT INTO 
                    <cfif StructKeyExists(assetData,"balcony")>
                    	BalconyAssets
                    <cfelse>
                    	PanoramaAssets
                    </cfif>
                    (
                    <cfloop index="z" from="1" to="#arrayLen(allAssets)#">
                        [#allAssets[z].image.side#],
                    </cfloop>
                    asset_id
                    <cfif StructKeyExists(assetData,"balcony")>
                    , floorName
                    </cfif>
                    )
                    
                    VALUES (
                    <cfloop index="z" from="1" to="#arrayLen(allAssets)#">
                        '#allAssets[z].image.file#', 
                     </cfloop>
                     #assetData.assetID#
                     <cfif StructKeyExists(assetData,"balcony")>
                     , '#assetData.floor#'
                     </cfif>
                     )
              	 </cfquery>
			
            </cfif>
            
          	<cfset assetID = assetData.assetID>
            
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 
 
 
   	<!--- QUIZ 																																--->
     <cffunction name="updateQuizAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
         
        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <!--- Update Asset --->
             <cfquery name="updateDetails">
          
                 UPDATE QuizAssets
                 SET complete = #assetData.quiz.needsToBeCompleted#, displayCorrect = #assetData.quiz.displayCorrect#, movable = #assetData.quiz.movable#, 
                 tryAgain = #assetData.question.tryAgain#, needResponse = #assetData.question.needResponse#, 
                 questionMessage = '#assetData.question.questionMessage#', correctMessage = '#assetData.question.correctMessage#', incorrectMessage = '#assetData.question.incorrectMessage#'
                 WHERE (asset_id = #assetID#)
                  
            </cfquery>
            
            <cfif assetData.selection.colums LTE 0>
            	<cfset assetData.selection.colums = 1>
            </cfif>
            
          	<!--- Check if Colors Exists for Selection --->
     		<cfquery name="selectionColors">
            	SELECT  color_id
                FROM	SelectionAssets
                WHERE	selection_id = #assetData.selection.selectionID#	
            </cfquery>
            
            <cfset colorID = selectionColors.color_id>

            <cfif colorID NEQ ''>
            
            	<!--- Update Colors --->
                <cfquery name="updateColors">
                     UPDATE Colors
                     SET foreColor = '#assetData.selection.colors.forecolor#', backColor = '#assetData.selection.colors.backcolor#', background = '#assetData.selection.colors.background#'
                     WHERE (color_id = #colorID#)           
            	</cfquery> 
                
            <cfelse>
            
            	<!--- Create Colors for Selection --->
                <cfquery name="newColor">       
                    INSERT INTO Colors (forecolor , backcolor, background)
                    VALUES ('#assetData.selection.colors.forecolor#', '#assetData.selection.colors.backcolor#', '#assetData.selection.colors.background#')
                    SELECT @@IDENTITY AS colorID
                </cfquery>
                
                <cfset colorID = newColor.colorID>
                
            </cfif> 
              
            <!--- Update Selection --->
             <cfquery name="updateDetails">       
                 UPDATE SelectionAssets
                 SET spacing = #assetData.selection.spacing#, padding = #assetData.selection.padding#, numberOfCols = #assetData.selection.colums#
                 <cfif colorID NEQ ''>, color_id = #colorID#</cfif>
                 WHERE (selection_id = #assetData.selection.selectionID#)
            </cfquery>          
            
        <cfelse>
        
        	<!--- New Asset --->
            
            <!--- New Selection Entry --->
            <cfquery name="newSelection"> 
            	INSERT INTO SelectionAsstes (spacing , padding, numberOfCols)
                VALUES (10, 20, 2)
                SELECT @@IDENTITY AS selectionID
            </cfquery>
            
            <cfset selectionID = newSelection.selectionID>
            
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
            	INSERT INTO QuizAssets (complete , tryAgain, movable, displayCorrect, needResponse, questionMessage, correctMessage, incorrectMessage, selection_id, asset_id)
                VALUES (#assetData.quiz.needsToBeCompleted#, #assetData.question.tryAgain#, #assetData.quiz.movable#, #assetData.quiz.displayCorrect#, #assetData.question.needResponse#,
                		'#assetData.question.questionMessage#', '#assetData.question.correctMessage#', '#assetData.question.incorrectMessage#',
                        #selectionID#, #assetData.assetID#)
            </cfquery>

          	<cfset assetID = assetData.assetID>

            <!--- New Choice (Boolean default) Entry --->
            
            <!--- YES --->
            <cfquery name="newAsset"> 
            	INSERT INTO ChoiceAssets (selection , url, text, sortOrder, correct)
                VALUES (#selectionID#, '', '_YES', 0, 1)
            </cfquery>
            
            <!--- NO --->
            <cfquery name="newAsset"> 
            	INSERT INTO ChoiceAssets (selection , url, text, sortOrder, correct)
                VALUES (#selectionID#, '', '_NO', 1, 0)
            </cfquery>
        
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>
 
 
 
 
 
 
 
 
  	<!--- WEB URL TYPE 																																--->
     <cffunction name="updateURLAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <cfif NOT StructKeyExists(assetData,"url")>
        	<cfset structAppend(assetData,{"url":""})>
        </cfif>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <!--- Update DB --->
             <cfquery name="updateDetails">
          
                 UPDATE URLAssets
                 SET url = '#assetData.url#'
                 WHERE (asset_id = #assetID#)
                  
            </cfquery>         
            
        <cfelse>
        
        	<!--- New Asset --->
       
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
            	INSERT INTO URLAssets (url , asset_id)
                VALUES ('#assetData.url#', #assetData.assetID#)
            </cfquery>

          <cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>
 
 
	 <!--- PROGRAM ASSET 																																--->
     <cffunction name="ProgramAssets" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
																
        <cfset assetID = 0>
        
        <cfif NOT StructKeyExists(assetData,"url")>
            <cfset structAppend(assetData,{"url":""})>
        </cfif>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
            <!--- Update Asset --->
            <cfset assetID = asset.asset_id>
            													
            <!--- Update DB --->
             <!---<cfquery name="updateDetails">
          
                  UPDATE ProgramAssets
                 SET displayAsset_id = #assetData.assetID#
                 WHERE (asset_id = #assetID#) 
                  
            </cfquery>--->         
            
        <cfelse>
        
            <!--- New Asset --->
   
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
                 INSERT INTO ProgramAssets (asset_id)
                  VALUES (#assetData.assetID#)
            </cfquery>

          <cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>
     
 
 
   	<!--- GPS LOCATION TYPE																															--->
     <cffunction name="updateGPSLocationAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <!--- Update DB --->
             <cfquery name="updateDetails">
          
                 UPDATE GPSAssets
                 SET gps_long = #assetData.gps.coords.long# , gps_latt = #assetData.gps.coords.latt#, 
                 
				 <cfif assetData.gps.coords.alt NEQ ''>
                 	gps_alt = #assetData.gps.coords.alt#, 
                 </cfif>
                 
                 radius = #assetData.gps.radius#
                 WHERE (asset_id = #assetID#)
                  
            </cfquery>         
            
        <cfelse>
        
        	<!--- New Asset --->
       
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
            	INSERT INTO GPSAssets(
                gps_long , gps_latt,
                <cfif assetData.gps.coords.alt NEQ ''> 
                	gps_alt, 
                </cfif>
                <!--- x_pos, y_pos, ---> 
                radius, asset_id
                )
                VALUES (#assetData.gps.coords.long#, #assetData.gps.coords.latt# , 
                <!--- #assetData.gps.position.x#,  --->
                <cfif assetData.gps.coords.alt NEQ ''>
                	#assetData.gps.coords.alt#, 
                </cfif>
                <!--- #assetData.gps.position.y#, ---> 
                #assetData.gps.radius#, #assetData.assetID#)
            </cfquery>
        	
            <cfset assetID = assetData.assetID>
            
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>
 
 





  	<!--- POINT TYPE 																																--->
     <cffunction name="updatePointAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <!--- Update DB --->
             <cfquery name="updateDetails">
          
                 UPDATE XYZPointAssets
                 SET x_pos = #assetData.x# , y_pos = #assetData.y# , objectRef = '#assetData.objectRef#', z_pos = <cfif assetData.z IS ''>NULL<cfelse>#assetData.z#</cfif>
                 WHERE (asset_id = #assetID#)
                  
            </cfquery>         
            
        <cfelse>
        
        	<!--- New Asset --->
       
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
            	INSERT INTO XYZPointAssets (x_pos , y_pos, objectRef, z_pos, asset_id)
                VALUES (#assetData.x#, #assetData.y#, '#assetData.objectRef#' , <cfif assetData.z IS ''>NULL<cfelse>#assetData.z#</cfif>, #assetData.assetID#)
            </cfquery>
        	
            <cfset assetID = assetData.assetID>
            
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>
     
     
     
     
     
     
     
   <!--- Game TYPE 																																--->
     <cffunction name="updateGameAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
		
        
        <!--- Convert Dates --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="startDate">
        	<cfinvokeargument name="TheDate" value="#assetData.startDate#"/>
        </cfinvoke>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="endDate">
        	<cfinvokeargument name="TheDate" value="#assetData.endDate#"/>
        </cfinvoke>
		
		<cfif assetData.email.sendClient><cfset sendClient = 1><cfelse><cfset sendClient = 0></cfif>
        <cfif assetData.email.sendPlayer><cfset sendPlayer = 1><cfelse><cfset sendPlayer = 0></cfif>
        <cfif assetData.useCode><cfset useCode = 1><cfelse><cfset useCode = 0></cfif>
        <cfif assetData.sendTargets><cfset useTargets = 1><cfelse><cfset useTargets = 0></cfif>
        <cfif assetData.silentMode><cfset silent = 1><cfelse><cfset silent = 0></cfif>
        
        <cfset assetID = '0'>
       
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <!--- Update DB --->
             <cfquery name="updateDetails">
          
                 UPDATE GameAssets
                 SET dateStart = #startDate# , dateEnd = #endDate#, messageStart = '#assetData.message.start#', messageEnd = '#assetData.message.end#', sendClientEmail = #sendClient#, emailMessage = '#assetData.email.emailMessage#', sendPlayerEmail = #sendPlayer#, useCode = #useCode#, sendTargets = #useTargets#, silentMode = #silent#
                 
                 ,email = <cfif assetData.email.sendClient>'#assetData.email.email#' <cfelse>''</cfif>
                 
                 
                 WHERE (asset_id = #assetID#)
                  
            </cfquery>         
            
        <cfelse>
        
        	<!--- New Asset --->
       
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
            	INSERT INTO GameAssets (dateStart, dateEnd, asset_id, messageStart, messageEnd, sendClientEmail, emailMessage, sendPlayerEmail, useCode, sendTargets, silentMode, email)
                VALUES (#startDate#, #endDate#, #assetData.assetID#, '#assetData.message.start#', '#assetData.message.end#', #sendClient#, '#assetData.email.emailMessage#', #sendPlayerEmail#, #useCode#, #sendTargets#, #silent#, <cfif assetData.email.sendClient>'#assetData.email.email#' <cfelse>''</cfif>)
            </cfquery>
        	
            <cfset assetID = assetData.assetID>
            
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>







  	<!--- MARKER TYPE 																																--->
     <cffunction name="updateMarkerAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">

        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
      
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

       
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
     	
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
   

         	<!--- Visualizer --->
            <cfif isDefined('assetData.visualizer.url')>
                
                <cfif asset.url NEQ '' AND assetData.visualizer.url NEQ ''>
                
					<!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.url>
          
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                
                </cfif>
                
                <cfif assetData.visualizer.url NEQ ''>
                 
					<!--- New File + Path --->
                    <cfset fileDestPath = assetPath & assetData.visualizer.url>
            
                    <!--- Move Asset --->
                    <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                      <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                    </cfinvoke>
                    
                    <cfset assetData.visualizer.url = ImageFileName>
                </cfif>
             
             <cfelseif NOT isDefined('assetData.visualizer')>
                   
                   <!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.url>
          
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                   
             </cfif>
              
             <cfif isDefined('assetData.visualizer.webURL')>
                
                <cfif asset.url NEQ '' AND assetData.visualizer.webURL NEQ ''>
                
					<!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.url>
      
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
             	
                </cfif>
                
             </cfif>
             
             
             <!--- VuForia - XML File --->
            <cfif isDefined('assetData.vuforia.xml')>
                
                <cfif asset.xml NEQ '' AND assetData.vuforia.xml NEQ ''>
                
					<!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.xml>
          
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                
                </cfif>
                
                <cfif assetData.vuforia.xml NEQ ''>
                
					<!--- New File + Path --->
                    <cfset fileDestPath = assetPath & assetData.vuforia.xml>
            
                    <!--- Move Asset --->
                    <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                      <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                      <cfinvokeargument name="norename" value="true"/>
                    </cfinvoke>
                    
                    <cfset assetData.vuforia.xml = ImageFileName>

                </cfif>
                
             <cfelseif NOT isDefined('assetData.vuforia.xml')>

                   <!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.xml>
          
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                   
             </cfif>
             
              <!--- VuForia - DAT File --->
            <cfif isDefined('assetData.vuforia.dat')>
            	
                <cfif asset.dat NEQ '' AND assetData.vuforia.dat NEQ ''>
                
					<!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.dat>
          
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                
                </cfif>

                 <cfif assetData.vuforia.dat NEQ ''>
                 
					<!--- New File + Path --->
                    <cfset fileDestPath = assetPath & assetData.vuforia.dat>
            
                    <!--- Move Asset --->
                    <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                      <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                      <cfinvokeargument name="norename" value="true"/>
                    </cfinvoke>
                    
                    <cfset assetData.vuforia.dat = ImageFileName>
                    
                </cfif>
                
             <cfelseif NOT isDefined('assetData.vuforia.dat')>
             	   
                   <!--- Old Asset Path --->
                    <cfset fileToDelete = assetPath & asset.dat>
          
                    <!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                   
             </cfif>

             
       <!--- <cfdump var="#assetData#"><cfabort>  --->

            <!--- Update DB --->
             <cfquery name="updateDetails">
          
                UPDATE MarkerAssets
                 SET asset_id = #assetID#
                 
                 <cfif isDefined('assetData.visualizer')>
                 	<!--- visualizer --->
					 <cfif isDefined('assetData.visualizer.webURL')>
                     	<cfif assetData.visualizer.webURL NEQ ''>
                            , webURL = '#assetData.visualizer.webURL#'
                            , url = NULL
                            , useVisualizer = 1
                        </cfif>
                     <cfelse>
                     	<cfif isDefined('assetData.visualizer.webURL')>
                        , webURL = #assetData.visualizer.webURL#
                        <cfelse>
                        , webURL = NULL
                        </cfif>
                        <cfif isDefined('assetData.visualizer.url')>
                        , url = '#assetData.visualizer.url#'
                        <cfelse>
                        , url = NULL
                        </cfif>
                        , useVisualizer = 1
                        , keySet = '#assetData.visualizer.keySet#'
                   	    , keyData = '#assetData.visualizer.keyData#'
                     </cfif>
                 <cfelse>
                 	, webURL = NULL, url = NULL, useVisualizer = 0
                 </cfif>
                 
                 
                 
                 <cfif isDefined('assetData.moodstocks')>
                 	<!--- moodstocks --->
                    <cfif assetData.moodstocks.key NEQ ''>
                    	, APIKey = '#assetData.moodstocks.key#' 
                    </cfif>
                    <cfif assetData.moodstocks.secret NEQ ''>
                    	, APISecret = '#assetData.moodstocks.secret#'
                    </cfif>
                    	, useMoodstocks = 1
                 <cfelse>
                 	, APIKey = NULL , APISecret = NULL, useMoodstocks = 0
                 </cfif>
                 
                 
                 <cfif isDefined('assetData.vuforia')>
                 
                 		<!--- vuforia --->
						<cfif assetData.vuforia.xml NEQ ''>
                        	, xml = '#assetData.vuforia.xml#' 
                        </cfif>
                        <cfif assetData.vuforia.dat NEQ ''>
                        	, dat = '#assetData.vuforia.dat#'
                        </cfif>

                        	, useVuforia = 1
                    
                 <cfelse>
                 	, xml = NULL, dat = NULL, useVuforia = 0
                 </cfif>
                 
                 WHERE (asset_id = #assetID#)
                  
            </cfquery>       
            
        <cfelse>
        
        	<!--- New Asset --->
            
            <!--- Move Asset --->
			<cfif StructKeyExists(assetData,"visualizer")>
            
            	<!--- image for target --->
                <cfif isDefined('assetData.visualizer.image')>
                
                    <!--- New File + Path --->
                    <cfset fileDestPath = assetPath & assetData.image.file>
                
                    <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                      <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                    </cfinvoke>
                    
                    <cfset assetData.url = ImageFileName>
                 </cfif>
             <cfelse>
             	
                <!--- webURL for target --->
             	      
             </cfif>
      
            <!--- New DB Entry --->
            <cfquery name="newAsset"> 
            	INSERT INTO MarkerAssets (
                asset_id, 

                 useVisualizer
				<cfif isDefined('assetData.visualizer.webURL')>
                    , webURL
                <cfelse>
                    , url
                </cfif>
                , keySet, keyData 
                
                , useMoodstocks, APIKey, APISecret
                , useVuforia ,xml, dat
                
                )
                
                VALUES (
                #assetData.assetID#
                
                <!--- Visualizer --->
                <cfif isDefined('assetData.visualizer')>
                	, 1
                	<cfif isDefined('assetData.visualizer.webURL')>
                		, '#assetData.visualizer.webURL#'
                    <cfelse>
                    	, '#assetData.visualizer.url#'
                    </cfif>
                    ,'#assetData.visualizer.keySet#'
                    , '#assetData.visualizer.keyData#'
                    
                <cfelse>
                	, 0, NULL, NULL, NULL
				</cfif>
                
                <!--- Moodstocks --->
                <cfif isDefined('assetData.moodstocks')>
					, 1, '#assetData.moodstocks.key#' ,'#assetData.moodstocks.secret#'
                <cfelse>
                	, 0, NULL, NULL
                </cfif>
                
                <!--- Vuforia --->
                <cfif isDefined('assetData.vuforia')>
					, 1, '#assetData.vuforia.xml#' ,'#assetData.vuforia.dat#'
                <cfelse>
                	, 0, NULL, NULL, NULL
                </cfif>
                )
            </cfquery>
        	
            <cfset assetID = assetData.assetID>
            
        </cfif>

        <cfreturn assetID>   
        
     </cffunction>

     


 
 
 
 
  	<!--- VIDEO TYPE 																																--->
     <cffunction name="updateVideoAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            
            <cfif StructKeyExists(assetData,"image") OR StructKeyExists(assetData,"video")>
            
            
			<!--- Placeholder Image 																--->
            <cfif StructKeyExists(assetData,"image")>
            
            	<!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.placeholder>
            
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.image.file>
                
				<!--- Move Asset --->
                <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.image.file = ImageFileName>
             </cfif>
             
       
              <!--- Video File 																		--->
              <cfif StructKeyExists(assetData,"video")>
            
            	<!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.url>
            
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.video>
                
				<!--- Move Asset --->
                <cfinvoke component="Assets" method="moveContent" returnvariable="VideoFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.video = VideoFileName>
             </cfif>
               
                <!--- Update DB Image--->
                <cfquery name="updateDetails">
                     UPDATE VideoAssets  
                    SET asset_id = #assetID#
					<cfif StructKeyExists(assetData,"video")>
                        ,url ='#assetData.video#', webURL = NULL, modified = #curDate#
                    </cfif>
                    <cfif StructKeyExists(assetData,"image")>
                        , placeholder = '#assetData.image.file#' , width = #assetData.image.size.width# , height = #assetData.image.size.height#
                    </cfif>
                    WHERE (asset_id = #assetID#)
                </cfquery>
                
            <cfelseif StructKeyExists(assetData,"webURL")>
            	
                <!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.url>
            
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
                <!--- Update DB URL Image--->
                <cfquery name="updateDetails">
                     UPDATE VideoAssets
                     SET webURL = '#assetData.webURL#' , URL = NULL, URL = NULL, modified = #curDate#
                     WHERE (asset_id = #assetID#)   
                </cfquery>
            
            </cfif>

        <cfelse>
        
        	<!--- New Asset --->
   
            <!--- Move Asset --->
			<cfif StructKeyExists(assetData,"image")>
            
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.image.file>
            
                <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.image.file = ImageFileName>
				 
				 <!--- Insert DB Image--->
                 <cfquery name="newAsset"> 
                    INSERT INTO VideoAssets (placeholder , width, height, asset_id)
                    VALUES ('#assetData.image.file#', #assetData.image.size.width#, #assetData.image.size.height#, #assetData.assetID#)
              	 </cfquery>
                 
             </cfif>
             
             <cfif StructKeyExists(assetData,"video")>
             
             	<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.video>
            
                <cfinvoke component="Assets" method="moveContent" returnvariable="videoFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.video = videoFileName>
				 
				 <!--- Insert DB Image--->
                 <cfquery name="newAsset"> 
                    INSERT INTO VideoAssets (url , asset_id, webURL, modified)
                    VALUES ('#assetData.video#', #assetData.assetID#, NULL, #curDate#)
              	 </cfquery>
             
             <cfelseif StructKeyExists(assetData,"webURL")>
                
                <!--- Insert DB URL Image--->
                <cfquery name="newAsset"> 
                    INSERT INTO VideoAssets (url, webURL ,asset_id, modified)
                    VALUES (NULL, '#assetData.webURL#', #assetData.assetID#, #curDate#)
          		</cfquery>
          
            </cfif>

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
 
 
 
 
 <!--- DOCUMENTS TYPE 																																--->
     <cffunction name="updateDocumentAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
       
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
     
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<!--- Update Asset --->
        	<cfset assetID = asset.asset_id>
            
            <cfif StructKeyExists(assetData,"pdf")>
            
            	<!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.url>
            	
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.pdf>
                
				<!--- Move Asset --->
                <cfinvoke component="Assets" method="moveContent" returnvariable="PDFFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.pdf = PDFFileName>
                
                <!--- Update DB Image--->
                <cfquery name="updateDetails">
                     UPDATE PDFAssets  
                     SET url = '#assetData.pdf#' , webURL = NULL
                     WHERE (asset_id = #assetID#)
            	</cfquery>
            
            <cfelseif StructKeyExists(assetData,"webURL")>
            	
                <!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & asset.url>
            
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
                <!--- Update DB URL Image--->
                <cfquery name="updateDetails">
                     UPDATE PDFAssets
                     SET webURL = '#assetData.webURL#' , URL = NULL
                     WHERE (asset_id = #assetID#)   
                </cfquery>
            
            </cfif>

        <cfelse>
        
        	<!--- New Asset --->
   
            <!--- Move Asset --->
			<cfif StructKeyExists(assetData,"pdf")>
            
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.pdf>
            
                <cfinvoke component="Assets" method="moveContent" returnvariable="PDFFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.pdf = PDFFileName>
				 
				 <!--- Insert DB Image--->
                 <cfquery name="newAsset"> 
                    INSERT INTO PDFAssets (url, asset_id, webURL)
                    VALUES ('#assetData.pdf#', #assetData.assetID#, NULL)
              	 </cfquery>
             
             
             <cfelseif StructKeyExists(assetData,"webURL")>
                
                <!--- Insert DB URL Image--->
                <cfquery name="newAsset"> 
                    INSERT INTO PDFAssets (url, webURL , asset_id)
                    VALUES (NULL,'#assetData.webURL#', #assetData.assetID#)
          		</cfquery>
          
            </cfif>

          	<cfset assetID = assetData.assetID>
        
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
     
     
     
     
     
     
     
     
     
     
     <!--- 3DMODEL TYPE 																																--->
     <cffunction name="update3DModelAsset" access="public" returntype="numeric" output="yes">
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
      
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>

        <cfset assetID = '0'>
   
        <!--- Assets Exists? --->
        <cfif asset.recordCount GT '0'>
        
        	<cfset assetID = asset.asset_id>
           
            <cfif StructKeyExists(assetData,"model3D")>
            
            	<!--- Old Asset Path --->
            	<!--- <cfset fileToDelete = assetPath & asset.url> --->
                
                <!--- Update Asset --->
                <cfswitch expression="#assetData.os#">
                
                    <cfcase value="1">
                    <!--- android --->
                    	<cfset fileToDelete = assetPath & asset.url_android>
                    </cfcase>
                    
                    <cfcase value="2">
                    <!--- OSX --->
                    	<cfset fileToDelete = assetPath & asset.url_osx>
                    </cfcase>
                    
                    <cfcase value="3">
                    <!--- Windows --->
                    	<cfset fileToDelete = assetPath & asset.url_windows>
                    </cfcase>
                    
                    <cfdefaultcase>
                    <!--- iOS --->
                    	<cfset fileToDelete = assetPath & asset.url>
                    </cfdefaultcase>
                
                </cfswitch>
                
            	<cfif fileToDelete NEQ ''>
				
                	<!--- Delete Old Asset --->
                    <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                      <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                    </cfinvoke>
                
                </cfif>
                
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.model3D>
                
				<!--- Move Asset --->
                <cfinvoke component="Assets" method="moveContent" returnvariable="ModelFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.model3D = ModelFileName>
            
            </cfif>
            
            <!--- ModDate --->
            <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="modDate" />
       		
             <cfif assetData.camera.position.x IS ''><cfset assetData.camera.position.x = 0></cfif>
             <cfif assetData.camera.position.y IS ''><cfset assetData.camera.position.y = 0></cfif>
             <cfif assetData.camera.position.z IS ''><cfset assetData.camera.position.z = 0></cfif>
             
             <cfif assetData.camera.target.x IS ''><cfset assetData.camera.target.x = 0></cfif>
             <cfif assetData.camera.target.y IS ''><cfset assetData.camera.target.y = 0></cfif>
             <cfif assetData.camera.target.z IS ''><cfset assetData.camera.target.z = 0></cfif>
             
             <cfif assetData.camera.control.pan IS ''><cfset assetData.camera.control.pan = 0></cfif>
             <cfif assetData.camera.control.zoom IS ''><cfset assetData.camera.control.zoom = 0></cfif>
             <cfif assetData.camera.control.rotation IS ''><cfset assetData.camera.control.rotation = 0></cfif>
       
            <!--- Update DB --->
            <cfquery name="updateDetails">
                 UPDATE Model3DAssets  
                 SET  
                 
                 scale = #assetData.model.scale#, 
                 loc_x = #assetData.model.position.x#, loc_y = #assetData.model.position.y#, loc_z = #assetData.model.position.z#,
                 rot_x = #assetData.model.rotation.x#, rot_y = #assetData.model.rotation.y#, rot_z = #assetData.model.rotation.z#,
                 
                 camPos_x = #assetData.camera.position.x#, camPos_y = #assetData.camera.position.y#, camPos_z = #assetData.camera.position.z#,
                 camTarget_x = #assetData.camera.target.x#, camTarget_y = #assetData.camera.target.y#, camTarget_z = #assetData.camera.target.z#,
                 camMin = #assetData.camera.limits.min#, camMax = #assetData.camera.limits.max#,
                 panMin = #assetData.camera.limits.panMin#, panMax = #assetData.camera.limits.panMax#, tiltMin = #assetData.camera.limits.tiltMin#, tiltMax = #assetData.camera.limits.tiltMax#,
                 
                 camPan = #assetData.camera.control.pan#, camZoom = #assetData.camera.control.zoom#, camRotation = #assetData.camera.control.rotation#,

                 
                 fov = #assetData.camera.fov#

                 <cfif StructKeyExists(assetData,"model3D")>
                 
                    <!--- 3DAsset --->
                    <cfswitch expression="#assetData.os#">
                    
                        <cfcase value="1">
                        <!--- android --->
                            ,url_android = '#assetData.model3D#'
                        </cfcase>
                        
                        <cfcase value="2">
                        <!--- OSX --->
                            ,url_osx = '#assetData.model3D#'
                        </cfcase>
                        
                        <cfcase value="3">
                        <!--- Windows --->
                            ,url_windows = '#assetData.model3D#'
                        </cfcase>
                        
                        <cfdefaultcase>
                        <!--- iOS --->
                            ,url = '#assetData.model3D#'
                        </cfdefaultcase>
                    
                    </cfswitch>
                 	
                    <!--- model changed - update modDate for model --->
                    , modifiedModel = #modDate#
                    
                 </cfif>
                 
                 WHERE (asset_id = #assetID#)
            </cfquery>

        <cfelse>
     
        	<!--- New Asset --->
            
   			<!--- ModDate --->
            <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="modDate" />
            
            <!--- Move Asset --->
			<cfif StructKeyExists(assetData,"model3D")>
            
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & assetData.model3D>
            
                <cfinvoke component="Assets" method="moveContent" returnvariable="ModelFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset assetData.model3D = ModelFileName>
			 
             <cfelse>
             	<cfset assetData.model3D = ''>
             </cfif>
             
             <!--- Set Defaults --->
             <cfif assetData.model.scale IS ''><cfset assetData.model.scale = 1></cfif>
             
             <cfif assetData.model.position.x IS ''><cfset assetData.model.position.x = 0></cfif>
             <cfif assetData.model.position.y IS ''><cfset assetData.model.position.y = 0></cfif>
             <cfif assetData.model.position.z IS ''><cfset assetData.model.position.z = 0></cfif>
             
             <cfif assetData.model.rotation.x IS ''><cfset assetData.model.rotation.x = 0></cfif>
             <cfif assetData.model.rotation.y IS ''><cfset assetData.model.rotation.y = 0></cfif>
             <cfif assetData.model.rotation.z IS ''><cfset assetData.model.rotation.z = 0></cfif>
             
             <cfif assetData.camera.position.x IS ''><cfset assetData.camera.position.x = 0></cfif>
             <cfif assetData.camera.position.y IS ''><cfset assetData.camera.position.y = 0></cfif>
             <cfif assetData.camera.position.z IS ''><cfset assetData.camera.position.z = 0></cfif>
             
             <cfif assetData.camera.target.x IS ''><cfset assetData.camera.target.x = 0></cfif>
             <cfif assetData.camera.target.y IS ''><cfset assetData.camera.target.y = 0></cfif>
             <cfif assetData.camera.target.z IS ''><cfset assetData.camera.target.z = 0></cfif>
             
             <cfif NOT isDefined("assetData.camera.limits.min")><cfset assetData.camera.limits.min = 0></cfif>
             <cfif NOT isDefined("assetData.camera.limits.max")><cfset assetData.camera.limits.max = 0></cfif>
             
             <cfif assetData.camera.limits.min IS ''><cfset assetData.camera.limits.min = 0></cfif>
             <cfif assetData.camera.limits.max IS ''><cfset assetData.camera.limits.max = 0></cfif>
             
             <cfif NOT isDefined("assetData.camera.limits.tiltMin")><cfset assetData.camera.limits.tiltMin = 0></cfif>
             <cfif NOT isDefined("assetData.camera.limits.tiltMax")><cfset assetData.camera.limits.tiltMax = 0></cfif>
             
             <cfif assetData.camera.limits.tiltMin IS ''><cfset assetData.camera.limits.tiltMin = 0></cfif>
             <cfif assetData.camera.limits.tiltMax IS ''><cfset assetData.camera.limits.tiltMax = 0></cfif>
             
             <cfif NOT isDefined("assetData.camera.limits.panMin")><cfset assetData.camera.limits.panMin = 0></cfif>
             <cfif NOT isDefined("assetData.camera.limits.panMax")><cfset assetData.camera.limits.panMax = 0></cfif>
             
             <cfif assetData.camera.limits.panMin IS ''><cfset assetData.camera.limits.panMin = 0></cfif>
             <cfif assetData.camera.limits.panMax IS ''><cfset assetData.camera.limits.panMax = 0></cfif>
             
             <cfif assetData.camera.fov IS ''><cfset assetData.camera.fov = 0></cfif>

             
			 <!--- Insert DB Model--->
             <cfquery name="newAsset"> 
                INSERT INTO Model3DAssets (
                
                <cfif isDefined("assetData.os")>
                <!--- 3DAsset --->
                <cfswitch expression="#assetData.os#">
                
                    <cfcase value="1">
                    <!--- android --->
                        url_android,
                    </cfcase>
                    
                    <cfcase value="2">
                    <!--- OSX --->
                        url_osx,
                    </cfcase>
                    
                    <cfcase value="3">
                    <!--- Windows --->
                        url_windows,
                    </cfcase>
                    
                    <cfdefaultcase>
                    <!--- iOS --->
                        url,
                    </cfdefaultcase>
                
                </cfswitch>
                
                </cfif>
                
                scale,  rot_x, rot_y, rot_z, loc_x, loc_y, loc_z, camPos_x, camPos_y, camPos_z, modifiedModel,
                
                <cfif isDefined("assetData.control")>
                camPan, camZoom, camRotation, 
                </cfif>
                
                camTarget_x, camTarget_y, camTarget_z, camMin, camMax, panMin, panMax, tiltMin, tiltMax, fov, modified, asset_id)
                VALUES (
                
                <cfif isDefined("assetData.os")>
                '#assetData.model3D#', 
                </cfif>
                
                #assetData.model.scale#,
                
                #assetData.model.rotation.x#, #assetData.model.rotation.y#, #assetData.model.rotation.z#,
                #assetData.model.position.x#, #assetData.model.position.y#, #assetData.model.position.z#,

                #assetData.camera.position.x#, #assetData.camera.position.y#, #assetData.camera.position.z#, #modDate#, 
                
				<cfif isDefined("assetData.control")>
                #assetData.camera.control.pan#, #assetData.camera.control.zoom#, #assetData.camera.control.rotation#,
				</cfif>
                
                #assetData.camera.target.x#, #assetData.camera.target.y#, #assetData.camera.target.z#,
                
                #assetData.camera.limits.min#, #assetData.camera.limits.max#, 
                #assetData.camera.limits.tiltMin#, #assetData.camera.limits.tiltMax#, 
                #assetData.camera.limits.panMin#, #assetData.camera.limits.panMax#, 
                
                #assetData.camera.fov#,

                #modDate#,
                
                #assetData.assetID#
                )
             </cfquery>

          	<cfset assetID = assetData.assetID>
            
        </cfif>

        <cfreturn assetID>       
        
     </cffunction>
 
 
 
   	<!--- DELETE 3D Model for Type --->
     <cffunction name="delete3DModel" access="remote" returntype="array" output="no">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
 		<cfargument name="assetTypeID" type="numeric" required="yes" default="0">
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfquery name="the3DAsset">
        	SELECT 
            <!--- 3DAsset --->
            <cfswitch expression="#assetTypeID#">
            
                <cfcase value="1">
                <!--- android --->
                    url_android
                </cfcase>
                
                <cfcase value="2">
                <!--- OSX --->
                    url_osx
                </cfcase>
                
                <cfcase value="3">
                <!--- Windows --->
                    url_windows
                </cfcase>
                
                <cfdefaultcase>
                <!--- iOS --->
                    url
                </cfdefaultcase>
            
            </cfswitch>
            AS url
            FROM	Model3DAssets
            WHERE	asset_id = #assetID# 
        </cfquery>
        
        <!--- Old Asset Path XML--->
		<cfset file3DToDelete = assetPath & the3DAsset.url>
        
        <!--- Delete Old Asset XML --->
        <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
          <cfinvokeargument name="fullAssetPath" value="#file3DToDelete#"/>
        </cfinvoke>
        
        <!--- ModDate --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="modDate" />
        
        <cfquery name="updateDetails">
                 UPDATE Model3DAssets  
                 SET    modifiedModel = #modDate#,
                 <!--- 3DAsset --->
                <cfswitch expression="#assetTypeID#">
                
                    <cfcase value="1">
                    <!--- android --->
                        url_android = NULL
                    </cfcase>
                    
                    <cfcase value="2">
                    <!--- OSX --->
                        url_osx = NULL
                    </cfcase>
                    
                    <cfcase value="3">
                    <!--- Windows --->
                        url_windows = NULL
                    </cfcase>
                    
                    <cfdefaultcase>
                    <!--- iOS --->
                        url = NULL
                    </cfdefaultcase>
                
                </cfswitch>
                 WHERE asset_id = #assetID#
        </cfquery>
 		
        <cfquery name="the3DAssets">
        	SELECT	url, url_android, url_osx, url_windows
            FROM	Model3DAssets
            WHERE	asset_id = #assetID#  
        </cfquery>
        
        <cfset the3DModels = [the3DAssets.url,the3DAssets.url_android,the3DAssets.url_osx,the3DAssets.url_windows]>
        
    	<cfreturn the3DModels>
    
    </cffunction>
    
    
    
      	<!--- DELETE 3D Model for Type --->
     <cffunction name="delete3DModelTarget" access="remote" returntype="boolean" output="no">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
 		<cfargument name="targetType" type="numeric" required="yes" default="0">
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfif targetType GT 0>
        
            <cfquery name="the3DAsset">
                SELECT 
                <!--- 3DAsset --->
                <cfswitch expression="#targetType#">
                
                    <cfcase value="1">
                    <!--- XML --->
                       ar_xml
                    </cfcase>
                    
                    <cfcase value="2">
                    <!--- DAT --->
                       ar_dat
                    </cfcase>
                
                </cfswitch>
                AS targetMap
                FROM	Model3DAssets
                WHERE	asset_id = #assetID# 
            </cfquery>
        
			<!--- Old Asset Path XML--->
            <cfset fileToDelete = assetPath & the3DAsset.targetMap>
            
            <!--- Delete Old Asset XML --->
            <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
              <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
            </cfinvoke>
        
            <cfquery name="updateDetails">
                     UPDATE Model3DAssets  
                     SET 
                     <!--- 3DAsset --->
                    <cfswitch expression="#targetType#">
                    
                        <cfcase value="1">
                        <!--- XML --->
                            ar_xml = NULL
                        </cfcase>
                        
                        <cfcase value="2">
                        <!--- DAT --->
                            ar_dat = NULL
                        </cfcase>
                    
                    </cfswitch>
                     WHERE asset_id = #assetID#
            </cfquery>

    		<cfreturn true>
    	
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>
    
    
 
 
  	<!--- DELETE Content --->
     <cffunction name="deleteContent" access="public" returntype="any" output="yes">
        <cfargument name="fullAssetPath" type="string" required="yes" default="">
 		
        <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
        	<cfinvokeargument name="fullPath" value="#fullAssetPath#"/>
        </cfinvoke>
        
        
        <cfset retina = fileSpecs.path & fileSpecs.file>
        <cfset nonretina = fileSpecs.path &"nonretina/"& fileSpecs.file>
		
        <cfset uniqueThread = CreateUUID()>

        <cfthread action="run" name="deletFile_#uniqueThread#" retina="#retina#">
			<cfif fileExists(retina)>
                <cffile action="delete" file="#retina#">
            </cfif>
        </cfthread>
        
        <cfset uniqueThread = CreateUUID()>
        <cfthread action="run" name="deletFile_#uniqueThread#" nonretina="#nonretina#">
			<cfif fileExists(nonretina)>
                <cffile action="delete" file="#nonretina#">
            </cfif>
        </cfthread>
        
        <cfreturn true>
 
 	</cffunction>
    
    
    
    
    
    

 
 
 
 
 <cffunction name="moveContent" access="public" returntype="string" output="yes">
    	
        <cfargument name="destPathFile" type="string" required="yes" default="structNew()">  
        <cfargument name="norename" type="boolean" required="no" default="false">

			<!---Temp Path--->
			
            <cfset srcPathFile = trim(GetTempDirectory() & GetFileFromPath(destPathFile))>
	
            <cfif norename>
            	<!--- Leave FileName --->
            <cfelse>
            
				<!---Make File Unique--->
                <cfinvoke component="CFC.File" method="makeFileUnique" returnvariable="destPathFile">          
                    <cfinvokeargument name="fullPath" value="#destPathFile#"/>
                </cfinvoke>
				
            </cfif>
            
            <cfset destPathFile = trim(destPathFile)>
        	
            <!--- Get File Info --->
            <cfinvoke component="CFC.File" method="getFileSpecs" returnvariable="fileSpecs">
                <cfinvokeargument name="fullPath" value="#destPathFile#"/>
            </cfinvoke>
            
            <cfset ext = trim(fileSpecs.ext)>
            
            <!--- Create Directory if NOT exist --->
            <cfset destPath = GetDirectoryFromPath(destPathFile)>
            
            <cfif NOT directoryExists(destPath)>
                <cfdirectory action="create" directory="#destPath#" mode="777">
            </cfif>
            
            <!---Move Image--->
            <cfif fileExists(srcPathFile)>
            	<cffile action="copy" source="#srcPathFile#" destination="#destPathFile#" nameconflict="overwrite">
            </cfif>
            
            <!--- Create Non Retina --->
            <cfif ext IS 'jpeg'  OR ext IS 'jpg' OR ext IS 'png'>

                <!---Check Path--->
                <cfset nonRetinaPath = GetDirectoryFromPath(destPathFile) & "nonretina/">
  				
				<!--- Create Directory if NOT exist --->
                <cfif NOT directoryExists(nonRetinaPath)>
                    <cfdirectory action="create" directory="#nonRetinaPath#" mode="777">
                </cfif>
                
                <!--- Copy and Scale Image 1/2 --->
                <cfset nonRetinaDest = nonRetinaPath & fileSpecs.file>

                <cfinvoke component="CFC.Misc" method="createNonRetinaImage" returnvariable="scaled">
                    <cfinvokeargument name="imageSrc" value="#srcPathFile#"/>
                    <cfinvokeargument name="imageDes" value="#nonRetinaDest#"/>
                </cfinvoke>
			
            </cfif>
          	
            <!--- delete file is exists at tmp --->
            <cfset sleep(250)> 
            <cfif fileExists(srcPathFile)>
            	<!--- <cffile action = "delete" file = "#trim(srcPathFile)#"> --->
            </cfif>
            
            <cfset destFileName = GetFileFromPath(destPathFile)>
           
    	<cfreturn destFileName>

 </cffunction>
 
 
 
     
    <!---Update Details																																--->
    <cffunction name="updateDetails" access="public" returntype="boolean" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        <cfargument name="detailData" type="struct" required="no" default="structNew()">

        <cfif isDefined("assetData.groupID")>
     
			<!---Get Group Asset, Details and Thumbs--->
            <cfinvoke component="Modules" method="getAllGroupDetails" returnvariable="details">          
                    <cfinvokeargument name="groupID" value="#assetData.groupID#"/>
             </cfinvoke>
      
        <cfelse>
       
			<!---Get Asset, Details and Thumbs--->
            <cfinvoke component="Assets" method="getAllAssetDetails" returnvariable="details">          
                    <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
             </cfinvoke>
             
		</cfif>
 
		<!---Details Exists--->
        <cfset detailID = details.detail_id>
        <cfif detailID IS ""><cfset detailID = '0'></cfif>
          
        <!---No Details--->
		<cfif structIsEmpty(detailData)>
        
            <cfif detailID GT '0'>
				
                <cfif isDefined("assetData.groupID")>
                	
                    <!---Update Group Asset for DetailID to NULL--->
                    <cfquery name="updateDetails">
                        UPDATE Groups
                        SET detail_id = NULL
                        WHERE group_id = #groupID#
                    </cfquery>
                    
                <cfelse>
                  
					<!---Update Asset for DetailID to NULL--->
                    <cfquery name="updateDetails">
                        UPDATE Assets
                        SET detail_id = NULL
                        WHERE asset_id = #assetID#
                    </cfquery>
                
				</cfif>
                
				<!---Delete Detail--->
                <cfquery name="deleteDetails">
                    DELETE FROM Details
                    WHERE detail_id = #detailID#
                </cfquery>
                
            </cfif>
        
        <cfelse>
        
			<!---Details--->
            <cfif detailID GT '0'>
            
				<!---Details Exists--->
                <cfquery name="UpdateAssetDetails">
                        UPDATE Details
                        SET 
                        title ='#detailData.title#'
                        ,subtitle ='#detailData.subtitle#'
                        ,description ='#detailData.description#'
                        ,other ='#detailData.other#'
                        
                        ,titleColor ='#detailData.titleColor#'
                        ,subtitleColor ='#detailData.subtitleColor#'
                        ,descriptionColor ='#detailData.descriptionColor#'
                        
                        WHERE detail_id = #detailID#
                </cfquery>
                    
            <cfelse>
            
            	<!---No Details Exists Insert New Record--->
                <cfquery name="newDetail"> 
                    INSERT INTO Details (title, subtitle, description, other, titleColor, subtitleColor, descriptionColor)
                    VALUES ('#detailData.title#', '#detailData.subtitle#', '#detailData.description#', '#detailData.other#', '#detailData.titleColor#', '#detailData.subtitleColor#', '#detailData.descriptionColor#')
                    SELECT @@IDENTITY AS detailID
                </cfquery>
            
                <cfset detailID = newDetail.detailID>
            	
                <cfif isDefined("assetData.groupID")>
                	
					<!---Update Group Assets with New DetailID--->
                    <cfquery name="updateAsset">
                        UPDATE Groups
                        SET detail_id = #detailID#
                        WHERE group_id = #groupID#
                    </cfquery>

                <cfelse>
                
					<!---Update Assets with New DetailID--->
                    <cfquery name="updateAsset">
                        UPDATE Assets
                        SET detail_id = #detailID#
                        WHERE asset_id = #assetID#
                    </cfquery>
                    
                </cfif>
            
            </cfif>
                
      </cfif>

    <cfreturn true>
    
    </cffunction> 
    
    
        
    <!---Update Thumbnail																									--->																		
    <cffunction name="updateThumbnail" access="public" returntype="boolean" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        <cfargument name="thumbData" type="struct" required="no" default="structNew()">


        <cfif isDefined("assetData.groupID")>
       		
            <cfif assetData.groupID IS 0><cfreturn false></cfif>
            
            
			<!---Get Group Asset, Details and Thumbs--->
            <cfinvoke component="Modules" method="getAllGroupDetails" returnvariable="details">          
                    <cfinvokeargument name="groupID" value="#assetData.groupID#"/>
             </cfinvoke>
             
             <!--- If GroupID then get AppID --->
             <cfquery name="apps">
                SELECT	app_id 
                FROM	Groups
                WHERE	group_id = #assetData.groupID#
             </cfquery>
             
             <cfset appID = apps.app_id>
             
			 <!--- Get Group Path --->
            <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
              <cfinvokeargument name="appID" value="#appID#"/>
              <cfinvokeargument name="createFolder" value="true"/>
              <cfinvokeargument name="images" value="true"/>
            </cfinvoke>
            
            <cfset assetPath = assetPath & "thumbs/">

        <cfelse>
        
			<!---Get Asset, Details and Thumbs--->
            <cfinvoke component="Assets" method="getAllAssetDetails" returnvariable="details">          
                    <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
             </cfinvoke>
             
			 <!--- Get Asset Path --->
            <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
              <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
              <cfinvokeargument name="createFolder" value="true"/>
              <cfinvokeargument name="thumbs" value="true"/>
            </cfinvoke>
         
		</cfif>

        <!---Details NOT Exists--->
        <cfif details.recordCount IS '0'>
        	<cfreturn false>
        </cfif>
        
        
        <!---Details Exists--->
        <cfset thumbID = details.thumb_id>
        <cfif details.thumb_id IS ''><cfset thumbID = '0'></cfif>
       
     	<!--- If No Thumbs then Delete Thumbs --->
        <cfif structIsEmpty(thumbData)>

        	<!---Delete Thumbnail--->
            
            <!--- Old Asset Path --->
			<cfset fileToDelete = assetPath & details.thumbnail>
            	
			<!--- Delete Old Asset --->
            <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
              <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
            </cfinvoke>
       
			 <cfif isDefined("assetData.groupID")>
             
                <!---Null Thumb_id in Group Asset table--->
                  <cfquery name="updateAsset">
                      UPDATE Groups
                      SET thumb_id = NULL
                      WHERE group_id = #groupID#
                  </cfquery>
                
             <cfelse>
             
                 <!---Null Thumb_id in Asset table--->
                  <cfquery name="updateAsset">
                      UPDATE Assets
                      SET thumb_id = NULL
                      WHERE asset_id = #assetID#
                  </cfquery>
             
             </cfif>
                 
			 <!---Delete thumbnails table Entry---> 
             <cfquery name="deleteThumb">
                DELETE FROM Thumbnails
                WHERE thumb_id = #thumbID#
             </cfquery>
   		
        <cfelse>

	<cfif NOT structIsEmpty(thumbData)>

        <cfif thumbData.thumb.file NEQ ''>

               
            <!---If thumbID IS 0 Create new Thumb entry--->
 
            <cfif thumbID GT '0'>
            
				<!--- Old Asset Path --->
            	<cfset fileToDelete = assetPath & details.thumbnail>
            	
            	<!--- Delete Old Asset --->
                <cfinvoke component="Assets" method="deleteContent" returnvariable="deleted">
                  <cfinvokeargument name="fullAssetPath" value="#fileToDelete#"/>
                </cfinvoke>
                
				<!--- New File + Path --->
                <cfset fileDestPath = assetPath & thumbData.thumb.file>
                
				<!--- Move Asset --->
                <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
                  <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                </cfinvoke>
                
                <cfset thumbData.thumb.file = ImageFileName>
               
				<!---Update Thumb--->
                <cfquery name="UpdateThumb">
                    UPDATE Thumbnails
                    SET image ='#thumbData.thumb.file#', width = #thumbData.thumb.size.width#, height = #thumbData.thumb.size.height#
                    WHERE thumb_id = #thumbID#
                </cfquery>
                
            <cfelse>
				
                <cfset thumbImagePath = assetPath & thumbData.thumb.file>
                
                <!---Transfer Thumbnail image - if exists--->
                <cfinvoke component="Assets" method="moveContent" returnvariable="imageFileName">
                  <cfinvokeargument name="destPathFile" value="#thumbImagePath#"/>
                </cfinvoke>
                
                <cfset thumbData.thumb.file = imageFileName>
            
                <!---New Thumb--->
                <cfquery name="newThumb"> 
                    INSERT INTO Thumbnails (image, width, height)
                    VALUES ('#thumbData.thumb.file#', #thumbData.thumb.size.width#, #thumbData.thumb.size.height#)
                    SELECT @@IDENTITY AS thumbID
                </cfquery>
                
                <cfset thumbID = newThumb.thumbID>
                
                <cfif isDefined("assetData.groupID")>
                	
                    <!---Upadte Group Asset with New Thumb--->
                    <cfquery name="addThumbToAsset">
                        UPDATE Groups
                        SET thumb_id = #thumbID#
                        WHERE group_id = #groupID#
                    </cfquery>
                
                <cfelse>
                
					<!---Upadte Asset with New Thumb--->
                    <cfquery name="addThumbToAsset">
                        UPDATE Assets
                        SET thumb_id = #thumbID#
                        WHERE asset_id = #assetID#
                    </cfquery>
                    
                </cfif>
                
            </cfif>
           
            
        </cfif>
      </cfif>  
  
  	</cfif>
    
    <cfreturn true>
    
    </cffunction>    
  
  
  
  
<!---Update Colors																																--->
    <cffunction name="updateColors" access="public" returntype="boolean" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        <cfargument name="colorData" type="struct" required="no" default="structNew()">
	
        <cfset deleteBK = false>
        
        <cfif isDefined('colorData.background')>
			<cfif colorData.background.file IS ''>
                <cfset structDelete(colorData,'background')>
            </cfif>
        </cfif>
        
        <cfif isDefined("assetData.groupID")>
     
			<!---Get Group Asset, Details and Thumbs--->
            <cfinvoke component="Modules" method="getAllGroupDetails" returnvariable="colors">          
                    <cfinvokeargument name="groupID" value="#assetData.groupID#"/>
             </cfinvoke>
             
      		<!--- get path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
              <cfinvokeargument name="groupID" value="#assetData.groupID#"/>
            </cfinvoke>
            
            <cfinvoke component="File" method="folderExists" returnvariable="folderExists">
              <cfinvokeargument name="groupID" value="#assetData.groupID#"/>
              <cfinvokeargument name="createFolder" value="true"/>
            </cfinvoke>
            
        <cfelse>
        
			<!--- Get Asset, Details, Thumbs and Colors --->
            <cfinvoke component="Assets" method="getAllAssetDetails" returnvariable="colors">          
                    <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
             </cfinvoke>
             
             <!--- get path --->
             <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
              <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
            </cfinvoke>
            
            <cfinvoke component="File" method="folderExists" returnvariable="folderExists">
              <cfinvokeargument name="assetID" value="#assetData.assetID#"/>
              <cfinvokeargument name="createFolder" value="true"/>
            </cfinvoke>
             
		</cfif>

		<!---Colors Exists--->
        <cfset colorID = colors.color_id>
        <cfif colorID IS ""><cfset colorID = '0'></cfif>
	   
       <cfif colorID GT '0'>
       
       	<cfif isDefined('colorData.background')>
			<!--- background --->
            <cfquery name="colorInfo">
                SELECT background
                FROM Colors
                WHERE color_id = #colorID#
            </cfquery>
            <!--- check if bk exists --->
            <cfif colorInfo.recordCount GT 0>
              
              <!--- delete image bk --->
              <cfset deleteBK = true> 
              
            <cfelse>
              <cfset bk = ''>
            </cfif>
            
        </cfif>
           
        </cfif>
        
        <!---No Details--->
		<cfif structIsEmpty(colorData)>

            <cfif colorID GT '0'>
	 		
                <cfif isDefined("assetData.groupID")>
           	
                    <!---Update Group Asset for DetailID to NULL--->
                    <cfquery name="updateColors">
                        UPDATE Groups
                        SET color_id = NULL
                        WHERE group_id = #groupID#
                    </cfquery>
                    
                    <!--- delete image bk --->
              		<cfset deleteBK = true> 
                    
                <cfelse>
                
					<!---Update Asset for DetailID to NULL--->
                    <cfquery name="updateColors">
                        UPDATE Assets
                        SET color_id = NULL
                        WHERE asset_id = #assetID#
                    </cfquery>
                
				</cfif>
        
				<!---Delete Detail--->
                <cfquery name="deleteColors">
                    DELETE FROM Colors
                    WHERE color_id = #colorID#
                </cfquery>
                
                <!--- delete image bk --->
              <cfset deleteBK = true> 
                
            </cfif>
        
        <cfelse>
        
			<!---Colors--->
            <cfif colorID GT '0'>
            	
                <!--- New File + Path --->
                <cfif isDefined('colorData.background')>
					<cfset fileDestPath = assetPath & colorData.background.file>
    
                    <cfif colorData.background.file NEQ ''>
                                                                                                                            
                        <!---Transfer BackgroundImage - if exists--->
                        <cfinvoke component="Assets" method="moveContent" returnvariable="imageFileName">
                          <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                        </cfinvoke>
                        
                        <cfset colorData.background.file = imageFileName>
                        
                    </cfif>
                </cfif>
            
				<!---Colors Exists--->
                <cfquery name="UpdateAssetDetails">
                        UPDATE Colors
                        SET forecolor ='#colorData.forecolor#', backcolor ='#colorData.backcolor#', othercolor ='#colorData.othercolor#'
                        <cfif isDefined('colorData.background')>
							<cfif colorData.background.file NEQ ''>
                            , background = '#colorData.background.file#'
                            </cfif>
                        </cfif>
                        WHERE color_id = #colorID#
                </cfquery>

            <cfelse>
          		
                <!--- New File + Path --->
                <cfif isDefined('colorData.background')>
                	<cfset fileDestPath = assetPath & colorData.background.file>
                </cfif>
                
            	<!---No Details Exists Insert New Record--->
                <cfquery name="newColors"> 
                    INSERT INTO Colors (forecolor, backcolor, otherColor
                    <cfif isDefined('colorData.background')>
                    , background
                    </cfif>
                    )
                    VALUES ('#colorData.forecolor#', '#colorData.backcolor#', '#colorData.othercolor#'
                    <cfif isDefined('colorData.background')>
                    , '#colorData.background.file#'
                    </cfif>
                    )
                    SELECT @@IDENTITY AS colorID
                </cfquery>
            	
                <cfif isDefined('colorData.background')>
					<cfif colorData.background.file NEQ ''>
                    
                        <!---Transfer BackgroundImage - if exists--->
                        <cfinvoke component="Assets" method="moveContent" returnvariable="imageFileName">
                          <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
                        </cfinvoke>
                        
                    </cfif>
                </cfif>
                
                <cfset colorID = newColors.colorID>
            	
                <cfif isDefined("assetData.groupID")>
                	
					<!---Update Group Assets with New DetailID--->
                    <cfquery name="updateAsset">
                        UPDATE Groups
                        SET color_id = #colorID#
                        WHERE group_id = #groupID#
                    </cfquery>

                <cfelse>
                
					<!---Update Assets with New DetailID--->
                    <cfquery name="updateAsset">
                        UPDATE Assets
                        SET color_id = #colorID#
                        WHERE asset_id = #assetID#
                    </cfquery>
                    
                </cfif>
            
            </cfif>
                
      </cfif>
     
     <cfif isDefined('colorInfo')>
      <cfif deleteBK>
		  <cfset bk = assetPath & colorInfo.background>
          <cfset bkNonRetina = assetPath &'nonretina/'& colorInfo.background>
         
          <!--- delete image bk --->
          <cfif fileExists(bk)>
              <cffile action="delete" file="#bk#">
          </cfif>  
          <cfif fileExists(bkNonRetina)>
              <cffile action="delete" file="#bkNonRetina#">
          </cfif>
		</cfif>
      </cfif>
        
    <cfreturn true>
    
    </cffunction> 
  
  
  
  
  
  
   <!---Update An Asset--->
    <cffunction name="updateAssetDetails" access="public" returntype="numeric" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        <cfargument name="detailData" type="struct" required="no" default="structNew()">
        <cfargument name="thumbData" type="struct" required="no" default="structNew()">
		<cfargument name="colorData" type="struct" required="no" default="structNew()">
   
		<!--- If AssetID = 0 then Create New Asst --->
        <cfif assetData.assetID IS '0'>
            
            <cfinvoke component="Assets" method="createAssetDetails" returnvariable="assetID">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>
              
			<cfset assetData.assetID = assetID>
			
            <!--- Get Asset Name --->
            <cfinvoke component="Assets" method="getAssetName" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            
            <cfset assetTypeID = asset.assetType_id>
            
            <cflocation url="AssetsView.cfm?assetID=#assetID#&assetTypeID=#assetTypeID#" addtoken="no">
        
        <cfelse>

			 <!---Update Details--->
            <cfinvoke component="Assets" method="updateDetails" returnvariable="updatedDetails">
                <cfinvokeargument name="assetData" value="#assetData#"/>
                <cfinvokeargument name="detailData" value="#detailData#"/>
            </cfinvoke>
            
            <!---Update Thumbnail--->
            <cfinvoke component="Assets" method="updateThumbnail" returnvariable="updatedThumbnail">
                <cfinvokeargument name="assetData" value="#assetData#"/>
                <cfinvokeargument name="thumbData" value="#thumbData#"/>
            </cfinvoke>
          
            <!---Update Asset Details--->
            <cfinvoke component="Assets" method="updateAsset" returnvariable="updatedAsset">
                <cfinvokeargument name="assetData" value="#assetData#"/>
            </cfinvoke>
            
            <!---Update Asset Colors--->
            <cfinvoke component="Assets" method="updateColors" returnvariable="updatedAsset">
            	<cfinvokeargument name="assetData" value="#assetData#"/>
                <cfinvokeargument name="colorData" value="#colorData#"/>
            </cfinvoke>
        
        </cfif>
        
        <!--- Get Asset Name --->
        <cfinvoke component="Assets" method="getAssetName" returnvariable="asset">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
     
        <!--- Update Group Asset Modified Date --->
        <cfinvoke component="Modules" method="updateGroupAssetModifiedDate" returnvariable="deleted">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>

        <cfset assetTypeID = asset.assetType_id>
    
        <cfreturn assetTypeID>
        
    </cffunction>
    
    



   <!---Create A New Asset--->
    <cffunction name="createAssetDetails" access="public" returntype="numeric" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
 
        <cfset assetID = '0'>
        
        <!--- Get Epoch --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!---Get Asset Table--->
        <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
        	<cfinvokeargument name="assetID" value="#assetData.assetID#"/>
        </cfinvoke>
        
        <!--- Insert New Record in DB Type --->
        <cfquery name="newAsset"> 
            INSERT INTO Assets (assetType_id, created, modified, app_id)
            VALUES (#assetData.assetTypeID#, #curDate#, #curDate#, #assetData.appID#)
            SELECT @@IDENTITY AS assetID
        </cfquery>
        
        <cfset assetID = newAsset.assetID>
        
        <cfreturn assetID>
        
    </cffunction>


    
    
    <!---Delete Asset--->
    <cffunction name="deleteListOfAssetDetails" access="public" returntype="boolean" output="yes">
    	
        <cfargument name="assetIDs" type="array" required="yes" default="0">
        
        <cfloop index="z" from="1" to="#arrayLen(assetIDs)#">
        	
            <cfset theAssetID = assetIDs[z]>
        
            <cfinvoke component="Assets" method="deleteAssetDetails" returnvariable="deleted">
                <cfinvokeargument name="assetID" value="#theAssetID#"/>
            </cfinvoke>
        
        </cfloop>
        
        <cfreturn true>
        
    </cffunction>
  
  
  
    <!---Delete Asset--->
    <cffunction name="deleteAssetDetails" access="remote" returntype="boolean" output="yes">
    	
        <cfargument name="assetID" type="numeric" required="yes" default="0">

        <!---Get Asset Table--->
        <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
        	<cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <!--- Get Asset --->
        <cfinvoke component="Assets" method="getAssets" returnvariable="assetDetials">
        	<cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
        	<cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <!--- Build Asset Path --->
        <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
<!---         AssetDetails<br>
        <cfdump var="#assetDetials#"><p>
        Asset<br>
		<cfdump var="#asset#"><p>
        Table<br>
        <cfdump var="#assetTable#"><br><br> --->

        <!--- Assets to Delete --->
        <cfset assetObjects = arrayNew(1)>
        
        <cfif assetDetials.recordCount GT '0' AND assetDetials.thumbnail NEQ ''>

			<!--- Thumbs --->
            <cfset arrayAppend(assetObjects, assetPath & "thumbs/" & assetDetials.thumbnail)>
            <cfset arrayAppend(assetObjects, assetPath & "thumbs/nonretina/" & assetDetials.thumbnail)>
    
            <cfquery name="deleteThumb">
                DELETE FROM Thumbnails
                WHERE thumb_id = #assetDetials.thumb_id#
            </cfquery>

        </cfif>


		<!--- Check if Asset needs to be deleted from Assets or Actions --->
        
        <cfquery name="AssetAction">
            SELECT asset_id, content_id
            FROM GroupAssets
            WHERE content_id = #assetID#
        </cfquery>
        
        <cfif AssetAction.recordCount GT 0>
        
			<cfoutput query="AssetAction">
            
                <!--- Delete Asset/Actions --->
                <cfquery name="deleteAssetAction">
                    DELETE FROM GroupAssets
                    WHERE content_id = #assetID#
                </cfquery>
            
            </cfoutput>
        
        </cfif>

        <!--- Delete Details --->
        <cfif assetDetials.detail_id NEQ ''>
        
            <cfquery name="deleteDetails">
                DELETE FROM Details
                WHERE detail_id = #assetDetials.detail_id#
            </cfquery>
            
		</cfif>
        
        

		<!--- List of File Assets to Delete --->
        <cfswitch expression="#assetTable#">
            
            <!--- Quiz --->    
            <cfcase value="QuizAssets"> 
	             
                <cfset selectionID = asset.selection_id>

				<!--- Delete Chcoices --->
                <cfquery name="allChoices">
                    SELECT choice_id, url
                    FROM ChoiceAssets
                    WHERE selection_id = #selectionID#
                </cfquery>
                
                <cfloop query="allChoices">
                
                <!--- Assets --->
                <cfset arrayAppend(assetObjects, assetPath & url)>
                <cfset arrayAppend(assetObjects, assetPath & "nonretina/" & url)>
                	
                    <cfset choiceID = choice_id>
                    
					<!--- Remove Choices --->
                    <cfinvoke component="Quiz" method="removeQuizChoice" returnvariable="deletedChoice">
                        <cfinvokeargument name="selectionID" value="#selectionID#"/>
                        <cfinvokeargument name="choiceID" value="#choiceID#"/>
                        <cfinvokeargument name="deleteAll" value="true"/>
                    </cfinvoke>
                    
                </cfloop>
                
                <!--- Delete Selection --->
                <cfquery name="SelectionAsset">
                    DELETE FROM SelectionAssets
                    WHERE selection_id = #selectionID#
                </cfquery>
                
                
            </cfcase>
            
            <!--- Image --->
            <cfcase value="ImageAssets">  
	
			<cfif asset.recordCount GT '0' AND asset.url NEQ ''>

                <!--- Assets --->
                <cfset arrayAppend(assetObjects, assetPath & asset.url)>
                <cfset arrayAppend(assetObjects, assetPath & "nonretina/" & asset.url)>
    
            </cfif>
    
            </cfcase>
            
            <!--- Video --->
            <cfcase value="VideoAssets">
	
            <cfif asset.recordCount GT '0' AND asset.url NEQ ''>

                <!--- Assets --->
                <cfset arrayAppend(assetObjects, assetPath & asset.url)>
                <cfset arrayAppend(assetObjects, assetPath & asset.placeholder)>
                <cfset arrayAppend(assetObjects, assetPath & "nonretina/" & asset.placeholder)>

        	</cfif>
            
            </cfcase>
            
            <!--- Documents --->
            <cfcase value="PDFAssets">
				
                <!--- Assets --->
                <cfset arrayAppend(assetObjects, assetPath & asset.url)>
                
            </cfcase>
            
            <!--- URL --->
            <cfcase value="URLAssets">
				
            </cfcase>
            
            <!--- GPS Location --->    
            <cfcase value="GPSAssets"> 

                
            </cfcase>
            
            <!--- 3D Model --->
            <cfcase value="Model3DAssets">
            
				<!--- Assets --->
                <cfset arrayAppend(assetObjects, assetPath & asset.url)>
                
            </cfcase>
            
            <!--- Panorama --->    
            <cfcase value="PanoramaAssets"> 
                
                <!--- Assets --->
                <cfset allImages = "back, front, down, left, right, up, pano">
                
                <cfloop index="z" list="#allImages#" delimiters=",">
                
                    <cfset theAsset = Evaluate("asset.#trim(z)#")>
            
                    <cfset arrayAppend(assetObjects, assetPath & theAsset)>
                    <cfset arrayAppend(assetObjects, assetPath & "nonretina/" & theAsset)>
                
                </cfloop>
                
                
            </cfcase>
            
            <!--- Balcony --->    
            <cfcase value="BalconyAssets"> 
 
  				<!--- Assets --->
                <cfset allImages = "north, south, west, east">
                
                <cfloop index="z" list="#allImages#" delimiters=",">
                
                    <cfset theAsset = Evaluate("asset.#trim(z)#")>
            
                    <cfset arrayAppend(assetObjects, assetPath & theAsset)>
                    <cfset arrayAppend(assetObjects, assetPath & "nonretina/" & theAsset)>
                
                </cfloop>
  
            </cfcase>
              
            <!--- Nothing Error --->
            <cfdefaultcase>
            	Error!!!!
            </cfdefaultcase>
            
        </cfswitch>
        
        <!--- Delete All Assets --->
        <cfloop index="z" from="1" to="#arrayLen(assetObjects)#">
        
			<cfset anAsset = assetObjects[z]>
            
            <cfif fileExists(anAsset)>
                <cffile action="delete" file="#anAsset#">
            </cfif>      
        
        </cfloop>
        
        <!--- Delete Asset from Group If Exists --->
        <cfinvoke component="Modules" method="deleteAssetFromGroups" returnvariable="deleted">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
         <!--- Delete Asset --->
        <cfquery name="deleteAsset">
            DELETE FROM Assets
            WHERE asset_id = #assetID#
        </cfquery>
        
        <!--- Delete Asset --->
        <cfquery name="deleteAsset">
            DELETE FROM #assetTable#
            WHERE asset_id = #assetID#
        </cfquery>
        
         
        <cfreturn true>
        
    </cffunction>      
    
	
		<!---Get Asset Info--->
    <cffunction name="getAssetInfo" access="public" returntype="struct">
   
        <cfargument name="assetID" type="numeric" required="no" default="0">
			
        <!--- Get Asset Data --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="Misc" method="QueryToStruct" returnvariable="info">
            <cfinvokeargument name="query" value="#asset#"/>
        </cfinvoke>
        
        <!--- Get Details and Thumbs Data --->
        <cfinvoke component="Assets" method="getAssets" returnvariable="details">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="Misc" method="QueryToStruct" returnvariable="other">
            <cfinvokeargument name="query" value="#details#"/>
        </cfinvoke>
        
        <!--- Merge Data --->
        <cfset structAppend(info,other)>
        
        <cfreturn info>

        
    </cffunction>
	
	
	<!---Asset Types--->
    <cffunction name="getAssetsTypes" access="public" returntype="query">
    	
        <cfargument name="AssetTypeID" type="numeric" required="no" default="0">
        <cfargument name="AssetTypeName" type="string" required="no" default="">

    	<cfquery name="allAssetsTypes"> 
    		SELECT	assetType_id AS type, path, name, icon, categoryType
    		FROM	AssetTypes
            WHERE   0=0
            
            <cfif AssetTypeID GT 0>
            
            	AND assetType_id = #AssetTypeID#
                
            <cfelseif AssetTypeName NEQ ''>
            
            	AND name = '#AssetTypeName#'
            	
            </cfif>
            AND active = 1
            ORDER BY categoryType, sortOrder 
    	</cfquery>
        
        <cfreturn allAssetsTypes>
        
    </cffunction>
    
    
 
 
 
 
     <!---Get DBTable for Asset--->
    <cffunction name="getAssetTable" access="public" returntype="string" description="Returns the Table used for an asset.">
    	
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
		<cfquery name="assetTable"> 
            SELECT        AssetTypes.dbTable 
            FROM          Assets INNER JOIN 
            			  AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id
            WHERE        (Assets.asset_id = #assetID#)
		</cfquery>
        
        <cfreturn assetTable.dbTable>
        
    </cffunction>  
    
    
    
    
    
     <!---Get DBTable for Asset--->
    <cffunction name="getAssetsViewed" access="public" returntype="numeric" description="Returns Total Assets View">
    	<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
		<cfquery name="assetsViewed"> 
            SELECT        count(content_id) AS totalAssets
            FROM          Tracking
            WHERE        0=0
            <cfif clientID GT '0'>
            	AND client_id = #clientID#
            </cfif>
            <cfif appID GT '0'>
            	AND app_id = #appID#
            </cfif>
            <cfif userID GT '0'>
            	AND user_id = #userID#
            </cfif>
            
		</cfquery>
        
        <cfreturn assetsViewed.totalAssets>
        
    </cffunction>  
    
    
    
    
    <!--- Remove Asset/Action from Asset --->
 	<cffunction name="removeAssetAction" access="remote" returntype="numeric">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
		<cfargument name="contentIDs" type="array" required="no" default="#arrayNew(1)#">
       <cfargument name="action" type="numeric" required="no" default="0">
       
       <cfset result = 0>
	   
	   <cfif assetID GT 0 AND arrayLen(contentIDs) GT 0>
       
        <cfquery name="content"> 
        	SELECT	asset_id, content_id, action
            FROM	GroupAssets
            WHERE	asset_id = #assetID# AND action = #action# AND content_id IN(
                
                <cfloop index="assetID" array="#contentIDs#">
                     #assetID#,
                </cfloop>
                0)
        </cfquery>

        <cfif content.recordCount GT 0>
       
            <cfloop query="content">
           
                <cfquery name="removecontent">
                    DELETE FROM	GroupAssets
                    WHERE	asset_id = #content.asset_id# AND action = #content.action# AND content_id = #content.content_id#
                </cfquery>
            
            </cfloop>
   
        </cfif>
        
        <cfset result = content.recordCount>
        
        </cfif>
        
        <cfreturn result>
        
    </cffunction>
    
     
     
     
 <!---Get Asset URL Image --->
    <cffunction name="getAssetsImage" access="public" returntype="struct" description="Get Asset Image + Thumb">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
          
         <cfif assetID IS '0'>
         	<cfreturn {"assetTypeID":0, "url":'', "name":''}>
         <cfelse>
         
			<!--- Asset Details --->
            <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
     
            <cfinvoke component="Assets" method="getAssets" returnvariable="assetInfo">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
    
            <!--- Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="server" value="true"/>
            </cfinvoke>
            
            <cfset imagePath = "">
            <cfset imageThumb = assetInfo.thumbnail>
            <cfset assetTypeID = assetInfo.assetType_id>
            
            <cfif assetTypeID IS 1>
            
                <!--- 2D Image --->
                <cfset assetImage = asset.url>
                <cfset imagePath = assetPath & assetImage>
    
            <cfelse>
            
                <!--- 3D Object --->	
                <cfinvoke component="Assets" method="getAssetInfo" returnvariable="assetInfo">
                        <cfinvokeargument name="assetID" value="#assetID#"/>
                </cfinvoke>									
                <cfset assetImage = assetInfo.thumbnail>
                
                <cfif assetImage NEQ ''>
                	<cfset imagePath = assetPath & assetImage>
                <cfelse>
                	<cfset imagePath = ''>
                </cfif>
                
            </cfif>
            
            <cfset theAsset = {"assetTypeID":assetTypeID, "url":imagePath, "name":assetInfo.assetName}>
            
            <cfif imagePath IS '' AND imageThumb NEQ ''>
                <cfset imageThumbPath = assetPath & 'thumbs/' & imageThumb>
                <cfset theAsset.url = imageThumbPath>
            <cfelse>
            	<!--- missing image --->
            </cfif>
 
            <cfreturn theAsset>
  		
        </cfif>
        
    </cffunction>   
        
</cfcomponent>