<cfcomponent>

	<cffunction name="getAllGames" access="public" returntype="query">
    
		<cfargument name="bundleID" type="string" required="yes">
        
        <!--- Get AppID --->
        <cfinvoke component="Apps" method="getAppID" returnvariable="app">
            <cfinvokeargument name="bundleID" value="#bundleID#">
        </cfinvoke>
        
        <cfset appID = app.app_id>
    
        <!--- CGet Game Asset Info --->
        <cfquery name="allGames">
            SELECT       GameAssets.asset_id, GameAssets.dateStart, GameAssets.dateEnd, Assets.name
			FROM         GameAssets INNER JOIN
                         Assets ON GameAssets.asset_id = Assets.asset_id
            WHERE		 Assets.app_id = #appID#
        </cfquery>
        
		<cfreturn allGames>
        
	</cffunction>
    
    

	<cffunction name="getGameUsers" access="public" returntype="query">
    
		<cfargument name="gameID" type="string" required="yes">
        
        <cfset data = structNew()>
        
        <!--- CGet Game Asset Info --->
        <cfquery name="allPlayers">
            SELECT      DISTINCT GameStats.user_id, Users.name, Users.email, Users.country
			FROM        GameStats INNER JOIN
                        Users ON GameStats.user_id = Users.user_id
        </cfquery>
        
		<cfreturn allPlayers>
        
	</cffunction>
    
    
    <cffunction name="getGameUserStatusCompleted" access="public" returntype="query">
    
		<cfargument name="gameID" type="string" required="yes">
        
        <cfset data = structNew()>
        
        <!--- CGet Game Asset Info --->
        <cfquery name="allPlayers">
            
        </cfquery>
        
		<cfreturn allPlayers>
        
	</cffunction>
    
    
    
    <!--- Get Game Assets --->
	<cffunction name="getGameAssets" access="public" returntype="query">
    
		<cfargument name="gameID" type="numeric" required="yes">
        
        <cfquery name="gameAssets">
            SELECT       GroupAssets.content_id
            FROM         GameAssets INNER JOIN GroupAssets ON GameAssets.asset_id = GroupAssets.asset_id
            WHERE        (GameAssets.asset_id = #gameID#)
        </cfquery>
        
		<cfreturn gameAssets>
        
	</cffunction>
    

    <!--- Status Game Assets Count --->
	<cffunction name="getGameAssetsCount" access="public" returntype="numeric">
    
		<cfargument name="gameID" type="numeric" required="yes">

        <!--- Get Game Asset Info --->
        <cfinvoke component="Games" method="getGameAssets" returnvariable="assets">
            <cfinvokeargument name="gameID" value="#gameID#">
        </cfinvoke>
        
        <cfset assetCount = assets.recordCount>
        
		<cfreturn assetCount>
        
	</cffunction>


    
    <!--- Start Game --->
	<cffunction name="getGameOptions" access="public" returntype="struct">
    
		<cfargument name="gameID" type="string" required="yes">
        
        <cfset data = structNew()>
        
        <!--- CGet Game Asset Info --->
        <cfquery name="gameOptions">
            SELECT	dateStart, dateEnd, SendClientEmail, emailMessage, SendPlayerEmail, email, useCode, sendTargets, silentMode
            FROM	GameAssets 
            WHERE   asset_id = #gameID#
        </cfquery>
    
        <cfif gameOptions.recordCount GT 0>
        	
            <cfset sendData = {"send": {"client":gameOptions.SendClientEmail, "player":gameOptions.SendPlayerEmail, "email":"#gameOptions.email#", "emailMessage":'#gameOptions.emailMessage#'} }>
            <cfset data = {"dateStart":gameOptions.dateStart, "dateEnd":gameOptions.dateEnd, "useCode":gameOptions.useCode, "sendTargets":gameOptions.sendTargets, "silentMode":gameOptions.silentMode}>
            <cfset structAppend(data,sendData)>
            
        <cfelse>
        	<!--- nothing --->
        </cfif>
        
		<cfreturn data>
        
	</cffunction>
    
    
    
    <!--- Status Game Assets Count --->
	<cffunction name="getGameUserAssetCount" access="public" returntype="numeric">
    
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="auth_token" type="string" required="no" default="">
        <cfargument name="gameID" type="numeric" required="yes">
        
        <cfif auth_token NEQ ''>
        
			<!--- Get UserID from Auth Token--->
             <cfinvoke component="Users" method="getUserID" returnvariable="userID">
                  <cfinvokeargument name="auth_token" value="#auth_token#">
             </cfinvoke>
            
        </cfif>
        
        <cfif userID GT 0>
        
			<!--- CGet Game Asset Info --->
            <cfquery name="userGameCount">
                SELECT        COUNT(user_id) as allTargets
                FROM          GameStats
                WHERE        (user_id = #userID#) AND asset_id = #gameID#
            </cfquery>
        
        </cfif>
        
		<cfreturn userGameCount.allTargets>
        
	</cffunction>
    
    
    
    <!--- Status Game Assets Count --->
	<cffunction name="getGameUserAssets" access="public" returntype="struct">
    
		<cfargument name="userID" type="numeric" required="yes">
        <cfargument name="gameID" type="numeric" required="yes">
        
        <!--- Assets --->
        <cfinvoke  component="Games" method="getGameAssets" returnvariable="gameAssets">
            <cfinvokeargument name="gameID" value="#gameID#"/>
        </cfinvoke>

		<cfinvoke  component="Misc" method="QueryToArray" returnvariable="targetIDS">
            <cfinvokeargument name="theQuery" value="#gameAssets#"/>
            <cfinvokeargument name="theColumName" value="content_id"/>
        </cfinvoke>
        
        <!--- Get Game User Info --->
        <cfquery name="userGameAssets">
            SELECT       GameStats.user_id, Assets.name, Assets.asset_id AS assetID, GameStats.collectedAssetDate AS date
			FROM         GameStats INNER JOIN
                         Assets ON GameStats.collectedAsset_id = Assets.asset_id
			WHERE        (GameStats.user_id = #userID#) AND (GameStats.asset_id = #gameID#)
            ORDER BY 	GameStats.collectedAssetDate ASC
        </cfquery>
        
        <cfset assetList = arrayNew(1)>
        
        <!--- Get Assets --->
        <cfoutput query="userGameAssets">
		
			<cfset found = ArrayFind(targetIDS,assetID)>
            
			<cfif found GT 0>
            	<cfset arrayAppend(assetList,{'assetID':assetID, 'date':date})>
			</cfif>
            
		</cfoutput>
        
        <cfif arrayIsEmpty(assetList)>
        	<cfset done = false>
        <cfelse>
        	<cfset done = true>
        </cfif>
        
        <cfset assetsFound = arrayNew(1)>
        
        <cfloop index="anAsset" array="#assetList#">
        
        	<!--- Get Asset --->
            <cfinvoke component="Assets" method="getAssets" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#anAsset.assetID#"/>
            </cfinvoke>
            
            <!--- Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#anAsset.assetID#"/>
                <cfinvokeargument name="server" value="true"/>
            </cfinvoke>
            
            <cfset thumbnailImage = assetPath & 'thumbs/' & asset.thumbnail>
        	<cfset arrayAppend(assetsFound, {'url':thumbnailImage, 'title':asset.title, 'date':anAsset.date})>
            
        </cfloop>
        
        <!--- Get Start Date --->
        <cfinvoke  component="CFC.Games" method="getUserGameStart" returnvariable="startExperience">
          <cfinvokeargument name="userID" value="#userID#"/>
          <cfinvokeargument name="gameID" value="#gameID#"/>
        </cfinvoke>
        
        <cfset userData = {'done':#done#,'urls':'', 'assets':assetsFound, 'date':startExperience}>
        
		<cfreturn userData>
        
	</cffunction>
    
    
    
 	<!--- Get Game User Start --->
	<cffunction name="getUserGameStart" access="public" returntype="date">
    
		<cfargument name="auth_token" type="string" required="no" default="">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="gameID" type="numeric" required="yes">   
    	
        <cfif auth_token NEQ ''>
			<!--- Get UserID from Auth Token--->
            <cfinvoke component="Users" method="getUserID" returnvariable="userID">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
       	</cfif>
       
    	<!--- Assets --->
        <cfinvoke  component="Games" method="getGameAssets" returnvariable="gameAssets">
            <cfinvokeargument name="gameID" value="#gameID#"/>
        </cfinvoke>

		<cfinvoke  component="Misc" method="QueryToArray" returnvariable="targetIDS">
            <cfinvokeargument name="theQuery" value="#gameAssets#"/>
            <cfinvokeargument name="theColumName" value="content_id"/>
        </cfinvoke>
        
        <cfset assetList = arrayNew(1)>
        
        <cfquery name="gameAssets">
            SELECT        asset_id
            FROM            GroupAssets
            WHERE        (content_id = #gameID#)
        </cfquery>
        
        <cfset targetID = gameAssets.asset_id>
        
        <cfquery name="gameInfo">
            SELECT        collectedAssetDate
            FROM            GameStats
            WHERE        (collectedAsset_id = #targetID#) AND (user_id = #userID#)
        </cfquery>
        
        <cfset theDate = gameInfo.collectedAssetDate>
        
        <cfinvoke  component="Misc" method="convertEpochToDate" returnvariable="gameStartDate">
            <cfinvokeargument name="TheEpoch" value="#theDate#"/>
        </cfinvoke>
        
        <cfreturn gameStartDate>
	
    </cffunction>
   
</cfcomponent>