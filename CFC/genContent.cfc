<cfcomponent>

	<!---  --->
	<cffunction name="generateContent" access="remote" returntype="string">
    
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        
		<cfquery name="content">
            SELECT  group_id, asset_id, subgroup_id, name, modified
            FROM Groups where app_id = #appID# AND subgroup_id = #groupID#
        </cfquery>
        
        <!--- <cfdump var="#content#"> --->
        
        <!--- get content details --->
        <cfloop query="content">
        
        <cfif asset_id IS ''>
        	Group
       		<!--- get group --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="groupData">
                <cfinvokeargument name="groupID" value="#group_id#">
            </cfinvoke>
            
            <cfinvoke component="genContent" method="getAssetsInGroup" returnvariable="assetsData">
                <cfinvokeargument name="groupID" value="#group_id#">
            </cfinvoke>
        	
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="groupAssetData">
                <cfinvokeargument name="theStruct" value="#groupData#"/>
            </cfinvoke>
            
            <cfset theObj = {"assets":assetsData, "modified":modified, "groupID":group_id, "assetID":asset_id, "name":name}>
         	<cfset structAppend(theObj,groupAssetData)>
            
             <cfdump var="#theObj#">
            
        <cfelse>
        	Asset
            <cfinvoke component="CFC.Content" method="getAssetContent" returnvariable="AssetData">
            	<cfinvokeargument name="assetID" value="#asset_id#"/>
            </cfinvoke>
            
             <cfdump var="#AssetData#">
        
        </cfif>    
            
            
            <!--- get assets in group --->
           <!---  <cfinvoke component="genContent" method="getAssetsInGroup" returnvariable="assetsData">
                <cfinvokeargument name="groupID" value="#group_id#">
            </cfinvoke>

             <cfset theObj = {"assets":assetsData, "modified":modified, "groupID":group_id, "assetID":asset_id, "name":name}>
         	<cfset structAppend(theObj,groupData)>
            
            <cfdump var="#theObj#"> 
            <cfdump var="#assetsData#">
        --->
        </cfloop>
        
		<cfreturn 0>
        
	</cffunction>
    
    
    <!---  --->
    <cffunction name="getAssetsInGroup" access="public" returntype="array">

		<cfargument name="groupID" type="string" required="yes">
        
        <cfset groupAssets = []>
        
        <!---  --->
        <cfinvoke component="Content" method="getGroupContent" returnvariable="theGroupData">
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
        
        <cfreturn groupAssets>
        
    </cffunction>
    
    
    
    
    
</cfcomponent>