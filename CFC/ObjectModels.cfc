<cfcomponent> 

    <cffunction	name="getAllObjectModels" access="public" returntype="struct" output="true">
    	
         <cfquery name="allTypes">
              SELECT	assetType_id,dbTable
              FROM		AssetTypes
          </cfquery> 
        
        <cfinvoke component="Misc" method="QueryToStruct" returnvariable="objectTypes">
            <cfinvokeargument name="query" value="#allTypes#">
        </cfinvoke>
        
        <cfset allObjectsTypes = structNew()>
        
        <cfloop index="z" from="1" to="#arrayLen(objectTypes)#">
        
        	<cfset structAppend(allObjectsTypes,{"#objectTypes[z].dbTable#":objectTypes[z].assetType_id})>
        
        </cfloop>
        
        <cfreturn allObjectsTypes>
        
    </cffunction>
	
    
    <cffunction	name="getObjectModel" access="public" returntype="struct" output="true">

        <cfargument	name="typeID" type="numeric" required="false" default="0" />
		<cfargument	name="typeName" type="string" required="false" default="" />
        <cfargument	name="objectData" type="any" required="no" default="#structNew()#" />
        
        <!--- Get ID from Type Name --->
       
		<!--- getID --->
        <cfquery name="theObjectType">
          SELECT	assetType_id
          FROM		AssetTypes
          WHERE
          <cfif typeID GT 0>
          			assetType_id = #typeID#
          <cfelse>
          			dbTable = '#trim(typeName)#'
          </cfif>
          		
        </cfquery> 
        
        <cfif theObjectType.recordCount GT 0>
            <cfset typeID = theObjectType.assetType_id>
        <cfelse>
            <cfset typeID = 0>
        </cfif>
	
        <cfset objectModel = structNew()>
         
         <cfif NOT isStruct(objectData)><cfset typeID = 0></cfif>
         
  		<!--- Get Type Model --->
    	<cfif typeID GT 0>
        
            <cfswitch expression="#typeID#">
            	
                <cfcase value="1">
                
                    <!--- ImageAssets 1 																										--->
                    <cfinvoke component="ObjectModels" method="imageModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>  
						
                </cfcase>
                
                
                
                <cfcase value="2">
                
                    <!--- VideoAssets 2 																										--->
                    <cfinvoke component="ObjectModels" method="videoModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
  																																												
                </cfcase>
                
                
                
                <cfcase value="3">
                
                    <!--- PDFAssets 3 																												--->
                    <cfinvoke component="ObjectModels" method="documentModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                
                <cfcase value="4">
                
                    <!--- URLAssets 4 																												--->
                    <cfinvoke component="ObjectModels" method="urlModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
                
                
                
                
                <cfcase value="5">
                    <!--- GPSAssets 5 																												--->
                    <cfinvoke component="ObjectModels" method="locationtModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                
                <cfcase value="6">
                
                    <!--- Model3DAssets 6 																											--->
                    <cfinvoke component="ObjectModels" method="model3DModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                
                <cfcase value="7">
                	<!--- Nothing --->
                </cfcase>
                
                
                
                
                <cfcase value="8">
                
                    <!--- PanoramaAssets 8 																												--->
                    <cfinvoke component="ObjectModels" method="panoramaModel" returnvariable="objectModel">
                        <cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="9">
                
                    <!--- BalconyAssets 9 																													--->
                    <cfinvoke component="ObjectModels" method="balconyModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
    
            
                
                
                <cfcase value="10">
                
                    <!--- XYZPointAssets 10 																												--->
                    <cfinvoke component="ObjectModels" method="xyzPointModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="11">
                	<!--- Nothing --->
                </cfcase>
                <cfcase value="12">
                	<!--- Nothing --->
                </cfcase>
                <cfcase value="13">
                	<!--- Nothing --->
                </cfcase>
                <cfcase value="14">
                	<!--- Nothing --->
                </cfcase>
                
                
                <cfcase value="15">
                	
                    <!--- MarkerAssets 15 																													--->
                    <cfinvoke component="ObjectModels" method="markerModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
  
                </cfcase>
                
                
                <cfcase value="16">
                	<!--- Nothing --->
                </cfcase>
                
                
                <cfcase value="17">
                
                    <!--- AnimationAssets 17 																											--->
                    <cfinvoke component="ObjectModels" method="animationModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="18">
                
                    <!--- GroupAssets 18 																												--->
                    <cfif isStruct(objectData)>
                    	<cfif NOT structIsEmpty(objectData)>
                    		<cfset objectData = [objectData]>
                        <cfelse>
                        	<cfset objectData = []>
                        </cfif>
                    </cfif>
                    
                    <cfinvoke component="ObjectModels" method="groupModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
   					
                    <cfif arrayLen(objectData) GT 0>
                    	<cfset objectData = {"asset_id":objectData[1].asset_id, "name":"#objectData[1].name#", "modified":objectData[1].assetModified}>
					<cfelse>
                    	<cfset objectData = {"asset_id":0, "name":"group", "modified":0}>
                    </cfif>
                    
                </cfcase>
                
                
 
                <cfcase value="19">
                
                    <!--- GameAssets 19 																												--->
                    <cfinvoke component="ObjectModels" method="gameModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                <cfcase value="20">
                
                    <!--- QuizAssets 20																												--->
                    <cfinvoke component="ObjectModels" method="quizModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
        
                </cfcase>
                
                
                <cfcase value="21">
                
                    <!--- MaterialAssets 21 																											--->
                    <cfinvoke component="ObjectModels" method="materialModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
                
                
                <cfcase value="22">
               
               		<!--- ProgramAsset 22 																											--->
                    <cfinvoke component="ObjectModels" method="programSchedule" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                                 
                </cfcase>
                
                
                <cfcase value="23">
                
                    <!--- GEOFenceAssets --->														 <!--- FUTURE OR COMBINE WITH GPS AS LIST OF POINTS?--->
                    <cfinvoke component="ObjectModels" method="geofenceModel" returnvariable="objectModel" />
                    <cfloop collection="#objectModel#" item="theKey"></cfloop>
					
                    <cfif NOT structIsEmpty(objectData)>
                    	
                        <!--- connect data --->
                        <cfdump var="#objectData#"><cfdump var="#objectModel#"><cfabort>

                    </cfif>
                                 
                </cfcase>
            	
            </cfswitch>
			
            <cfif structKeyExists(objectData,'asset_id')>
				<cfset assetID = objectData.asset_id>
			<cfelse>
            	<cfset assetID = 0>
            </cfif>


            <!--- Assets and Actions 																													--->
            <cfinvoke  component="Content" method="getAssetsActions" returnvariable="theAssetsActions">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
         
            <cfif structCount(theAssetsActions) GT 0>
            	
                <cfloop collection="#objectModel#" item="theKey"></cfloop>
                
                <cfset structAppend(objectModel[theKey],{"assets":[],"actions":[]})>
              
                <!--- Actions --->
                <cfif NOT arrayIsEmpty(theAssetsActions.actions)>
                    <cfloop index="anObject" array="#theAssetsActions.actions#">
                        
                        <!--- Get Asset Data --->
                        <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                            <cfinvokeargument name="assetID" value="#anObject.asset_id#"/>
                        </cfinvoke>
                       
                       <cfif NOT structIsEmpty(assetData)>
                       
							<!--- Set AccessLevel, Shared, Cached --->
                            <cfloop collection="#assetData#" item="theAssetKey"></cfloop>
                            <cfif structKeyExists(anObject,'accessLevel')>
                                <cfset assetData[theAssetKey].access = anObject.accessLevel>
                            </cfif>
                            <cfif structKeyExists(anObject,'sharable')>
                                <cfset assetData[theAssetKey].sharable = anObject.sharable>
                            </cfif>
                            <cfif structKeyExists(anObject,'cached')>
                                <cfset assetData[theAssetKey].cached = anObject.cached>
                            </cfif>
    
                            <!--- Add Assets to Group Object --->
                            <cfset arrayAppend(objectModel[theKey].actions,assetData)>
                      
                      	</cfif>
                        
                    </cfloop>
                </cfif>
                
                <!--- Assets --->
                <cfif NOT arrayIsEmpty(theAssetsActions.assets) AND typeID NEQ 20>
                    <cfloop index="anObject" array="#theAssetsActions.assets#">
                        
                        <!--- Get Asset Data --->
                        <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                            <cfinvokeargument name="assetID" value="#anObject.asset_id#"/>
                        </cfinvoke>
                        
                        <cfif NOT structIsEmpty(assetData)>
                        
							<!--- Set AccessLevel, Shared, Cached --->
                            <cfloop collection="#assetData#" item="theAssetKey"></cfloop>
                            <cfif structKeyExists(anObject,'accessLevel')>
                                <cfset assetData[theAssetKey].access = anObject.accessLevel>
                            </cfif>
                            <cfif structKeyExists(anObject,'sharable')>
                                <cfset assetData[theAssetKey].sharable = anObject.sharable>
                            </cfif>
                            <cfif structKeyExists(anObject,'cached')>
                                <cfset assetData[theAssetKey].cached = anObject.cached>
                            </cfif>
                            
                            <!--- Add Assets to Group Object --->
                            <cfset arrayAppend(objectModel[theKey].assets,assetData)>
                        
                        </cfif>
                        
                    </cfloop>
                </cfif>
            
            </cfif>
          
            <cfif arrayIsEmpty(objectModel[theKey].assets)>
				<cfset structDelete(objectModel[theKey],'assets')>
            </cfif>
          
            <cfif arrayIsEmpty(objectModel[theKey].actions)>
				<cfset structDelete(objectModel[theKey],'actions')>
            </cfif>
            
            <cfif structKeyExists(objectData,'asset_id')>
            	<cfif structKeyExists(objectData,"name")>
                    <cfloop collection="#objectModel#" item="theKey"></cfloop>
                    <cfset objectModel = {"#objectData.name#":objectModel[theKey]}>
                </cfif>
            </cfif>
            
        </cfif>
        	  
        <cfreturn objectModel>
    
    </cffunction>
	
    
    
    
    
    <!--- Get THUMBS, DETAILS AND COLOR --->
    <cffunction	name="assetInfo" access="public" returntype="struct" output="true">
    	<cfargument	name="assetID" type="numeric" required="false" default="0" />
        <cfargument	name="groupID" type="numeric" required="false" default="0" />
        
        <cfset otherPath = "">
                 
        <cfif assetID GT 0>
			<!--- Get Asset --->
            <cfinvoke component="Assets" method="getAssets" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            <!--- Other path for group color assets --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="otherPath">
              <cfinvokeargument name="assetID" value="#assetID#"/>
              <cfinvokeargument name="server" value="true"/>
            </cfinvoke>
        <cfelse>
        	<!--- Get Group --->
            <cfinvoke component="Modules" method="getGroupDetails" returnvariable="assetData">
                <cfinvokeargument name="groupID" value="#groupID#"/>
            </cfinvoke>
            <!--- Other path for group color assets --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="otherPath">
              <cfinvokeargument name="groupID" value="#groupID#"/>
              <cfinvokeargument name="server" value="true"/>
            </cfinvoke>
            
        </cfif>
     	
        <cfif assetData.recordCount GT 0>
        
			<!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="objectData">
                <cfinvokeargument name="query" value="#assetData#"/>
             </cfinvoke>
       		
            <cfif isStruct(objectData)>
            
				<!--- Get Detail --->
                <cfinvoke  component="ObjectModels" method="detailsModel" returnvariable="details">
                    <cfinvokeargument name="objectData" value="#objectData#"/>
                </cfinvoke>
                                                                                                                                                      
                <!--- Get Thumb --->
                <cfinvoke  component="ObjectModels" method="thumbsModel" returnvariable="thumb">
                    <cfinvokeargument name="objectData" value="#objectData#"/>
                </cfinvoke>
                 
                <!--- Get Colors --->
                <cfinvoke  component="ObjectModels" method="colorsModel" returnvariable="colors">
                    <cfinvokeargument name="objectData" value="#objectData#"/>
                </cfinvoke>
              																									
	 			<!--- Colors Background Path --->
                <cfif structKeyExists(objectData,'background')>
					<cfif objectData.background NEQ ''>
                        <cfset objectData.background = "#otherPath##objectData.background#">
                        <cfset colors.colors.background = "#objectData.background#">
                    </cfif>
                </cfif>
  
                <cfset details = details.details>
                <cfset thumb = thumb.thumb>
                <cfset colors = colors.colors>
                
            <cfelse>
            
            	<cfset details = {}>
				<cfset thumb = {}>
                <cfset colors = {}>
            
            </cfif>
            
        <cfelse>
        	
            <cfset details = {}>
            <cfset thumb = {}>
            <cfset colors = {}>
            
        </cfif>
        
        <cfset assetInfo = {"details":details, "thumb":thumb, "colors":colors}>
                   
        <cfreturn assetInfo>
        
    </cffunction>
    
    
    
    
    
    
    
    <!--- Object Models --->
    
    <!--- DETAILS --->
    <cffunction	name="detailsModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
		<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"details":{}})>
        
        <cfset structAppend(theObjModel.details,{"title":""})>
        <cfset structAppend(theObjModel.details,{"titleColor":""})>
        
        <cfset structAppend(theObjModel.details,{"subtitle":""})>
        <cfset structAppend(theObjModel.details,{"subtitleColor":""})>
        
        <cfset structAppend(theObjModel.details,{"description":""})>
        <cfset structAppend(theObjModel.details,{"descriptionColor":""})>
        
        <cfset structAppend(theObjModel.details,{"other":""})>
		
        <cfif NOT structIsEmpty(objectData)>
            
            <cfloop collection="#theObjModel#" item="theKey"></cfloop>
            
            <cfset theObjModel[theKey].title = objectData.title>
            
            <cfif objectData.titleColor NEQ ''>
            
                <cfset theObjModel[theKey].titleColor = objectData.titleColor>

            </cfif>
            
            <cfset theObjModel[theKey].subtitle = objectData.subtitle>
            
            <cfif objectData.subtitleColor NEQ ''>
                
                <cfset theObjModel[theKey].subtitleColor = objectData.subtitleColor>

            </cfif>
            
            <cfset theObjModel[theKey].description = objectData.description>
            
            <cfif objectData.descriptionColor NEQ ''>

                <cfset theObjModel[theKey].descriptionColor = objectData.descriptionColor>

            </cfif>
            
            <cfset theObjModel[theKey].other = objectData.other>
            
        </cfif>
         
        <cfreturn theObjModel>
      
    </cffunction>
    
    
    
    <!--- THUMBS --->
    <cffunction	name="thumbsModel" access="public" returntype="struct" output="true">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
  
    	<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"thumb":{"ratio":0,"width":0, "height":0}})>
        
        <cfset imgObj = {"url":""}>
 
        <cfset structAppend(theObjModel.thumb,{"xdpi":{"url":""},"mdpi":{"url":""}})>
        
        <cfif NOT structIsEmpty(objectData)>

            <cfif objectData.thumbnail NEQ ''>

                <cfif objectData.asset_id NEQ ''>
                    <cfset assetID = objectData.asset_id>
                <cfelse>
                    <cfset assetID = 0>
                </cfif>
                
                <!--- Build Asset Path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="appID" value="#objectData.app_id#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
     
                <!--- ASSET Thumb --->
                <cfif StructKeyExists(objectData,"assetType_id")>
                    
                    <cfif objectData.assetType_id NEQ ''>
         
                        <!--- Get Asset Type Path --->
                        <cfinvoke component="File" method="getAssetPath" returnvariable="assetTypePath">
                              <cfinvokeargument name="assetType" value="#objectData.assetType_id#"/>
                        </cfinvoke>
                        
                        <cfset path = 'assets/#assetTypePath#/'>
                    
                    <cfelse>
                        <cfset path = 'images/'>
                    </cfif>
                    
                <cfelse>
                    <cfset path = 'images/'>
                </cfif>
    
                <cfset assetPathNomral = assetPath & "#path#thumbs/nonretina/" & objectData.thumbnail>
                <cfset assetPathRetina = assetPath & "#path#thumbs/" & objectData.thumbnail>
          
                <cfloop collection="#theObjModel#" item="theKey"></cfloop>
                
                <cfif NOT isDefined('objectData.width')><cfset w = 0></cfif>
                <cfif NOT isDefined('objectData.height')><cfset h = 0></cfif>
                
                <cfif isDefined('objectData.width') AND isDefined('objectData.height')>
                    
                    <cfset w = objectData.width>
                    <cfset h = objectData.height>
                    
                    <cfif w IS ''><cfset w = 0></cfif>
                    <cfif h IS ''><cfset h = 0></cfif>
                    
                    <cfset r = 0>
                    
                    <cfif w NEQ 0 OR h NEQ 0>
                        <cfset r = w/h>
                    </cfif>
                    
                    <cfset theObjModel[theKey].ratio = r>
                    <cfset theObjModel[theKey].width = w>
                    <cfset theObjModel[theKey].height = h>
                    
                </cfif>
                
                <cfif objectData.thumbnail NEQ ''>
                    <cfset theObjModel[theKey].mdpi.url = assetPathNomral>
                    <cfset theObjModel[theKey].xdpi.url = assetPathRetina>
                <cfelse>
                    <cfset theObjModel[theKey] = {}>
                </cfif>
               
            </cfif>
			
            </cfif>
            
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- COLORS --->
    <cffunction	name="colorsModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
      
    	<cfset theObjModel = structNew()>
        
        <!--- <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" /> --->
        
        <cfset structAppend(theObjModel,{"colors":{}})>
		<cfset structAppend(theObjModel.colors,{"forecolor":''})>
        <cfset structAppend(theObjModel.colors,{"backcolor":''})>
        <cfset structAppend(theObjModel.colors,{"othercolor":''})>
        <cfset structAppend(theObjModel.colors,{"background":''})><!--- theImageModel --->
      
        <cfif NOT structIsEmpty(objectData)>
            
            <cfloop collection="#theObjModel#" item="theKey"></cfloop>
			
			 
        	<cfset theObjModel[theKey].forecolor = objectData.forecolor>
            <cfset theObjModel[theKey].backcolor = objectData.backcolor>
            
            <cfif isDefined('objectData.othercolor')> 
            	<cfset theObjModel[theKey].othercolor = objectData.othercolor>
    		</cfif>
            
            <cfset theObjModel[theKey].background = objectData.background>
            
            <!--- <cfif objectData.background NEQ ''>
            
            	<cfset theObjModel[theKey].background.url.xdpi = objectData.background>
          																																				
                <cfinvoke  component="Misc" method="getImageSpecs" returnvariable="imageSpec">
                    <cfinvokeargument name="imageSrc" value="#objectData.background#"/>
                </cfinvoke>
            																															
                <cfif NOT structIsEmpty(imageSpec)>
					<cfset theObjModel[theKey].background.ratio = imageSpec.ratio>
                    <cfset theObjModel[theKey].background.width = imageSpec.width>
                    <cfset theObjModel[theKey].background.height = imageSpec.height>
            	</cfif>
                
            </cfif>
			
			<!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
            <cfif structKeyExists(theObjModel[theKey],'background')>
				<cfif NOT structKeyExists(theObjModel[theKey].background,'url')>
                    <cfset structDelete(theObjModel[theKey],"background")>
                </cfif>
            </cfif>
         --->
         
        </cfif>
  
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- OPTIONS --->
    <cffunction	name="optionsModel" access="public" returntype="struct" output="false">

    	<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"access":0})>
		<cfset structAppend(theObjModel,{"modified":0})>
        <cfset structAppend(theObjModel,{"sharable":0})>
        <cfset structAppend(theObjModel,{"cached":0})>
        
        <cfset structAppend(theObjModel,{"assetType":0})>
        <cfset structAppend(theObjModel,{"asset_id":0})>
        
        <cfset structAppend(theObjModel,{"assets":[]})>
        <cfset structAppend(theObjModel,{"actions":[]})>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- Image Object Container --->
    <cffunction	name="imageContainerModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
		<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"url":{}})>
        <cfset structAppend(theObjModel.url,{"mdpi":""})>
        <cfset structAppend(theObjModel.url,{"xdpi":""})>
        
        <cfset structAppend(theObjModel,{"width":0})>
        <cfset structAppend(theObjModel,{"height":0})>
        <cfset structAppend(theObjModel,{"ratio":0})>
                
        <cfreturn theObjModel>
        
    </cffunction>  
    
    
    <!--- BASIC --->
    <cffunction	name="basicModel" access="public" returntype="struct" output="false">

    	<cfset theObjModel = structNew()>
        
        <cfinvoke component="ObjectModels" method="optionsModel" returnvariable="optionsModel" />
        <cfinvoke component="ObjectModels" method="thumbsModel" returnvariable="thumbsModel" />
        <cfinvoke component="ObjectModels" method="colorsModel" returnvariable="colorsModel" />
        <cfinvoke component="ObjectModels" method="detailsModel" returnvariable="detailsModel" />
        
        <cfset structAppend(theObjModel,{"objectName":{}})>
        
        <cfset structAppend(theObjModel.objectName,detailsModel)>
		<cfset structAppend(theObjModel.objectName,thumbsModel)>
        <cfset structAppend(theObjModel.objectName,colorsModel)>
        <cfset structAppend(theObjModel.objectName,optionsModel)>
    
        <cfreturn theObjModel>
        
    </cffunction>
    
    <!--- 									MODELS										 --->
    
    <!--- IMAGE 1 --->
    <cffunction	name="imageModel" access="public" returntype="struct" output="true">
		<cfargument	name="objectData" type="any" required="no" default="#structNew()#" />
        
        <cfif NOT isStruct(objectData)>
	        <cfdump var="#objectData#">
            <cfinvoke component="Misc" method="CallStackDump" />
            <cfabort>
        </cfif>
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"image":{}}>
        <cfset structAppend(theObjModel.image,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 1>
        
        <cfset structAppend(theObjModel.image,{"width":0, "height":0})>
        <cfset structAppend(theObjModel.image,{"ratio":0})>
        
        <cfset structAppend(theObjModel.image,{"url":{}})>
        <cfset structAppend(theObjModel.image.url,{"xdpi":{"url":""}})>
		<cfset structAppend(theObjModel.image.url,{"mdpi":{"url":""}})>
    	
        
        <!--- Inject Data --->
                   
        <cfif NOT structIsEmpty(objectData)>

            <cfif structKeyExists(objectData,'width') AND structKeyExists(objectData,'height')>
            
				<cfset w = objectData.width>
                <cfset h = objectData.height>
                
                <cfif w IS ''><cfset w = 0></cfif>
                <cfif h IS ''><cfset h = 0></cfif>
                
                <cfif w IS 0 OR h IS 0>
                    <cfset r = 0>
                <cfelse>
                    <cfset r = w/h>
                </cfif>
            
				<cfset theObjModel[theKey].width = w>
                <cfset theObjModel[theKey].height = h>
                <cfset theObjModel[theKey].ratio = r>
            
            </cfif>
 
            <cfif objectData.webURL NEQ ''>
                <cfset theObjModel[theKey].url = objectData.webURL>
            <cfelse>
            
                <!--- Get Image Paths for M and X dpi --->
                <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePaths">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                    <cfinvokeargument name="fileName" value="#objectData.url#">
                </cfinvoke>

                <cfif isDefined('imagePaths.xdpi')>
                    <cfset theObjModel[theKey].url.xdpi.url = imagePaths.xdpi>
                </cfif>
                <cfif isDefined('imagePaths.mdpi')>
                    <cfset theObjModel[theKey].url.mdpi.url = imagePaths.mdpi>
                </cfif>
                
            </cfif>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>


    <!--- VIDEO 2 --->
    <cffunction	name="videoModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"video":{}}>
        <cfset structAppend(theObjModel.video,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 2>
        
        <!--- <cfset structAppend(theObjModel.video,{"width":0, "height":0, "ratio":0})> --->
		<cfset structAppend(theObjModel.video,{"url":{}})>
        <cfset structAppend(theObjModel.video.url,{"xdpi":{"url":""}})>
		<cfset structAppend(theObjModel.video.url,{"mdpi":{"url":""}})>
        
        <cfset structAppend(theObjModel.video.url,{"modified":0})>
        
        <cfset structAppend(theObjModel.video,{"placeholder":{'width':0,'height':0,'ratio':0}})>
        <cfset structAppend(theObjModel.video.placeholder,{"url":{}})>
        <cfset structAppend(theObjModel.video.placeholder.url,{"xdpi":{"url":""}})>
		<cfset structAppend(theObjModel.video.placeholder.url,{"mdpi":{"url":""}})>
        
        <cfset structAppend(theObjModel.video,{"controls":{'rewind':0,'zoom':0,'seek':0}})>
        
        <cfset structAppend(theObjModel.video,{"speedControl":0})>
        <cfset structAppend(theObjModel.video,{"loop":0})>
        <cfset structAppend(theObjModel.video,{"autoplay":0})>
        <cfset structAppend(theObjModel.video,{"release":0})>
		
        
        <!--- Inject Data --->
	 			
        <cfif NOT structIsEmpty(objectData)>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
		<cfset addDateModified = false>

			<!--- Web URL --->
            <cfif objectData.webURL NEQ ''>
            
                <cfset theObjModel[theKey].url = objectData.webURL>
                
                <cfif isDefined('objectData.modified')>
					<cfif objectData.modified NEQ ''>
                    	<cfset theObjModel[theKey].url.modified = objectData.modified>
                    <cfelse>
                    	<cfset addDateModified = true>
                    	<cfset theObjModel[theKey].url.modified = curDate>
                    </cfif>
                <cfelse>
                	<cfset addDateModified = true>
                	<cfset theObjModel[theKey].url.modified = curDate>
                </cfif>
                
            <cfelse>
            <!--- Asset URL --->
            
                <!--- Get Video Paths --->
                <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePaths">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                    <cfinvokeargument name="fileName" value="#objectData.url#">
                </cfinvoke>
            
                <cfif isDefined('imagePaths.xdpi')>
                	<cfif fileExists(imagePaths.xdpi)>
                    	<cfset theObjModel[theKey].url.xdpi.url = imagePaths.xdpi>
                    </cfif>
                </cfif>
                <cfif isDefined('imagePaths.mdpi')>
                	<cfif fileExists(imagePaths.mdpi)>
                    	<cfset theObjModel[theKey].url.mdpi.url = imagePaths.mdpi>
                    </cfif>
                </cfif>
                
                <cfif isDefined('objectData.modified')>
					<cfif objectData.modified NEQ ''>
                    	<cfset theObjModel[theKey].url.modified = objectData.modified>
                    <cfelse>
                    	<cfset addDateModified = true>
                    	<cfset theObjModel[theKey].url.modified = curDate>
                    </cfif>
                <cfelse>
                	<cfset addDateModified = true>
                	<cfset theObjModel[theKey].url.modified = curDate>
                </cfif>
          
            </cfif>
          
            <cfif structKeyExists(objectData,'placeholder')>
            
                <!--- Get Video Paths --->
                <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePlaceholderPaths">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                    <cfinvokeargument name="fileName" value="#objectData.placeholder#">
                </cfinvoke>
            	
                <cfif objectData.placeholder NEQ ''>
                
					<!--- Placeholder --->
                    <cfif isDefined('imagePlaceholderPaths.xdpi')>
                        <cfset theObjModel[theKey].placeholder.url.xdpi.url = imagePlaceholderPaths.xdpi>
                    <cfelse>
                        <cfset structDelete(theObjModel[theKey].placeholder.url,'xdpi')>
                    </cfif>
                    
                    <cfif isDefined('imagePlaceholderPaths.mdpi')>
                        <cfset theObjModel[theKey].placeholder.url.mdpi.url = imagePlaceholderPaths.mdpi>
                    <cfelse>
                    	<cfset structDelete(theObjModel[theKey].placeholder.url,'mdpi')>
                    </cfif>
                    
                    <!--- <cfif NOT structKeyExists(theObjModel[theKey].placeholder.url.xdpi) AND NOT structKeyExists(theObjModel[theKey].placeholder.url.mdpi)>
                    	<cfset structDelete(theObjModel[theKey].placeholder,'url')>
                    </cfif> --->
                   	
                    <cfset w = objectData.width>
					<cfset h = objectData.height>
                
                    <cfif w IS ''><cfset w = 0></cfif>
                    <cfif h IS ''><cfset h = 0></cfif>
                    
                    <cfset theObjModel[theKey].placeholder.width = w>
                    <cfset theObjModel[theKey].placeholder.height = h>
                    
                    <cfif w IS 0 OR h IS 0>
                    	<cfset r = 0>
                    <cfelse>
                        <cfset r = w/h>
                    </cfif>
                    
                    <cfset theObjModel[theKey].placeholder.ratio = r>
                    
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].placeholder,'width')>
                    <cfset structDelete(theObjModel[theKey].placeholder,'height')>
                    <cfset structDelete(theObjModel[theKey].placeholder,'ratio')>
                </cfif>
            
            </cfif>
            
            <!--- Size and Ratio --->
            <!--- <cfif structKeyExists(objectData,'width') AND structKeyExists(objectData,'height')>
            
				<cfset w = objectData.width>
                <cfset h = objectData.height>
            
				<cfif w IS ''><cfset w = 0></cfif>
                <cfif h IS ''><cfset h = 0></cfif>
                
                <cfif w IS 0 OR h IS 0>
                    <cfset r = 0>
                <cfelse>
                    <cfset r = w/h>
                </cfif>
                
                <cfset theObjModel[theKey].width = w>
                <cfset theObjModel[theKey].height = h>
                <cfset theObjModel[theKey].ratio = r>
            
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'width')>
                <cfset structDelete(theObjModel[theKey],'height')>
                <cfset structDelete(theObjModel[theKey],'ratio')>
            </cfif> --->
            
            <!--- if curDate added, update DB for Asset --->
            <cfif addDateModified>
            	<cfquery name="info">
                      UPDATE VideoAssets
                      SET modified = #curDate#
                      WHERE	 asset_id = #objectData.asset_id#
                 </cfquery>
          
            </cfif>

            
            <!--- video options --->
            <cfif objectData.autoplay>
                <cfset theObjModel[theKey].autoplay = objectData.autoplay>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'autoplay')>
            </cfif>
            
            <cfif objectData.loop>
                <cfset theObjModel[theKey].loop = objectData.loop>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'loop')>
            </cfif>
            
            <cfif objectData.speedControls>
                <cfset theObjModel[theKey].speedControl = objectData.speedControls>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'speedControl')> 
            </cfif>
            
            <cfif objectData.releaseControls>
                <cfset theObjModel[theKey].release = objectData.releaseControls>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'release')>
            </cfif>
            
            <!--- video controls --->
            <cfif objectData.playpause>
                
                <cfif objectData.rewind>
                    <cfset theObjModel[theKey].controls.rewind = objectData.rewind>
                </cfif>
                
                <cfif objectData.zoom>
                    <cfset theObjModel[theKey].controls.zoom = objectData.zoom>
                </cfif>
                
                <cfif objectData.scrubbar>
                    <cfset theObjModel[theKey].controls.seek = objectData.scrubbar>
                </cfif>
            
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'controls')>
            </cfif>
      
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            
            <cfif structKeyExists(objectData,"assetModified")>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
			</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>

            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
         
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
	<!--- DOCUMENTS 3 --->
    <cffunction	name="documentModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"pdf":{}}>
        <cfset structAppend(theObjModel.pdf,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 3>
        
        <cfset structAppend(theObjModel.pdf,{"url":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<!--- Build PDF Document Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

            <!--- URL --->
            <cfif objectData.webURL NEQ ''>
                <cfset theObjModel[theKey].url = objectData.webURL>
            <cfelseif objectData.url NEQ '' AND fileExists(assetPath & objectData.url)>
                <cfset theObjModel[theKey].url = assetPath & objectData.url>
            </cfif>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- URL LINK 4 --->
    <cffunction	name="urlModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"url":{}}>
        <cfset structAppend(theObjModel.url,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 4>
        
        <cfset structAppend(theObjModel.url,{"url":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    	
			<cfset theObjModel[theKey].url = objectData.url>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>

        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- LOCATION GPS 5 --->
    <cffunction	name="locationtModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"location":{}}>
        <cfset structAppend(theObjModel.location,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 5>
        
        <cfset structAppend(theObjModel.location,{"gps_alt":0})>
        <cfset structAppend(theObjModel.location,{"gps_long":0})>
        <cfset structAppend(theObjModel.location,{"gps_latt":0})>
        <cfset structAppend(theObjModel.location,{"radius":0})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<cfset theObjModel[theKey].gps_alt = objectData.gps_alt>
            <cfset theObjModel[theKey].gps_latt = objectData.gps_latt>
            <cfset theObjModel[theKey].gps_long = objectData.gps_long>
            <cfset theObjModel[theKey].radius = objectData.radius>
        
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
         
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- 3DMODEL 6 --->
    <cffunction	name="model3DModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"model3D":{}}>
        <cfset structAppend(theObjModel.model3D,theObjModelStuct.objectName)>

		<cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 6>

        <cfset model = {"scale":1}>
		<cfset structAppend(model,{"rotation":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(model,{"url":{"android":"","ios":"","osx":"","windows":""}})>
        <cfset structAppend(model,{"position":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(model,{"modified":0})>
		
		<cfset camera = {"fov":45}>
        <cfset structAppend(camera,{"position":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(camera,{"target":{"x":0,"y":0,"z":0}})>
		<cfset structAppend(camera,{"limits": {"zoomMin":0, "zoomMax":0, "panMin":0, "panMax":0, "rotationMin":0, "rotationMax":0}})>
        
        <cfset structAppend(camera,{"pan":0,"zoom":0, "rotation":0})>
        
        <cfset structAppend(theObjModel.model3D,{"model":model})>
        <cfset structAppend(theObjModel.model3D,{"camera":camera})>
        	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                       	
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
       
            <cfset camLimits = {'zoomMin':objectData.camMin, 'zoomMax':objectData.camMax, 'rotationMin':objectData.panMin, 'rotationMax':objectData.panMax, 'panMin':objectData.tiltMin, 'panMax':objectData.tiltMax}>
            <cfset camPosition = {'x':objectData.camPos_x, 'y':objectData.camPos_y, 'z':objectData.camPos_z}>
            <cfset camTarget = {'x':objectData.camTarget_x, 'y':objectData.camTarget_y, 'z':objectData.camTarget_z}>
            
            <!--- Camera --->
            <cfset theObjModel[theKey].camera.limits = camLimits>
            <cfset theObjModel[theKey].camera.position = camPosition>
            <cfset theObjModel[theKey].camera.target = camTarget>
            
            <cfset theObjModel[theKey].camera.pan = objectData.camPan>
            <cfset theObjModel[theKey].camera.rotation = objectData.camRotation>
            <cfset theObjModel[theKey].camera.zoom = objectData.camZoom>
            
            <cfset modelPos = {'x':objectData.loc_x, 'y':objectData.loc_y, 'z':objectData.loc_z}>
            <cfset modelRot = {'x':objectData.rot_x, 'y':objectData.rot_y, 'z':objectData.rot_z}>
            
            <!--- Model --->
            <cfset modelURL = structNew()>
            
            <cfif objectData.url NEQ ''>
                <cfset structAppend(modelURL,{'ios':assetPath & objectData.url})>
            </cfif>
            
            <cfif objectData.url_android NEQ ''>
                <cfset structAppend(modelURL,{'android':assetPath & objectData.url_android})>
            </cfif>
            
            <cfif objectData.url_osx NEQ ''>
                <cfset structAppend(modelURL,{'osx':assetPath & objectData.url_osx})>
            </cfif>
            
            <cfif objectData.url_windows NEQ ''>
                <cfset structAppend(modelURL,{'windows':assetPath & objectData.url_windows})>
            </cfif>
            
            <cfset model = {'position':modelPos, 'rotation':modelRot, 'scale':objectData.scale, 'url':modelURL, 'modified':objectData.modifiedModel}>
            
            <cfset theObjModel[theKey].model = model>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
   
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- NONE 7 --->
 
 
 	<!--- PANORAMA 8 --->
    <cffunction	name="panoramaModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"panorama":{}}>
        <cfset structAppend(theObjModel.panorama,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 8>
        
        <cfset links = {"xdpi":"","mdpi":""}>
        
        <cfset lf = {"left":StructCopy(links)}>
        <cfset rt = {"right":StructCopy(links)}>
        <cfset up = {"up":StructCopy(links)}>
        <cfset fr = {"front":StructCopy(links)}>
        <cfset bk = {"back":StructCopy(links)}>
        <cfset dn = {"down":StructCopy(links)}>
        
        <cfset pano = {"pano":StructCopy(links)}>
        
        <cfset structAppend(theObjModel.panorama,pano)>
        
        <cfset structAppend(theObjModel.panorama,lf)>
        <cfset structAppend(theObjModel.panorama,rt)>
        <cfset structAppend(theObjModel.panorama,up)>
        <cfset structAppend(theObjModel.panorama,fr)>
        <cfset structAppend(theObjModel.panorama,bk)>
        <cfset structAppend(theObjModel.panorama,dn)>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

            <cfset modelURL = structNew()>

            <!--- XDPI --->
            <cfif objectData.left NEQ '' AND fileExists(assetPath & objectData.left)>
                <cfset theObjModel[theKey].left.xdpi = assetPath & objectData.left>
            </cfif>
            <cfif objectData.up NEQ '' AND fileExists(assetPath & objectData.up)>
                <cfset theObjModel[theKey].up.xdpi = assetPath & objectData.up>
            </cfif>
            <cfif objectData.front NEQ '' AND fileExists(assetPath & objectData.front)>
                <cfset theObjModel[theKey].front.xdpi = assetPath & objectData.front>
            </cfif>
            <cfif objectData.back NEQ '' AND fileExists(assetPath & objectData.back)>
                <cfset theObjModel[theKey].back.xdpi = assetPath & objectData.back>
            </cfif>
            <cfif objectData.down NEQ '' AND fileExists(assetPath & objectData.down)>
                <cfset theObjModel[theKey].down.xdpi = assetPath & objectData.down>
            </cfif>
            <cfif objectData.right NEQ '' AND fileExists(assetPath & objectData.right)>
                <cfset theObjModel[theKey].right.xdpi = assetPath & objectData.right>
            </cfif>
            
            <cfif objectData.pano NEQ '' AND fileExists(assetPath & objectData.pano)>
                <cfset theObjModel[theKey].pano.xdpi = assetPath & objectData.pano>
            </cfif>
            
            <cfset nonretina = assetPath &'nonretina/'>
            
            <!--- MDPI --->
            <cfif objectData.left NEQ '' AND fileExists(nonretina & objectData.left)>
                <cfset theObjModel[theKey].left.mdpi = nonretina & objectData.left>
            </cfif>
            <cfif objectData.up NEQ '' AND fileExists(nonretina & objectData.up)>
                <cfset theObjModel[theKey].up.mdpi = nonretina & objectData.up>
            </cfif>
            <cfif objectData.front NEQ '' AND fileExists(nonretina & objectData.front)>
                <cfset theObjModel[theKey].front.mdpi = nonretina & objectData.front>
            </cfif>
            <cfif objectData.back NEQ '' AND fileExists(nonretina & objectData.back)>
                <cfset theObjModel[theKey].back.mdpi = nonretina & objectData.back>
            </cfif>
            <cfif objectData.down NEQ '' AND fileExists(nonretina & objectData.down)>
                <cfset theObjModel[theKey].down.mdpi = nonretina & objectData.down>
            </cfif>
            <cfif objectData.right NEQ '' AND fileExists(nonretina & objectData.right)>
                <cfset theObjModel[theKey].right.mdpi = nonretina & objectData.right>
            </cfif>
            
            <cfif objectData.pano NEQ '' AND fileExists(nonretina & objectData.pano)>
                <cfset theObjModel[theKey].pano.mdpi = nonretina & objectData.pano>
            </cfif>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            
            <cfif isDefined('objectData.assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
                    
        <cfreturn theObjModel>
        
    </cffunction>
 

 	<!--- BALCONY 9 --->
    <cffunction	name="balconyModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"balcony":{}}>
        <cfset structAppend(theObjModel.balcony,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 9>
        
        <cfset links = {"xdpi":"","mdpi":""}>
        
        <cfset north = {"north":StructCopy(links)}>
        <cfset south = {"south":StructCopy(links)}>
        <cfset west = {"west":StructCopy(links)}>
        <cfset east = {"east":StructCopy(links)}>
        
        <cfset floor = {"floorName":""}>
        
        <cfset structAppend(theObjModel.balcony,north)>
        <cfset structAppend(theObjModel.balcony,south)>
        <cfset structAppend(theObjModel.balcony,west)>
        <cfset structAppend(theObjModel.balcony,east)>
        
        <cfset structAppend(theObjModel.balcony,{'floorName':''})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

            <cfset modelURL = structNew()>
            
            <!--- XDPI --->
            <cfif objectData.north NEQ '' AND fileExists(assetPath & objectData.north)>
                <cfset theObjModel[theKey].north.xdpi = assetPath & objectData.north>
            </cfif>
            <cfif objectData.east NEQ '' AND fileExists(assetPath & objectData.east)>
                <cfset theObjModel[theKey].east.xdpi = assetPath & objectData.east>
            </cfif>
            <cfif objectData.south NEQ '' AND fileExists(assetPath & objectData.south)>
                <cfset theObjModel[theKey].south.xdpi = assetPath & objectData.south>
            </cfif>
            <cfif objectData.west NEQ '' AND fileExists(assetPath & objectData.west)>
                <cfset theObjModel[theKey].west.xdpi = assetPath & objectData.west>
            </cfif>
            
            <cfset nonretina = assetPath &'nonretina/'>
            
            <!--- MDPI --->
            <cfif objectData.north NEQ '' AND fileExists(nonretina & objectData.north)>
                <cfset theObjModel[theKey].north.mdpi = nonretina & objectData.north>
            </cfif>
            <cfif objectData.east NEQ '' AND fileExists(nonretina & objectData.east)>
                <cfset theObjModel[theKey].east.mdpi = nonretina & objectData.east>
            </cfif>
            <cfif objectData.south NEQ '' AND fileExists(nonretina & objectData.south)>
                <cfset theObjModel[theKey].south.mdpi = nonretina & objectData.south>
            </cfif>
            <cfif objectData.west NEQ '' AND fileExists(nonretina & objectData.west)>
                <cfset theObjModel[theKey].west.mdpi = nonretina & objectData.west>
            </cfif>

            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
            <cfset theObjModel[theKey].floorName = objectData.floorName>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
       
        <cfreturn theObjModel>
        
    </cffunction>

    

 	<!--- XYZPOINT 10 --->
    <cffunction	name="xyzPointModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />

        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"xyzpoint":{}}>
        <cfset structAppend(theObjModel.xyzpoint,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 10>
        
		<cfset structAppend(theObjModel.xyzpoint,{"objectRef":""})>
		<cfset structAppend(theObjModel.xyzpoint,{"x_pos":0,"y_pos":0,"z_pos":0})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    	
			<cfset theObjModel[theKey].x_pos = objectData.x_pos>
            <cfset theObjModel[theKey].y_pos = objectData.y_pos>
            <cfset theObjModel[theKey].z_pos = objectData.z_pos>
            
            <cfset theObjModel[theKey].objectRef = objectData.objectRef>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    <!--- NOTHING 11 --->
	<!--- NOTHING 12 --->
    <!--- NOTHING 13 --->
    <!--- NOTHING 14 --->
    
    <!--- MARKER 15 --->
    <cffunction	name="markerModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"marker":{}}>
        <cfset structAppend(theObjModel.marker,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        
        <cfset theObjModel[theKey].assetType = 15>
        
		<cfset structAppend(theObjModel.marker,{"visualizer":{'url':'', 'key':'', 'data':''}})>
        <cfset structAppend(theObjModel.marker,{"moodstocks":{'key':'','secret':''}})>
        <cfset structAppend(theObjModel.marker,{"vuforia":{'license':'','xml':'',"dat":''}})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                        
			<cfset data = structNew()>
            
            <!--- Moodstocks --->
            <cfif objectData.useMoodstocks>
                <cfset moodstocks = {"key":objectData.APIKey, "secret":objectData.APISecret}>
                <cfset theObjModel[theKey].moodstocks = moodstocks>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'moodstocks')>
            </cfif>
            
            <!--- Visualizer --->
            <cfif objectData.useVisualizer>
            
            	<!--- Build Asset Path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
                
                <cfset visualizer = {"key":objectData.keyData, "set":objectData.keySet, "url":""}>
                
                <cfif objectData.webURL NEQ ''>
                    <cfset visualizer.url = objectData.webURL>
                <cfelse>
                    <cfset visualizer.url = assetPath & objectData.url>
                </cfif>
                
                <cfset theObjModel[theKey].visualizer = visualizer>
                
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'visualizer')>   
            </cfif>

            <!--- Vuforia --->
            <cfif objectData.useVuforia>
            
            	<!--- Build Asset Path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
				
                <cfset vuforia = {"dat":"", "xml":""}>
                
                <cfif objectData.dat NEQ ''>
                    <cfset vuforia.dat = assetPath & objectData.dat>
                </cfif>
                
                <cfif objectData.xml NEQ ''>
                    <cfset vuforia.xml = assetPath & objectData.xml>
                </cfif>
                
                <cfset theObjModel[theKey].vuforia = vuforia>
                
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'vuforia')>   
            </cfif>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- NOTHING 16 --->
    
    
    <!--- ANIMATION 17 --->
    <cffunction	name="animationModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"animation":{}}>
        <cfset structAppend(theObjModel.animation,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 17>
        
		<cfset structAppend(theObjModel.animation,{"model3d":""})>
        <cfset structAppend(theObjModel.animation,{"animationSequence":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                        
			<cfset theObjModel[theKey].animationSequence = objectData.animationSequence>
            <cfset theObjModel[theKey].model3D = objectData.model3D>
        
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
 
    
    <!--- GROUP 18 --->
    <cffunction	name="groupModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="array" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"group":{}}>
        <cfset structAppend(theObjModel.group,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        
        <cfset theObjModel[theKey].assetType = 18>
        
		<cfset structAppend(theObjModel.group,{"display":{}})>
		<cfset structAppend(theObjModel.group.display,{"type":'',"model":'',"assetID":0})>
        
        <cfset structAppend(theObjModel.group,{"assets":[]})>
        <cfset structAppend(theObjModel.group,{"actions":[]})>
    	
        <!--- Inject Data --->

        <cfif NOT arrayIsEmpty(objectData)>
        
        	<cfset assetID = objectData[1].asset_id>
        
        	<cfquery name="contentAsset">  
                SELECT  displayType, instanceName, instanceAsset_id
                FROM	ContentAssets
                WHERE   asset_id = #assetID#           
            </cfquery>
            
            <cfset allTypes = ["3D Model","Modal","Panel","PopOver"]>
           	
            <cfset displayType = allTypes[contentAsset.displayType]>
            <cfset theObjModel[theKey].display.type = displayType>
  
            <cfif contentAsset.displayType GT 0>
            
            	<!--- 3D Model properties --->
                <cfif contentAsset.displayType IS 1><!--- 3dModel type --->

                    <!--- model --->
                    <cfif contentAsset.instanceName NEQ ''>
                		<cfset theObjModel[theKey].display.model = contentAsset.instanceName>
                    <cfelse>
                    	<cfset structDelete(theObjModel[theKey].display,'model')>
                    </cfif>
                    <!--- asset id --->
					<cfif contentAsset.instanceAsset_id GT 0>
                    	<cfset theObjModel[theKey].display.assetID = contentAsset.instanceAsset_id>
                    <cfelse>
                    	<cfset structDelete(theObjModel[theKey].display,'assetID')>
                    </cfif>
                    
                <cfelse>
                	<!--- Other Display types --->
                    <cfset structDelete(theObjModel[theKey].display,'model')>
                    <cfset structDelete(theObjModel[theKey].display,'assetID')>
                    
                </cfif>  
                
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'display')>
        	</cfif>
            
            <cfloop index="anObject" array="#objectData#">

                <!--- Get Asset Data --->
                <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                    <cfinvokeargument name="assetID" value="#anObject.content_id#"/>
                </cfinvoke>
                
                <!--- Add Assets to Group Object --->
            	<cfset arrayAppend(theObjModel[theKey].assets,assetData)>
                
            </cfloop>

			<cfset theObjModel[theKey].asset_id = anObject.asset_id>
            <cfset theObjModel[theKey].modified = anObject.assetModified>
               
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#anObject.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
   
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- GAME 19 --->
    <cffunction	name="gameModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"game":{}}>
        <cfset structAppend(theObjModel.game,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 19>
        
		<cfset structAppend(theObjModel.game,{"silentmode":0})>
		
        <cfset structAppend(theObjModel.game,{"startmessage":''})>
        <cfset structAppend(theObjModel.game,{"endMessage":''})>
        
		<cfset structAppend(theObjModel.game,{"gameStart":''})>
        <cfset structAppend(theObjModel.game,{"gameEnd":''})>
        
        <cfset structAppend(theObjModel.game,{"sendplayeremail":''})>
        <cfset structAppend(theObjModel.game,{"sendclientemail":''})>
        <cfset structAppend(theObjModel.game,{"clientemail":''})>
        <cfset structAppend(theObjModel.game,{"emailmessage":''})>
        
        <cfset structAppend(theObjModel.game,{"includecode":''})>
        <cfset structAppend(theObjModel.game,{"sendassets":''})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>

            <cfset theObjModel[theKey].clientemail = objectData.email>
            
            <cfset theObjModel[theKey].emailMessage = objectData.emailMessage>
            <cfset theObjModel[theKey].endMessage = objectData.messageEnd>
            <cfset theObjModel[theKey].startMessage = objectData.messageStart>
            
            <cfset theObjModel[theKey].gameStart = objectData.dateStart>
            <cfset theObjModel[theKey].gameEnd = objectData.dateEnd>  
            
            <cfset theObjModel[theKey].includeCode = objectData.useCode>
            <cfset theObjModel[theKey].sendClientEmail = objectData.sendClientEmail>
            <cfset theObjModel[theKey].sendPlayerEmail = objectData.sendPlayerEmail>
            <cfset theObjModel[theKey].silentmode = objectData.silentmode> 
            
            <cfset theObjModel[theKey].sendAssets = objectData.sendTargets>
            
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>

            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- QUIZ 20 --->
    <cffunction	name="quizModel" access="public" returntype="struct" output="true">
        <cfargument	name="choices" type="array" required="no" default="#arrayNew(1)#" />
        <cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
		
        <!--- Get Colors --->
        <cfinvoke  component="ObjectModels" method="colorsModel" returnvariable="colors" />
   
        <cfif arrayLen(choices) IS 0>
        	<cfset aChoiceSelection = {"message":"A Selection","correct":true, "choice_id":0, "colors":Duplicate(colors.colors)}>
            <cfset choices = []>
            <cfset arrayAppend(choices,Duplicate(aChoiceSelection))>
            <cfset arrayAppend(choices,Duplicate(aChoiceSelection))>
        </cfif>
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"quiz":{}}>
    	<cfset structAppend(theObjModel.quiz,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 20>
        
		<!--- spacing: spacing between each grid item --->
        <!--- padding: padding inside grid container --->
        <!--- cols: number of cols h --->
        
        <cfset structAppend(theObjModel.quiz,{"selection":{}})>
        <cfset structAppend(theObjModel.quiz.selection,{"layout":{"spacing":10, "padding":20, "cols":2}})>
        <cfset structAppend(theObjModel.quiz.selection,{"colors":Duplicate(colors.colors)})>
		
        <!--- Image assets for Choice --->
        <cfset selectionImage = {}>
        <cfset structAppend(selectionImage,{"width":0, "height":0, "ratio":0})>
        <cfset structAppend(selectionImage,{"url":{}})>
        <cfset structAppend(selectionImage.url,{"xdpi":{"url":""}})>
		<cfset structAppend(selectionImage.url,{"mdpi":{"url":""}})>
        
        <cfset choice = {"message":"The Answers Text", "image":Duplicate(selectionImage), "correct":false, "choice_id":0}>
        <cfset structAppend(theObjModel.quiz.selection,{"choices":[]})>

		<!--- Choices, Default Boolean --->
        <cfloop index="z" from="1" to="#arrayLen(choices)#">
            
            <cfset choiceSelection = Duplicate(choice)>
            <cfset choiceSelection.message = choices[z].message>			<!--- Text for Choice --->
            <cfset choiceSelection.correct = choices[z].correct>			<!--- Correct Flag --->
            <cfset choiceSelection.choice_id = choices[z].choice_id>		<!--- Choice ID --->
            
            <cfset arrayAppend(theObjModel.quiz.selection.choices,choiceSelection)>
            
        </cfloop>  
             
 		<!--- Highlight Correct immediatly after selection has been made --->
		<cfset structAppend(theObjModel.quiz,{"displayCorrect":true})>
        
        <!--- Display Response - Correct or Incorrect --->
        <cfset content = {"assets":[], "message":"Sample Message"}>
        <cfset structAppend(theObjModel.quiz,{"response":{"correct":Duplicate(content),"incorrect":Duplicate(content)}})>
        
        <!--- complete: is whether you want the quiz is optional --->
        <!--- tryagain: is whether you want the quiz have them try again until correct --->
        <!--- displaycorrect: is whether you want the quiz highlight the selection if it is correct --->
        
        <!--- Must Complete Question to continue otherwise optional --->
        <cfset structAppend(theObjModel.quiz,{"complete":true})>
        
        <!--- Retry Question --->
        <cfset structAppend(theObjModel.quiz,{"tryAgain":0})>
        
        <!--- Messages for Question, Incorrect and Correct --->
        <cfset structAppend(theObjModel.quiz,{"question":Duplicate(content)})>
        
        <!--- Allow selections to be reordered - movable --->
        <cfset structAppend(theObjModel.quiz,{"movable":false})>
		
        <!--- Inject Data --->
		
		<cfif NOT structIsEmpty(objectData)>
            
            <cfset dataRec = structNew()>
       
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
        
			<!--- Assets for Quiz, Correct and Incorrect --->
            <cfinvoke  component="Modules" method="getGroupAssetsActions" returnvariable="allQuizAssets">
              <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
             </cfinvoke>  
             
             
             <!--- Assets --->
             <cfset quizAssets = {"quiz":[], "correct":[], "incorrect":[]}>

             <cfloop query="allQuizAssets.assets">

                <cfinvoke component="Content" method="getAssetContent" returnvariable="AssetData">
                    <cfinvokeargument name="assetID" value="#content_id#"/>
                </cfinvoke>

                <cfif quiz>
                    <cfset arrayAppend(quizAssets.quiz,AssetData)>
                <cfelseif quizCorrect>
                    <cfset arrayAppend(quizAssets.correct,AssetData)>
                <cfelseif quizIncorrect>
                    <cfset arrayAppend(quizAssets.incorrect,AssetData)>
                </cfif>
              
             </cfloop>
            
			<!--- Quiz --->   
              
            <cfif objectData.complete>
                <cfset theObjModel[theKey].complete = objectData.complete>
             <cfelse>
            	<cfset structDelete(theObjModel[theKey],'complete')>
            </cfif>
            
            <cfset theObjModel[theKey].question= {"message":objectData.questionMessage,"assets":quizAssets.quiz}>
            
            <cfif objectData.needResponse>
           
                <cfset theObjModel[theKey].response.correct = {"message":objectData.correctMessage,"assets":quizAssets.correct}>
                <cfset theObjModel[theKey].response.incorrect = {"message":objectData.incorrectMessage,"assets":quizAssets.incorrect}>
                
                <cfif objectData.tryAgain GT 0>
                    <cfset theObjModel[theKey].tryAgain = objectData.tryAgain>
                <cfelse>
            		<cfset structDelete(theObjModel[theKey],'tryAgain')>
                </cfif>
            
            <cfelse>
            	
                <cfset structDelete(theObjModel[theKey],'response')>
            	<cfset structDelete(theObjModel[theKey],'tryAgain')>
              
            </cfif>
        
			<!--- Selection ---> 
            
            <cfif objectData.displayCorrect>
                <cfset theObjModel[theKey].displayCorrect = objectData.displayCorrect>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'displayCorrect')>
            </cfif>
            
            <cfif objectData.movable>
                <cfset theObjModel[theKey].movable = objectData.movable>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'movable')>
            </cfif>

     
			<!--- Grid --->
            <cfquery name="selectionGrid">  
                SELECT  SelectionAssets.spacing, SelectionAssets.padding, SelectionAssets.numberOfCols, Colors.forecolor, Colors.backcolor, Colors.background
                FROM	SelectionAssets LEFT OUTER JOIN Colors ON SelectionAssets.color_id = Colors.color_id
                WHERE   selection_id = #objectData.selection_id#           
            </cfquery>
            
            <cfset theObjModel[theKey].selection.layout.cols = selectionGrid.numberOfCols>
            <cfset theObjModel[theKey].selection.layout.padding = selectionGrid.padding>
            <cfset theObjModel[theKey].selection.layout.spacing = selectionGrid.spacing>
            
            <cfset theObjModel[theKey].selection.colors.forecolor = selectionGrid.forecolor>
            <cfset theObjModel[theKey].selection.colors.backcolor = selectionGrid.backcolor>
            <cfset theObjModel[theKey].selection.colors.background = selectionGrid.background>
        
			<!--- Choices --->
            
            <cfquery name="selectionChoices">  
                SELECT  url, text, correct, choice_id, selection_id, width, height, aspectRatio
                FROM	ChoiceAssets
                WHERE   selection_id = #objectData.selection_id#           
            </cfquery>
        
			<cfset choices = arrayNew(1)>
            
            <cfloop query="selectionChoices">
                
                <cfset aChoice = structNew()>
                
                <cfset structAppend(aChoice,{"choice_id":choice_id})>
                
                <cfif url NEQ ''>
                
                    <cfset filePath = assetPath &'nonretina/'& url>
                    <cfset filePathRetina = assetPath & url>
                    
                    <cfset structAppend(aChoice,{"image":{}})>
					
                    <cfset structAppend(aChoice.image,{"url": {"mdpi": {"url": filePath},"xdpi": {"url": filePathRetina}}})>
                    
                    <cfif width GT 0 AND height GT 0>>
						<cfset structAppend(aChoice.image,{"width": width})>
                        <cfset structAppend(aChoice.image,{"height": height})>
                    </cfif>
                    
                    <cfset structAppend(aChoice.image,{"ratio": aspectRatio})>
                    
                </cfif>
                
                <!--- Colors 						- TODO
                <cfif colors.forecolor NEQ ''>
                
                <cfelse>
                
                </cfif>
                
                <cfif colors.backcolor NEQ ''>
                
                <cfelse>
                
                </cfif>
                
                <cfif colors.background NEQ ''>
                
                <cfelse>
                	
                </cfif>
                --->
                
                <cfif text NEQ ''>
                    <cfset theMessage = JavaCast("string",text)>
                    <cfset structAppend(aChoice,{"message":theMessage})>
                </cfif>
                
                <cfif correct>
                    <cfset structAppend(aChoice,{"correct":correct})>
                </cfif>
                
                <cfset arrayAppend(choices,aChoice)>
            
            </cfloop>
        
			<cfset theObjModel[theKey].selection.choices = choices>
			
            
            <!--- Other --->
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
       		<cfset theObjModel[theKey].modified = objectData.assetModified>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
   	     
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- MATERIALS 21 --->
    <cffunction	name="materialModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"material":{}}>
        <cfset structAppend(theObjModel.material,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 21>
        
		<cfset structAppend(theObjModel.material,{"model3d":""})>
        <cfset structAppend(theObjModel.material,{"material":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<cfset theObjModel[theKey].material = objectData.material>
            <cfset theObjModel[theKey].model3D = objectData.model3D>
        
            <cfset theObjModel[theKey].asset_id = objectData.asset_id>
            <cfset theObjModel[theKey].modified = objectData.assetModified>
       
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    
    <!--- GEO-FENCE 22 --->
    <cffunction	name="geofenceModel" access="public" returntype="struct" output="false">

        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"geofence":{}}>
        <cfset structAppend(theObjModel.geofence,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 22>
        
		<cfset structAppend(theObjModel.geofence,{"geofence":[]})>
        <cfset structAppend(theObjModel.geofence,{"message":""})>
    	
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- PROGRAM SCEDULE 22 --->
    <cffunction	name="programSchedule" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />

        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"programSchedule":{}}>
        <cfset structAppend(theObjModel.programSchedule,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 22>
        
		<cfset structAppend(theObjModel.programSchedule,{"isScheduled":"", "startTime":0, "endTime":0, "duration":0, "assetID":0, "assets":[]})>

        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
             
            <cfset theObjModel[theKey].isScheduled = objectData.isScheduled>
            
            <cfif objectData.isScheduled>
				<cfset theObjModel[theKey].startTime = objectData.startTime>
            	<cfset theObjModel[theKey].endTime = objectData.endTime>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],"startTime")>
            	<cfset structDelete(theObjModel[theKey],"endTime")>
            </cfif>
            
            <cfset theObjModel[theKey].duration = objectData.duration>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <!--- getAsset for Display --->
            <cfif objectData.displayAsset_id GT 0>
            
                <cfinvoke component="Content" method="getAssetContent" returnvariable="displayAssetData">
                    <cfinvokeargument name="assetID" value="#objectData.displayAsset_id#"/>
                </cfinvoke>
            
            	<cfset arrayAppend(theObjModel[theKey].assets, displayAssetData)>
                
            </cfif>
             
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <cffunction	name="buildImagePaths" access="public" returntype="struct" output="false">
      	
        <cfargument	name="assetID" type="numeric" required="yes" />
        <cfargument	name="fileName" type="string" required="yes" />
        
      	<cfset theUrls = structNew()>
		
		<!--- Build Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="retinaPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
        	
		<cfset theUrl = retinaPath & fileName>
        <cfset structAppend(theUrls,{'xdpi':theUrl})>

		<cfset nonretinaPath = retinaPath & 'nonretina/'>
        	
		<cfset theUrl = nonretinaPath & fileName>
        <cfset structAppend(theUrls,{'mdpi':theUrl})>
        
        <cfreturn theUrls>
        
    </cffunction>

</cfcomponent>