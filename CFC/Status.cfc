<cfcomponent>
	<cffunction name="assetListingCheck" access="public" returntype="string">
		<cfargument name="appID" type="string" required="yes">
        
		<cfset myResult="foo">
        
        <cfquery name="allAssets">
            SELECT        Assets.asset_id, Assets.assetType_id, Assets.modified, Assets.name, Thumbnails.image, Details.title
            FROM            Assets LEFT OUTER JOIN
                                    Details ON Assets.detail_id = Details.detail_id LEFT OUTER JOIN
                                    Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id
            WHERE        (Assets.app_id = #appID#)
        </cfquery>
        
       
        
        
        <cfdump var="#allAssets#">
        
        <cfoutput query="allAssets">
        	
            <!--- getAsset --->
            <cfinvoke component="Assets" method="getAsset" returnvariable="theAsset">
                <cfinvokeargument name="assetID" value="#asset_id#"/>
            </cfinvoke>
            
            <!--- Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#asset_id#"/>
            </cfinvoke>
            
            <cfset links = structNew()>
            
            <cfswitch expression="assetType">
            
				<!--- AR --->
                <cfcase value="15">
                    <cfset filePath_D = assetPath & dat>
            		<cfset filePath_X = assetPath & xml>
                </cfcase>
                
                <!--- IMG --->
                <cfcase value="1">
                	<cfset filePath_I = assetPath & url>
                    <cfset filePath_I = assetPath &'nonretina/'& url>
                </cfcase>
                
                <!--- MOV --->
                <cfcase value="2">
                	<cfset filePath_M = assetPath & url>
					<cfset filePath_P = assetPath & placeholder>
                    <cfset filePath_P = assetPath &'nonretina/'& placeholder>
                </cfcase>
                
                <!--- PAN --->
                <cfcase value="8">
                	<cfset filePath_B = assetPath & back>
                    <cfset filePath_F = assetPath & front>
                    <cfset filePath_D = assetPath & down>
                    <cfset filePath_L = assetPath & left>
                    <cfset filePath_R = assetPath & right>
                    <cfset filePath_U = assetPath & up>
                    <cfset filePath_P = assetPath & pano>
                    
                    <cfset filePath_B = assetPath &'nonretina/'& back>
                    <cfset filePath_F = assetPath &'nonretina/'& front>
                    <cfset filePath_D = assetPath &'nonretina/'& down>
                    <cfset filePath_L = assetPath &'nonretina/'& left>
                    <cfset filePath_R = assetPath &'nonretina/'& right>
                    <cfset filePath_U = assetPath &'nonretina/'& up>
                    <cfset filePath_P = assetPath &'nonretina/'& pano>
                </cfcase>
                
                <!--- BAL --->
                <cfcase value="9">
                    <cfset filePath_N = assetPath & north>
                    <cfset filePath_S = assetPath & south>
                    <cfset filePath_W = assetPath & west>
                    <cfset filePath_E = assetPath & east>
                    
                    <cfset filePath_N = assetPath &'nonretina/'& north>
                    <cfset filePath_S = assetPath &'nonretina/'& south>
                    <cfset filePath_W = assetPath &'nonretina/'& west>
                    <cfset filePath_E = assetPath &'nonretina/'& east>
                </cfcase>
                
                <!--- PDF --->
                <cfcase value="3">
                	<cfset filePath_P = assetPath & url>
                </cfcase>
                
                <!--- 3D --->
                <cfcase value="6">
                    <cfset filePath_IOS = assetPath & url>
                    <cfset filePath_AND = assetPath & url_android>
                    <cfset filePath_OSX = assetPath & url_osx>
                    <cfset filePath_WIN = assetPath & url_windows>
                </cfcase>
                
                <!--- nothing --->
                <cfdefaultcase>
                
                </cfdefaultcase>
            
            </cfswitch>
                                                            
            <cfdump var="#theAsset#">
            
			<!--- Thumb Path --->
            <cfset filePath_X = assetPath &'thumbs'& image>
            <cfset filePath_M = assetPath &'thumbs/nonretina/'& image>
            
            
			<cfif fileExists(filePath_X)><cfset thumbRetina = "YES"><cfelse><cfset thumbRetina = "NO"></cfif>
            <cfif fileExists(filePath_M)><cfset thumbNorm = "YES"><cfelse><cfset thumbNorm = "NO"></cfif>
        

            
       </cfoutput>
        
        <cfabort>
		<cfreturn myResult>
        
	</cffunction>
</cfcomponent>