<cfcomponent>


<!--- Update AccessLevel --->
	<cffunction name="updateAccessOption" access="remote" returntype="boolean">
    
    	<cfargument name="appID" type="numeric" required="no" default="0">
    	<cfargument name="type" type="string" required="no" default="0">
        <cfargument name="accessLevel" type="numeric" required="no" default="0">
        
        <cfif type IS ''><cfreturn false></cfif>
        
        <cfif appID GT '0'>
        
            <cfquery name="UpdateLink">
                UPDATE AppOptions
                SET 
        
            <cfswitch expression="#type#">
            
            <cfcase value="email">
            	email_accessLevel = '#accessLevel#'
            </cfcase>
            
            <cfcase value="facebook">
            	facebook_accessLevel = '#accessLevel#'
            </cfcase>
            
            <cfcase value="twitter">
            	twitter_accessLevel = #accessLevel#
            </cfcase>
            
            <cfcase value="pintrest">
            	pintrest_accessLevel = #accessLevel#
            </cfcase>
            
            <cfcase value="instagram">
            	instagram_accessLevel = #accessLevel#
            </cfcase>
            
            <cfcase value="addressBook">
            	addressBook_accessLevel = #accessLevel#
            </cfcase>
            
            </cfswitch>
            
                WHERE	app_id = #appID#
            </cfquery>

        <cfelse>
        	<cfreturn false>
        </cfif>
    
        <cfreturn true>
        
	</cffunction>
    


	<!--- Delete SupportLink --->
	<cffunction name="deleteSupportLink" access="remote" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="linkID" type="numeric" required="no" default="0">
        
        <cfif linkID GT 0 AND appID GT 0>
        
           	<!---Delete Link--->
            <cfquery name="DeleteLink">
                DELETE FROM Support
                WHERE  support_id = #linkID# AND app_id = #appID#
            </cfquery>
            
        	<cfreturn true>
       
       <cfelse>
       		<cfreturn false>
       </cfif>
        
	</cffunction>
    
    
    
    <!--- Update SupportLink --->
	<cffunction name="updateSupportLink" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="linkID" type="numeric" required="no" default="0">
        <cfargument name="urlLink" type="string" required="yes" default="0">
        <cfargument name="urlName" type="string" required="yes" default="0">
        
        <cfargument name="theLevel" type="numeric" required="no" default="0">
        
        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
          <cfinvokeargument name="accessLevel" value="#theLevel#"/>
        </cfinvoke>
        
        <cfset access_id = accessID.access_id>
        
        <!---Update Link--->
		 <cfif urlName NEQ '' OR urlName NEQ ''>
                 
            <cfquery name="UpdateLink">
                UPDATE Support
                SET url = '#urlLink#', urlName = '#urlName#', access_id = #access_id#
                WHERE	support_id = #linkID#
            </cfquery>

         </cfif>
        
         <cfreturn true>
        
	</cffunction>
    
    
    <!--- New SupportLink --->
	<cffunction name="newSupportLink" access="remote" returntype="boolean">
        
		<cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="urlLink" type="string" required="yes" default="0">
        <cfargument name="urlName" type="string" required="yes" default="0">
        <cfargument name="type" type="numeric" required="no" default="0">
        
        <cfargument name="theLevel" type="numeric" required="no" default="0">
        
        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
          <cfinvokeargument name="accessLevel" value="#theLevel#"/>
        </cfinvoke>
 
        <cfset access_id = accessID.access_id>
 		
		<!---New Link--->
        <cfif urlName NEQ '' OR urlName NEQ ''>
            
            <cfquery name="newLink">
                 INSERT INTO Support (url, urlName, app_id, type, access_id)
                 VALUES ('#urlLink#', '#urlName#', #appID#, #type#, #access_id#) 
            </cfquery>  

            <cfreturn true>
            
        <cfelse>
            <cfreturn false>
        </cfif>
        
	</cffunction>
    


	<!--- Get App Options --->
	<cffunction name="getAppOptions" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="yes">
        
        <cfset theOptions = structNew()>
        
        <!--- Check if appOptions Exists --->
		<cfinvoke component="Options" method="appOptionsExist" returnvariable="OptionsExists">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfif OptionsExists>
        	<!--- Get Options --->
            <cfquery name="options">
                SELECT      userReports, userSettings, basicReports, advancedReports, sendEmail, facebook, twitter, pintrest, instagram, vuforiaLicense, registration, verification, addressBook, 
                			email_accessLevel, facebook_accessLevel, twitter_accessLevel, pintrest_accessLevel, instagram_accessLevel, addressBook_accessLevel
                FROM        AppOptions
                WHERE		app_id = #appID#
            </cfquery>
            
            <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theOptions">
                <cfinvokeargument name="query" value="#options#"/>
            </cfinvoke>
		
        </cfif>
        
		<cfreturn theOptions>
        
	</cffunction>
    
    
    <!--- Set App Options --->
    <cffunction name="setAppOptions" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="options" type="struct" required="yes">
        
        <!--- Check if appOptions Exists --->
		<cfinvoke component="Options" method="appOptionsExist" returnvariable="OptionsExists">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfset success = false>
        
        <cfif options.addressBook IS ""><cfset options.addressBook = 0></cfif>
        <cfif options.verification IS ""><cfset options.verification = 0></cfif>
        
        <cfif OptionsExists>
        
			<!--- Update DB Options --->
            <cfquery name="options"> 
                UPDATE AppOptions
                SET userReports = #options.userReports#, userSettings = #options.userSettings#,
                    basicReports = #options.basicReports#, advancedReports = #options.advancedReports#, 
                    sendEmail = #options.sendEmail#, facebook = #options.facebook#, twitter = #options.twitter#, pintrest = #options.pintrest#, instagram = #options.instagram#, registration = #options.registration#, verification = #options.verification#, addressBook = #options.addressBook#
                WHERE app_id = #appID#  
            </cfquery>
            
            <cfset success = true>

        <cfelse>
			<cfset success = false>
        </cfif>
        
        <cfreturn success>

	</cffunction>
    
    
    
    <!--- Create App Options --->
    <cffunction name="createAppOptions" access="public" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        
        <!--- Check if AppID Exists --->
        <cfquery name="apps">
            SELECT      app_id
            FROM        Applications
            WHERE		app_id = #appID#
        </cfquery>
        
        <!--- If App Exists --->
        <cfif apps.recordCount GT 0>
        
			<!--- Create App Options for AppID --->
            <cfquery name="options"> 
                INSERT INTO AppOptions (app_id, userReports, userSettings, basicReports, advancedReports, sendEmail, facebook, twitter, pintrest, instagram, addressBook)
                VALUES (#appID#, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0)
            </cfquery>
        	
            <cfreturn true>
            
        <cfelse>
        	<cfreturn false>
        </cfif>
   
	</cffunction>
    
    
    <!--- App Options Exists? --->
    <cffunction name="appOptionsExist" access="public" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        
        <cfset success = false>
        
		<cfquery name="options">
            SELECT      option_id
            FROM        AppOptions
            WHERE		app_id = #appID#
        </cfquery>
        
        <cfif options.recordCount GT 0>
			<cfset success = true>
        <cfelse>

			<!--- Create Default Options if not exist --->
            <cfinvoke component="Options" method="createAppOptions" returnvariable="success">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn success>
        
	</cffunction>
  
  
  	
    <!--- Update Social Message --->
    <cffunction name="getSocialNetworks" access="remote" returntype="array">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfset success = false>
        
		<cfquery name="socialOptions">
            SELECT		Social.message, SocialNetworks.networkName
            FROM        Social LEFT OUTER JOIN SocialNetworks ON Social.socialType = SocialNetworks.id
            WHERE		app_id = #appID# AND group_id = #groupID#
        </cfquery>
        
        <cfset socialNetworks = arrayNew(1)>
        
        <cfif socialOptions.recordCount GT 0>

            <cfoutput query="socialOptions">
            	<cfset arrayAppend(socialNetworks,{"message":message,"networkName":networkName})>
			</cfoutput>
            
        </cfif>

        <cfreturn socialNetworks>
        
	</cffunction> 
  
  
  
    <!--- Update Social Message --->
    <cffunction name="updateSocialNetwork" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="socialInfo" type="struct" required="yes">
        
        <cfset success = false>
        
        <!--- Check if Message Exists --->
        <cfinvoke component="Options" method="socialNetworkExists" returnvariable="socialMessageExists">
            <cfinvokeargument name="socialType" value="#socialInfo.type#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="groupID" value="#socialInfo.groupID#"/>
        </cfinvoke>
        
        <cfset message = trim(socialInfo.message)>
        
        <cfif socialMessageExists>
        
            <cfquery name="social">
                UPDATE Social
                <cfif message NEQ ''>
                	SET message = '#socialInfo.message#'
                <cfelse>
                	SET message = NULL
                </cfif>
                WHERE app_id = #appID# AND group_id = #socialInfo.groupID# AND socialType = #socialInfo.type#
            </cfquery>
        	
            <cfset success = true>

        </cfif>
        
        <cfreturn success>
        
	</cffunction>  
    
    
    
    <!--- Update Social Message --->
    <cffunction name="getSocialNetworkType" access="public" returntype="numeric">
		<cfargument name="socialType" type="numeric" required="yes">
        
        <cfquery name="networks">
            SELECT		social_id
            FROM        SocialNetworks
            WHERE		id = #socialType#
        </cfquery>
        
        <cfreturn networks.social_id>
        
	</cffunction>  
    
    
    
    <!--- Social Message Exists --->
    <cffunction name="socialNetworkExists" access="public" returntype="boolean">
		<cfargument name="socialType" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="networks">
            SELECT		social_id
            FROM        Social
            WHERE		socialType = #socialType# AND app_id = #appID# AND group_id = #groupID#
        </cfquery>
        
        <cfset success = false>
        
        <cfif networks.recordCount GT 0>
        	<cfset success = true>
        <cfelse>
        	
		<!--- Create SocialMessage --->
            <cfquery name="options"> 
                INSERT INTO Social (socialType, app_id, group_id, message)
                VALUES (#socialType#, #appID#, #groupID#, '')
            </cfquery>
            
            <cfset success = true>
            
        </cfif>

        <cfreturn success>
        
	</cffunction> 
    
    
    <!--- Create Social Options --->
    <cffunction name="createAppSocialNetworks" access="public" returntype="boolean">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="allNetworks">
            SELECT		social_id
            FROM        SocialNetworks
        </cfquery>
        
        <cfset success = false>
        
        <cfloop query="allNetworks">
        
			<!--- Create SocialMessage --->
            <cfquery name="socialNetworks"> 
                INSERT INTO Social (socialType, app_id, group_id, message)
                VALUES (#social_id#, #appID#, #groupID#, '')
            </cfquery>
            
            <cfset success = true>
            
        </cfloop>

        <cfreturn success>
        
	</cffunction> 
    
    
    
    
    
    <!--- Get Social Message --->
    <cffunction name="getSocialMessage" access="public" returntype="query">
        <cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">

		<!--- Get Options --->
 		<cfinvoke component="Options" method="getAppOptions" returnvariable="theOptions">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
 		
        <cfset socialNetworks = ["facebook","twitter","instagram","pintrest"]>
        
        <cfset supportedSocial = arrayNew(1)>
        
        <cfloop collection="#theOptions#" item="sn">
        
			<cfif ArrayFind(socialNetworks,sn)>
            	<cfif theOptions[sn] IS 1>
            		<cfset arrayAppend(supportedSocial,sn)>
                </cfif>
            </cfif>
        </cfloop> 
        
        <!--- All Networks --->
        <cfquery name="allNetworks">
            SELECT       lower(SocialNetworks.networkName)AS networkName, Social.message, Social.group_id, Social.socialType
			FROM         Social LEFT OUTER JOIN SocialNetworks ON Social.socialType = SocialNetworks.id
            WHERE		 app_id = #appID# AND message IS NOT NULL
        </cfquery>
        

        <!--- Network Messages --->
        <cfquery dbtype="query" name="allNetworkMessages">
        	SELECT		message, LOWER(networkName), socialType
            FROM        allNetworks
            
            <cfif groupID GT 0>
            
                WHERE
                (
                <cfloop index="z" from="1" to="#arrayLen(supportedSocial)#">
                    networkName = '#supportedSocial[z]#'
                    
                    <cfif z GT arrayLen(supportedSocial)-1>
                        <cfbreak>
                    <cfelse>
                        OR
                    </cfif>
                </cfloop>
                ) 
            	AND (group_id = #groupID#)
                
            </cfif>
            
        </cfquery>
        
        <!--- Find Missing --->
        <cfloop index="z" from="1" to="#arrayLen(supportedSocial)#">
            
            <cfquery dbtype="query" name="foundNetwork1">
                SELECT		message, LOWER(networkName), socialType
                FROM        allNetworks
            	WHERE		networkName = '#supportedSocial[z]#'
            </cfquery>
            
            <cfquery dbtype="query" name="foundNetwork2">
                SELECT		message, LOWER(networkName), socialType
                FROM        allNetworkMessages
            	WHERE		networkName = '#supportedSocial[z]#'
            </cfquery>

			<!--- Add Missing --->
            <cfif foundNetwork2.recordCount IS 0>
          		
                <cfif foundNetwork1.message NEQ ''>
					 <cfset temp = QueryAddRow(allNetworkMessages)>
                     <cfset temp = QuerySetCell(allNetworkMessages, "message", "#foundNetwork1.message#")>
                     <cfset temp = QuerySetCell(allNetworkMessages, "networkName", "#supportedSocial[z]#")>
			    </cfif>
                
            </cfif>
            
        </cfloop>
        
        <cfreturn allNetworkMessages>
        
	</cffunction> 
    
    
    
    
    <!--- Update Vuforia License --->
    <cffunction name="updateVuforiaLicense" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="license" type="string" required="yes">
        
        <cfset success = false>
        
		<cfquery name="UpdateLink">
            UPDATE AppOptions
            SET vuforiaLicense = <cfif license IS ''>NULL<cfelse>'#license#'</cfif>
            WHERE	app_id = #appID#
        </cfquery>

        <cfreturn true>
        
	</cffunction> 
    
    
</cfcomponent>