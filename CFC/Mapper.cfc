<cfcomponent>
 
    <!--- Get Mapper Points --->
    <cffunction name="getMapperPoints" access="remote" returntype="struct">
    	
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="active" type="boolean" required="no" default="true">
        
        <cfinvoke component="Assets" method="getMapperAssets" returnvariable="mapperAsset">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfset locationID = mapperAsset.location_id>
        
        <cfif active>
        	<cfset active = 1>
        <cfelse>
        	<cfset active = 0>
        </cfif>
        
        <!--- Get Mapper Points --->
        <cfquery name="mapperAssets">
            SELECT location_id
            FROM   MapperPoints
            WHERE asset_id = #assetID# AND active = #active#
        </cfquery>
		
        <cfset mapPoints = arrayNew(1)>
        
        <cfoutput query="mapperAssets">
		
        	<cfset arrayAppend(mapPoints,location_id)>
		
		</cfoutput>
        
        <cfset mapper = {"points":mapPoints, "map":locationID}>
 
        <cfreturn mapper>

	</cffunction>
    
    
</cfcomponent>