<cfcomponent>

 	<!--- Set Content of Asset --->
 	<cffunction name="setContentAccess" access="remote" returntype="boolean" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
		<cfargument name="accessLevel" type="numeric" default="0">
        
          <cfquery name="info">
              UPDATE Groups
              SET accessLevel = #accessLevel#
              WHERE	 group_id = #groupID#
          </cfquery>
        
        <cfreturn true>
        
	</cffunction>
    
    


 	<!--- Get Group of Asset --->
 	<cffunction name="getAssetGroup" access="remote" returntype="query" output="no">
        <cfargument name="assetID" type="numeric" required="yes" default="0">

          <cfquery name="info">
              SELECT group_id AS groupID, app_id AS appID
              FROM	 Groups
              WHERE	 asset_id = #assetID#
          </cfquery>
        
        <cfreturn info>
        
	</cffunction>





 	<!--- Set Group Sort Order --->
 	<cffunction name="setSortOrder" access="remote" returntype="boolean" output="yes">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="order" type="numeric" required="no" default="0">

          <cfquery name="result">
          
              UPDATE Groups
              SET sortOrder = #order#
              WHERE 0 = 0
                    
			  <cfif assetID GT '0'>
                  AND(asset_id = #assetID#)
              </cfif>
              <cfif groupID GT '0'>
                  AND(group_id = #groupID#)
              </cfif>  
              
          </cfquery>

        <cfreturn true>
        
	</cffunction>
    
    
    

  	<!--- Set Group OR Group Asset Sharable State --->
 	<cffunction name="setAssetShareState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="share" type="numeric" required="no" default="0">
        
        <cfif groupID GT '0' OR assetID GT '0'>

                <cfquery name="result">
                    UPDATE Groups
                    SET sharable = #share#
                    WHERE 0 = 0
                    
                    <cfif assetID GT '0'>
                        AND(asset_id = #assetID#)
                    </cfif>
                    <cfif groupID GT '0'>
                        AND(group_id = #groupID#)
                    </cfif> 
                    
                </cfquery>

        </cfif>

        <!--- Get Sharable state --->
        <cfinvoke component="Modules" method="getAssetShareState" returnvariable="shareState">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
        
        <cfreturn shareState>
        
	</cffunction>
    
    
    
 
   	<!--- Get Group OR Group Asset Sharable State --->
 	<cffunction name="getAssetShareState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
		
        <cfset shareState = ''>
        
        <cfif groupID GT '0' OR assetID GT '0'>
        
			<cfif groupID GT '0'>
                
                <cfquery name="result">
                    SELECT sharable
                    FROM Groups
                    WHERE group_id = #groupID#
                </cfquery>
            	
                <cfif result.recordCount GT '0'>
                	<cfset shareState = result.sharable>
                </cfif>
                
            <cfelse>
            
                <cfquery name="result">
                    SELECT sharable
                    FROM Groups
                    WHERE asset_id = #assetID#
                </cfquery>
                
                <cfif result.recordCount GT '0'>
                	<cfset shareState = result.sharable>
                </cfif>
                
            </cfif>  

        </cfif>

        <cfreturn shareState>  
        
	</cffunction> 




  	<!--- Set Group OR Group Asset Cached State --->
 	<cffunction name="setAssetCachedState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="active" type="numeric" required="no" default="0">
        
        <cfif groupID GT '0' OR assetID GT '0'>

                <cfquery name="result">
                    UPDATE Groups
                    SET cached = #active#
                    WHERE 0 = 0
                    
                    <cfif assetID GT 0>
                        AND(asset_id = #assetID#)
                    </cfif>
                    <cfif groupID GT 0>
                        AND(group_id = #groupID#)
                    </cfif> 
                    
                </cfquery>

        </cfif>

        <!--- Get Share state --->
        <cfinvoke component="Modules" method="getAssetCachedState" returnvariable="activeState">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
        
        <cfreturn activeState>
        
	</cffunction>
    
    
    
    <!--- Get Group OR Group Asset Cached State --->
 	<cffunction name="getAssetCachedState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
		
        <cfset activeState = ''>
        
        <cfif groupID GT '0' OR assetID GT '0'>
        
			<cfif groupID GT '0'>
                
                <cfquery name="result">
                    SELECT cached
                    FROM Groups
                    WHERE group_id = #groupID#
                </cfquery>
            	
                <cfif result.recordCount GT '0'>
                	<cfset activeState = result.cached>
                </cfif>
                
            <cfelse>
            
                <cfquery name="result">
                    SELECT cached
                    FROM Groups
                    WHERE asset_id = #assetID#
                </cfquery>
                
                <cfif result.recordCount GT '0'>
                	<cfset activeState = result.cached>
                </cfif>
                
            </cfif>  

        </cfif>

        <cfreturn activeState>  
        
	</cffunction>   
    
    
    
    
    



  	<!--- Set Group OR Group Asset Active State --->
 	<cffunction name="setAssetActiveState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="active" type="numeric" required="no" default="0">
        
        <cfif groupID GT '0' OR assetID GT '0'>

                <cfquery name="result">
                    UPDATE Groups
                    SET active = #active#
                    WHERE 0 = 0
                    
                    <cfif assetID GT '0'>
                        AND(asset_id = #assetID#)
                    </cfif>
                    <cfif groupID GT '0'>
                        AND(group_id = #groupID#)
                    </cfif> 
                    
                </cfquery>

        </cfif>

        <!--- Get Share state --->
        <cfinvoke component="Modules" method="getAssetShareState" returnvariable="activeState">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
        
        <cfreturn activeState>
        
	</cffunction>
    
    
    
    
 
   	<!--- Get Group OR Group Asset Active State --->
 	<cffunction name="getAssetActiveState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
		
        <cfset activeState = ''>
        
        <cfif groupID GT '0' OR assetID GT '0'>
        
			<cfif groupID GT '0'>
                
                <cfquery name="result">
                    SELECT active
                    FROM Groups
                    WHERE group_id = #groupID#
                </cfquery>
            	
                <cfif result.recordCount GT '0'>
                	<cfset activeState = result.active>
                </cfif>
                
            <cfelse>
            
                <cfquery name="result">
                    SELECT active
                    FROM Groups
                    WHERE asset_id = #assetID#
                </cfquery>
                
                <cfif result.recordCount GT '0'>
                	<cfset activeState = result.active>
                </cfif>
                
            </cfif>  

        </cfif>

        <cfreturn activeState>  
        
	</cffunction>   


    
    
    
    
	<!--- Get Groups --->
 	<cffunction name="getAppGroups" access="public" returntype="array" output="yes">
        <cfargument name="appID" type="numeric" required="yes" default="0">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="allGroups" type="array" required="no" default="#arrayNew(1)#">
        
        <cfquery name="groups">
			SELECT	group_id, Groups.name, Details.title AS title
            FROM 	Groups LEFT OUTER JOIN Details ON Groups.detail_id = Details.detail_id
            WHERE	app_id = #appID# AND name <> ''
            <cfif groupID GT '0'>
            AND subgroup_id = #groupID#
            </cfif>
            ORDER BY subgroup_id, sortOrder ASC
        </cfquery>
        
        <cfif groups.recordCount EQ 0><cfreturn []></cfif>
        
        <cfoutput query="groups">
		
        	<cfset arrayAppend(allGroups, {"group_id":#group_id#, "name":#Groups.name#, "title":#title#})>
			
            <cfinvoke component="Modules" method="getAppGroups" returnvariable="moreData">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#group_id#"/>
                <cfinvokeargument name="allGroups" value="#allGroups#"/>
            </cfinvoke>
            
            <cfif arrayLen(moreData) GT 0>
                <cfinvoke component="Misc" method="mergeArrays" returnvariable="allGroups">
                    <cfinvokeargument name="array1" value="#allGroups#"/>
                    <cfinvokeargument name="array2" value="#moreData#"/>
                </cfinvoke>
            </cfif>
   
		</cfoutput>
        																		
        <cfreturn allGroups>
        
    </cffunction>   
    
    
    
    <!--- Get Groups --->
 	<cffunction name="getCMSAppGroups" access="public" returntype="query">
        <cfargument name="appID" type="numeric" required="yes" default="0">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfquery name="groups">
			SELECT	group_id, Groups.name, Details.title AS title
            FROM 	Groups LEFT OUTER JOIN Details ON Groups.detail_id = Details.detail_id
            WHERE	app_id = #appID# AND name <> ''
            <cfif groupID GT '0'>
            AND subgroup_id = #groupID#
            </cfif>
            ORDER BY subgroup_id, sortOrder ASC
        </cfquery>
        
        <cfreturn groups>
        
    </cffunction>   
    
    
    
    <!--- Get Groups with codes --->
 	<cffunction name="getAccessAppGroups" access="public" returntype="query">
        <cfargument name="appID" type="numeric" required="yes" default="0">
        
        <cfquery name="groups">
            SELECT DISTINCT Groups.name, Groups.subgroup_id, Groups.sortOrder, Groups.group_id AS group_id
			FROM            AccessCodes RIGHT OUTER JOIN
                        Groups ON AccessCodes.group_id = Groups.group_id
            WHERE        (Groups.app_id = #appID#) AND (Groups.name <> '')
            ORDER BY 	  Groups.subgroup_id, Groups.sortOrder
        </cfquery>
        
        <cfreturn groups>
        
    </cffunction> 



	<!--- Move Group and Assets to Group --->
 	<cffunction name="moveGroupAsset" access="remote" returntype="boolean">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="moveToGroupID" type="numeric" required="yes" default="0">

        <cfquery name="moveToGroup">
			UPDATE Groups
            SET subgroup_id = #moveToGroupID#
            WHERE asset_id = #assetID#
        </cfquery>
        
        <cfreturn true>
        
    </cffunction>       
 
 
 
 
 <!--- Move Assets to Group --->
 	<cffunction name="moveGroup" access="public" returntype="boolean">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        <cfargument name="moveToGroupID" type="numeric" required="yes" default="0">
      
        <cfquery name="moveToGroup">
        	UPDATE Groups
            SET subgroup_id = #moveToGroupID#
            WHERE group_id = #groupID#
        </cfquery>
        
        <cfreturn true>
        
    </cffunction>     
    
     
     

 	<!--- Get Group Details --->
 	<cffunction name="getGroupDetails" access="public" returntype="query">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <!--- Group Details --->
		<cfquery name="groupInfo">
            SELECT        AccessLevels.accessLevel, Groups.name, Groups.sharable, Thumbnails.image AS thumbnail, Thumbnails.width, Thumbnails.height, 
                         Details.detail_id, Details.title, Details.subtitle, Details.description, 
                         Details.other, Details.titleColor, Details.subtitleColor, Details.descriptionColor, Groups.asset_id, 
                         AssetTypes.path, AssetTypes.dbTable, Groups.modified, Groups.app_id, Assets.assetType_id, Groups.sortOrder, 
                         Colors.background, Colors.backColor, Colors.foreColor, Colors.otherColor, Colors.color_id, Colors.displayAsset_id
			FROM            Colors RIGHT OUTER JOIN
                         Groups ON Colors.color_id = Groups.color_id LEFT OUTER JOIN
                         AssetTypes INNER JOIN
                         Assets ON AssetTypes.assetType_id = Assets.assetType_id ON 
                         Groups.asset_id = Assets.asset_id LEFT OUTER JOIN
                         Thumbnails ON Groups.thumb_id = Thumbnails.thumb_id LEFT OUTER JOIN
                         Details ON Groups.detail_id = Details.detail_id LEFT OUTER JOIN
                         AccessLevels ON Groups.accessLevel = AccessLevels.access_id
            WHERE        
            <cfif groupID IS '-1'>
            (Groups.subgroup_id = #groupID#)
            <cfelse>
            (Groups.group_id = #groupID#)
            </cfif>
        </cfquery>

        <cfreturn groupInfo>
        
    </cffunction>
    
    
    
    <!--- delete Asset From A Unknown Groups --->
 	<cffunction name="deleteAssetFromGroups" access="public" returntype="boolean">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <!--- Delete All Assets In Groups --->
		<cfquery name="allGroups">
        	DELETE FROM Groups
            WHERE asset_id = #assetID#
		</cfquery>

        <cfreturn true>
        
    </cffunction>



    <!--- update GroupAsset ModifiedDate --->
 	<cffunction name="updateGroupAssetModifiedDate" access="public" returntype="boolean">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
     
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- Update All Groups Where Asset is In --->
		<cfquery name="allGroups">
        	UPDATE Groups
            SET modified = #curDate#
            WHERE asset_id = #assetID#
		</cfquery>
        
        <!--- Get All Groups Where Asset is in and Update Group Modified Date --->
        <cfquery name="groups">
        	SELECT subgroup_id
            FROM Groups
            WHERE asset_id = #assetID#
        </cfquery>

        
			<cfoutput query="groups">
            
                <cfset groupID = groups.subgroup_id>
                
      			<cfif subgroup_id NEQ ''>
                    <cfquery name="group">
                        UPDATE	Groups
                        SET		modified = #curDate#
                        WHERE	group_id = #subgroup_id#                
                    </cfquery>
            	</cfif>
                
            </cfoutput>
		
        
        
        <cfreturn true>
        
    </cffunction>



    
    <!---Update Asset Details--->
    <cffunction name="updateGroupAsset" access="public" returntype="any" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">

        <!--- Get Epoch --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="modDate" />
        
        <cfset groupID = assetData.groupID> 
        
        <!--- Update Group Asset - Name and Modified --->
		<cfif assetData.groupID GT '0'>
        
            <cfquery name="updateAsset">
                UPDATE Groups
                SET modified = #modDate#, name = '#assetData.name#'
                WHERE group_id = #groupID#
            </cfquery>

        </cfif>
        
    <cfreturn groupID>
    
    </cffunction> 
    
 
 
 
 
     <!--- Get AssetID From Content GroupID--->
    <cffunction name="getAssetIDFromContentGroup" access="remote" returntype="struct" output="no">
    	
        <cfargument name="groupID" type="numeric" required="yes" default="0">
	
        <cfquery name="asset">
        	SELECT asset_id, subgroup_id, app_id
            FROM Groups
            WHERE group_id = #groupID#
        </cfquery>
        
		<cfif asset.recordCount GT '0'>
        	<cfset assetID = {"groupID":asset.subgroup_id, "assetID":asset.asset_id, "appID":asset.APP_ID}>
        <cfelse>
        	<cfset assetID = {"groupID":0, "assetID":0, "appID":0}>
        </cfif>
        
    	<cfreturn assetID>
    
    </cffunction> 
 
    
    
	
	
	<!---Update An Asset--->
    <cffunction name="updateGroupDetails" access="public" returntype="boolean" output="yes">
    	
        <cfargument name="assetData" type="struct" required="yes" default="structNew()">
        <cfargument name="detailData" type="struct" required="no" default="structNew()">
        <cfargument name="thumbData" type="struct" required="no" default="structNew()">
        <cfargument name="colorData" type="struct" required="no" default="structNew()">
        
        <cfargument name="appID" type="numeric" required="no" default="0">

		<!---Update Details--->
        <cfinvoke component="Assets" method="updateDetails" returnvariable="updatedDetails">
            <cfinvokeargument name="assetData" value="#assetData#"/>
            <cfinvokeargument name="detailData" value="#detailData#"/>
        </cfinvoke>

        <!---Update Thumbnail--->
        <cfinvoke component="Assets" method="updateThumbnail" returnvariable="updatedThumbnail">
            <cfinvokeargument name="assetData" value="#assetData#"/>
            <cfinvokeargument name="thumbData" value="#thumbData#"/>
        </cfinvoke>
        
        <!---Update Colors--->
        <cfinvoke component="Assets" method="updateColors" returnvariable="updatedColors">
            <cfinvokeargument name="assetData" value="#assetData#"/>
            <cfinvokeargument name="colorData" value="#colorData#"/>
        </cfinvoke>
        
        <!---Update Asset Details--->
        <cfinvoke component="Modules" method="updateGroupAsset" returnvariable="updatedAsset">
            <cfinvokeargument name="assetData" value="#assetData#"/>
        </cfinvoke>
        
        <!--- <cfif appID GT '0'>
        
        <!--- Update Cashed JSON --->
            <cfinvoke component="Apps" method="generateContentJSON" returnvariable="result">
                <cfinvokeargument name="groupID" value="#assetData.groupID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
        </cfif> --->
        
        <cfreturn true>
        
    </cffunction>
    
    
    
    
    

 	<!--- Delete Group Asset --->
 	<cffunction name="deleteAssetInGroup" access="remote" returntype="boolean">
    	<cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <!--- Delete Asset --->
		<cfquery name="deleteDetails">
            DELETE FROM Groups
            WHERE asset_id = #assetID# AND subgroup_id = #groupID#
        </cfquery>
        
        <cfreturn true>
        
    </cffunction>
    
    
    
    
    
    <!--- Delete Group and All Assets In Group--->
 	<cffunction name="deleteGroupAssets" access="remote" returntype="boolean">
    	<cfargument name="subgroupID" type="numeric" required="yes" default="0">

		<!--- Get Subgroup All Assets --->
        <cfinvoke component="Modules" method="getAssetsInGroup" returnvariable="assetsToDelete">
            <cfinvokeargument name="groupID" value="#subgroupID#"/>
        </cfinvoke>
		
        <cfif assetsToDelete.recordCount GT '0'>
            
        <!--- To Delete --->
        <cfoutput query="assetsToDelete">
			
            <cfif asset_id NEQ ''>
				<!--- Assets --->
                 <cfinvoke component="Modules" method="deleteAssetInGroup" returnvariable="assetsDeleted">
                    <cfinvokeargument name="assetID" value="#asset_id#"/>
					<cfinvokeargument name="groupID" value="#subgroupID#"/>
                </cfinvoke>
            <cfelse>
                <!--- Groups --->
                <cfinvoke component="Modules" method="deleteGroupAssets" returnvariable="groupsToDeletes">
                    <cfinvokeargument name="subgroupID" value="#group_id#"/>
                </cfinvoke>
                
                <!--- Delete Group --->
                <cfquery name="deleteGroup">
                    DELETE FROM Groups
                    WHERE group_id = #subgroupID#
                </cfquery>
        
            </cfif>
            
		</cfoutput>
        
        </cfif>
        
        <!--- Delete Group --->
        <cfquery name="deleteGroup">
            DELETE FROM Groups
            WHERE group_id = #subgroupID#
        </cfquery>
        
        <cfreturn true>
        
    </cffunction>


	<!---Get All Group Details--->
    <cffunction name="getAllGroupDetails" access="public" returntype="query" output="yes">
  	
        <cfargument name="groupID" type="numeric" required="yes" default="0">
           
          <cfquery name="details"> 
                SELECT        Details.detail_id, Thumbnails.thumb_id, Thumbnails.width, Thumbnails.height, Thumbnails.image AS thumbnail, Colors.color_id
				FROM            Groups LEFT OUTER JOIN
                         Colors ON Groups.color_id = Colors.color_id LEFT OUTER JOIN
                         Thumbnails ON Groups.thumb_id = Thumbnails.thumb_id LEFT OUTER JOIN
                         Details ON Groups.detail_id = Details.detail_id
    
                WHERE        (Groups.group_id = #groupID#)
           </cfquery>
          
          <cfreturn details>
      
    </cffunction>



 	<!--- Add Group Multiple Assets (Multiple)--->
 	<cffunction name="addGroupMultipleAssets" access="public" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
    	<cfargument name="subgroupID" type="numeric" required="yes" default="0">
        <cfargument name="assetIDs" type="string" required="yes" default="0">

        <cfif listLen(assetIDs) GT '0'>
        	<!--- Loop all Assets IDS --->
            <cfloop index="z" list="#assetIDs#" delimiters=",">
            
                <!--- Add an Asset --->
                <cfinvoke component="Modules" method="addGroupAsset" returnvariable="items">
                    <cfinvokeargument name="appID" value="#session.appID#"/>
                    <cfinvokeargument name="subgroupID" value="#session.subgroupid#"/>
                    <cfinvokeargument name="assetID" value="#z#"/>
                </cfinvoke>
                
            </cfloop>
        
        </cfif>
        
        <cfreturn true>
        
    </cffunction>





 	<!--- Add Group Asset--->
 	<cffunction name="addGroupAsset" access="public" returntype="numeric" output="yes">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
    	<cfargument name="subgroupID" type="numeric" required="yes" default="0">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfargument name="accessLevel" type="numeric" required="no" default="0">
        <cfargument name="active" type="numeric" required="no" default="0">
        <cfargument name="order" type="numeric" required="no" default="0">
        
        <cfset groupID = '0'>
        <cfif assetID GT '0'>
        
        <!--- Get Access Level from ID --->
        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
          <cfinvokeargument name="accessID" value="#accessLevel#"/>
        </cfinvoke>
        
        <cfset accessLevel = accessID.accessLevel>

        <!--- Get Epoch --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
            <cfquery name="newGroup"> 
                INSERT INTO Groups (accessLevel, active, name, sortOrder, app_id, asset_ID, created, modified, subgroup_id)								<!---  FIX ACCESS LEVEL --->
                VALUES (#accessLevel#, #active#, '', #order#, #appID#, #assetID#, #curDate#, #curDate#
                <cfif subgroupID GTE '0'>
            	,#subgroupID#
                <cfelse>
                , NULL
            	</cfif>
                )
                SELECT @@IDENTITY AS groupID
            </cfquery>
    
            <cfset groupID = newGroup.groupID>
            
		</cfif>
        
        <cfreturn groupID>
        
    </cffunction>

 	<!--- Add Group --->
 	<cffunction name="addGroup" access="public" returntype="numeric">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
    	<cfargument name="subgroupID" type="numeric" required="yes" default="0">
        
        <cfargument name="accessLevel" type="numeric" required="no" default="0">
        <cfargument name="active" type="numeric" required="no" default="0">
        <cfargument name="name" type="string" required="no" default="default">
        <cfargument name="order" type="numeric" required="no" default="0">
        
        <cfset groupID = '0'>
  		
        <!--- Get Access Level from ID --->
        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
          <cfinvokeargument name="accessID" value="#accessLevel#"/>
        </cfinvoke>
        
        <cfset accessLevel = accessID.accessLevel>
        
        
        <!--- Get Epoch --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
		<cfquery name="newGroup"> 
            INSERT INTO Groups (accessLevel, active, name, sortOrder, app_id, created, modified, subgroup_id)											<!---  FIX ACCESS LEVEL --->
            VALUES (#accessLevel#, #active#, '#name#', #order#, #appID# , #curDate#, #curDate#
            <cfif subgroupID GT '0'>
            	,#subgroupID#
                <cfelse>
                , 0
            </cfif>
            )
            SELECT @@IDENTITY AS groupID
        </cfquery>

		<cfset groupID = newGroup.groupID>
		
        <cfreturn groupID>
        
    </cffunction>
    
 
  	<!--- Get All Assets in Module --->
 	<cffunction name="getAssetsInGroup" access="public" returntype="query">
    	<cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfquery name="allGroups">
            SELECT        group_id, asset_id
            FROM          Groups
			WHERE		 (subgroup_id = #groupID#)
        </cfquery>
        
        <cfreturn allGroups>
        
    </cffunction>
    
    
    
 	<!--- Get Groups from Module --->
 	<cffunction name="getGroupName" access="public" returntype="struct">
    	<cfargument name="subgroupID" type="numeric" required="yes" default="0">
        
        <cfset groupInfo = structNew()>
        
        <cfquery name="allGroups">
            SELECT        Groups.group_id, Groups.name, Groups.active, Details.title, AccessLevels.accessLevel AS accessLevel
			FROM          Groups LEFT OUTER JOIN
                         AccessLevels ON Groups.accessLevel = AccessLevels.access_id LEFT OUTER JOIN
                         Details ON Groups.detail_id = Details.detail_id																		
			WHERE		 (group_id = #subgroupID#)
        </cfquery>
       
        <cfif allGroups.name IS ''>
        	<cfset groupName = allGroups.title>
        <cfelse>
        	<cfset groupName = allGroups.name>
        </cfif>
        
        <cfset groupInfo = {"name": groupName, "id": allGroups.group_id, "active": allGroups.active, "access":allGroups.accessLevel}>
        
        <cfreturn groupInfo>
        
    </cffunction>



 	<!--- Get ContentID from GroupID and AssetID --->
 	<cffunction name="getContentID" access="public" returntype="numeric">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">

        <cfquery name="content">
            SELECT        group_id
            FROM          Groups
			WHERE		 0 = 0
						<cfif groupID GT '0'>
                        	AND (subgroup_id = #groupID#) 
                    	</cfif>
                    	<cfif assetID GT '0'>    
                        	AND (asset_id = #assetID#)
                    	</cfif>
        </cfquery>
        
        <cfif content.recordCount GT '0'>
        	<cfset contentID = content.group_id>
        <cfelse>
        	<cfset contentID = 0>
        </cfif>
        
        <cfreturn contentID>
        
    </cffunction>







 	<!--- Get Groups Listing --->
 	<cffunction name="getGroupListing" access="public" returntype="query">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
        
			<cfquery name="content">
                SELECT      group_id, name
                FROM        Groups
                WHERE		app_id = #appID# AND name <> ''
			</cfquery>
            
            <cfreturn content>
            
	</cffunction>
    
    
    
    


 	<!--- Get Groups from Module --->
 	<cffunction name="getGroups" access="public" returntype="array">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
		
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="subgroupID" type="numeric" required="no" default="0">
        
        <cfargument name="active" type="any" required="no" default="">
        
        <cfquery name="allGroups">
            SELECT        Groups.group_id, Groups.created, Groups.modified, Groups.name, Groups.detail_id, Groups.asset_id, 
                         Groups.subgroup_id, Groups.active, Groups.cached, Groups.sortOrder, Groups.sharable, AccessLevels.accessLevel, Groups.accessLevel AS access
			FROM            Groups LEFT OUTER JOIN AccessLevels ON Groups.accessLevel = AccessLevels.access_id
			WHERE		0 = 0
            		<cfif appID GTE 0>
            			AND	(app_id = #appID#) 
                    </cfif>
                    <cfif active NEQ ''>
            			AND (active = #active#)
            		</cfif>
                    <cfif groupID GT 0>
            			AND (group_id = #groupID#)
            		<cfelse>
                    
						<cfif subgroupID GT '0'>
                            AND (subgroup_id = #subgroupID#)
                        <cfelse>
                            AND (subgroup_id = 0)
                        </cfif>
                        
                    </cfif>
            ORDER BY sortOrder
        </cfquery>
       
        <cfset groups = arrayNew(1)>

        <cfloop query="allGroups">

			<cfif asset_id LT '0'>

            <!--- Get Items --->
             <cfset items = arrayNew(1)>
                         
				<!--- Number of Items in Group --->
<!---                  <cfinvoke component="CFC.Modules" method="getGroups" returnvariable="items">
                	<cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="subgroupID" value="#group_id#"/>
                </cfinvoke> --->

            	<cfset totalItems = 0> <!--- arrayLen(items) --->
       		
            <cfelse>
            	<cfset totalItems = -1>
            </cfif>
            
            <cfset data = {"group_id":#group_id#, "created":#created#, "modified":#modified#, "name":"#name#", "detail_id":#detail_id#, "items":#totalItems#, "asset_id":#asset_id#, "subgroup_id":#subgroup_id#, "sharable":#sharable#, "order":#sortOrder#, "access":#access#}>
			<!--- Active --->
            <cfif active IS ''>
                <cfset structAppend(data,{"active":allGroups.active})>
            </cfif>
    
            <!--- Access Level --->
            <cfif accessLevel GT '0'>
                <cfset structAppend(data,{"access":accessLevel})>																		
            </cfif>
            
            <!--- Cashed --->
            <cfif cached IS ''>
                <cfset structAppend(data,{"cached":0})>			
            <cfelse>
            	<cfset structAppend(data,{"cached":cached})>																
            </cfif>
                      
            <cfset arrayAppend(groups,data)>
            
        </cfloop>
        
		<cfreturn groups>
        
	</cffunction>
   
    
	
	
	<!--- Generate JSON for Group --->
 	<cffunction name="updateGroupJSON" access="public" returntype="struct">
    	<cfargument name="subgroupID" type="numeric" required="yes" default="0">
        
        
        <cfinvoke component="LiveAPI" method="getGroups" returnvariable="JSON">
            <cfinvokeargument name="appID" value="#session.appID#"/>
            <cfinvokeargument name="subgroupID" value="#session.subgroupid#"/>
            <cfinvokeargument name="assetID" value="#z#"/>
        </cfinvoke>
        
        <!--- <cffile action="write" file="#fileToSave#" output="#dataJSON#" charset="utf-8"> --->
        
    </cffunction>
    
    
    
    
    <!--- Get GroupID From AssetID --->
 	<cffunction name="getGroupIDFromAssetID" access="public" returntype="numeric">
    	<cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfquery name="groups"> 
    		SELECT	Groups.group_id
    		FROM	Assets LEFT OUTER JOIN Groups ON Assets.asset_id = Groups.asset_id
            WHERE   Assets.asset_id = #assetID#
    	</cfquery>
        
        <cfif groups.recordCount GT '0'>
        	<cfset groupID = groups.group_id>
        <cfelse>
        	<cfset groupID = 0>
        </cfif>
        
        <cfreturn groupID>
        
    </cffunction>
  
  


  <!--- Reverse Lookup for ContentID from AssetID AND GroupID --->
  <cffunction name="groupExists" access="public" returntype="boolean">
    	
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
    	<cfquery name="groups"> 
    		SELECT	group_id
    		FROM	Groups
            WHERE   group_id = #groupID#
    	</cfquery>
        
        <cfif groups.recordCount GT '0'>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>

  
  
  
  <!--- Reverse Looup for ContentID from AssetID AND GroupID --->
  <cffunction name="getContentInfo" access="public" returntype="numeric">
    	
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
    	<cfquery name="contentAsset"> 
    		SELECT	group_id AS contentID
    		FROM	Groups
            WHERE   0 = 0
            		<cfif groupID GT '0'>
                    AND subgroup_id = #groupID#
                    </cfif>
                    <cfif assetID GT '0'>
                    AND asset_id = #assetID#
                    </cfif>
    	</cfquery>
        
        <cfif contentAsset.recordCount GT '0'>
        	<cfreturn contentAsset.contentID>
        <cfelse>
        	<cfreturn 0>
        </cfif>
        
    </cffunction>
    
    
    
    <!---Get Assets in Group--->
<!---http://localhost:8501/wavecoders_new/API/v8/CFC/liveAPI.cfc?method=getGroups&auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E619--->

    <cffunction name="getGroupAssets" access="remote" returntype="struct" output="yes">
        
	    <cfargument name="groupID" type="numeric" required="yes" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="yes" default="0">
        
        <cfset data = structNew()>
      
        <!--- <!--- Group Exists --->
        <cfinvoke  component="Modules" method="groupExists" returnvariable="groupExists">
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke> --->

        <!--- Get Group Assets --->
        <cfif assetID GT 0 AND groupID IS 0>
        <cfquery name="assets">
                SELECT       Assets.assetType_id, Assets.name, AssetTypes.path, AssetTypes.dbTable, Assets.modified, Assets.app_id,
                             Thumbnails.image AS thumbnail, Thumbnails.width, Thumbnails.height, Details.title, Details.subtitle, Details.description, 
                             Details.other, Details.titleColor, Details.subtitleColor, 
                             Details.descriptionColor, Colors.foreColor, Colors.backColor, Colors.background, Assets.asset_id
                FROM         Colors FULL OUTER JOIN
                             Assets INNER JOIN
                             AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id AND 
                             Assets.assetType_id = AssetTypes.assetType_id ON Colors.color_id = Assets.color_id LEFT OUTER JOIN
                             Details ON Assets.detail_id = Details.detail_id LEFT OUTER JOIN
                             Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id
                WHERE        (Assets.asset_id = #assetID#) 
        	</cfquery>
        <cfelse>
        
            <cfquery name="assets">
                SELECT        Groups.asset_id, Groups.accessLevel, Groups.sharable, Assets.assetType_id, Assets.name, Assets.app_id,
                             AssetTypes.path, AssetTypes.dbTable, Assets.modified, Thumbnails.image AS thumbnail, Thumbnails.width, Thumbnails.height, 
                             Details.title, Details.subtitle, Details.description, Details.other, 
                             Details.titleColor, Details.subtitleColor, Details.descriptionColor, Groups.sortOrder, Groups.cached, Colors.foreColor, 
                             Colors.backColor, Colors.background
                FROM         Assets INNER JOIN
                             Groups ON Assets.asset_id = Groups.asset_id INNER JOIN
                             AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id AND 
                             Assets.assetType_id = AssetTypes.assetType_id LEFT OUTER JOIN
                             Colors ON Groups.color_id = Colors.color_id AND Assets.color_id = Colors.color_id LEFT OUTER JOIN
                             Details ON Assets.detail_id = Details.detail_id LEFT OUTER JOIN
                             Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id                     
                WHERE        (Groups.subgroup_id = #groupID#) 
                			<cfif appID GT 0>
                			AND (Groups.app_id = #appID#) 
                            </cfif>
                            AND (Groups.asset_id IS NOT NULL) AND Groups.active = 1 
                ORDER BY	 Groups.sortOrder
            </cfquery>
        
        </cfif>

        <!--- Parse Data --->
        <!--- QueryToStruct --->
         <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theData">
            <cfinvokeargument name="query" value="#assets#"/>
            <cfinvokeargument name="forceArray" value="true"/>
         </cfinvoke>

		 <cfset allAssets = structNew()>
       
         <cfloop index="z" from="1" to="#arrayLen(theData)#">
            
            <cfset aRec = theData[z]>
            
            <!--- appId check --->
            <cfif appID IS 0>
            	<cfset appID = aRec.app_id>
            </cfif> 
            
            <!--- Setup Rec --->
			<cfset dataRec = structNew()>
 
        	<!--- Get Detail --->
            <cfinvoke  component="Modules" method="getDetailsData" returnvariable="details">
                <cfinvokeargument name="data" value="#aRec#"/>
            </cfinvoke>
        
            <!--- Get Thumb --->
            <cfinvoke  component="Modules" method="getThumbData" returnvariable="thumb">
                <cfinvokeargument name="data" value="#aRec#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

     		<!--- Get Colors --->
            <cfinvoke  component="Modules" method="getColorData" returnvariable="colors">
                <cfinvokeargument name="data" value="#aRec#"/>
            </cfinvoke>
           
            <!--- Add Asset --->
            <cfset DBTable = aRec.DBTable>
            
            <!--- Get Asset Info --->
            <cfinvoke  component="Assets" method="getAsset" returnvariable="theAssetData">
              <cfinvokeargument name="assetID" value="#aRec.asset_id#"/>
            </cfinvoke>
            
            <!--- Convert to Struct --->
            <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="theAssetInfo">
              <cfinvokeargument name="query" value="#theAssetData#"/>
            </cfinvoke> 
        		
             <!--- Content Group Assets --->
             <!--- <cfif DBTable IS "GroupAssets">

				<cfset groupContent = arrayNew(1)>
                
        		<cfloop index="z" from="1" to="#arrayLen(theAssetInfo)#">
                	
                    <cfset groupAsset = theAssetInfo[z]>
                    
                    <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="data">
                        <cfinvokeargument name="assetID" value="#groupAsset.content_id#"/>
                    </cfinvoke>
                    
                    <cfset arrayAppend(groupContent,data.assets[1])>
                    
                </cfloop>

                <cfset groupData = {"asset_id":aRec.asset_id, "assets":groupContent, "sharable":assets.sharable, "access":assets.accessLevel, "assetType":assets.assetType_id,"modified":assets.modified, "order":assets.sortOrder}>
            	<cfset structAppend(allAssets,{"#assets.name#":groupData})>
                
            </cfif> --->
  
            <!--- Get Data --->
            <cfif NOT structIsEmpty(details)>
            	<cfset structAppend(dataRec,{"details":details})>
            </cfif>
            <cfif NOT structIsEmpty(thumb)>
            	<cfset structAppend(dataRec,{"thumb":thumb})>
            </cfif>
             
            <cfif NOT structIsEmpty(colors)>
            	<cfset structAppend(dataRec,colors)>
            </cfif>

          	<cfif isStruct(theAssetInfo)>
            	
				<cfif NOT structIsEmpty(theAssetInfo)>
        
					<!--- Build Asset Path --->
                    <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                        <cfinvokeargument name="assetID" value="#theAssetInfo.asset_id#"/>
                        <cfinvokeargument name="server" value="yes"/>
                    </cfinvoke>
             
                    <!--- Non Retina  Asset Path--->
                    <cfset assetPathNonRetina = assetPath & "nonretina/">
                    
                    <cfset assetData = structNew()>
    
                    <cfloop collection="#theAssetInfo#" item="z">
          
                      <cfif theAssetInfo[z] NEQ ''>
                    
                        <cfset theKey = z>
                        <!--- Check for Web or URL and Create Path --->
                        <cfif z IS 'webURL' OR z IS 'url'>
                       
                            <cfif z IS 'url' AND theAssetInfo[z] NEQ '' AND aRec.assetType_id NEQ 4>
                                
                                <cfset theAssetDataNonRetina = "#assetPathNonRetina##theAssetInfo[z]#">
                                <cfset theAssetDataRetina = "#assetPath##theAssetInfo[z]#">
                                
                                <cfset theAssetData = structNew()>
                               
                                <cfif fileExists(theAssetDataNonRetina)>
                                	<cfset structAppend(theAssetData,{"mdpi":{"url": theAssetDataNonRetina}})>
                                </cfif>
                                
                                <cfset structAppend(theAssetData,{"xdpi":{"url": theAssetDataRetina}})>
                                
                            <cfelse>
                                <cfset theAssetData = theAssetInfo[z]>
                            </cfif>
        
                            <cfset theKey = "url">
                            
                        <cfelse>
                            <!--- If Balcony or Pano Images add Path --->
                            <cfswitch expression="#z#">
                                <!--- Pano --->
                                <cfcase value="down,right,up,front,left,back,pano" delimiters=",">
                                
                                    <cfset theAssetDataNonRetina = "#assetPathNonRetina##theAssetInfo[z]#">
                                    <cfset theAssetDataRetina = "#assetPath##theAssetInfo[z]#">
                                    
                                    <cfset theAssetData = structNew()>
                                    
                                    <cfif fileExists(theAssetDataNonRetina)>
										<cfset structAppend(theAssetData,{"mdpi":{"url":theAssetDataNonRetina}})>
                                    </cfif>
                                    
                                    <cfset structAppend(theAssetData,{"xdpi":{"url":theAssetDataRetina}})>
                                    
                                    <!--- <cfset theAssetData = {"mdpi": theAssetDataNonRetina, "xdpi":theAssetDataRetina}> --->
                                    
                                </cfcase>
                                
                                <!--- Balcony --->
                                <cfcase value="south,north,west,east" delimiters=",">
                               
                                    <cfset theAssetDataNonRetina = "#assetPathNonRetina##theAssetInfo[z]#">
                                    <cfset theAssetDataRetina = "#assetPath##theAssetInfo[z]#">
                                    
                                    <cfset theAssetData = structNew()>
                                    
                                    <cfif fileExists(theAssetDataNonRetina)>
										<cfset structAppend(theAssetData,{"mdpi":{"url": theAssetDataNonRetina}})>
                                    </cfif>
                                    
                                    <cfset structAppend(theAssetData,{"xdpi":{"url": theAssetDataRetina}})>
                                    
                                    <!--- <cfset theAssetData = {"mdpi":{"url": theAssetDataNonRetina},"xdpi":{"url":theAssetDataRetina}}> --->
                                    
                                </cfcase>
                                
                                <cfdefaultcase>
                                    <cfset theAssetData = theAssetInfo[z]>
                                </cfdefaultcase>
                            
                            </cfswitch>
         
                        </cfif>


                        <!--- 3DModel Asset --->
                        <cfif DBTable IS 'Model3DAssets'>
								
                            <!--- Get 3Model Struct --->
                            <cfinvoke component="Modules" method="get3DData" returnvariable="Data3DInfo">
                                <cfinvokeargument name="data" value="#theAssetInfo#"/>
                            </cfinvoke>
                            
                            <cfset structAppend(dataRec,Data3DInfo)>

                        <cfelse>
                            <cfset structAppend(dataRec,{"#theKey#":theAssetData})>
                        </cfif>
                        
					
                        <!--- Group Assets --->
                        <cfif DBTable IS 'GroupAssets'>
                            
							<cfquery name="ContentAssets">
                                SELECT       *
                                FROM         ContentAssets                    
                                WHERE        asset_id = #theAssetInfo.asset_id#
                            </cfquery>
                            
                            <cfset typeDisplay = structNew()>
                            
                            <cfswitch expression="#ContentAssets.displayType#">
                            
                                <!--- 3d --->
                                <cfcase value="1">
                                    <cfset typeDisplay = {"type":"3d", "model":ContentAssets.instanceName, "asset_id":ContentAssets.instanceAsset_id}>
                                </cfcase>
                                <!--- modal --->
                                <cfcase value="2">
                                    <cfset typeDisplay = {"type":"modal"}>
                                </cfcase>
                                <!--- Panel --->
                                <cfcase value="3">
                                    <cfset typeDisplay = {"type":"panel"}>
                                </cfcase>
                                <!--- default none --->
                                <cfdefaultcase>
                                    <cfset typeDisplay = "none">
                                </cfdefaultcase>
                            
                            </cfswitch>
                            
                            <cfset structAppend(dataRec,{"display": typeDisplay})>
                                
                        <cfelse>
                            <cfset structAppend(dataRec,{"#theKey#":theAssetData})>
                        </cfif>
                        
                        
                          
                      </cfif>
                      
     
                    </cfloop>
                    	
                    <!--- Game Asset --->
                    <cfif DBTable IS "GameAssets">
                    
						<cfif StructKeyExists(dataRec,"email")><cfset structDelete(dataRec,'email')></cfif>
                        
						<cfif StructKeyExists(dataRec,"sendClientEmail")><cfset structDelete(dataRec,'sendClientEmail')></cfif>
                        <cfif StructKeyExists(dataRec,"emailMessage")><cfset structDelete(dataRec,'emailMessage')></cfif>
                        
                        <cfif StructKeyExists(dataRec,"sendPlayerEmail")><cfset structDelete(dataRec,'sendPlayerEmail')></cfif>
                        <cfif StructKeyExists(dataRec,"sendTargets")><cfset structDelete(dataRec,'sendTargets')></cfif>
                        
                        <cfif StructKeyExists(dataRec,"useCode")><cfset structDelete(dataRec,'useCode')></cfif>

                        <cfif StructKeyExists(dataRec,"dateStart")><cfset structDelete(dataRec,'dateStart')></cfif>
                        <cfif StructKeyExists(dataRec,"dateEnd")><cfset structDelete(dataRec,'dateEnd')></cfif>
                        <cfif StructKeyExists(dataRec,"sharable")><cfset structDelete(dataRec,'sharable')></cfif>
                    
                    </cfif>
                    
                    
                    
               		<!--- Video Asset --->
                    <cfif DBTable IS "VideoAssets">
                        
                        <!--- Get Object Model --->
                        <cfinvoke  component="ObjectModels" method="getObjectModel" returnvariable="objectModel">
                            <cfinvokeargument name="typeID" value="#aRec.assetType_id#"/>
                            <cfinvokeargument name="objectData" value="#theAssetInfo#"/>
                        </cfinvoke>
                        
                        <!--- Clean --->
                        <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="DataVideoInfo">
                            <cfinvokeargument name="theStruct" value="#objectModel#"/>
                        </cfinvoke> 
                        
                        
						<cfset dataRec = DataVideoInfo.video>
                        <!--- <cfset structAppend(dataRec,DataVideoInfo.video)> --->
                		
                    </cfif>
                    
                    
                    
                    
                    <!--- Quiz Asset --->
					<cfif DBTable IS 'QuizAssets'>
                            
                        <!--- Get Quiz Struct --->
                        <cfinvoke component="Modules" method="getQuizData" returnvariable="DataQuizInfo">
                            <cfinvokeargument name="data" value="#theAssetInfo#"/>
                        </cfinvoke>
                        
                        <cfset dataRec = DataQuizInfo>

                    </cfif>
                    
                    
                    
                    
                    
                    <!--- Mapper Asset Type --->
					<cfif DBTable IS "MarkerAssets">
                    
                    	<!--- Get Marker --->                  
                        <cfinvoke  component="Assets" method="getAsset" returnvariable="theMarker">
                          <cfinvokeargument name="assetID" value="#theAssetInfo.asset_id#"/>
                        </cfinvoke>
                        
                        <!--- Get MARKER ASSET --->
                        <cfinvoke  component="Modules" method="getMarkerContent" returnvariable="theMarkerAsset">
                            <cfinvokeargument name="appID" value="#appID#"/>
                            <cfinvokeargument name="markerData" value="#theMarker#"/>
                        </cfinvoke>

                        <cfset dataRec = theMarkerAsset>
                        
                    </cfif>
                    

					<!--- AccessLevel --->
                    <cfif isDefined('aRec.sharable')>
                        <cfset structAppend(dataRec,{"sharable":aRec.sharable})>
                    <cfelse>
                        <cfset structAppend(dataRec,{"sharable":0})>
                    </cfif>
                    
                    <!--- Get Access Level from ID --->
                    <cfif isDefined('aRec.accessLevel')>
                    
                        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
                          <cfinvokeargument name="accessID" value="#aRec.accessLevel#"/>
                        </cfinvoke>
                        
                        <cfset accessLevel = accessID.accessLevel>
        
                        <cfif accessLevel GT '0'>
                            <cfset structAppend(dataRec,{"access":accessLevel})>
                        </cfif>
                    
                    <cfelse>
                        <cfset structAppend(dataRec,{"access":0})>
                    </cfif>
                    
                    <cfset structAppend(dataRec,{"assetType":aRec.assetType_id})>
                    <cfset structAppend(dataRec,{"modified":aRec.modified})>
                    
                    <cfif isDefined('aRec.sortOrder')>
                        <cfset structAppend(dataRec,{"order":aRec.sortOrder})>
                    <cfelse>
                        <cfset structAppend(dataRec,{"order":0})>
                    </cfif>
                
                	<!--- Get Actions and Assets --->
                    <cfinvoke  component="Modules" method="getGroupAssetsActions" returnvariable="allAssetsActions">
                      <cfinvokeargument name="assetID" value="#theAssetInfo.asset_id#"/>
                     </cfinvoke>  
                     
                     <!--- Assets --->
                     <cfset assets = arrayNew(1)>
                     <cfloop query="allAssetsActions.assets">
	
                        <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="AssetData">
                            <cfinvokeargument name="assetID" value="#content_id#"/>
                        </cfinvoke>
                   		
                        <cfif quiz OR quizCorrect OR quizIncorrect>
                        	<!--- nothing --->
                        <cfelse>
                        	<cfset arrayAppend(assets,AssetData.assets[1])>
						</cfif>
                        
                     </cfloop>
                    
                     <cfif arrayLen(assets) GT 0>
                     	<cfset structAppend(dataRec,{"assets":assets})>
                     </cfif>
                     
                     <!--- Actions --->
                     <cfset actions = arrayNew(1)>
                     <cfloop query="allAssetsActions.actions">
                     
                         <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="ActionData">
                            <cfinvokeargument name="assetID" value="#content_id#"/>
                        </cfinvoke>
                        
                        <cfset arrayAppend(actions,ActionData.assets[1])>

                     </cfloop>
                     
                     <cfif arrayLen(actions) GT 0>
                     	<cfset structAppend(dataRec,{"actions":actions})>
					 </cfif>
                    
                    <!--- Create Asset --->
                    <cfset structAppend(allAssets,{"#aRec.name#":dataRec})>
                	
                </cfif>
              
          	<cfelse>
          	  		<!--- no asset --->
         	</cfif>
 
              </cfloop> 
	   			
              <cfset structAppend(data,{"assets":#allAssets#})>
           
               <cfset newData = arrayNew(1)>
                
                <cfloop array="#structSort(data.assets, 'numeric','asc','order')#" index="z">
                
                    <cfset StructDelete(data.assets[z], "order")>
    				
                    <!--- 3D Object --->
    				<!--- delete  modifiedModel date from root of 3D Asset --->
                    <cfset unwated3dObjects = ['modifiedModel','camZoom','camPos_z','camMin','fov','scale','loc_x','loc_y','loc_z','camTarget_x','camTarget_y','camTarget_z','camPan','camPos_x','camPos_y','camPos_z','rot_x','rot_y','rot_z','camMax','tiltMin','tiltMax','panMin','panMax','camRotation']>
    				<cfloop index="i" array="#unwated3dObjects#">
					  <cfif structKeyExists(data.assets[z],'#i#')>
    					<cfset structDelete(data.assets[z],'#i#')>
                      </cfif>
                    </cfloop>
	
                    <cfset arrayAppend(newData,{"#z#":data.assets[z]})>

                </cfloop>
                         
              <!--- ok Error --->
              <cfinvoke  component="Errors" method="getError" returnvariable="error">
                  <cfinvokeargument name="error_code" value="1000"/>
              </cfinvoke>             
          
          <!--- <cfset structAppend(newData,{"error":#error#})> ---> 
        
          <cfset myData = {"assets":newData}>

          <cfreturn myData>
   
    </cffunction>
    



<!--- Get MAP Data --->   
<cffunction name="getGroupAssetsActions" access="remote" returntype="struct" output="no">
	<cfargument name="assetID" type="numeric" required="yes"> 
    
    <cfquery name="assets">
        SELECT       *
        FROM         GroupAssets                    
        WHERE        asset_id = #assetID# AND action = 0 AND (quiz = 0 OR quizCorrect = 0 OR quizIncorrect = 0)
        ORDER BY	 sortOrder
    </cfquery>
    
    <cfquery name="actions">
        SELECT       *
        FROM         GroupAssets                    
        WHERE        asset_id = #assetID# AND action = 1 AND (quiz = 0 OR quizCorrect = 0 OR quizIncorrect = 0)
        ORDER BY	 sortOrder
    </cfquery>
	
    <cfset assetData = {"assets":assets, "actions":actions}>
    
    <cfreturn assetData>
    
</cffunction>  
    
    
    
    
<!--- Get MAP Data --->   
<cffunction name="getMarkerContent" access="remote" returntype="struct" output="no">
	<cfargument name="appID" type="numeric" required="yes"> 
    <cfargument name="markerData" type="query" required="yes">  
    
    <cfset theMarkerObject = structNew()>

    <!--- Build Asset Path --->
    <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
        <cfinvokeargument name="assetID" value="#markerData.asset_id#"/>
        <cfinvokeargument name="server" value="yes"/>
    </cfinvoke>

    
    <cfif markerData.useMoodstocks>
    	<cfset structAppend(theMarkerObject,{"moodstocks":{"key":markerData.APIKey, "secret":markerData.APISecret}})>
    </cfif>
    
    <cfif markerData.useVuforia>
    	<cfset structAppend(theMarkerObject,{"vuforia":{"xml":assetPath & markerData.xml, "dat":assetPath & markerData.dat, "license":""}})>
    </cfif>
    
    <cfif markerData.useVisualizer>
    	
        <cfif markerData.url NEQ ''>
        	<cfset structAppend(theMarkerObject,{"visualizer":{"url":assetPath & markerData.url}})>
        <cfelse>
        	<cfset structAppend(theMarkerObject,{"visualizer":{"url":assetPath & markerData.weburl}})>
        </cfif>
        
    </cfif>

    <cfset structAppend(theMarkerObject,{"asset_id":markerData.asset_id})>
																							
    <cfreturn theMarkerObject>
    
</cffunction>
    
    
     

<!--- Get MAP Data --->   
<cffunction name="getMapperContent" access="remote" returntype="struct" output="no">
	<cfargument name="appID" type="numeric" required="yes"> 
    <cfargument name="mapAsset_id" type="numeric" required="yes">   
	
    <!--- Get Map Asset --->
	<cfset mapObject = structNew()>
    
    <cfset mapAsset = mapAsset_id>
    
    <!--- Get Asset --->
    <cfinvoke  component="Assets" method="getAssets" returnvariable="mapInfo">
        <cfinvokeargument name="assetID" value="#mapAsset#"/>
    </cfinvoke>
    
    <!--- Convert to Struct --->
    <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="theInfo">
      <cfinvokeargument name="query" value="#mapInfo#"/>
    </cfinvoke>
 
    <!--- Get Detail --->
    <cfinvoke  component="Modules" method="getDetailsData" returnvariable="assetDetails">
        <cfinvokeargument name="data" value="#theInfo#"/>
    </cfinvoke>
  
    <!--- Get Thumb --->
    <cfinvoke  component="Modules" method="getThumbData" returnvariable="assetThumb">
        <cfinvokeargument name="data" value="#theInfo#"/>
        <cfinvokeargument name="appID" value="#appID#"/>
        <cfinvokeargument name="server" value="yes"/>
    </cfinvoke>
    
    <!--- Get Object Data --->
    <cfinvoke  component="Assets" method="getAsset" returnvariable="theObject">
        <cfinvokeargument name="assetID" value="#mapAsset#"/>
    </cfinvoke>
   
    <!--- Build Asset Path --->
    <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
        <cfinvokeargument name="appID" value="#appID#"/>
        <cfinvokeargument name="assetTypeID" value="#mapInfo.assetType_id#"/>
        <cfinvokeargument name="server" value="yes"/>
    </cfinvoke>
    

    <cfset retinaPath = assetPath & theObject.url>
    <cfset nonRetinaPath = assetPath &"nonretina/"& theObject.url>

    <cfset mapObject = {"modified":mapInfo.modified, "assetType":mapInfo.assetType_id, "asset_id":mapInfo.asset_id}>
    
     <cfif NOT structIsEmpty(assetDetails)>
        <cfset mapObject = {"details":assetDetails}>
     </cfif>
     
     <cfif NOT structIsEmpty(assetThumb)>
        <cfset mapObject = {"thumb":assetThumb}>
     </cfif>
    
    <cfif NOT isDefined("theObject.width") OR NOT isDefined("theObject.height")>
        <!--- no width and height for 3D object --->
       
        <!--- Get 3D Model from Query return Struct --->
        <cfinvoke component="Modules" method="get3DData" returnvariable="Data3DInfo">
            <cfinvokeargument name="data" value="#theObject#"/>
        </cfinvoke>
        
        <cfset structAppend(mapObject,Data3DInfo)>
    
    <cfelse>
        <!--- Add Width and Hieght if an Image Map --->
        <cfset urlLinks = {"xdpi":{"url":retinaPath},"mdpi":{"url":nonRetinaPath}}>
        <cfset structAppend(mapObject,{"url":urlLinks})>
        <cfset structAppend(mapObject, {"width":theObject.width, "height":theObject.height})>
    </cfif>
    
    <cfreturn mapObject>
    
    
</cffunction>    




<!--- Get PIN Data --->   
<cffunction name="getPINContent" access="remote" returntype="array" output="no">
	<cfargument name="appID" type="numeric" required="yes"> 
    <cfargument name="mapAssetIDs" type="array" required="yes">   
	
    <cfset mapperRec = arrayNew(1)>
    
    <cfloop index="z" from="1" to="#arrayLen(mapAssetIDs)#">
                             
        <cfset mapperAsset = structNew()>
       
        <!--- Get AssetID --->
        <cfset pointAssetID = mapAssetIDs[z]>
        
        <!--- Get Asset Data --->
        <cfinvoke  component="Assets" method="getAssets" returnvariable="theAssetData">
          <cfinvokeargument name="assetID" value="#pointAssetID#"/>
        </cfinvoke>
    
        <!--- Convert to Struct --->
        <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="theInfo">
          <cfinvokeargument name="query" value="#theAssetData#"/>
        </cfinvoke>

        <!--- Get Detail --->
        <cfinvoke  component="Modules" method="getDetailsData" returnvariable="assetDetails">
            <cfinvokeargument name="data" value="#theInfo#"/>
        </cfinvoke>
    
        <!--- Get Thumb --->
        <cfinvoke  component="Modules" method="getThumbData" returnvariable="assetThumb">
            <cfinvokeargument name="data" value="#theInfo#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
    
        <!--- Setup Rec --->
        <cfset dataRec = structNew()>
        
        <!--- Get Data --->
        <cfif NOT structIsEmpty(assetDetails)>
            <cfset structAppend(mapperAsset,{"details":assetDetails})>
        </cfif>
        <cfif NOT structIsEmpty(assetThumb)>
            <cfset structAppend(mapperAsset,{"thumb":assetThumb})>
        </cfif>
        
        <!--- Asset General Data --->
        <cfset structAppend(mapperAsset,{"assetType":theInfo.assetType_id})>
        <cfset structAppend(mapperAsset,{"modified":theInfo.modified})>
        <cfset structAppend(mapperAsset,{"asset_id":theInfo.asset_id})>
        
        <!--- Asset Data --->
        <cfinvoke  component="Assets" method="getAsset" returnvariable="theAssetData">
          <cfinvokeargument name="assetID" value="#pointAssetID#"/>
        </cfinvoke>
        
        <!--- Convert to Struct --->
        <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="theAssetDetails">
          <cfinvokeargument name="query" value="#theAssetData#"/>
        </cfinvoke>
        
        <cfset coords = structNew()>
        
        <cfif theInfo.assetType_id IS 10>
            <!--- XYZ Point --->
            <cfif theAssetDetails.x_pos NEQ ''>
                <cfset structAppend(coords,{"x":theAssetDetails.x_pos})> 
            </cfif>
            <cfif theAssetDetails.y_pos NEQ ''>
                <cfset structAppend(coords,{"y":theAssetDetails.y_pos})> 
            </cfif>
            <cfif theAssetDetails.z_pos NEQ ''>
                <cfset structAppend(coords,{"z":theAssetDetails.z_pos})> 
            </cfif>
            <cfif theAssetDetails.objectRef NEQ ''>
                <cfset structAppend(coords,{"objref":theAssetDetails.objectRef})> 
            </cfif>
            <cfif NOT StructIsEmpty(assetDetails)>
                <cfset structAppend(mapperAsset,{"details":assetDetails})> 
            </cfif>
            <cfif NOT StructIsEmpty(assetThumb)>
                <cfset structAppend(mapperAsset,{"thumb":assetThumb})> 
            </cfif>
    
            <cfset structAppend(mapperAsset,{"location":coords})>
            
        <cfelseif theInfo.assetType_id IS 5>
            <!--- GPS Point --->
            <cfif theAssetDetails.gps_alt NEQ ''>
                <cfset structAppend(coords,{"altitude":theAssetDetails.gps_alt})> 
            </cfif>
            <cfif theAssetDetails.gps_latt NEQ ''>
                <cfset structAppend(coords,{"lattitude":theAssetDetails.gps_latt})> 
            </cfif>
            <cfif theAssetDetails.gps_long NEQ ''>
                <cfset structAppend(coords,{"longitude":theAssetDetails.gps_long})> 
            </cfif>
            <cfif theAssetDetails.radius NEQ ''>
                <cfset structAppend(coords,{"radius":theAssetDetails.radius})> 
            </cfif>
            <cfif theAssetDetails.x_pos NEQ ''>
                <cfset structAppend(coords,{"x":theAssetDetails.x_pos})> 
            </cfif>
            <cfif theAssetDetails.y_pos NEQ ''>
                <cfset structAppend(coords,{"y":theAssetDetails.y_pos})> 
            </cfif>
            <cfif NOT StructIsEmpty(assetDetails)>
                <cfset structAppend(mapperAsset,{"details":assetDetails})> 
            </cfif>
            <cfif NOT StructIsEmpty(assetThumb)>
                <cfset structAppend(mapperAsset,{"thumb":assetThumb})> 
            </cfif>
            
            <cfset structAppend(mapperAsset,{"location":coords})>
    
        </cfif>
        
        <cfset arrayAppend(mapperRec,mapperAsset)>
         
    </cfloop>
    
    <cfreturn mapperRec>

</cffunction>



<!--- Get Cached Objects --->

<cffunction name="getContentAsset" access="remote" returntype="struct" output="no">

    <cfargument name="assetID" type="numeric" required="yes" default="0">
    <cfargument name="allContent" type="boolean" required="no" default="no">
    
    <cfquery name="assetData">
        SELECT	Thumbnails.image AS thumbnail, Thumbnails.width, Thumbnails.height, Colors.foreColor, Colors.backColor, Colors.background, Details.title, Details.subtitle, 
                Details.description, Details.other, Details.titleColor, 
				Details.subtitleColor, Details.descriptionColor, Assets.assetType_id, Assets.created, Assets.modified, 
				Assets.name, Assets.app_id
        FROM	Assets LEFT OUTER JOIN
                Details ON Assets.detail_id = Details.detail_id LEFT OUTER JOIN
                Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id LEFT OUTER JOIN
                Colors ON Assets.color_id = Colors.color_id
        WHERE	Assets.asset_id = #assetID#
    </cfquery>
 
    <cfset appID = assetData.app_id>
    
    <!--- Convert to Struct --->
    <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="theAssetData">
      <cfinvokeargument name="query" value="#assetData#"/>
    </cfinvoke> 
    
    <!--- Get Details --->
    <cfinvoke  component="Modules" method="getDetailsData" returnvariable="details">
        <cfinvokeargument name="data" value="#theAssetData#"/>
    </cfinvoke>

    <!--- Get Thumb --->
    <cfinvoke  component="Modules" method="getThumbData" returnvariable="thumb">
        <cfinvokeargument name="data" value="#theAssetData#"/>
        <cfinvokeargument name="appID" value="#appID#"/>
        <cfinvokeargument name="server" value="yes"/>
    </cfinvoke>
	
    <!--- Get Colors --->
    <cfinvoke  component="Modules" method="getColorData" returnvariable="colors">
        <cfinvokeargument name="data" value="#theAssetData#"/>
    </cfinvoke>

    <!--- Get Asset --->
    <cfinvoke  component="Assets" method="getAsset" returnvariable="assetObjectData">
        <cfinvokeargument name="assetID" value="#assetID#"/>
    </cfinvoke>
   
    <!--- Convert to Struct --->
    <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="objectData">
      <cfinvokeargument name="query" value="#assetObjectData#"/>
    </cfinvoke> 
    
    <!--- Get Object Model --->
    <cfinvoke  component="ObjectModels" method="getObjectModel" returnvariable="objectModel">
        <cfinvokeargument name="typeID" value="#assetData.assetType_id#"/>
        <cfinvokeargument name="objectData" value="#objectData#"/>
    </cfinvoke>

    <!--- Get Root Key --->
    <cfloop collection="#objectModel#" item="theKey"></cfloop>

    <cfif NOT StructIsEmpty(details)>
    	<cfset objectModel[theKey].details = details>
    <cfelse>
    	<cfset structDelete(objectModel[theKey], 'details')>
    </cfif>
    
    <cfif NOT StructIsEmpty(thumb)>
    	<cfset objectModel[theKey].thumb = thumb>
    <cfelse>
    	<cfset structDelete(objectModel[theKey], 'thumb')>
    </cfif>

    <cfif NOT StructIsEmpty(colors)>
    	<cfloop collection="#colors#" item="theKeyColor"></cfloop>
    	<cfset objectModel[theKey].colors = colors[theKeyColor]>
    <cfelse>
    	<cfset structDelete(objectModel[theKey], 'colors')>
    </cfif>

    <!--- Clean --->
    <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="objectModel">
        <cfinvokeargument name="theStruct" value="#objectModel#"/>
    </cfinvoke>
   
    <!--- Asset Access Info --->
    <cfinvoke  component="Modules" method="getAssetAccessInfo" returnvariable="accessInfo">
        <cfinvokeargument name="assetID" value="#assetID#"/>
    </cfinvoke>
    
    <cfset objectModel[theKey].assetType = theAssetData.assetType_id>
    <cfset objectModel[theKey].asset_id = assetID>

    <!--- Access Info --->
    <cfset objectModel[theKey]['access'] = accessInfo.access>
	<cfset objectModel[theKey]['modified'] = accessInfo.modified>
    <cfset objectModel[theKey]['sharable'] = accessInfo.sharable>
    <cfset objectModel[theKey]['cached'] = accessInfo.cached>
    
    <!--- Assets --->
    <cfif structKeyExists(objectModel[theKey],'assets')>
		<cfif arrayIsEmpty(objectModel[theKey].assets)>
            <cfset structDelete(objectModel[theKey], 'assets')>
        </cfif>
    </cfif>
    
    <!--- Actions --->
    <cfif structKeyExists(objectModel[theKey],'actions')>
    	<cfset structDelete(objectModel[theKey], 'actions')>
    </cfif>
    
    <cfset objectAsset = {"#theAssetData.name#":objectModel[theKey]}>
	
    <cfreturn objectAsset>

</cffunction>


<!--- Clean Up Struct --->
<cffunction name="cleanUpStruct" access="remote" returntype="struct" output="yes">

    <cfargument name="theStruct" type="struct" required="yes" default="structNew()">
    
    <cfloop collection="#theStruct#" item="anItem">
    	
        <cfif isStruct(theStruct[anItem])>
            
				<!--- Clean --->
                <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="embeddedItem">
                    <cfinvokeargument name="theStruct" value="#theStruct[anItem]#"/>
                </cfinvoke>
            
            <cfset theStruct[anItem] = embeddedItem>
            
            <cfif structIsEmpty(theStruct[anItem])>
            	<cfset structDelete(theStruct,anItem)>
            </cfif>
            
        <cfelse>
        	
            <cfif NOT isStruct(theStruct[anItem]) AND NOT isArray(theStruct[anItem])>
            
				<cfif theStruct[anItem] IS ''>
                    <cfset structDelete(theStruct,anItem)>
                </cfif>
                
            <cfelse>
				<!--- nothing --->	
            </cfif>
            
    	</cfif>
        
        <!--- Thumb --->
        <cfif anItem IS 'thumb'>  
        
        	<cfif structKeyExists(theStruct,'thumb')>
            
				<cfif isDefined("theStruct.thumb.mdpi") AND isDefined("theStruct.thumb.xdpi")>
                
                    <cfif NOT isDefined("theStruct.thumb.xdpi.url")>
                        <cfif theStruct.thumb.xdpi.url IS ''>
                            <cfset structDelete(theStruct.thumb,'xdpi')>
                        </cfif>
                    </cfif>
        
                    <cfif NOT isDefined("theStruct.thumb.mdpi.url")>
                        <cfif theStruct.thumb.mdpi.url IS ''>
                            <cfset structDelete(theStruct.thumb,'mdpi')>
                        </cfif>
                    </cfif>
        
                    <cfif NOT isDefined("theStruct.thumb.mdpi") AND NOT isDefined("theStruct.thumb.xdpi")>
                        <cfset structDelete(theStruct,'thumb')>
                    </cfif>
    
                </cfif> 
                
                <cfif NOT structKeyExists(theStruct.thumb,'mdpi') AND NOT structKeyExists(theStruct.thumb,'Xdpi')>
                    <cfset structDelete(theStruct,'thumb')>
                </cfif>
            
            </cfif>
            
        </cfif>
     
        <!--- Assets-Actions --->
        <cfif anItem IS 'assets'>
        	<cfif arrayLen(theStruct.assets) IS 0>
                <cfset structDelete(theStruct,'assets')>
            </cfif>
        </cfif>
        <cfif anItem IS 'actions'>
        	<cfif arrayLen(theStruct.actions) IS 0>
                <cfset structDelete(theStruct,'actions')>
            </cfif>
        </cfif>
       
    </cfloop>

    <cfreturn theStruct>
    
</cffunction>



<!--- Get Group Access --->
<cffunction name="getAssetAccessInfo" access="remote" returntype="struct" output="yes">

    <cfargument name="assetID" type="numeric" required="yes" default="structNew()">
    
    <cfset theAccess = {'access':0, 'modified':0, 'sharable':0, 'cached':0}>
    
        <cfquery name="accessInfo"> 
            SELECT        DISTINCT Assets.asset_id, Groups.sharable, Groups.cached, Groups.accessLevel, Assets.modified
            FROM          Groups LEFT OUTER JOIN
                          Assets ON Groups.asset_id = Assets.asset_id
            WHERE        (Groups.asset_id = #assetID#)
        </cfquery>
    
    <cfif accessInfo.recordCount IS 0>
    
        <cfquery name="accessInfo"> 
            SELECT        DISTINCT GroupAssets.asset_id, GroupAssets.sharable, GroupAssets.cached, GroupAssets.accessLevel, Assets.modified
            FROM          GroupAssets LEFT OUTER JOIN
                          Assets ON GroupAssets.asset_id = Assets.asset_id
            WHERE        (GroupAssets.content_id = #assetID#)
        </cfquery>
        
    </cfif>
	
    <cfif accessInfo.recordCount GT 0>
    
		<cfset theAccess.access = accessInfo.accessLevel>
        <cfset theAccess.modified = accessInfo.modified>
        <cfset theAccess.sharable = accessInfo.sharable>
        <cfset theAccess.cached = accessInfo.cached>

    </cfif>
    
    <cfreturn theAccess>
    
</cffunction>    



<!--- Get Cashed Content from JSON --->
<cffunction name="getCachedContent" access="remote" returntype="array" output="yes">
	<cfargument name="appID" type="numeric" required="yes" default="0">

    <cfinvoke component="LiveAPI" method="getContentJSON" returnvariable="data">
        <cfinvokeargument name="appID" value="#appID#">
        <cfinvokeargument name="groupID" value="-1">
    </cfinvoke>
    
    <cfset data = deserializeJSON(data)>

    <cfinvoke component="Modules" method="getCachedObjects" returnvariable="cachedData">
      <cfinvokeargument name="data" value="#data#"/>
    </cfinvoke>
	
    <cfloop index="anObj" array="#cachedData#">
    
    	<cfloop collection="#anObj#" item="theKey"></cfloop>
    	<cfset structDelete(anObj[theKey],"assets")>
        
    </cfloop>
    
    <cfset objectsToCache = []>
    
    <cfloop index="anObj" array="#cachedData#">
    
    	<cfloop collection="#anObj#" item="theKey"></cfloop>
    	<cfif structKeyExists(anObj[theKey],"assetType")>
        	<cfset arrayAppend(objectsToCache,anObj)>
        </cfif>
        
    </cfloop>

    <cfreturn objectsToCache>
    
</cffunction>


<!--- Process Cashed Objects --->
<cffunction name="getCachedObjects" access="remote" returntype="array" output="yes">
	<cfargument name="data" type="struct" required="yes">
    <cfargument name="cached" type="array" required="no" default="#arrayNew(1)#">
    
    <cfloop collection="#data#" item="anObject">
    	
        <cfif anObject IS 'assets'>
        	
            <cfloop index="aObj" array="#data[anObject]#">
            	
            	<cfloop collection="#aObj#" item="theKey"></cfloop>
                
            	<cfif structKeyExists(aObj[theKey],'cached')>
                
					<cfif aObj[theKey].cached>
                    <cfif structKeyExists(aObj[theKey],'assets')>
                    	<cfif arrayLen(aObj[theKey].assets) GT 0>

                                <cfloop index="aSubObj" array="#aObj[theKey].assets#">
                                    <cfloop collection="#aSubObj#" item="theSubKey"></cfloop>
                                    <cfset structAppend(aSubObj[theSubKey],{'cached':1})>
                                </cfloop>
                           
                        </cfif>
                    </cfif>
                       
                        <cfif NOT ArrayFind(cached,aObj)>
                        	<cfset arrayAppend(cached,aObj)>
                        </cfif>
                        
                    </cfif>
                    
                </cfif>
                
                <cfif structKeyExists(aObj[theKey],'assets')>
					<cfif arrayLen(aObj[theKey].assets) GT 0>
    					
                        <cfinvoke component="Modules" method="getCachedObjects" returnvariable="cached">
                          <cfinvokeargument name="data" value="#aObj[theKey]#"/>
                          <cfinvokeargument name="cached" value="#cached#"/>
                        </cfinvoke>
                        
                    </cfif>
                </cfif>
                
            </cfloop>

        <cfelse>
    		
    	</cfif>
        
    </cfloop>
	
    <cfreturn cached>
    
</cffunction>


    
    
<!---Get Group--->
<!---http://localhost:8501/wavecoders_new/API/v8/CFC/liveAPI.cfc?method=getGroups&auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E619--->

    <cffunction name="getGroupContent" access="remote" returntype="struct" output="yes">
        
	    <cfargument name="groupID" type="numeric" required="yes" default="0">
        <cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="theData" type="struct" required="no" default="#structNew()#">
        <cfargument name="fullStructure" type="boolean" required="no" default="no">
        <cfargument name="upLevel" type="numeric" required="yes" default="0">
        
        <cfif groupID IS -1><cfset groupID = 0></cfif>
      	
        <!--- Get Group Assets --->
        <cfinvoke component="Content" method="getGroupContent" returnvariable="theGroupData">
            <cfinvokeargument name="groupID" value="#groupID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
     
        <cfif structIsEmpty(theData)>
        
			<!--- Get Group Info --->
            <cfinvoke component="Content" method="getGroupInfo" returnvariable="theGroupInfo">
                <cfinvokeargument name="groupID" value="#groupID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>

        	<cfset theData = theGroupInfo>
            <cfloop collection="#theData#" item="theKey"></cfloop>
            <cfset structAppend(theData,{"assets":[]})>
            
             <!---Current Date--->
        	<cfinvoke component="misc" method="convertDateToEpoch" returnvariable="curDate" />
            
            <cfset structAppend(theData,{"modified":#curDate#})>
            
        </cfif>
  
        <cfloop index="aAsset" array="#theGroupData#">
			
            <cfloop collection="#aAsset#" item="theKey"></cfloop>
																																					
		   		<cfif NOT structKeyExists(aAsset[theKey],'assetID')>
                   
                   <cfset structAppend(aAsset[theKey],{"assets":[]})>
					
                   	<!--- Normal Structure --->
                   	<cfif upLevel NEQ 2 AND NOT fullStructure><!--- This controls how many deep to go - subgroups --->

                        <cfset getUpLevel = upLevel + 1>
                        
						<!--- Group --->
                        <cfinvoke component="Modules" method="getGroupContent" returnvariable="theAssets">
                            <cfinvokeargument name="groupID" value="#aAsset[theKey].groupID#"/>
                            <cfinvokeargument name="appID" value="#appID#"/>
                            <cfinvokeargument name="theData" value="#aAsset#"/>
                            <cfinvokeargument name="fullStructure" value="#fullStructure#"/>
                            <cfinvokeargument name="upLevel" value="#getUpLevel#"/>
                        </cfinvoke>

                        <cfif structKeyExists(theData,'assets')>
                        	<cfset arrayAppend(theData.assets,aAsset)>
                        <cfelse>
                            <cfloop collection="#theData#" item="theKey"></cfloop>
                            <cfset arrayAppend(theData[theKey].assets,aAsset)>
                       </cfif>
                        
                    <cfelseif fullStructure>
                    
                      <!--- Full Structure ---> 
                       
                        <!--- Group --->
                        <cfinvoke component="Modules" method="getGroupContent" returnvariable="theAssets">
                            <cfinvokeargument name="groupID" value="#aAsset[theKey].groupID#"/>
                            <cfinvokeargument name="appID" value="#appID#"/>
                            <cfinvokeargument name="fullStructure" value="#fullStructure#"/>
                            <cfinvokeargument name="theData" value="#aAsset#"/>
                        </cfinvoke>
                        
					</cfif>
                    
                    <cfif fullStructure>
                    
                        <cfloop collection="#theData#" item="theKey"></cfloop>
                        
                        <cfif structKeyExists(theData,'assets')>
                            <cfset arrayAppend(theData.assets,aAsset)>
                        </cfif>
  
                        <!--- More Groups --->
                        <cfif NOT structKeyExists(theData,'assets')>
                        	<cfset arrayAppend(theData[theKey].assets,theAssets)>
                      	</cfif>
                        
					</cfif>
		 			
            	<cfelse>
        	      
   	              	<!--- Options --->
    				<cfinvoke component="Content" method="getAssetOptions" returnvariable="theAssetOptions">
                        <cfinvokeargument name="assetID" value="#aAsset[theKey].assetID#"/>
                        <cfinvokeargument name="groupID" value="#groupID#"/>
                    </cfinvoke>
                    
                    <!--- Asset --->
                    <cfinvoke component="Content" method="getAssetContent" returnvariable="theAssets">
                        <cfinvokeargument name="assetID" value="#aAsset[theKey].assetID#"/>
                    </cfinvoke>
                    
                    <!--- check if asset is valid --->
                    <cfif NOT structIsEmpty(theAssets)>
      																															
                        <cfloop collection="#theAssets#" item="theKeyAsset"></cfloop>
                            
                        <!--- Sharable, Access and Cached --->
                        <cfif structKeyExists(theAssetOptions,'sharable')>
                            <cfset theAssets[theKeyAsset].sharable = theAssetOptions.sharable>
                        </cfif>
                        
                        <cfif structKeyExists(theAssetOptions,'access')>
                            <cfset theAssets[theKeyAsset].access = theAssetOptions.access>
                        </cfif>
                        
                        <cfif structKeyExists(theAssetOptions,'cached')>
                            <cfset theAssets[theKeyAsset].cached = theAssetOptions.cached>
                        </cfif>
    
                        <!--- Sharable, Access and Cached --->
                        <cfif theAssets[theKeyAsset].sharable IS 0>
                            <cfset structDelete(theAssets[theKeyAsset],'sharable')>
                        </cfif>
                        <cfif theAssets[theKeyAsset].cached IS 0>
                            <cfset structDelete(theAssets[theKeyAsset],'cached')>
                        </cfif>
                        <cfif theAssets[theKeyAsset].access IS 0>
                            <cfset structDelete(theAssets[theKeyAsset],'access')>
                        </cfif>
                        
                        <cfif isDefined('theGroupInfo')>
                        
                            <!--- Pass If NOT sharable from Root Group --->
                            <cfif NOT structKeyExists(theGroupInfo,'sharable')>
                                <cfset structDelete(theAssets[theKeyAsset],'sharable')>
                            </cfif>
                            <!--- Pass Cashed from Root Group --->
                            <cfif NOT structKeyExists(theGroupInfo,'cached')>
                                <cfset structDelete(theAssets[theKeyAsset],'cached')>
                            </cfif>
                            <!--- Pass If NOT access from Root Group --->
                            <cfif structKeyExists(theGroupInfo,'access')>
                                <cfset structAppend(theAssets[theKeyAsset],{'access':theGroupInfo.access})>
                            </cfif>
                        
                        </cfif>
                        
                        <cfloop collection="#theData#" item="theKey"></cfloop>
                        
                        <cfif IsStruct(theData[theKey])>
                            <cfset arrayAppend(theData[theKey].assets,theAssets)>
                        <cfelse>
                            <cfif NOT isDefined('theData')><cfset theData = theGroupInfo></cfif>
                            <cfset arrayAppend(theData.assets,theAssets)>
                        </cfif>
                        
               		</cfif>
                    
             	</cfif>
        
        </cfloop>
     																																
        <cfloop collection="#theData#" item="theKey"></cfloop>
        <cfif theKey IS ''>
			<cfset newData = theData[theKey]>
            <cfset theData = newData>
        </cfif>

        <cfreturn theData>
            
    </cffunction>
    
    
    <!---Update Group Assets Sharable State--->
    <cffunction name="setGroupAssetSharable" access="public" returntype="boolean" output="yes">
        <cfargument name="groupID" type="boolean" required="yes" default="0">
        <cfargument name="assetID" type="boolean" required="yes" default="0">
        <cfargument name="sharable" type="boolean" required="no" default="0">
        
        <cfif groupID GT '0'>
            <cfquery name="UpdateGroupAssetSharable">
                UPDATE Groups
                SET sharable = #sharable#
                WHERE 0 = 0
                <cfif groupID GT '0'>
                	AND subgroup_id = #groupID#
				</cfif>
                <cfif assetID GT '0'>
					AND asset_id = #assetID#
				</cfif>
            </cfquery>
        </cfif>
        
        <cfreturn true>
        
    </cffunction>   
    
 
 <!--- Get Details Data --->   
   <cffunction name="getDetailsData" access="remote" returntype="struct" output="yes">
        
        <cfargument name="data" type="struct" required="yes" default="0">
   
        <cfset dataRecord = structNew()>
  																													 
        <cfif structKeyExists(data,'title')>
        <cfif data.title NEQ ''>
        	<cfset structAppend(dataRecord,{"title":data.title})>
        </cfif>
        </cfif>
        <cfif structKeyExists(data,'subtitle')>
        <cfif data.subtitle NEQ ''>
        	<cfset structAppend(dataRecord,{"subtitle":data.subtitle})>
        </cfif>
        </cfif>
        <cfif structKeyExists(data,'description')>
        <cfif data.description NEQ ''>
        	<cfset structAppend(dataRecord,{"description":data.description})>
        </cfif>
        </cfif>

        <cfif structKeyExists(data,'other')>
       	<cfif data.other NEQ ''>
        	<cfset structAppend(dataRecord,{"other":data.other})>
        </cfif>
        </cfif>
	
    	<!--- Color --->
        <cfif isDefined("data.titleColor")>
			<cfif data.titleColor NEQ ''>
            
                <!--- Convert Color hexToRGB --->
                <cfinvoke component="Misc" method="hexToRGB" returnvariable="color">
                    <cfinvokeargument name="hexValue" value="#data.titleColor#"/>
                </cfinvoke>
            
                <cfset structAppend(dataRecord,{"titleColor":color})>
                
            </cfif>
        </cfif>
        
        <cfif isDefined("data.subtitleColor")>
			<cfif data.subtitleColor NEQ ''>
                
                <!--- Convert Color hexToRGB --->
                <cfinvoke component="Misc" method="hexToRGB" returnvariable="color">
                    <cfinvokeargument name="hexValue" value="#data.subtitleColor#"/>
                </cfinvoke>
            
                <cfset structAppend(dataRecord,{"subtitleColor":color})>
                
            </cfif>
        </cfif>
        
        <cfif isDefined("data.descriptionColor")>
			<cfif data.descriptionColor NEQ ''>
                
                <!--- Convert Color hexToRGB --->
                <cfinvoke component="Misc" method="hexToRGB" returnvariable="color">
                    <cfinvokeargument name="hexValue" value="#data.descriptionColor#"/>
                </cfinvoke>
            
                <cfset structAppend(dataRecord,{"descriptionColor":color})>
                
            </cfif>
        </cfif>
																												
        <cfreturn dataRecord>
        
   </cffunction>
   
   
   
   
   <!--- Get Colors Data --->   
   <cffunction name="getColorData" access="remote" returntype="struct" output="yes">
        
        <cfargument name="data" type="struct" required="yes" default="0">
   
        <cfset dataRecord = structNew()>
        
        <cfif data.forecolor NEQ ''>
        
			<!--- Convert Color hexToRGB --->
            <cfinvoke component="Misc" method="hexToRGB" returnvariable="color">
                <cfinvokeargument name="hexValue" value="#data.forecolor#"/>
            </cfinvoke>
          
        	<cfset structAppend(dataRecord,{"forecolor":color})>
            
        </cfif>
        
        <cfif data.backcolor NEQ ''>
        
			<!--- Convert Color hexToRGB --->
            <cfinvoke component="Misc" method="hexToRGB" returnvariable="color">
                <cfinvokeargument name="hexValue" value="#data.backcolor#"/>
            </cfinvoke>
            
        	<cfset structAppend(dataRecord,{"backcolor":color})>

        </cfif>
        
  
		<cfif data.background NEQ ''>
            
        	<cfset structAppend(dataRecord,{"background":data.background})>
            
        </cfif>
    
        <cfif NOT StructIsEmpty(dataRecord)>
        	<cfset color = {"colors":dataRecord}>
		<cfelse>
        	<cfset color = {}>
        </cfif>
        																						
        <cfreturn color>
        
   </cffunction>
   
   
   
   
   
   
<!--- Get 3D Data --->
   <cffunction name="get3DData" access="remote" returntype="struct" output="yes">
        
        <cfargument name="data" type="any" required="yes" default="0">   
   		
        <cfset dataRec = structNew()>
        
        <!--- Build Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#data.asset_id#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
        
        <cfset arData = structNew()>
       
		<cfset camera = {"position":{"x":data.camPos_x,"y":data.camPos_y,"z":data.camPos_z}, 
                        "target":{"x":data.camTarget_x,"y":data.camTarget_y,"z":data.camTarget_z}, 
                        "fov":data.fov, "limits": {"zoomMin":data.camMin, "zoomMax":data.camMax, "panMin":data.tiltMin, "panMax":data.tiltMax, "rotationMin":data.panMin, "rotationMax":data.panMax}}>
        
        <!--- Camer Controls --->     
        <cfif data.camPan NEQ ''>
        	<cfset structAppend(camera,{"pan":data.camPan})>
        </cfif>
        <cfif data.camZoom NEQ ''>
        	<cfset structAppend(camera,{"zoom":data.camZoom})>
        </cfif>
        <cfif data.camRotation NEQ ''>
        	<cfset structAppend(camera,{"rotation": data.camRotation})>
        </cfif>  
        
        <!--- 3D Model Asset Types --->
        <cfset asset3D = structNew()>

        <cfif data.url NEQ ''>
        	<cfset structAppend(asset3D,{"ios":assetPath & data.url})>
        </cfif>
        
        <cfif data.url_android NEQ ''>
        	<cfset structAppend(asset3D,{"android":assetPath & data.url_android})>
        </cfif>
        
        <cfif data.url_osx NEQ ''>
			<cfset structAppend(asset3D,{"osx":assetPath & data.url_osx})>
        </cfif>
        
        <cfif data.url_windows NEQ ''>
			<cfset structAppend(asset3D,{"windows":assetPath & data.url_windows})>
        </cfif>
        
        <cfset model = {"rotation":{"x":data.rot_x,"y":data.rot_y,"z":data.rot_z}, 
                        "position":{"x":data.loc_x,"y":data.loc_y,"z":data.loc_z}, 
                        "scale":data.scale, "url":asset3D, "modified":data.modifiedModel}>
        
        <cfset structAppend(dataRec,{"model":model})>
        
        <cfset structAppend(dataRec,{"asset_id":data.asset_id})>
        
        <cfif NOT structIsEmpty(arData)>
            <cfset structAppend(dataRec,{"ar":arData})>
        </cfif>
        
        <cfset structAppend(dataRec,{"camera":camera})>
        
        <cfreturn dataRec>

</cffunction>








<!--- Get Quiz Data --->
   <cffunction name="getQuizData" access="remote" returntype="struct" output="yes">
        
        <cfargument name="data" type="any" required="yes" default="0">   
   		
        <cfset dataRec = structNew()>
    
        <!--- Build Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#data.asset_id#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
        
        <!--- Assets for Quiz, Correct and Incorrect --->
        <cfinvoke  component="Modules" method="getGroupAssetsActions" returnvariable="allQuizAssets">
          <cfinvokeargument name="assetID" value="#data.asset_id#"/>
         </cfinvoke>  
         
         <!--- Assets --->
         <cfset quizAssets = {"quiz":[], "correct":[], "incorrect":[]}>
         
         <cfloop query="allQuizAssets.assets">

            <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="AssetData">
                <cfinvokeargument name="assetID" value="#content_id#"/>
            </cfinvoke>
            
            <cfif quiz>
				<cfset arrayAppend(quizAssets.quiz,AssetData.assets[1])>
            <cfelseif quizCorrect>
                <cfset arrayAppend(quizAssets.correct,AssetData.assets[1])>
            <cfelseif quizIncorrect>
                <cfset arrayAppend(quizAssets.incorrect,AssetData.assets[1])>
            <cfelse>
                <!--- nothing --->
            </cfif>
            
         </cfloop>
        
        <!--- Quiz --->     
        <cfif data.complete>
        	<cfset structAppend(dataRec,{"complete":data.complete})>
        </cfif>
        
        <cfset structAppend(dataRec,{"question":{"message":data.questionMessage,"assets":quizAssets.quiz}})>
        
        <cfif data.needResponse>
        
			<cfset structAppend(dataRec,{"response":{}})>
            <cfset structAppend(dataRec.response,{"correct":{"message":data.correctMessage,"assets":quizAssets.correct}})>
            <cfset structAppend(dataRec.response,{"incorrect":{"message":data.incorrectMessage,"assets":quizAssets.incorrect}})>
            
            <cfif data.tryAgain>
            	<cfset structAppend(dataRec.response,{"tries":data.tryAgain})>
            </cfif>
            
        </cfif>

       
        <!--- Selection ---> 
        <cfset structAppend(dataRec,{"selection":{}})>
        
        <cfif data.displayCorrect>
        	<cfset structAppend(dataRec.selection,{"displayCorrect":data.displayCorrect})>
        </cfif>
        
        <cfif data.movable>
        	<cfset structAppend(dataRec.selection,{"movable":data.movable})>
		</cfif>
        
		<!--- Grid --->
        <cfquery name="selectionGrid"> 
            SELECT        SelectionAssets.spacing, SelectionAssets.padding, SelectionAssets.numberOfCols, Colors.foreColor, Colors.backColor, Colors.background
            FROM          SelectionAssets INNER JOIN
                          Colors ON SelectionAssets.color_id = Colors.color_id
            WHERE        (SelectionAssets.selection_id = #data.selection_id# )
		</cfquery>
                
        <cfset structAppend(dataRec.selection,{"layout":{"spacing":selectionGrid.spacing, "padding":selectionGrid.padding, "cols":selectionGrid.numberOfCols}})>
        <cfset structAppend(dataRec.selection.layout,{"colors":{"forecolor":selectionGrid.forecolor, "backcolor":selectionGrid.backcolor, "background":selectionGrid.background}})>
        
        <!--- Choices --->
        <cfquery name="selectionChoices">  
            SELECT  url, text, correct, choice_id, selection_id
            FROM	ChoiceAssets
            WHERE   selection_id = #data.selection_id#           
        </cfquery>
        
        <cfset choices = arrayNew(1)>
        
        <cfloop query="selectionChoices">
        	
            <cfset aChoice = structNew()>
            
        	<cfset structAppend(aChoice,{"choice_id":choice_id})>
            
			<cfif url NEQ ''>
            	<cfset filePath = assetPath &'nonretina/'& url>
                <cfset filePathRetina = assetPath & url>
            	<cfset structAppend(aChoice,{"url": {"mdpi": {"url": filePath},"xdpi": {"url": filePathRetina}}})>
            </cfif>
            
            <cfif text NEQ ''>
            	<cfset theMessage = JavaCast("string",text)>
				<cfset structAppend(aChoice,{"message":theMessage})>
            </cfif>
            
            <cfif correct>
            	<cfset structAppend(aChoice,{"correct":correct})>
            </cfif>
            
            <cfset arrayAppend(choices,aChoice)>
        
        </cfloop>
        
        <cfset structAppend(dataRec.selection,{"choices":choices})>
		<cfset structAppend(dataRec,{"asset_id":data.asset_id})>

        <cfreturn dataRec>

</cffunction>







<!--- Get Thumb Data --->
   <cffunction name="getThumbData" access="remote" returntype="struct" output="yes">
        
        <cfargument name="data" type="struct" required="yes" default="0">
        <cfargument name="appID" type="numeric" required="yes" default="0">
		<cfargument name="server" type="boolean" required="no" default="no">
        
        <!--- Path to URL Assets for Group --->
         <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="groupAssetPath">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="server" value="#server#"/>
          </cfinvoke>

        <cfset dataRecord = structNew()>
        
        <cfset path = ''>
        
		<!--- ASSET Thumb --->
        <cfif StructKeyExists(data,"assetType_id")>
    
			<!--- Get Asset Type Path --->
          <cfinvoke component="File" method="getAssetPath" returnvariable="assetPath">
          		<cfinvokeargument name="assetType" value="#data.assetType_id#"/>
          </cfinvoke>

        	<cfset path = 'assets/#assetPath#/'>
            
        <cfelse>
        <!--- GROUP Thumb --->
        	<cfset path = 'images/'>
        </cfif>
       
        <cfif isDefined("data.thumbnail")>
        
			<cfif data.thumbnail NEQ ''>
                <!--- retina --->
                <cfset urlPathRetina = groupAssetPath &"#path#thumbs/"& data.thumbnail>
                
                <!--- Size --->
                <cfset size = {"width":0,"height":0}>
                
                <!--- width --->
				<cfif isDefined("data.width")>
                	<cfset structAppend(size,{"width":data.width})>
                </cfif>
                <!--- height --->
                <cfif isDefined("data.height")>
                	<cfset structAppend(size,{"height":data.height})>
                </cfif>
              	
                <!--- retina --->
				<cfset retinaObj = {"url":urlPathRetina }>
                
                <cfif NOT structIsEmpty(size)>
                	<cfset structAppend(retinaObj,{"size":size})>
                </cfif>
                
                <cfset structAppend(dataRecord,{"xdpi":retinaObj})>
                
                <!--- non retina --->
                <cfset urlPathNormal = groupAssetPath &"#path#thumbs/nonretina/"& data.thumbnail>
 				
                <!--- Size --->
                <cfset nonsize = {"width":0,"height":0}>
                
                <cfset normObj = {"url":urlPathNormal}>
                
                <cfif NOT structIsEmpty(size)>
                	<cfset nonsize.width = size.width / 2>
                	<cfset nonsize.height = size.height / 2>
                    <cfset structAppend(normObj,{"size":nonsize})> 
                </cfif>
                
                <cfset structAppend(dataRecord,{"mdpi":normObj})>
                
            </cfif>
        
		</cfif>

        <cfreturn dataRecord>
        
   </cffunction>  
   
   
   
   
   <!--- Get Asset By KEY Name --->
   <cffunction name="getAssetByKeyName" access="remote" returntype="struct" output="yes">
        
        <cfargument name="assetKeyName" type="string" required="yes" default="0">
        <cfargument name="appID" type="numeric" required="yes" default="0">

		<cfquery name="Asset">
            SELECT asset_id
            FROM	Assets
            WHERE	name = '#assetKeyName#' AND app_id = #appID#
        </cfquery>
		
        <cfset data = structNew()>
        
        <cfif Asset.recordCount GT '0'>
        
			<cfset assetID = Asset.asset_id>
            
            <!--- Get Asset Info --->
            <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="theAssetData">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            
            <cfset data = theAssetData.assets[1][assetKeyName]>

        </cfif>
        
        <cfreturn data>
        
   </cffunction>   
   
   
   
   
   
    <!--- Get Root Details --->
 	<cffunction name="getRootModuleDetails" access="public" returntype="query" output="yes">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
		<cfinvoke component="Modules" method="getGrouptPath" returnvariable="rootGroup">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>

         <cfset rootGroupID = rootGroup[arrayLen(rootGroup)].id>
        
        <cfquery name="theDetails">
            SELECT        Thumbnails.image, Thumbnails.width, Thumbnails.height, Details.title, Details.subtitle, Details.description
            FROM            Groups LEFT OUTER JOIN
                                     Thumbnails ON Groups.thumb_id = Thumbnails.thumb_id LEFT OUTER JOIN
                                     Details ON Groups.detail_id = Details.detail_id
            WHERE        (Groups.group_id = #rootGroupID#)
        </cfquery>
        
        <cfreturn theDetails>
        
        
      </cffunction>
      
      
      
    	<!--- BreadCrumb Index--->
 	 <cffunction name="groupPathIndex" access="public" returntype="numeric" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
     
     	<cfinvoke component="Modules" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>
        
        <cfreturn arrayLen(crumb)>
        
      </cffunction>
      
      
   
 
 
    	<!--- BreadCrumb --->
 	<cffunction name="getGrouptPath" access="public" returntype="array" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfset crumb = arrayNew(1)>
        
		<!--- Others --->
        <cfloop condition="groupID GT 0">
        
            <cfquery name="theGroup">
                SELECT	name, subgroup_id
                FROM	Groups
                WHERE	group_id = #groupID# 
            </cfquery>
        
            <cfset arrayAppend(crumb,{"name":theGroup.name,"id":groupID})>
            
            <cfset groupID = theGroup.subgroup_id>

        </cfloop>
        
        <cfreturn crumb>
        
      </cffunction>
   
   
   
   
   	<!--- BreadCrumb --->
 	<cffunction name="getBreadcrumb" access="public" returntype="void" output="yes">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfinvoke component="Modules" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>
        
        <cfloop index="z" from="#arrayLen(crumb)#" to="1" step="-1">
        	<cfoutput>
        		<a href="AppsView.cfm?subgroupID=#crumb[z].id#" class="plainLinkWhite">#crumb[z].name#</a> <span class="contentGreyed">/</span>
            </cfoutput>
        </cfloop>
        
    </cffunction>   

    
</cfcomponent>