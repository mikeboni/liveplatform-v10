<cfcomponent> 

<cffunction	name="QueryToCSV" access="public" returntype="string" output="false" hint="I take a query and convert it to a comma separated value string.">

	<cfargument	name="Query" type="query" required="true" hint="I am the query being converted to CSV."	/>
	<cfargument name="Fields" type="string"	required="true"	hint="I am the list of query fields to be used when creating the CSV value." />
	<cfargument name="CreateHeaderRow" type="boolean" required="false" default="true" hint="I flag whether or not to create a row of header values." />
	<cfargument name="Delimiter" type="string" required="false"	default=","	hint="I am the field delimiter in the CSV value."/>

	
	<cfset var LOCAL = {} />
	<cfset LOCAL.ColumnNames = {} />

	<cfloop	index="LOCAL.ColumnName" list="#ARGUMENTS.Fields#" delimiters=",">

		<!--- Store the current column name. --->
		<cfset LOCAL.ColumnNames[ StructCount( LOCAL.ColumnNames ) + 1 ] = Trim( LOCAL.ColumnName ) />

	</cfloop>

	<!--- Store the column count. --->
	<cfset LOCAL.ColumnCount = StructCount( LOCAL.ColumnNames ) />

	<cfset LOCAL.Buffer = CreateObject( "java", "java.lang.StringBuffer" ).Init() />

	<!--- Create a short hand for the new line characters. --->
	<cfset LOCAL.NewLine = (Chr( 13 ) & Chr( 10 )) />


	<!--- Check to see if we need to add a header row. --->
	<cfif ARGUMENTS.CreateHeaderRow>

		<!--- Loop over the column names. --->
		<cfloop	index="LOCAL.ColumnIndex" from="1" to="#LOCAL.ColumnCount#"	step="1">

			<!--- Append the field name. --->
			<cfset LOCAL.Buffer.Append(
				JavaCast(
					"string",
					"""#LOCAL.ColumnNames[ LOCAL.ColumnIndex ]#"""
					)
				) />

			<cfif (LOCAL.ColumnIndex LT LOCAL.ColumnCount)>

				<!--- Field delimiter. --->
				<cfset LOCAL.Buffer.Append(
					JavaCast( "string", ARGUMENTS.Delimiter )
					) />

			<cfelse>

				<!--- Line delimiter. --->
				<cfset LOCAL.Buffer.Append(
					JavaCast( "string", LOCAL.NewLine )
					) />

			</cfif>

		</cfloop>

	</cfif>

	<!--- Loop over the query. --->
	<cfloop query="ARGUMENTS.Query">

		<!--- Loop over the columns. --->
		<cfloop
			index="LOCAL.ColumnIndex"
			from="1"
			to="#LOCAL.ColumnCount#"
			step="1">

			<!--- Append the field value. --->
			<cfset LOCAL.Buffer.Append(
				JavaCast(
					"string",
					"""#ARGUMENTS.Query[ LOCAL.ColumnNames[ LOCAL.ColumnIndex ] ][ ARGUMENTS.Query.CurrentRow ]#"""
					)
				) />

			<cfif (LOCAL.ColumnIndex LT LOCAL.ColumnCount)>

				<!--- Field delimiter. --->
				<cfset LOCAL.Buffer.Append(
					JavaCast( "string", ARGUMENTS.Delimiter )
					) />

			<cfelse>

				<!--- Line delimiter. --->
				<cfset LOCAL.Buffer.Append(
					JavaCast( "string", LOCAL.NewLine )
					) />

			</cfif>

		</cfloop>

	</cfloop>

	<!--- Return the CSV value. --->
	<cfreturn LOCAL.Buffer.ToString() />
    
</cffunction>





<cffunction	name="CSVToQuery" access="public" returntype="query" output="yes" hint="Converts the given CSV string to a query.">
	<cfargument	name="CSV" type="string" required="true" hint="This is the CSV string that will be manipulated."/>
	<cfargument	name="Delimiter" type="string" required="false"	default=","	hint="This is the delimiter that will separate the fields within the CSV value."/>
	
    <!--- Parse CSV as Arraay --->
    <cfinvoke component="CSV" method="csvToArray" returnvariable="csvConvertedData">
        <cfinvokeargument name="CSV" value="#CSV#"/>
        <cfinvokeargument name="Delimiter" value="#Delimiter#"/>
    </cfinvoke>
    
	<cfset NewLine = (Chr( 13 ) & Chr( 10 )) />
	<cfset cnt = 0>
    <cfset totalRecs = listLen(CSV,NewLine)>
  
    <!--- <cfloop index="z" list="#CSV#" delimiters="#NewLine#"> --->
        <cfloop index="z" from="1" to="#arrayLen(csvConvertedData)#">
        
        <cfset theLine = csvConvertedData[z]>
        
        <cfif cnt IS 0>
            
            <cfset dbTypes = "">
            <cfset dbData = "">
               
            <!--- <cfloop index="i" list="#z#" delimiters="#Delimiter#"> --->
            <cfloop index="i" from="1" to="#arrayLen(theLine)#">

				<cfif theLine[i] NEQ ''>
                	
                    <cfset dbTypes = dbTypes & "VarChar">
					<cfset dbData = dbData & theLine[i]>
                
					<cfif i GT arrayLen(theLine)-1>
                        <cfbreak>
                    <cfelse>
                        <cfset dbTypes = dbTypes & ",">
                        <cfset dbData = dbData & ",">
                    </cfif> 
                
                </cfif>
                
            </cfloop>
	
            <cfset cols = ReReplace(dbData, "[[:space:]]","","all")>
            <cfset cols = reReplace(cols, "[ _'/]+", "_", "all")>
            <cfset cols = reReplace(cols, "[ -]+", "", "all")> 
            
            <cfset myQuery = QueryNew('#trim(cols)#', '#dbTypes#')>
          
       		<cfset cnt++>
			<cfcontinue />

        </cfif>
        
		    <!--- Add Row --->
            <cfset QueryAddRow(myQuery)>
         
            <!--- Add Col data --->
            <cfset totalLen = arrayLen(theLine)>
            
            <cfif arrayLen(theLine) GT listLen(dbData,",")>
				<cfset totalLen = totalLen - 1>
            </cfif>
            
            <cfloop index="i" from="1" to="#totalLen#">

			  <cfset colData = listGetAt(dbData,i,",")>
              
              <cfset colName = ReReplace(colData, "[[:space:]]","","all")>
              <cfset colName = reReplace(colName, "[ _'/]+", "_", "all")>
              <cfset colName = reReplace(colName, "[ -]+", "", "all")>
          
              <cfset QuerySetCell(myQuery, colName, theLine[i])>
                
            </cfloop>
    
            <cfset cnt++>
        
    </cfloop>
	
    <cfreturn myQuery>

</cffunction>

<cffunction name="csvToArray" access="public" returntype="array" output="false" hint="I take a CSV file or CSV data value and convert it to an array of arrays based on the given field delimiter. Line delimiter is assumed to be new line / carriage return related.">
 
    <cfargument name="file" type="string" required="false" default="" hint="I am the optional file containing the CSV data."/>
    <cfargument name="csv" type="string" required="false" default="" hint="I am the CSV text data (if the file argument was not used)."/>
    <cfargument name="delimiter" type="string" required="false" default="," hint="I am the field delimiter (line delimiter is assumed to be new line / carriage return)."/>
    <cfargument name="trim" type="boolean" required="false" default="true" hint="I flags whether or not to trim the END of the file for line breaks and carriage returns." />
     
    <!--- Define the local scope. --->
    <cfset var local = {} />
    
    <cfif len( arguments.file )>
     
    <!--- Read the file into Data. --->
    <cfset arguments.csv = fileRead( arguments.file ) />
     
    </cfif>
    
    <cfif arguments.trim>
     
    <!--- Remove trailing line breaks and carriage returns. --->
    <cfset arguments.csv = reReplace(arguments.csv,"[\r\n]+$","","all") />
     
    </cfif>
     
    <!--- Make sure the delimiter is just one character. --->
    <cfif (len( arguments.delimiter ) neq 1)>
     
    <!--- Set the default delimiter value. --->
    <cfset arguments.delimiter = "," />
     
    </cfif>
    
    <cfsavecontent variable="local.regEx">(?x)
    <cfoutput>
    \G
     
    (?:
     
    <!--- Quoted value - GROUP 1 --->
    "([^"]*+ (?>""[^"]*+)* )"
     
    |
     
    <!--- Standard field value - GROUP 2 --->
    ([^"\#arguments.delimiter#\r\n]*+)
     
    )
     
    <!--- Delimiter - GROUP 3 --->
    (
    \#arguments.delimiter# |
    \r\n? |
    \n |
    $
    )
     
    </cfoutput>
    </cfsavecontent>
     
    <cfset local.pattern = createObject("java","java.util.regex.Pattern").compile(javaCast( "string", local.regEx )) />
     
    <cfset local.matcher = local.pattern.matcher(javaCast( "string", arguments.csv )) />
    
    <cfset local.csvData = [ [] ] />
     
    <cfloop condition="local.matcher.find()">
     
    <cfset local.fieldValue = local.matcher.group(javaCast( "int", 1 )) />
     
    <cfif structKeyExists( local, "fieldValue" )>
     
    <cfset local.fieldValue = replace(local.fieldValue,"""""","""","all") />
     
    <cfelse>
    
    <cfset local.fieldValue = local.matcher.group(javaCast( "int", 2 )) />
     
    </cfif>
     
    <cfset arrayAppend(
    local.csvData[ arrayLen( local.csvData ) ],local.fieldValue) />
     
    <cfset local.delimiter = local.matcher.group(javaCast( "int", 3 )) />
    
    <cfif (len( local.delimiter ) && (local.delimiter neq arguments.delimiter))>
    
    <cfset arrayAppend(local.csvData,arrayNew( 1 )) />
     
    <cfelseif !len( local.delimiter )>
     
    <cfbreak />
     
    </cfif>
     
    </cfloop>
    
    <cfreturn local.csvData />
    
</cffunction> 

</cfcomponent>