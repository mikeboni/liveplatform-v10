<cfcomponent>
	
    <!--- Get Assets from Target --->
 	<cffunction name="getTargets" access="public" returntype="array">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="targetID" type="numeric" required="no" default="0">
        
        <cfargument name="active" type="any" required="no" default="">
        <cfargument name="access" type="numeric" required="no" default="-1">
        
        <cfquery name="allTargets">
            SELECT        target_id, created, modified, name, detail_id, asset_id, active, accessLevel
            FROM          Targets
			WHERE		0 = 0
            		<cfif appID GTE '0'>
            			AND	(app_id = #appID#) 
                    </cfif>
                    <cfif active NEQ ''>
            			AND (active = #active#)
            		</cfif>
                    <cfif access GT '0'>
                    	AND (accessLevel = 0)
                    </cfif>
                    <cfif targetID GT '0'>
                    	AND (target_id = #targetID#)
                    </cfif>
        </cfquery>
       
        <cfset groups = arrayNew(1)>

        <cfloop query="allTargets">

			<cfif asset_id LT '0'>

            <!--- Get Items --->
             <cfset items = arrayNew(1)>

            	<cfset totalItems = 0>
       		
            <cfelse>
            	<cfset totalItems = -1>
            </cfif>
            
            <cfset data = {"group_id":#target_id#, "created":#created#, "modified":#modified#, "name":"#name#", "detail_id":#detail_id#, "items":#totalItems#, "asset_id":#asset_id#}>

            <cfif active IS ''>
                <cfset structAppend(data,{"active":allTargets.active})>
            </cfif>
            <cfif access LT '0'>
                <cfset structAppend(data,{"level":accessLevel})>
            </cfif>
          
            <cfset arrayAppend(groups,data)>
            
        </cfloop>
        
		<cfreturn groups>
        
	</cffunction>
    
    
    
    
    
    <!--- Get Groups from Module --->
 	<cffunction name="getTargetName" access="public" returntype="struct">
    	<cfargument name="targetID" type="numeric" required="yes" default="0">
        
        <cfset targetInfo = structNew()>
        
        <cfquery name="allTargets">
            SELECT        target_id, name, active, accessLevel
            FROM          Targets
			WHERE		 (target_id = #targetID#)
        </cfquery>
        
        <cfset targetInfo = {"name": allTargets.name, "active": allTargets.active, "accessLevel":allTargets.accessLevel}>
        
        <cfreturn targetInfo>
        
    </cffunction>
    
    
</cfcomponent>